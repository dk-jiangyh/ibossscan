package dongkesoft.com.dkmodule.responseinterface;

/**
 * 项目名称：DKMVPTestDemo
 * 类描述：基类状态接口
 * 创建人：apple
 * 创建时间：2018/6/19 上午11:45
 * 修改人：apple
 * 修改时间：2018/6/19 上午11:45
 * 修改备注：
 */
public interface AsyncHttpResponseHandler {
    /**
     * 成功
     * @param statusCode
     * @param content
     */
    void onSuccess(int statusCode, String content);

    /**
     * 失败
     * @param statusCode
     * @param content
     */
    void onFailure(int statusCode, String content);
}
