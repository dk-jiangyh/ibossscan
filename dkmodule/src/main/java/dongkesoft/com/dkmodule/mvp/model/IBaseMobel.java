/*******************************************************
 * Copyright(c) 2018 DongkeSoft All rights reserved. / Confidential
 *
 * @Title : IBaseMobel.java
 * @Package : dongkesoft.com.dkmodel.mvp.presenter;
 * @Description : 基类接口
 * @Author : dongke
 * @Date : 2018年6月18日
 * @Version : 1.00
 ********************************************************/
package dongkesoft.com.dkmodule.mvp.model;

/**
 * 项目名称：DKMVPTestDemo
 * 类描述：基类接口
 * 创建人：apple
 * 创建时间：2018/6/19 下午12:54
 * 修改人：apple
 * 修改时间：2018/6/19 下午12:54
 * 修改备注：
 */
public interface IBaseMobel {
    ////定义一个所有presenter初始化数据的方法

}
