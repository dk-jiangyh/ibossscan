/*******************************************************************************
 * Copyright(c) 2018 dongke All rights reserved. / Confidential
 * 类的信息：
 *		1.程序名称：DataModelManager
 *		2.功能描述：DataModelManager负责数据请求的分发
 * 编辑履历：
 *		作者				日期					版本				修改内容
 *		dongke			2018/6/8			1.00			新建
 *******************************************************************************/
package dongkesoft.com.dkmodule.mvp.model;



/**
 * 项目名称：dklibrary
 * 类描述：负责数据请求的分发
 * 创建人：apple
 * 创建时间：2018/6/20 下午2:05
 * 修改人：apple
 * 修改时间：2018/6/20 下午2:05
 * 修改备注：
 */
public class DataModelManager {
    /**
     * 负责数据请求的分发
     *
     * @param token
     * @return
     */
    public static BaseModel request(String token) {
        /**
         * 声明一个空的BaseModel
         */
       BaseModel model = null;

        try {
            //用反射机制会是一个比较理想的办法，
            // 请求数据时以具体Model的包名+类型作为Token，
            // 利用反射机制直接找到对应的Model
            model = (BaseModel) Class.forName(token).newInstance();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return model;
    }

    /**
     * 负责数据请求的分发
     *
     * @param className
     * @return
     */
    public static BaseModel newInstance(String className) {

        /**
         * 声明一个空的BaseModel
         */
       BaseModel model = null;

        try {
            //用反射机制会是一个比较理想的办法，
            // 请求数据时以具体Model的包名+类型作为Token，
            // 利用反射机制直接找到对应的Model
            model = (BaseModel) Class.forName(className).newInstance();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return model;
    }
}
