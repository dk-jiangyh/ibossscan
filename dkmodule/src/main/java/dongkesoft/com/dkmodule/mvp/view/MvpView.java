package dongkesoft.com.dkmodule.mvp.view; /*******************************************************
 * Copyright(c) 2018 DongkeSoft All rights reserved. / Confidential
 *
 * @Title : MvpView.java
 * @Package : dongkesoft.com.dkmodel.mvp.view;
 * @Description : MvpView-View的基础类
 * @Author : dongke
 * @Date : 2018年6月18日
 * @Version : 1.00
 ********************************************************/

/**
 * 项目名称：DKMVPTestDemo
 * 类描述：MvpView-View的基础类
 * 创建人：apple
 * 创建时间：2018/6/18 上午8:49
 * 修改人：apple
 * 修改时间：2018/6/18 上午8:49
 * 修改备注：
 */
public interface MvpView {

}
