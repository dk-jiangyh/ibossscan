package dongkesoft.com.dkmodule.mvp.presenter; /*******************************************************
 * Copyright(c) 2018 DongkeSoft All rights reserved. / Confidential
 *
 * @Title : MvpPresenter.java
 * @Package : dongkesoft.com.dkmodel.mvp.presenter;
 * @Description : Presenter的基础类
 * @Author : dongke
 * @Date : 2018年6月18日
 * @Version : 1.00
 ********************************************************/


import dongkesoft.com.dkmodule.mvp.view.MvpView;

/**
 * 项目名称：DKMVPTestDemo
 * 类描述：Presenter的基础类
 * 创建人：apple
 * 创建时间：2018/6/18 上午8:49
 * 修改人：apple
 * 修改时间：2018/6/18 上午8:49
 * 修改备注：
 */
public interface MvpPresenter<V extends MvpView> {
    /**
     * 添加view
     * @param view
     */
    void attachView(V view);

    /**
     * 销毁view
     */
    void detachView();
}
