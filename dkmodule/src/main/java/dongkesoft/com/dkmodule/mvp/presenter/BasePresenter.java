/*******************************************************************************
 * Copyright(c) 2018 dongke All rights reserved. / Confidential
 * 类的信息：
 *		1.程序名称：BasePresenter
 *		2.功能描述：acitivity的presenter基类
 * 编辑履历：
 *		作者				日期					版本				修改内容
 *		dongke			2018/6/8			1.00			新建
 *******************************************************************************/
package dongkesoft.com.dkmodule.mvp.presenter;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Looper;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;

import dongkesoft.com.dkmodule.common.Constants;
import dongkesoft.com.dkmodule.mvp.view.MvpView;


/**
 * 项目名称：dklibrary
 * 类描述：acitivity的presenter基类
 * 创建人：apple
 * 创建时间：2018/6/18 上午9:05
 * 修改人：apple
 * 修改时间：2018/6/18 上午9:05
 * 修改备注：
 */
public abstract class BasePresenter<V extends MvpView> implements MvpPresenter<V> {
    /**
     * view的弱应用
     */
    protected WeakReference<V> reference;

    /**
     * 数据处理handler
     */
    protected Handler mViewHandler = new Handler(Looper.getMainLooper());

    @Override
    public void attachView(V view) {
        if (view == null) {
            throw new NullPointerException("view can not be null when in attachview() in BasePresenter");

        } else {
            if (reference == null) {
                //将View置为弱引用，当view被销毁回收时，
                //依赖于view的对象（即Presenter）也会被回收，而不会造成内存泄漏
                reference = new WeakReference<V>(view);
            }
        }
    }

    @Override
    public void detachView() {
        if (reference != null) {
            reference.clear();
            reference = null;
        }
    }

    /**
     * 返回存在的view
     * @return
     */
    public V getView() {
        if (isAlive()) {
            return reference.get();
        } else {
            return null;
            //throw new NullPointerException("have you ever called attachView() in BasePresenter");
        }
    }

    /**
     * 是否存在view
     * @return
     */
    public boolean isAlive() {
        return reference != null && reference.get() != null;
    }

    /**
     * 返回url请求地址
     * @return
     */
    protected String getURL(){

        return String.format(Constants.URL, Constants.SERVER_IP,Constants.SERVER_PORT);
    }
    /**
     * 返回请求参数
     * @return
     */
    protected  HashMap<String, String> getLoginParams(Map<String,String> params){
        HashMap<String, String> paramsMap = new HashMap<>();
        paramsMap.put("UserID", "admin");
        paramsMap.put("UserCode", "admin");

        return paramsMap;
    }
}
