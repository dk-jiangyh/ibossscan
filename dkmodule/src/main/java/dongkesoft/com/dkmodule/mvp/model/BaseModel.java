/*******************************************************************************
 * Copyright(c) 2018 dongke All rights reserved. / Confidential
 * 类的信息：
 *		1.程序名称：BaseModel
 *		2.功能描述：定义了对外的请求数据规则
 * 编辑履历：
 *		作者				日期					版本				修改内容
 *		dongke			2018/6/8			1.00			新建
 *******************************************************************************/
package dongkesoft.com.dkmodule.mvp.model;

import java.util.HashMap;
import java.util.Map;

import dongkesoft.com.dkmodule.responseinterface.AsyncHttpResponseHandler;


/**
 * 项目名称：dklibrary
 * 类描述：定义了对外的请求数据规则
 * 创建人：apple
 * 创建时间：2018/6/19 下午1:33
 * 修改人：apple
 * 修改时间：2018/6/19 下午1:33
 * 修改备注：
 */
public abstract class BaseModel  implements IBaseMobel {
    /**
     * 数据请求参数
     */
    protected Map<String, String> params = new HashMap<String, String>();
    /**
     * url
     */
    protected String url;

    /**
     * 设置数据请求参数
     * @param args
     * @return
     */
    public  BaseModel params(HashMap<String, String> args){
        params = args;
        return this;
    }

    /**
     * url
     * @param arg
     * @return
     */
    public  BaseModel url(String arg){
        this.url = arg;
        return this;
    }

    /**
     * 添加Callback并执行数据请求 具体的数据请求由子类实现
     * @param callback
     */
    public abstract void execute(AsyncHttpResponseHandler callback);

    /**
     * 执行Get网络请求，此类看需求由自己选择写与不写
     * @param url
     * @param callback
     */
    protected void requestGetAPI(String url,AsyncHttpResponseHandler  callback){
     //这里写具体的网络请求
    }

    /**
     * 执行Post网络请求，此类看需求由自己选择写与不写
     * @param url
     * @param params
     * @param callback
     */
    protected void requestPostAPI(String url, HashMap<String, String>  params,
                                  AsyncHttpResponseHandler  callback){
        //这里写具体的网络请求
    }
}
