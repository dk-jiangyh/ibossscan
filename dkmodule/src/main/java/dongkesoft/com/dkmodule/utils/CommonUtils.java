/*******************************************************************************
 * Copyright(c) 2018 dongke All rights reserved. / Confidential
 * 类的信息：
 *		1.程序名称： CommonUtils
 *		2.功能描述：  通用类
 * 编辑履历：
 *		作者				日期					版本				修改内容
 *		dongke			2018/6/8			1.00			新建
 *******************************************************************************/
package dongkesoft.com.dkmodule.utils;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Environment;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CommonUtils {
    public static boolean CompareServerVersion(String localVersion,
                                               String ServerVersion) {
        boolean b = false;
        String localVersionNo = localVersion.replace(".", "");
        String serverVersionNo = ServerVersion.replace(".", "");
        if (localVersionNo.startsWith("0")) {
            localVersionNo = localVersionNo.replaceFirst("^0*", "");
        }
        if (serverVersionNo.startsWith("0"))

        {
            serverVersionNo = serverVersionNo.replaceFirst("^0*", "");
        }

        if (Long.parseLong(serverVersionNo) > Long.parseLong(localVersionNo)) {
            b = true;
        }
        return b;

    }

    public static int getLenByteStr(String str) {
        if (str == null || str.length() == 0) {
            return 0;
        }
        return str.getBytes().length;
    }

    public boolean isWifiConnected(Context context) {
        if (context != null) {
            ConnectivityManager mConnectivityManager = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mWiFiNetworkInfo = mConnectivityManager
                    .getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            if (mWiFiNetworkInfo != null) {
                return mWiFiNetworkInfo.isAvailable();
            }
        }
        return false;
    }


    public static double getMin(double... values) {
        Double min = Double.MAX_VALUE;
        for (Double d : values) {
            if (d < min) min = d;
        }
        return min;
    }

    public static long getIntervalDays(String startDate, String endDate) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        long end;
        long days = 0;
        try {
            end = df.parse(endDate).getTime();
            long start = df.parse(startDate).getTime();
            days = (end - start) / (1000 * 60 * 60 * 24);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return days;
    }

    public static String getCurrentDate() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }

    /**
     * 获取当前日期-前一天
     *
     * @return
     */
    public static String getBeforeDate() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd ");
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, -1); //向前走一天
        Date date = calendar.getTime();
        String str = formatter.format(date);
        return str;
    }

    public static String commonDateConverter(String datestr) {
        Date date = null;
        String dateString = null;
        DateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        DateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
        try {
            date = format1.parse(datestr);
            dateString = format2.format(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dateString;

    }

    public static String commonTimeConverter(String datestr) {
        Date date = null;
        String dateString = null;
        DateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        DateFormat format2 = new SimpleDateFormat("HH:mm");
        try {
            date = format1.parse(datestr);
            dateString = format2.format(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dateString;

    }


//    public static void setDefault(int defaults, Context context) {
//        int NOTIFICATIONS_ID = 1;
//        NotificationManager mNotificationManager;
//        final Notification notification = new Notification();
//        notification.defaults = defaults;
//        mNotificationManager = (NotificationManager) context
//                .getSystemService(context.NOTIFICATION_SERVICE);
//        mNotificationManager.notify(NOTIFICATIONS_ID, notification);
//    }


    public static void setVibrate(Context context) {
        Vibrator v = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            v.vibrate(VibrationEffect.createOneShot(500, VibrationEffect.DEFAULT_AMPLITUDE));
        } else {
            //deprecated in API 26
            v.vibrate(500);
        }

    }


    public static boolean CompareTime(String time1, String time2) {
        //如果想比较日期则写成"yyyy-MM-dd"就可以了
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        //将字符串形式的时间转化为Date类型的时间
        Date a = null;
        boolean flag = false;
        try {
            a = sdf.parse(time1);
            Date b = sdf.parse(time2);
            if (a.before(b)) {
                flag = true;
            } else {
                flag = false;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        //Date类的一个方法，如果a早于b返回true，否则返回false


		/*
		 * 如果你不喜欢用上面这个太流氓的方法，也可以根据将Date转换成毫秒
		if(a.getTime()-b.getTime()<0)
			return true;
		else
			return false;
		*/
        return flag;
    }

    public static boolean CompareEqualTime(String time1, String time2) {
        //如果想比较日期则写成"yyyy-MM-dd"就可以了
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        //将字符串形式的时间转化为Date类型的时间
        Date a = null;
        boolean flag = false;
        try {
            a = sdf.parse(time1);
            Date b = sdf.parse(time2);
            if (a.before(b)) {
                flag = true;
            } else if (a.after(b)) {
                flag = false;
            } else {
                flag = true;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        //Date类的一个方法，如果a早于b返回true，否则返回false


		/*
		 * 如果你不喜欢用上面这个太流氓的方法，也可以根据将Date转换成毫秒
		if(a.getTime()-b.getTime()<0)
			return true;
		else
			return false;
		*/
        return flag;
    }


    public static boolean CompareDate(String systemDate, String currentDate) {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date system;
        boolean compareresult = false;
        try {
            system = sdf.parse(systemDate);
            Date current = sdf.parse(currentDate);
            if (current.before(system)) {
                compareresult = false;
            } else {
                compareresult = true;
            }
        } catch (ParseException e) {

            e.printStackTrace();
        }
        return compareresult;

    }

    public static boolean hasSDCard() {
        String status = Environment.getExternalStorageState();
        if (!status.equals(Environment.MEDIA_MOUNTED)) {
            return false;
        }
        return true;
    }

    public static String getRootFilePath() {
        if (hasSDCard()) {
            return Environment.getExternalStorageDirectory().getAbsolutePath()
                    + "";
        } else {
            return Environment.getDataDirectory().getAbsolutePath() + "/data";
        }
    }

    @SuppressWarnings("unchecked")
    public static <GoodsType> List<GoodsType> deepCopy(List<GoodsType> srcList) throws IOException, ClassNotFoundException {
        ByteArrayOutputStream byteOut = new ByteArrayOutputStream();
        ObjectOutputStream out = new ObjectOutputStream(byteOut);
        out.writeObject(srcList);

        ByteArrayInputStream byteIn = new ByteArrayInputStream(byteOut.toByteArray());
        ObjectInputStream in = new ObjectInputStream(byteIn);
        List<GoodsType> destList = (List<GoodsType>) in.readObject();
        return destList;
    }

    public static boolean isInteger(String s) {

        Pattern p = Pattern.compile(
                "^[+]{0,1}(\\d+)$",
                Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(s);
        return m.matches();

    }

    //設置隱藏軟鍵盤
    public static void hideSoftKeyboard(View v, Context context) {
        // TODO Auto-generated method stub
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

    }


}
