package dongkesoft.com.dkmodule.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.UUID;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;

/*
 * Copyright(c) 2016 dongke All rights reserved. / Confidential
 * 类的信息：
 *		1.程序名称： Installation
 *		2.功能描述：
 * 编辑履历：
 *		作者				日期					版本				修改内容
 *		dongke			2016/12/07			1.00			新建
 *******************************************************************************/
public class Installation {

	@SuppressLint("MissingPermission")
	@SuppressWarnings("unused")
	public synchronized static String id(Context context) {
		// if (sID == null) {
		// File installation = new File(context.getFilesDir(), INSTALLATION);
		// try {
		// if (!installation.exists())
		// writeInstallationFile(installation);
		// sID = readInstallationFile(installation);
		// } catch (Exception e) {
		// throw new RuntimeException(e);
		// }
		// }
		// return sID;
		final TelephonyManager tm = (TelephonyManager) context
				.getSystemService(Context.TELEPHONY_SERVICE);
		final String tmDevice, tmSerial, tmPhone, androidId;
		tmDevice = "" + tm.getDeviceId();
		tmSerial = "" + tm.getSimSerialNumber();
		androidId = ""
				+ android.provider.Settings.Secure.getString(
				context.getContentResolver(),
				android.provider.Settings.Secure.ANDROID_ID);
		UUID deviceUuid = new UUID(androidId.hashCode(),
				((long) tmDevice.hashCode() << 32) | tmSerial.hashCode());
		String uniqueId = deviceUuid.toString();
		return uniqueId;
	}

	@SuppressLint("ObsoleteSdkInt")
	public static String getIMEIDeviceId(Context context) {

		String deviceId = "";
         try {
			 if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
				 deviceId = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
			 } else {
				 final TelephonyManager mTelephony = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
				 if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
					 if (context.checkSelfPermission(Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
						 return "";
					 }
				 }
				 assert mTelephony != null;
				 if (mTelephony.getDeviceId() != null) {
					 if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
						 deviceId = mTelephony.getImei();
					 } else {
						 deviceId = mTelephony.getDeviceId();
					 }
				 } else {
					 deviceId = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
				 }
			 }
		 }
         catch (Exception e)
		 {
		 	e.printStackTrace();
		 }
	
		return deviceId;
	}

	@SuppressWarnings("unused")
	private static String readInstallationFile(File installation)
			throws IOException {
		RandomAccessFile f = new RandomAccessFile(installation, "r");
		byte[] bytes = new byte[(int) f.length()];
		f.readFully(bytes);
		f.close();
		return new String(bytes);
	}

	@SuppressLint("MissingPermission")
	public static String getIMEI(Context context) {


		return ((TelephonyManager) context.getSystemService(
				Context.TELEPHONY_SERVICE)).getDeviceId();

	}

	@SuppressWarnings("unused")
	private static void writeInstallationFile(File installation)
			throws IOException {
		FileOutputStream out = new FileOutputStream(installation);
		String id = UUID.randomUUID().toString();
		out.write(id.getBytes());
		out.close();
	}
}