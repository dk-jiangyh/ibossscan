/*******************************************************************************
 * Copyright(c) 2018 dongke All rights reserved. / Confidential
 * 类的信息：
 *		1.程序名称： Md5Utils
 *		2.功能描述：  加密解密工具类
 * 编辑履历：
 *		作者				日期					版本				修改内容
 *		dongke			2018/6/8			1.00			新建
 *******************************************************************************/
package dongkesoft.com.dkmodule.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * 项目名称：DKMVPTestDemo
 * 类描述：加密解密工具类
 * 创建人：apple
 * 创建时间：2018/6/18 上午8:49
 * 修改人：apple
 * 修改时间：2018/6/18 上午8:49
 * 修改备注：
 */
public class Md5Utils {
	/**
	 * 编码
	 * @param password
	 * @return
	 */
	public static String encode(String password) {
		try {
			MessageDigest messageDigest = MessageDigest.getInstance("MD5");
			byte[] result = messageDigest.digest(password.getBytes());
			StringBuilder sb = new StringBuilder();
			for (byte b : result) {
				int number = b & 0xff;
				String hexstr = Integer.toHexString(number);
				if (hexstr.length() == 1) {
					sb.append("0");
				}
				sb.append(hexstr);
			}
			return sb.toString();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			// can't reach
			return "";
		}

	}
}
