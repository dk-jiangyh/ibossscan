/*******************************************************************************
 * Copyright(c) 2018 dongke All rights reserved. / Confidential
 * 类的信息：
 *		1.程序名称： OkAsyncHttpClient
 *		2.功能描述：  okhttp请求类
 * 编辑履历：
 *		作者				日期					版本				修改内容
 *		dongke			2018/6/8			1.00			新建
 *******************************************************************************/
package dongkesoft.com.dkmodule.utils;

import android.content.Context;
import android.os.Build;
import android.os.Handler;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import dongkesoft.com.dkmodule.bean.ReqCallBack;
import dongkesoft.com.dkmodule.bean.ReqProgressCallBack;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * 项目名称：DKMVPTestDemo
 * 类描述：okhttp请求类
 * 创建人：apple
 * 创建时间：2018/6/18 上午8:49
 * 修改人：apple
 * 修改时间：2018/6/18 上午8:49
 * 修改备注：
 */
public class OkAsyncHttpClient {
    /**
     * OkHttpClient
     */
    private final OkHttpClient httpClient;
    /**
     * 返回Handler
     */
    private Handler okHttpHandler;
    /**
     * call对象数组
     */
    private HashMap<String, Call> hashMapCall = new HashMap<String, Call>();

    /**
     * 当前call对象
     *
     * @return
     */
    public HashMap<String, Call> getHashMapCall() {
        return hashMapCall;
    }

    /**
     * 当前call对象
     *
     * @return
     */
    public void setHashMapCall(HashMap<String, Call> hashMapCall) {
        this.hashMapCall = hashMapCall;
    }

    /**
     * 单例引用
     */
    private static volatile OkAsyncHttpClient mInstance;//
    /**
     * mdiatype 这个需要和服务端保持一致
     */
    private static final MediaType MEDIA_TYPE_JSON = MediaType.parse("application/json; charset=utf-8");//
    /**
     * get请求
     */
    public static final int TYPE_GET = 0;//
    /**
     * post请求参数为json
     */
    public static final int TYPE_POST_JSON = 1;//
    /**
     * post请求参数为表单
     */
    public static final int TYPE_POST_FORM = 2;//
    /**
     * 数据流类型
     */
    public static final MediaType MEDIA_OBJECT_STREAM = MediaType.parse("application/octet-stream");

    /**
     * 构造函数
     *
     * @param context
     */
    public OkAsyncHttpClient(Context context) {
        //由于奶粉调整 一跺的数据量864条数据  所以必须加上相应的时间
        httpClient = new OkHttpClient().newBuilder().connectTimeout(150, TimeUnit.SECONDS)// 设置超时时间
                .readTimeout(120, TimeUnit.SECONDS)// 设置读取超时时间
                .writeTimeout(150, TimeUnit.SECONDS)// 设置写入超时时间
//                .addInterceptor(new Interceptor() {
//                    @Override
//                    public Response intercept(Chain chain) throws IOException {
//                        final Request original = chain.request();
//
//                        final Request authorized = original.newBuilder()
//                                .addHeader("Cookie", "cookie-name=cookie-value")
//                                .build();
//
//                        return chain.proceed(authorized);
//                    }
//                })
                .build();
        okHttpHandler = new Handler(context.getMainLooper());

    }

    /**
     * 单利模式
     *
     * @param context
     * @return
     * @author Administrator
     * @since 2017年1月6日
     */
    public static OkAsyncHttpClient getInstance(Context context) {
        OkAsyncHttpClient inst = mInstance;
        if (inst == null) {
            synchronized (OkAsyncHttpClient.class) {
                inst = mInstance;
                if (inst == null) {
                    inst = new OkAsyncHttpClient(context.getApplicationContext());
                    mInstance = inst;
                }
            }
        }
        return inst;
    }

    /**
     * 头信息
     *
     * @return
     */
    private Request.Builder addHeaders() {
        Request.Builder builder = new Request.Builder().addHeader("Connection", "keep-alive")
                .addHeader("platform", "2").addHeader("phoneModel", Build.MODEL)
                .addHeader("systemVersion", Build.VERSION.RELEASE)
                .addHeader("appVersion", "1.0.0.0");
        return builder;
    }

    /*
     * okHttp get同步请求
     *
     * @param actionUrl 接口地址
     *
     * @param paramsMap 请求参数
     */
    public void requestGetBySyn(String actionUrl, HashMap<String, String> paramsMap) {
        StringBuilder tempParams = new StringBuilder();
        try {
            // 处理参数
            int pos = 0;
            for (String key : paramsMap.keySet()) {
                if (pos > 0) {
                    tempParams.append("&");
                }
                // 对参数进行URLEncoder
                tempParams.append(String.format("%s=%s", key,
                        URLEncoder.encode(paramsMap.get(key), "utf-8")));
                pos++;
            }
            // 补全请求地址
            String requestUrl = String.format("%s?%s", actionUrl, tempParams.toString());
            // 创建一个请求
            Request request = addHeaders().url(requestUrl).build();
            // 创建一个Call
            final Call call = httpClient.newCall(request);
            // 执行请求
            final Response response = call.execute();
            response.body().string();
        } catch (Exception e) {

        }
    }

    /**
     * okHttp post同步请求
     *
     * @param requestUrl 接口地址
     * @param paramsMap  请求参数
     */
    public String requestPostBySyn(String requestUrl, HashMap<String, String> paramsMap) {
        String result = null;
        try {
            // 处理参数
            StringBuilder tempParams = new StringBuilder();
            JSONObject jsonObject = new JSONObject();

            for (String key : paramsMap.keySet()) {

                jsonObject.put(key, paramsMap.get(key));

            }
            String params = jsonObject.toString();

            // 创建一个请求实体对象 RequestBody
            RequestBody body = RequestBody.create(MEDIA_TYPE_JSON, params);
            // 创建一个请求
            final Request request = addHeaders().url(requestUrl).post(body).build();
            // 创建一个Call
            final Call call = httpClient.newCall(request);
            // 执行请求
            Response response = call.execute();
            // 请求执行成功
            if (response.isSuccessful()) {
                // 获取返回数据 可以是String，bytes ,byteStream
                result = response.body().string();
            }
        } catch (Exception e) {
            result = "";
        }
        return result;
    }

    /**
     * okHttp post异步请求
     *
     * @param requestUrl 接口地址
     * @param paramsMap  请求参数
     * @param callBack   请求返回数据回调
     * @param <T>        数据泛型
     * @return
     */
    public <T> Call requestPostByAsyn(String requestUrl, Map<String, String> paramsMap,
                                      final ReqCallBack<T> callBack) {
        try {
            StringBuilder tempParams = new StringBuilder();
            JSONObject jsonObject = new JSONObject();
            int pos = 0;
            for (String key : paramsMap.keySet()) {
                if (pos > 0) {
                    tempParams.append("&");
                }
                jsonObject.put(key, paramsMap.get(key));
                tempParams.append(String.format("%s=%s", key,
                        URLEncoder.encode(paramsMap.get(key), "utf-8")));
                pos++;
            }
            String params = jsonObject.toString();


            RequestBody body = RequestBody.create(MEDIA_TYPE_JSON, params);

            final Request request = addHeaders().url(requestUrl).post(body).build();
            final Call call = httpClient.newCall(request);
            call.enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    failedCallBack("访问失败", callBack);
                }

                @SuppressWarnings("unchecked")
                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    if (response.isSuccessful()) {
                        String string = response.body().string();
                        successCallBack((T) string, callBack);
                    } else {
                        failedCallBack("服务器错误", callBack);
                    }
                }
            });
            return call;
        }
        catch (Exception e) {
        }
        return null;
    }

    /**
     * okHttp post异步请求表单提交
     *
     * @param requestUrl 接口地址
     * @param paramsMap  请求参数
     * @param callBack   请求返回数据回调
     * @param <T>        数据泛型
     * @return
     */
    public <T> Call requestPostByAsynWithForm(String requestUrl, HashMap<String, String> paramsMap,
                                              final ReqCallBack<T> callBack) {
        try {
            FormBody.Builder builder = new FormBody.Builder();
            for (String key : paramsMap.keySet()) {
                builder.add(key, paramsMap.get(key));
            }
            RequestBody formBody = builder.build();

            final Request request = addHeaders().url(requestUrl).post(formBody).build();
            final Call call = httpClient.newCall(request);
            call.enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    failedCallBack("访问失败", callBack);
                }

                @SuppressWarnings("unchecked")
                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    if (response.isSuccessful()) {
                        String string = response.body().string();
                        successCallBack((T) string, callBack);
                    } else {
                        failedCallBack("服务器错误", callBack);
                    }
                }
            });
            return call;
        } catch (Exception e) {

        }
        return null;
    }

    /**
     * 成功回调
     *
     * @param result
     * @param callBack
     * @author Administrator
     * @since 2017年1月6日
     */
    private <T> void successCallBack(final T result, final ReqCallBack<T> callBack) {
        okHttpHandler.post(new Runnable() {
            @Override
            public void run() {
                if (callBack != null) {
                    callBack.onReqSuccess(result);
                }
            }
        });
    }

    /**
     * 请求失败回调
     *
     * @param errorMsg
     * @param callBack
     * @author Administrator
     * @since 2017年1月6日
     */
    private <T> void failedCallBack(final String errorMsg, final ReqCallBack<T> callBack) {
        okHttpHandler.post(new Runnable() {
            @Override
            public void run() {
                if (callBack != null) {
                    callBack.onReqFailed(errorMsg);
                }
            }
        });
    }

    /**
     * post请求
     *
     * @param url
     * @param map
     * @return
     * @throws Exception
     * @author Administrator
     * @since 2017年1月6日
     */
    public String okPost(String url, HashMap<String, String> map) throws Exception {
        String jsonstr = null;
        try {
            FormBody body = null;
            if (map != null) {
                FormBody.Builder builder = new FormBody.Builder();
                Iterator<?> iter = map.entrySet().iterator();
                while (iter.hasNext()) {
                    Map.Entry entry = (Map.Entry) iter.next();
                    String key = (String) entry.getKey();
                    String val = (String) entry.getValue();
                    builder.add(key, val);
                }
                body = builder.build();

            }
            Request.Builder builder = new Request.Builder();
            Request request = builder.url(url).post(body).build();
            Call call = httpClient.newCall(request);
            Response response = call.execute();
            if (response.isSuccessful()) {
                jsonstr = response.body().string();
            } else {
                throw new Exception("请求服务器失败");

            }

        } finally {

        }
        return jsonstr;

    }

    /**
     * 下载文件
     *
     * @param fileUrl  文件url
     * @param filePath 存储目标目录
     */
    @SuppressWarnings("unchecked")
    public <T> void downLoadFile(String fileUrl, String filePath, Map<String, String> map,
                                 final ReqCallBack<T> callBack) {

        final File file = new File(filePath);
        if (file.exists() && file.length() > 0) {
            // 确定下载是否完成一个完成的bitmap图片
            successCallBack((T) file, callBack);
            return;
        }
        JSONObject jsonObject = new JSONObject();

        for (String key : map.keySet()) {

            try {
                jsonObject.put(key, map.get(key));
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        String params = jsonObject.toString();


        RequestBody body = RequestBody.create(MEDIA_TYPE_JSON, params);
        final Request request = new Request.Builder().url(fileUrl).post(body).build();
        final Call call = httpClient.newCall(request);
        call.enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                if (file.exists()) {
                    file.delete();
                }
                failedCallBack("下载失败", callBack);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                InputStream is = null;
                byte[] buf = new byte[2048];
                int len = 0;
                FileOutputStream fos = null;
                try {
                    is = response.body().byteStream();
                    fos = new FileOutputStream(file);
                    while ((len = is.read(buf)) != -1) {
                        fos.write(buf, 0, len);
                    }
                    fos.flush();

                    successCallBack((T) file, callBack);
                } catch (IOException e) {
                    failedCallBack("下载失败", callBack);
                } finally {
                    try {
                        if (is != null) {
                            is.close();
                        }
                        if (fos != null) {
                            fos.close();
                        }
                    } catch (IOException e) {

                    }
                }
            }
        });
    }


    /**
     * @param url          下载连接
     * @param listener     下载监听
     */
    public void download(final String url, final String filePath, final int apkSize, Map<String, String> map, final OnDownloadListener listener) {

        JSONObject jsonObject = new JSONObject();

        for (String key : map.keySet()) {

            try {
                jsonObject.put(key, map.get(key));
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        String params = jsonObject.toString();
        RequestBody body = RequestBody.create(MEDIA_TYPE_JSON, params);
        Request request = new Request.Builder().url(url).post(body).build();
        httpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                // 下载失败监听回调
                listener.onDownloadFailed(e);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                InputStream is = null;
                byte[] buf = new byte[2048];
                int len = 0;
                FileOutputStream fos = null;

                File file = new File(filePath);
                if (!file.exists()) {
                    file.createNewFile();
                }
                try {
                    is = response.body().byteStream();
                    long total = apkSize;
                    fos = new FileOutputStream(file);
                    int sum = 0;
                    while ((len = is.read(buf)) != -1) {
                        fos.write(buf, 0, len);
                        sum += len;

                        listener.onDownloading(sum);
                    }
                    fos.flush();
                    // 下载完成
                    listener.onDownloadSuccess(file);
                } catch (Exception e) {
                    listener.onDownloadFailed(e);
                } finally {
                    try {
                        if (is != null)
                            is.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    try {
                        if (fos != null)
                            fos.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    public interface OnDownloadListener {
        /**
         * @param file 下载成功后的文件
         */
        void onDownloadSuccess(File file);

        /**
         * @param progress 下载进度
         */
        void onDownloading(int progress);

        /**
         * @param e 下载异常信息
         */
        void onDownloadFailed(Exception e);
    }

    /**
     * 下载文件时，终止下载，并删除不可用文件
     *
     * @param fileUrl  文件url
     * @param filePath 存储目标blic <T> void downFile(String fileUrl, String filePath, Map<String, String> map,
     *                              final ReqCallBack<T> callBack) {
     *
     *         final File file = new File(filePath);
     *         if (file.exists() && file.length() > 0) {
     *             // 确定下载是否完成一个完成的bitmap图片
     *             successCallBack((T) file, callBack);
     *             return;
     *         }
     *         FormBody body = null;
     *         if (map != null) {
     *             FormBody.Builder builder = new FormBody.Builder();
     *             Iterator<?> iter = map.entrySet().iterator();
     *             while (iter.hasNext()) {
     *                 @SuppressWarnings("rawtypes")
     *                 Map.Entry entry = (Map.Entry) iter.next();
     *                 String key = (String) entry.getKey();
     *                 String val = (String) entry.getValue();
     *                 builder.add(key, val);
     *             }
     *             body = builder.build();
     *
     *         }
     *         final Request request = new Request.Builder().url(fileUrl).post(body).build();
     *         final Call call = httpClient.newCall(request);
     *
     *         hashMapCall.put(filePath, call);
     *         call.enqueue(new Callback() {
     *             @Override
     *             public void onFailure(Call call, IOException e) {
     *                 if (file.exists()) {
     *                     file.delete();
     *                 }
     *                 failedCallBack("下载失败", callBack);
     *             }
     *
     *             @Override
     *             public void onResponse(Call call, Response response) throws IOException {
     *                 InputStream is = null;
     *                 byte[] buf = new byte[2048];
     *                 int len = 0;
     *                 FileOutputStream fos = null;
     *                 try {
     *                     is = response.body().byteStream();
     *                     fos = new FileOutputStream(file);
     *                     while ((len = is.read(buf)) != -1) {
     *                         fos.write(buf, 0, len);
     *
     *                     }
     *                     fos.flush();
     *
     *                     successCallBack((T) file, callBack);
     *                 } catch (IOException e) {
     *                     if (file.exists()) {
     *                         file.delete();
     *                     }
     *                     failedCallBack("下载失败", callBack);
     *                 } finally {
     *                     try {
     *                         if (is != null) {
     *                             is.close();
     *                         }
     *                         if (fos != null) {
     *                             fos.close();
     *                         }
     *                     } catch (IOException e) {
     *
     *                     }
     *                 }
     *             }
     *         });
     *     }目录
     */
    // @SuppressWarnings("unchecked")


    /**
     * 下载文件
     *
     * @param fileUrl     文件url
     * @param destFileDir 存储目标目录
     */
    @SuppressWarnings("unchecked")
    public <T> void downLoadFile(String fileUrl, final String destFileDir,
                                 final ReqProgressCallBack<T> callBack) {
        final String fileName = Md5Utils.encode(fileUrl);
        final File file = new File(destFileDir, fileName);
        if (file.exists()) {
            successCallBack((T) file, callBack);
            return;
        }
        final Request request = new Request.Builder().url(fileUrl).build();
        final Call call = httpClient.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                failedCallBack("下载失败", callBack);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                InputStream is = null;
                byte[] buf = new byte[1024];
                int len = 0;
                FileOutputStream fos = null;
                try {
                    long total = response.body().contentLength();
                    long current = 0;
                    is = response.body().byteStream();
                    fos = new FileOutputStream(file);
                    while ((len = is.read(buf)) != -1) {
                        current += len;
                        fos.write(buf, 0, len);
                        progressCallBack(total, current, callBack);
                    }
                    fos.flush();
                    successCallBack((T) file, callBack);
                } catch (IOException e) {
                    failedCallBack("下载失败", callBack);
                } finally {
                    try {
                        if (is != null) {
                            is.close();
                        }
                        if (fos != null) {
                            fos.close();
                        }
                    } catch (IOException e) {
                    }
                }
            }
        });
    }

    /**
     * 上传文件
     *
     * @param requestUrl 接口地址
     * @param filePath   本地文件地址
     */
    public <T> void upLoadAsyncFile(String requestUrl, String filePath, final ReqCallBack<T> callBack) {
        // 创建File
        File file = new File(filePath);
        // 创建RequestBody
        RequestBody body = RequestBody.create(MEDIA_OBJECT_STREAM, file);
        // 创建Request
        final Request request = new Request.Builder().url(requestUrl).post(body).build();
        final Call call = httpClient.newBuilder().writeTimeout(60, TimeUnit.SECONDS).build()
                .newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                failedCallBack("上传失败", callBack);
            }

            @SuppressWarnings("unchecked")
            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    String string = response.body().string();
                    successCallBack((T) string, callBack);
                } else {
                    failedCallBack("上传失败", callBack);
                }
            }
        });
    }


    public String uploadSyncFile(String requestUrl, String filePath) {
        // 创建File
        File file = new File(filePath);
        String respStr;
        // 创建RequestBody
        RequestBody body = RequestBody.create(MEDIA_OBJECT_STREAM, file);
        final Request request = new Request.Builder().url(requestUrl).post(body).build();
        // 单独设置参数 比如读取超时时间


        try {
            Response resp = httpClient.newCall(request).execute();

            if (resp.isSuccessful()) {
                respStr = resp.body().string();
            } else {
                respStr = "";
            }

        } catch (IOException e) {
            respStr = "";
            e.printStackTrace();
        }
        return respStr;
    }

    /**
     * 上传文件
     *
     * @param requestUrl 接口地址
     * @param paramsMap  参数
     * @param callBack   回调
     * @param <T>
     */
    public <T> void upLoadAsyncFile(String requestUrl, HashMap<String, Object> paramsMap,
                                    final ReqCallBack<T> callBack) {
        try {
            // 补全请求地址

            MultipartBody.Builder builder = new MultipartBody.Builder();
            // 设置类型
            builder.setType(MultipartBody.FORM);
            // 追加参数
            for (String key : paramsMap.keySet()) {
                Object object = paramsMap.get(key);
                if (!(object instanceof File)) {
                    builder.addFormDataPart(key, object.toString());
                } else {
                    File file = (File) object;
                    builder.addFormDataPart(key, file.getName(), RequestBody.create(null, file));
                }
            }
            // 创建RequestBody
            RequestBody body = builder.build();
            // 创建Request
            final Request request = new Request.Builder().url(requestUrl).post(body).build();
            // 单独设置参数 比如读取超时时间
            final Call call = httpClient.newBuilder().writeTimeout(50, TimeUnit.SECONDS).build()
                    .newCall(request);
            call.enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    failedCallBack("上传失败", callBack);
                }

                @SuppressWarnings("unchecked")
                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    if (response.isSuccessful()) {
                        String string = response.body().string();
                        successCallBack((T) string, callBack);
                    } else {
                        failedCallBack("上传失败", callBack);
                    }
                }
            });
        } catch (Exception e) {

        }
    }

    /**
     * 上传文件
     *
     * @param requestUrl
     * @param paramsMap
     * @return
     * @author Administrator
     * @since 2017年1月6日
     */
    public String upLoadSyncFile(String requestUrl, HashMap<String, Object> paramsMap) {
        String jsonString = "";
        try {
            // 补全请求地址

            MultipartBody.Builder builder = new MultipartBody.Builder();
            // 设置类型
            builder.setType(MultipartBody.FORM);
            // 追加参数
            for (String key : paramsMap.keySet()) {
                Object object = paramsMap.get(key);
                if (!(object instanceof File)) {
                    builder.addFormDataPart(key, object.toString());
                } else {
                    File file = (File) object;
                    builder.addFormDataPart(key, file.getName(),
                            RequestBody.create(MediaType.parse("image/png"), file));
                }
            }
            // 创建RequestBody
            RequestBody body = builder.build();
            // 创建Request
            final Request request = new Request.Builder().url(requestUrl).post(body).build();
            // 单独设置参数 比如读取超时时间
            Response response = httpClient.newCall(request).execute();

            if (response.isSuccessful()) {
                jsonString = response.body().string();
            } else {
                jsonString = "";
            }

        } catch (Exception e) {
            jsonString = "";
        }
        return jsonString;
    }

    /**
     * 统一处理进度信息
     *
     * @param total    总计大小
     * @param current  当前进度
     * @param callBack
     * @param <T>
     */
    private <T> void progressCallBack(final long total, final long current,
                                      final ReqProgressCallBack<T> callBack) {
        okHttpHandler.post(new Runnable() {
            @Override
            public void run() {
                if (callBack != null) {
                    callBack.onProgress(total, current);
                }
            }
        });
    }

}
