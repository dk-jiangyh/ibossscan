/*******************************************************************************
 * Copyright(c) 2018 dongke All rights reserved. / Confidential
 * 类的信息：
 *		1.程序名称： ExecutorServiceUtils
 *		2.功能描述：  线程池
 * 编辑履历：
 *		作者				日期					版本				修改内容
 *		dongke			2018/6/8			1.00			新建
 *******************************************************************************/
package dongkesoft.com.dkmodule.utils;

import android.content.Context;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import dongkesoft.com.dkmodule.common.Constants;

/**
 * 项目名称：DKMvpDemo
 * 类描述：线程池
 * 创建人：apple
 * 创建时间：2018/6/20 下午4:56
 * 修改人：apple
 * 修改时间：2018/6/20 下午4:56
 * 修改备注：
 */
public class ExecutorServiceUtils {

    private static  ExecutorServiceUtils mInstance;// 单例引用
    private ExecutorService service;

    /**
     * 单利模式
     *
     * @return
     * @author Administrator
     * @since 2017年1月6日
     */
    public static ExecutorServiceUtils getInstance() {
        if (mInstance==null){
            mInstance = new ExecutorServiceUtils();
        }
        return mInstance;
    }

    /**
     * // 创建一个固定大小的线程池
     */
    public ExecutorServiceUtils(){
        service = Executors.newFixedThreadPool(Constants.DEFAULT_MAX_THREAD);
    }

    /**
     * 加入线程执行
     * @param runnable
     */
    public void excuteRunnable(Runnable runnable){
        service.execute(runnable);
    }

}
