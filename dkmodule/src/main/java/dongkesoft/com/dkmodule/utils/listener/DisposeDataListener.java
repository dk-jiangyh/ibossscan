/*******************************************************************************
 * Copyright(c) 2018 dongke All rights reserved. / Confidential
 * 类的信息：
 *		1.程序名称： DisposeDataListener
 *		2.功能描述：  返回接口类
 * 编辑履历：
 *		作者				日期					版本				修改内容
 *		dongke			2018/6/8			1.00			新建
 *******************************************************************************/
package dongkesoft.com.dkmodule.utils.listener;


import dongkesoft.com.dkmodule.utils.exception.OkHttpException;

/**
 * @author:dongke
 * @date: 2017/10/27 13:49
 * @desc:
 */
public interface DisposeDataListener {
    /**
     * 请求服务器数据成功时回调的方法
     *
     * @param responseObj 需要回调到上层的请求结果
     */
    void onSuccess(Object responseObj);

    /**
     * 请求服务器失败时候的回调方法
     *
     * @param exception 需要回调到上层的错误反馈
     */
    void onFailure(OkHttpException exception);

}
