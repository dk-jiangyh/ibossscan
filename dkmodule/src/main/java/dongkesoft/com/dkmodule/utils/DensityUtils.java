/*******************************************************************************
 * Copyright(c) 2018 dongke All rights reserved. / Confidential
 * 类的信息：
 *		1.程序名称： DensityUtils
 *		2.功能描述：  dp与sp相互转换工具类
 * 编辑履历：
 *		作者				日期					版本				修改内容
 *		dongke			2018/6/8			1.00			新建
 *******************************************************************************/
package dongkesoft.com.dkmodule.utils;

import android.content.Context;

public class DensityUtils {
	/**
	 * name：付强
	 * 功能：dp值转换成px
	 * @param dip
	 * @param ctx
	 * @return
	 */
	public static int dip2px(float dip, Context ctx) {
		float density = ctx.getResources().getDisplayMetrics().density;
		int px = (int) (dip * density + 0.5f);// 4.9->4, 4.1->4, 四舍五入
		return px;
	}
	/**
	 *  name：付强
	 *  功能：px值转换成dp
	 * @param px
	 * @param ctx
	 * @return
	 */
	public static float px2dip(int px, Context ctx) {
		float density = ctx.getResources().getDisplayMetrics().density;
		float dp = px / density;
		return dp;
	}

}
