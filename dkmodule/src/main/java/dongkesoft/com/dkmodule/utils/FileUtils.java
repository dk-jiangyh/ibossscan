/*******************************************************************************
 * Copyright(c) 2018 dongke All rights reserved. / Confidential
 * 类的信息：
 *		1.程序名称： FileUtils
 *		2.功能描述：  文件处理类
 * 编辑履历：
 *		作者				日期					版本				修改内容
 *		dongke			2018/6/8			1.00			新建
 *******************************************************************************/
package dongkesoft.com.dkmodule.utils;

import java.io.File;
import java.io.IOException;

import android.os.Environment;
import android.text.TextUtils;

public class FileUtils {
	/**
	 * 初始化文件夹
	 * 
	 * @param path
	 */

	public static final String RECORD_SAVE_PATH = "/idmms/record";
	public static final String TAKE_HOME = "/idmms";;
	public static final String CACHE = "/idmms/cache";
	public static final String PHOTO = "/idmms/photo";
	public static final String DATA_CRASH_PATH = "/idmms/crash/";
	private static final String TAG = "FileUtil";
	public static void initFolder(String path) {
		if(hasSdcard()){
			File file = new File(path);
			if (!file.exists()) {
				file.mkdirs();
			}
		}
		
	}

	/**
	 * 判断文件是否存在，如果存在FileInfo的Empty为true
	 * 
	 * @param path
	 * @return FileInfo
	 * @throws IOException
	 */
	public static FileInfo initFile(String path) throws IOException {
		if(hasSdcard()){
			File file = new File(path);
			FileInfo fileInfo = new FileInfo();
			if (!file.exists()) {

				file.createNewFile();
				fileInfo.setFile(file);
				fileInfo.setEmpty(true);
				return fileInfo;
			} else {
				fileInfo.setFile(file);
				fileInfo.setEmpty(false);
			}
			return fileInfo;
		}
		return null;
	}

	/**
	 * 删除文件夹下所有文件
	 * @param filePath
	 * @param deleteThisPath
	 * @throws IOException
	 */
	public static void deleteFolderFile(String filePath, boolean deleteThisPath)
			throws IOException {
		File file1 = null;
		if (!TextUtils.isEmpty(filePath)) {
			file1 = new File(filePath);
		}
		if(!file1.exists()){
			return;
		}
		if (file1.isDirectory()) {// 处理目录
			File files[] = file1.listFiles();
			for (int i = 0; i < files.length; i++) {
				deleteFolderFile(files[i].getAbsolutePath(), true);
			}
		}
		if (deleteThisPath) {
			if (!file1.isDirectory()) {// 如果是文件，删除
				file1.delete();
			} else {// 目录
				if (file1.listFiles().length == 0) {// 目录下没有文件或者目录，删除
					file1.delete();
				}
			}
		}
	}
	
	
	
	public static class FileInfo {
		private File file;
		private boolean isEmpty;

		public FileInfo() {

		}

		public boolean isEmpty() {
			return isEmpty;
		}

		public File getFile() {
			return file;
		}

		public void setFile(File file) {
			this.file = file;
		}

		public void setEmpty(boolean isEmpty) {
			this.isEmpty = isEmpty;
		}

	}
	
	/**
	 * 判断是否有SD卡
	 * 
	 * @return
	 */
	public static boolean hasSdcard() {

		if (Environment.getExternalStorageState().equals(
				Environment.MEDIA_MOUNTED)) {

			return true;

		} else {

			return false;

		}

	}
    public static String getExtensionName(String filename) {    
        if ((filename != null) && (filename.length() > 0)) {    
            int dot = filename.lastIndexOf('.');    
            if ((dot >-1) && (dot < (filename.length() - 1))) {    
                return filename.substring(dot + 1);    
            }    
        }    
        return filename;    
    }    
    
    /**
	 * 递归 获取文件夹大小
	 */
	public static long getFileSize(File file) {
		long size = 0;
		File flist[] = file.listFiles();
		if (flist == null) {
			return 0;
		}
		for (int i = 0; i < flist.length; i++) {
			if (flist[i].isDirectory()) {
				size = size + getFileSize(flist[i]);
			} else {
				size = size + flist[i].length();
			}
		}
		return size;
	}
	
	
}
