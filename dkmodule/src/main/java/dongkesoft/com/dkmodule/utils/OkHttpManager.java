/*******************************************************************************
 * Copyright(c) 2018 dongke All rights reserved. / Confidential
 * 类的信息：
 *		1.程序名称： OkHttpManager
 *		2.功能描述：  OkHttpManager管理器
 * 编辑履历：
 *		作者				日期					版本				修改内容
 *		dongke			2018/6/8			1.00			新建
 *******************************************************************************/
package dongkesoft.com.dkmodule.utils;


import android.os.Build;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;

import dongkesoft.com.dkmodule.common.Constants;
import dongkesoft.com.dkmodule.utils.listener.DisposeDataHandler;
import dongkesoft.com.dkmodule.utils.request.CommonRequest;
import dongkesoft.com.dkmodule.utils.request.RequestParams;
import dongkesoft.com.dkmodule.utils.response.CommonJsonCallback;
import okhttp3.Cookie;
import okhttp3.CookieJar;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;

/**
 * @author:dongke
 * @date: 2017/10/27 17:57
 * @desc:
 */
public class OkHttpManager {
    private static volatile OkHttpManager sManager;
    private  OkHttpClient mOkHttpClient;
    private final HashMap<String, List<Cookie>> cookieStore = new HashMap<>();

    private OkHttpManager() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
         builder.cookieJar(new CookieJar() {
            @Override
            public void saveFromResponse(HttpUrl httpUrl, List<Cookie> list) {
                cookieStore.put(httpUrl.host(), list);
            }

            @Override
            public List<Cookie> loadForRequest(HttpUrl httpUrl) {
                List<Cookie> cookies = cookieStore.get(httpUrl.host());
                return cookies != null ? cookies : new ArrayList<Cookie>();
            }
        });
        builder.hostnameVerifier(new HostnameVerifier() {
            @Override
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        });
        builder.connectTimeout(Constants.HTTP_TIME_OUT, TimeUnit.SECONDS);
        builder.readTimeout(Constants.HTTP_TIME_OUT, TimeUnit.SECONDS);
        builder.writeTimeout(Constants.HTTP_TIME_OUT, TimeUnit.SECONDS);
        //允许重定向
        builder.followRedirects(true);
        // TODO: 2017/10/27 https
        mOkHttpClient = builder.build();
    }


    public static OkHttpManager getInstance() {
        if (sManager == null) {
            synchronized (OkHttpManager.class) {
                if (sManager == null) {
                    sManager = new OkHttpManager();
                }
            }
        }
        return sManager;
    }

    /**
     * 使用{@link OkHttpClient}想服务器端请求数据的方法Post方式
     * @param baseUrl baseUrl
     * @param paramsMap 请求url的参数,以键值对的形式存放
     * @param handler
     */
    public void requestPostServerData(String baseUrl,
                                      HashMap<String, String> paramsMap,
                                      DisposeDataHandler handler) {
        try {
            RequestParams requestParams = new RequestParams(paramsMap);
            Request request = null;
            request = CommonRequest.createPostRequest(baseUrl, requestParams);
            if (request != null) {
                mOkHttpClient.newCall(request).enqueue(new CommonJsonCallback(handler));
            }
        }catch (Exception e){
            throw  e;
        }
    }




    /**
     * 使用{@link OkHttpClient}想服务器端请求数据的方法 Get方式
     * @param baseUrl baseUrl
     * @param paramsMap 请求url的参数,以键值对的形式存放
     * @param handler
     */
    public void requestGetServerData(int method, String baseUrl,
                                     HashMap<String, String> paramsMap,
                                     DisposeDataHandler handler) {
        try {
            RequestParams requestParams = new RequestParams(paramsMap);
            Request request = null;
            request = CommonRequest.createGetRequest(baseUrl, requestParams);
            if (request != null) {
                mOkHttpClient.newCall(request).enqueue(new CommonJsonCallback(handler));
            }
        }catch (Exception e){
            throw  e;
        }
    }
}
