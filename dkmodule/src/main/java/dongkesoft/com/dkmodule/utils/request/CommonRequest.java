/*******************************************************************************
 * Copyright(c) 2018 dongke All rights reserved. / Confidential
 * 类的信息：
 *		1.程序名称： CommonRequest
 *		2.功能描述：  okhttp请求类
 * 编辑履历：
 *		作者				日期					版本				修改内容
 *		dongke			2018/6/8			1.00			新建
 *******************************************************************************/
package dongkesoft.com.dkmodule.utils.request;

import android.os.Build;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.Map;
import okhttp3.FormBody;
import okhttp3.Request;

/**
 * @author: dongke
 * @date: 2017/10/27 14:08
 * @desc: response for build various kind of {@link Request} include Get Post upload etc.
 */
public class CommonRequest {
    private static final String TAG = "CommonRequest";
    /**
     * create a Get request
     *
     * @param baseUrl base url
     * @param params see {@link RequestParams}
     * @return {@link Request}
     * @created at 2017/10/27 14:39
     */
    public static Request createGetRequest(@NonNull String baseUrl, @Nullable RequestParams params) {
        StringBuilder urlBuilder = new StringBuilder(baseUrl).append("?");
        if (params != null) {
            //将请求参数合并进url中
            for (Map.Entry<String, String> entry : params.getUrlParams().entrySet()) {
                urlBuilder.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
            }

            Log.d(TAG,">> createGetRequest >> " + urlBuilder.toString());
        }
        return new Request.Builder().get().url(urlBuilder.substring(0, urlBuilder.length() - 1)).build();
    }

    /**
     * create a post request
     *
     * @param baseUrl base url
     * @param params see {@link RequestParams}
     * @return {@link Request}
     * @created at 2017/10/27 14:39
     */
    public static Request createPostRequest(@NonNull String baseUrl, @NonNull RequestParams params) {
        FormBody.Builder mFormBodyBuilder = new FormBody.Builder();
        for (Map.Entry<String, String> entry : params.getUrlParams().entrySet()) {
            mFormBodyBuilder.add(entry.getKey(), entry.getValue());
        }
        FormBody formBody = mFormBodyBuilder.build();
        return new Request.Builder().post(formBody).url(baseUrl).build();
    }

}
