/*******************************************************************************
 * Copyright(c) 2018 dongke All rights reserved. / Confidential
 * 类的信息：
 *		1.程序名称： OkHttpException
 *		2.功能描述：  okhhtp异常类
 * 编辑履历：
 *		作者				日期					版本				修改内容
 *		dongke			2018/6/8			1.00			新建
 *******************************************************************************/
package dongkesoft.com.dkmodule.utils.exception;

/**
 * @author:dongke
 * @date: 2017/10/27 13:44
 * @desc:
 */
public class OkHttpException extends Exception {
    /**
     * 错误码
     */
    private int mErrorCode;
    /**
     * 错误消息
     */
    private String mErrorMsg;

    /**
     * 构造函数
     * @param errorCode
     * @param errorMsg
     */
    public OkHttpException(int errorCode, String errorMsg) {
        this.mErrorCode = errorCode;
        this.mErrorMsg = errorMsg;
    }

    public int getErrorCode() {
        return mErrorCode;
    }

    public String getErrorMsg() {
        return mErrorMsg;
    }
}
