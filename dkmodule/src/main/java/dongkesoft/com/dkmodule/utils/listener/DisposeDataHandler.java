/*******************************************************************************
 * Copyright(c) 2018 dongke All rights reserved. / Confidential
 * 类的信息：
 *		1.程序名称： DisposeDataHandler
 *		2.功能描述：  okhttp处理类
 * 编辑履历：
 *		作者				日期					版本				修改内容
 *		dongke			2018/6/8			1.00			新建
 *******************************************************************************/
package dongkesoft.com.dkmodule.utils.listener;


import dongkesoft.com.dkmodule.utils.exception.OkHttpException;

/**
 * @author:dongke
 * @date: 2017/10/27 13:52
 * @desc: 代理模式,使用DisposeDataHandler 代理 DisposeDataListener的操作
 */
public class DisposeDataHandler {

    public DisposeDataListener mListener;
    public Class<?> mClass;

    public DisposeDataHandler(DisposeDataListener listener) {
        mListener = listener;
    }

    public DisposeDataHandler(DisposeDataListener listener, Class<?> aClass) {
        mListener = listener;
        mClass = aClass;
    }

    public void onSuccess(Object responseObj) {
        mListener.onSuccess(responseObj);
    }

    public void onFailure(OkHttpException exception) {
        mListener.onFailure(exception);
    }

    public Class<?> getClassType() {
        return mClass;
    }
}
