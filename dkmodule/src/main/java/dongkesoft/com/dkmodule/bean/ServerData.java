/*******************************************************************************
 * Copyright(c) 2018 dongke All rights reserved. / Confidential
 * 类的信息：
 *		1.程序名称：BaseBean
 *		2.功能描述：基础model类
 * 编辑履历：
 *		作者				日期					版本				修改内容
 *		dongke			2018/6/8			1.00			新建
 *******************************************************************************/
package dongkesoft.com.dkmodule.bean;

import java.util.HashMap;

/**
 * 项目名称：dklibrary
 * 类描述：取得Fragment共用的数据基础类
 * 创建人：apple
 * 创建时间：2018/6/22 上午9:28
 * 修改人：apple
 * 修改时间：2018/6/22 上午9:28
 * 修改备注：
 */
public class ServerData {
    /**
     * 几个fragment返回几个值
     */
   private HashMap<String, Object> paramsMap;

    public HashMap<String, Object> getParamsMap() {
        return paramsMap;
    }

    public void setParamsMap(HashMap<String, Object> paramsMap) {
        this.paramsMap = paramsMap;
    }
}
