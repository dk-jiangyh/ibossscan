/*******************************************************************************
 * Copyright(c) 2018 dongke All rights reserved. / Confidential
 * 类的信息：
 *		1.程序名称：ReqProgressCallBack
 *		2.功能描述：响应进度更新
 * 编辑履历：
 *		作者				日期					版本				修改内容
 *		dongke			2018/6/8			1.00			新建
 *******************************************************************************/
package dongkesoft.com.dkmodule.bean;


public interface ReqProgressCallBack <T>  extends ReqCallBack<T> {
	 /**
     * 响应进度更新
     */
    void onProgress(long total, long current);
}
