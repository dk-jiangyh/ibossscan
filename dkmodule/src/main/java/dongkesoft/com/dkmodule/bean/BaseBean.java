/*******************************************************************************
 * Copyright(c) 2018 dongke All rights reserved. / Confidential
 * 类的信息：
 *		1.程序名称：BaseBean
 *		2.功能描述：基础model类
 * 编辑履历：
 *		作者				日期					版本				修改内容
 *		dongke			2018/6/8			1.00			新建
 *******************************************************************************/
package dongkesoft.com.dkmodule.bean;

/**
 * 项目名称：dklibrary
 * 类描述：基础model类
 * 创建人：apple
 * 创建时间：2018/6/19 上午11:41
 * 修改人：apple
 * 修改时间：2018/6/19 上午11:41
 * 修改备注：
 */
public class BaseBean {
}
