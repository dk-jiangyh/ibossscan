/*******************************************************************************
 * Copyright(c) 2018 dongke All rights reserved. / Confidential
 * 类的信息：
 *		1.程序名称：FragmentStatePagerAdapter
 *		2.功能描述：FragmentStatePagerAdapter
 * 编辑履历：
 *		作者				日期					版本				修改内容
 *		dongke			2018/6/8			1.00			新建
 *******************************************************************************/
package dongkesoft.com.dkmodule.adapter;


import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import java.util.ArrayList;

import dongkesoft.com.dkmodule.mvp.presenter.BaseFragmentPresenter;
import dongkesoft.com.dkmodule.mvp.view.MvpView;
import dongkesoft.com.dkmodule.ui.fragment.BaseFragment;

/**
 * 项目名称：DKMvpDemo
 * 类描述：FragmentStatePagerAdapter
 * 创建人：apple
 * 创建时间：2018/6/22 上午10:06
 * 修改人：apple
 * 修改时间：2018/6/22 上午10:06
 * 修改备注：
 */
public class MvpFragmentPagerAdapter extends FragmentStatePagerAdapter {

    /**
     * 数据源
     */
    private ArrayList<BaseFragment<MvpView,BaseFragmentPresenter<MvpView>>> listFragments;
    /**
     * FragmentManager管理器
     */
    private FragmentManager mFragmentManager;

    /**
     * 构造行数
     * @param fm
     * @param al
     */
    public MvpFragmentPagerAdapter(FragmentManager fm,
                                   ArrayList<BaseFragment<MvpView,BaseFragmentPresenter<MvpView>>> al)
    {
        super(fm);
        mFragmentManager = fm;
        listFragments = al;
        notifyDataSetChanged();
    }

    /*
   * 通常Fragment在onResume中做了数据刷新工作，缺点是当前正在显示的Fragment不能及时刷新数据
   * （FragmentStatePagerAdapter 会同时加载2-3个Fragment，这三个Fragment相互切换不会调用onResume方法，
   * 一直为显示状态）
   * 如何通知正在显示的Fragment刷新，一般采用广播机制来实现。（但感觉广播在底层进行遍历判断很费力）
   * 所以这里对FragmentList进行遍历，找到当前正在显得Fragment进行刷新
   *
   * 疑问：这个方法是放在adapter里合适还是Activity里合适
   * */
    public void refreshData(){
        for (BaseFragment fragment:listFragments) {
            if (fragment.isVisible()){
                fragment.refreshFragmentView();
            }
        }
    }

    @Override
    public Fragment getItem(int position) {
        return listFragments.get(position);
    }

    @Override
    public int getCount() {
        return listFragments.size();
    }

    @Override
    public int getItemPosition(Object object) {
        return super.getItemPosition(object);
    }
}
