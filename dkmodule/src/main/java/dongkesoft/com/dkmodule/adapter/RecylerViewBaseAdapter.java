package dongkesoft.com.dkmodule.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import dongkesoft.com.dkmodule.adapter.hold.RecycleViewBaseHolder;


/**
 * 项目名称：DKMvpDemo
 * 类描述：BaseAdapter基类，第一个泛型为数据类型，第二个泛型为holder类型
 * 创建人：apple
 * 创建时间：2018/6/22 下午4:30
 * 修改人：apple
 * 修改时间：2018/6/22 下午4:30
 * 修改备注：
 */
public abstract class RecylerViewBaseAdapter<E, T extends RecycleViewBaseHolder>
        extends RecyclerView.Adapter   {
    /**
     * 数据源
     */
    protected List<E> data = new ArrayList<E>();
    /**
     * 上下文
     */
    protected Context context;

    public  OnItemClickListener itemClickListener;


    /**
     * 构造函数
     */
    public RecylerViewBaseAdapter() {
    }

    /**
     * 构造函数
     *
     * @param list
     */
    public RecylerViewBaseAdapter(List<E> list) {
        this.data = list;
    }

    /**
     * 数据源
     *
     * @return
     */
    public List<E> getData() {
        return data;
    }

    /**
     * 数据源
     *
     * @param data
     */
    public void setData(List<E> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    /**
     * 增加全部list
     *
     * @param data
     */
    public void addDatas(List<E> data) {
        this.data.addAll(data);
        notifyDataSetChanged();
    }

    /**
     * 增加某个item
     *
     * @param data
     */
    public void addData(E data) {
        this.data.add(data);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {

        return data == null ? 0 : data.size();
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    /**
     * 删除某项item
     *
     * @param position
     */
    public void delItem(int position) {
        if (data != null && data.size() > position) {
            data.remove(position);
            this.notifyDataSetChanged();
        }
    }

    /**
     * 删除全部
     */
    public void delAll() {
        data.clear();
        this.notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        T holder = null;
        View convertView = getItemView(getView(), parent);
        convertView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        //通过泛型自动转换holder类型，由于butterknife需要在holder里面绑定控件，
        // 所以不能写成同一个holder，而需要自动转换。
        holder = getHolder(convertView);
        convertView.setTag(holder);
        return holder;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        //逻辑处理的抽象方法

        if (itemClickListener != null) {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int pos = holder.getPosition();
                    itemClickListener.onItemClick(holder.itemView, pos);
                }
            });


        }
        bindEvent((T) holder, position);
    }


    //-----------------三个抽象方法

    /**
     * 当前Holder
     *
     * @param v
     * @return
     */
    protected abstract T getHolder(View v);

    /**
     * layout的view
     *
     * @return
     */
    protected abstract int getView();

    /**
     * 逻辑处理的抽象方法
     *
     * @param holder
     * @param position
     */
    protected abstract void bindEvent(T holder, int position);

    /**
     * 当前view的item
     *
     * @param id
     * @param parent
     * @return
     */
    protected View getItemView(int id, ViewGroup parent) {
        return LayoutInflater.from(parent.getContext()).inflate(id, parent, false);
    }

    //点击事件接口
    public  interface OnItemClickListener {
        void onItemClick(View v, int position);
    }

    //设置点击事件的方法
   public void setItemClickListener(OnItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;

    }
}
