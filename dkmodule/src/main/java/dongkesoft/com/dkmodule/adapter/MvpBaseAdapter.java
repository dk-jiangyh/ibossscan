package dongkesoft.com.dkmodule.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import java.util.ArrayList;
import java.util.List;

import dongkesoft.com.dkmodule.adapter.hold.BaseHolder;


/**
 * 项目名称：DKMvpDemo
 * 类描述：BaseAdapter基类，第一个泛型为数据类型，第二个泛型为holder类型
 * 创建人：apple
 * 创建时间：2018/6/22 下午4:30
 * 修改人：apple
 * 修改时间：2018/6/22 下午4:30
 * 修改备注：
 */
public abstract class MvpBaseAdapter<E, T extends BaseHolder> extends BaseAdapter {
    /**
     * 数据源
     */
    protected List<E> data = new ArrayList<E>();
    /**
     * 上下文
     */
    protected Context context;

    /**
     * 构造函数
     */
    public MvpBaseAdapter() {
    }

    /**
     * 构造函数
     * @param list
     */
    public MvpBaseAdapter(List<E> list) {
        this.data = list;
    }

    /**
     * 数据源
     * @return
     */
    public List<E> getData() {
        return data;
    }

    /**
     * 数据源
     * @param data
     */
    public void setData(List<E> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    /**
     * 增加全部list
     *
     * @param data
     */
    public void addDatas(List<E> data) {
        this.data.addAll(data);
        notifyDataSetChanged();
    }

    /**
     * 增加某个item
     *
     * @param data
     */
    public void addData(E data) {
        this.data.add(data);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    /**
     * 删除某项item
     *
     * @param position
     */
    public void delItem(int position) {
        if (data != null && data.size() > position) {
            data.remove(position);
            this.notifyDataSetChanged();
        }
    }

    /**
     * 删除全部
     */
    public void delAll() {
        data.clear();
        this.notifyDataSetChanged();
    }

    /**
     * getView
     * @param position
     * @param convertView
     * @param parent
     * @return
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        context = parent.getContext();
        T holder = null;
        if (convertView == null) {
            //getView()的抽象方法，返回值为int
            convertView = getItemView(getView(), parent);
            //通过泛型自动转换holder类型，由于butterknife需要在holder里面绑定控件，
            // 所以不能写成同一个holder，而需要自动转换。
            holder = getHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (T) convertView.getTag();
        }
        //逻辑处理的抽象方法
        bindEvent(holder,position);
        return convertView;
    }

    //-----------------三个抽象方法
    /**
     * 当前Holder
     * @param v
     * @return
     */
    protected abstract T getHolder(View v);

    /**
     * layout的view
     * @return
     */
    protected abstract int getView();

    /**
     * 逻辑处理的抽象方法
     * @param holder
     * @param position
     */
    protected abstract void bindEvent(T holder,int position);

    /**
     * 当前view的item
     * @param id
     * @param parent
     * @return
     */
    protected View getItemView(int id, ViewGroup parent) {
        return LayoutInflater.from(parent.getContext()).inflate(id, parent, false);
    }
}
