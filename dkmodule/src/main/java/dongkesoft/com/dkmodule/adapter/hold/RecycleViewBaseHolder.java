/*******************************************************************************
 * Copyright(c) 2018 dongke All rights reserved. / Confidential
 * 类的信息：
 *		1.程序名称：BaseHolder
 *		2.功能描述：BaseHolder基类
 * 编辑履历：
 *		作者				日期					版本				修改内容
 *		dongke			2018/6/8			1.00			新建
 *******************************************************************************/
package dongkesoft.com.dkmodule.adapter.hold;

import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

import butterknife.ButterKnife;

/**
 * 项目名称：DKMvpDemo
 * 类描述：有butterknife基本功能
 * 创建人：apple
 * 创建时间：2018/6/22 下午4:26
 * 修改人：apple
 * 修改时间：2018/6/22 下午4:26
 * 修改备注：
 */
public class RecycleViewBaseHolder extends RecyclerView.ViewHolder{
    /**
     * 构造函数
     * @param view
     */
    public RecycleViewBaseHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
    }


}
