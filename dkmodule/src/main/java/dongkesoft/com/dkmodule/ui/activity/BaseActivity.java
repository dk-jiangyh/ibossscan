/*******************************************************************************
 * Copyright(c) 2018 dongke All rights reserved. / Confidential
 * 类的信息：
 *		1.程序名称： BaseActivity
 *		2.功能描述：  Activity基类
 * 编辑履历：
 *		作者				日期					版本				修改内容
 *		dongke			2018/6/8			1.00			新建
 *******************************************************************************/

package dongkesoft.com.dkmodule.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.PixelFormat;

import android.os.Bundle;
import android.view.Gravity;
import android.view.WindowManager;
import butterknife.ButterKnife;
import dongkesoft.com.dkmodule.common.Constants;
import dongkesoft.com.dkmodule.mvp.presenter.BasePresenter;
import dongkesoft.com.dkmodule.mvp.view.MvpView;
import dongkesoft.com.dkmodule.ui.CustomViewGroup;

import androidx.appcompat.app.AppCompatActivity;


/**
 * 项目名称：dklibrary
 * 类描述：Activity基类
 * 创建人：apple
 * 创建时间：2018/6/18 上午9:05
 * 修改人：apple
 * 修改时间：2018/6/18 上午9:05
 * 修改备注：
 */
public abstract class BaseActivity<V extends MvpView, P extends BasePresenter<V>>
        extends AppCompatActivity {

    CustomViewGroup view;
    WindowManager manager;

    /**
     * SharedPreferences对象
     */
    public SharedPreferences mPreferences;
    /**
     * ip
     */
    public String mServerAddressIp;

    /**
     * 端口
     */
    public String mServerAddressPort;
    /**
     * 帐套
     */
    public String mAccountCode;

    /**
     * 用户
     */
    public String mUserCode;

    public String mUserId;

    /**
     * 用户
     */
    public String mUserName;

    /**
     * 密码
     */
    public String mPassword;
    //token
    public String token;
    //生产产区
    public String producingArea;
    //基台号
    public String platformNumber;
    //罐号
    public String productionTankNumber;
    //冠词
    public String productionTankCount;
    //erp批次
    public String erpBatchno;

    /**
     * SessionKey
     */
    public String mSessionKey;

    public String warehouseId;

    public String warehouseName;

    public String mRights;

    public String macAddress;
    /**
     * presenter对象
     */
    protected P presenter;
    public String mLicenseCode;

    /**
     * 创建一个与之关联的Presenter
     *
     * @return
     */
    protected abstract P createPresenter();

    /**
     * layoutResID
     *
     * @return
     */
    protected abstract int setMvpView();

    /**
     * 初始化view
     */
    protected abstract void initView();

    /**
     * 初始化data
     */
    protected abstract void initData();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!isTaskRoot()) {
            final Intent intent = getIntent();
            final String intentAction = intent.getAction();
            if (intent.hasCategory(Intent.CATEGORY_LAUNCHER) && intentAction != null
                    && intentAction.equals(Intent
                    .ACTION_MAIN)) {
                finish();
                return ;
            }
        }
        setContentView(setMvpView());
        //  int height = getStatusBarHeight(this);
        //   prohibitDropDown(height);
        mPreferences = getSharedPreferences(Constants.STR_SHARED_PRE_KEYS,
                Context.MODE_PRIVATE);
        mServerAddressIp = mPreferences.getString("ServerAddressIp", "");
        mServerAddressPort = mPreferences.getString("ServerAddressPort", "");
        mAccountCode = mPreferences.getString("AccountCode", "");
        mUserCode = mPreferences.getString("UserCode", "");
        mUserName = mPreferences.getString("UserName", "");
        mPassword = mPreferences.getString("UserPassword", "");
        mSessionKey = mPreferences.getString("SessionKey", "");
       mLicenseCode = mPreferences.getString("LicenseCode", "");
        mRights = mPreferences.getString("rights", "");
        // 创建presenter
        presenter = createPresenter();
        presenter.attachView((V) this);
        // ButterKnife绑定
        ButterKnife.bind(BaseActivity.this);
        // 初始化view
        initView();
        //初始化数据
        initData();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (presenter != null) {
            presenter.detachView();
        }
    }

    /**
     * 禁止下拉状态栏
     *
     * @param height
     */
    private void prohibitDropDown(int height) {
        manager = ((WindowManager) getApplicationContext()
                .getSystemService(Context.WINDOW_SERVICE));

        WindowManager.LayoutParams localLayoutParams = new WindowManager.LayoutParams();
        localLayoutParams.type = WindowManager.LayoutParams.TYPE_SYSTEM_ERROR;
        localLayoutParams.gravity = Gravity.TOP;
        localLayoutParams.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE |
                // this is to enable the notification to recieve touch events
                WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL |
                // Draws over status bar
                WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN;
        localLayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
//        localLayoutParams.height = (int) (24 * getResources().getDisplayMetrics().scaledDensity);
        localLayoutParams.height = height;
        localLayoutParams.format = PixelFormat.TRANSPARENT;
        view = new CustomViewGroup(this);
        manager.addView(view, localLayoutParams);
    }

    /**
     * 获取状态栏高度
     *
     * @param context
     * @return
     */
    public static int getStatusBarHeight(Context context) {
        Resources resources = context.getResources();
        int resourceId = resources.getIdentifier("status_bar_height", "dimen", "android");
        int height = resources.getDimensionPixelSize(resourceId);
        return height;
    }

}
