/*******************************************************************************
 * Copyright(c) 2018 dongke All rights reserved. / Confidential
 * 类的信息：
 *		1.程序名称： BaseActivity
 *		2.功能描述：  Activity基类
 * 编辑履历：
 *		作者				日期					版本				修改内容
 *		dongke			2018/6/8			1.00			新建
 *******************************************************************************/

package dongkesoft.com.dkmodule.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.FragmentActivity;

import butterknife.ButterKnife;
import dongkesoft.com.dkmodule.common.Constants;
import dongkesoft.com.dkmodule.mvp.presenter.BasePresenter;
import dongkesoft.com.dkmodule.mvp.view.MvpView;


/**
 * 项目名称：dklibrary
 * 类描述：Activity基类
 * 创建人：apple
 * 创建时间：2018/6/18 上午9:05
 * 修改人：apple
 * 修改时间：2018/6/18 上午9:05
 * 修改备注：
 */
public abstract class BaseFragmentActivity<V extends MvpView,P extends BasePresenter<V>>
        extends FragmentActivity {

    /**
     * SharedPreferences对象
     */
    public SharedPreferences mPreferences;
    public String token;
    /**
     * 帐套
     */
    public String mAccountCode;

    /**
     * 用户
     */
    public String mUserCode;

    /**
     * 用户
     */
    public String mUserName;

    /**
     * 密码
     */
    public String mPassword;

    /**
     * SessionKey
     */
    public String mSessionKey;

    public String mRights;

    /**
     * presenter对象
     */
    protected P presenter;

    /**
     * 创建一个与之关联的Presenter
     * @return
     */
    protected abstract P createPresenter();

    /**
     * layoutResID
     * @return
     */
    protected abstract int setMvpView();

    /**
     * 初始化view
     */
    protected abstract void initView();

    /**
     * 初始化data
     */
    protected abstract void initData();
    /**
     * ip
     */
    public String mServerAddressIp;

    /**
     * 端口
     */
    public String mServerAddressPort;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!isTaskRoot()) {
            final Intent intent = getIntent();
            final String intentAction = intent.getAction();
            if (intent.hasCategory(Intent.CATEGORY_LAUNCHER) && intentAction != null
                    && intentAction.equals(Intent
                    .ACTION_MAIN)) {
                finish();
                return ;
            }
        }
        setContentView(setMvpView());
        mPreferences = getSharedPreferences(Constants.STR_SHARED_PRE_KEYS,
                Context.MODE_PRIVATE);
        mServerAddressIp = mPreferences.getString("ServerAddressIp", "");
        mServerAddressPort = mPreferences.getString("ServerAddressPort", "");
        mAccountCode = mPreferences.getString("AccountCode", "");
        mUserCode = mPreferences.getString("UserCode", "");
        mUserName = mPreferences.getString("UserName", "");
        mPassword = mPreferences.getString("UserPassword", "");
        mSessionKey = mPreferences.getString("SessionKey", "");
      //  mLicenseCode = mPreferences.getString("LicenseCode", "");
        mRights=mPreferences.getString("rights","");
        // 创建presenter
        presenter = createPresenter();
        presenter.attachView((V)this);
        // ButterKnife绑定
        ButterKnife.bind(BaseFragmentActivity.this);
        // 初始化view
        initView();
        //初始化数据
        initData();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (presenter != null) {
            presenter.detachView();
        }
    }


}
