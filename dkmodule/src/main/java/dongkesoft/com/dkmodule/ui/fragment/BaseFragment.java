/*******************************************************************************
 * Copyright(c) 2018 dongke All rights reserved. / Confidential
 * 类的信息：
 *		1.程序名称： BaseFragment
 *		2.功能描述：  BaseFragment基类
 * 编辑履历：
 *		作者				日期					版本				修改内容
 *		dongke			2018/6/8			1.00			新建
 *******************************************************************************/
package dongkesoft.com.dkmodule.ui.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import dongkesoft.com.dkmodule.common.Constants;
import dongkesoft.com.dkmodule.mvp.presenter.BaseFragmentPresenter;
import dongkesoft.com.dkmodule.mvp.view.MvpView;


/**
 * 项目名称：dklibrary
 * 类描述：BaseFragment基类
 * 创建人：apple
 * 创建时间：2018/6/18 上午8:52
 * 修改人：apple
 * 修改时间：2018/6/18 上午8:52
 * 修改备注：
 */
public abstract class BaseFragment<V extends MvpView,P extends BaseFragmentPresenter<V>> extends Fragment {
    /**
     * Unbinder对象
     */
    protected Unbinder unbinder;
    /**
     * presenter对象
     */
    protected P presenter;


    /**
     * SharedPreferences对象
     */
    public SharedPreferences mPreferences;

    /**
     * 帐套
     */
    public String mAccountCode;

    /**
     * 用户
     */
    public String mUserCode;

    /**
     * 用户
     */
    public String mUserName;

    /**
     * 密码
     */
    public String mPassword;

    /**
     * SessionKey
     */
    public String mSessionKey;
    public  String warehouseId;
    public  String warehouseName;

    /**
     * 创建一个与之关联的Presenter
     * @return
     */
    protected abstract P createPresenter();

    /**
     * layoutResID
     * @return
     */
    protected abstract int setMvpView();

    /**
     * 初始化view
     */
    protected abstract void initView();

    /**
     * 初始化data
     */
    protected abstract void initData();

    /**
     * Fragment进行刷新
     */
    public abstract void refreshFragmentView();
    /**
     * ip
     */
    public String mServerAddressIp;

    /**
     * 端口
     */
    public String mServerAddressPort;
    public String token;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater,container,savedInstanceState);
        View view = inflater.inflate(setMvpView(), container, false);
        mPreferences = getActivity().getSharedPreferences(Constants.STR_SHARED_PRE_KEYS,
                Context.MODE_PRIVATE);
        mServerAddressIp = mPreferences.getString("ServerAddressIp", "");
        mServerAddressPort = mPreferences.getString("ServerAddressPort", "");
        mAccountCode = mPreferences.getString("AccountCode", "");
        mUserCode = mPreferences.getString("UserCode", "");
        mUserName = mPreferences.getString("UserName", "");
        mPassword = mPreferences.getString("UserPassword", "");
        mSessionKey = mPreferences.getString("SessionKey", "");
        //mLicenseCode = mPreferences.getString("LicenseCode", "");
        presenter = createPresenter();
        presenter.attachView((V)this);

        // 返回一个Unbinder值（进行解绑），注意这里的this不能使用getActivity()
        unbinder = ButterKnife.bind(this, view);
        initView();
        initData();
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        //解绑操作
        unbinder.unbind();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    /*通常需要载入*/
    @Override
    public void onResume() {
        super.onResume();
        //refreshView();
    }




}
