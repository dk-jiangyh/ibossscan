package dongkesoft.com.dkmodule.common;

/**
 * Created by guanhonghou on 2018/9/26.
 */

/*
 * Copyright(c) 2016 dongke All rights reserved. / Confidential
 * 类的信息：
 *		1.程序名称： Constants
 *		2.功能描述：常量
 * 编辑履历：
 *		作者				日期					版本				修改内容
 *		dongke			2016/12/07			1.00			新建
 *******************************************************************************/


import android.os.Environment;

/**
 * 项目名称：dklibrary
 * 类描述：常量
 * 创建人：apple
 * 创建时间：2018/6/19 下午1:33
 * 修改人：apple
 * 修改时间：2018/6/19 下午1:33
 * 修改备注：
 */
public class Constants {


    /********************************************/
    /**
     * serverAction
     */
    public static String serverAction = "/DKService/PDAModuleService";

    public static String STR_SHARED_PRE_KEYS = "user";
    /**
     * 图片下载手机端路径
     */
    public static final String TAKE_PHOTO_PATH = "/hsPicture/";
    /**
     * 图片下载手机端临时文件夹
     */
    public static final String DONGKE_TEMP_FOLDER = "/hsPicture/tempimg";
    /**
     * 成功
     */
    public static final int SUCCESS = 0;

    public static final int MIN_CLICK_TIME = 1000;

    /**
     * 异常返回值
     */
    public static final int NegativeThree = -3;
    /**
     * 异常返回值
     */
    public static final int NegativeTwo = -2;


    public static final int ACTION_RESULT_STATUS = -4;
    /**
     * 长时间无操作已失效,请重新登录
     */
    public static final int ACTION_INVALID_STATUS = -990;
    /**
     * 每页显示的行数
     */
    public static final int PAGE_SIZE = 20;

    /**
     * 最大线程数
     */
    public static final int DEFAULT_MAX_THREAD = 5;
    /**
     * 请求的最大时间
     */
    public static final int HTTP_TIME_OUT = 60;
    /**
     * URL-返回的json包含d的值
     */
    public static final String ANDROID_URL = "http://%s:%s/api/android/doAction";
    /**
     * URL-返回的json不包含d的值
     */
    public static final String URL = "http://%s:%s/SmartStorageAPI.svc/DoActionMessage";
    /**
     * 上传图片地址
     */
    public static final String ImageURL = "http://%s:%s/SmartStorageAPI.svc/SaveImg";
    /**
     * 下载图片地址
     */
    public static final String DownLoadImageURL = "http://%s:%s/SmartStorageAPI.svc/DownFileOrImage";
    /**
     * 自动升级地址
     */
    public static final String DownloadURL = "http://%s:%s/SmartStorageAPI.svc/AutoUpgrade";
    /**
     * 打印二维码地址
     */
    public static final String PRINT_QR_URL = "http://%s:%s/WebService/SMD/DKSMD.aspx";
    /**
     * URL
     */
    public static final String LOGIN_URL ="http://%s:%s/WebService/SMD/DKSMD.aspx";

    /**
     * URLOther
     */
    public static final String URL_OTHER = "http://%s:%s/api/android/%s";
    /**
     * 图片url
     */
    public static final String URL_IMG =  "http://%s:%s/SmartGroupAPI.svc/DownFileOrImage";

    /**
     * SERVER_IP
     */
    public static final String SERVER_IP = "192.168.0.30";

    /**
     * SERVER_PORT
     */
    public static final String SERVER_PORT = "8088";
    /**
     * 每个品默认的时间间隔
     */
    public static final int DEFAUL_CODE_TINTERVAL_TIME = 5;
}
