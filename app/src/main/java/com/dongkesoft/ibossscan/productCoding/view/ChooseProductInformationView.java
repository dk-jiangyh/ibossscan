package com.dongkesoft.ibossscan.productCoding.view;

import dongkesoft.com.dkmodule.mvp.view.MvpView;

public interface ChooseProductInformationView extends MvpView {
    /**
     * 返回异常数据
     * @param message
     */
    void showFailedError(String message);

    void saveFailedError(String message);

    /**
     * 返回正常数据
     * @param result
     */
    void success(String result);

    /**
     * 重新登录数据
     * @param message
     */
    void getRelogin(String message);

}
