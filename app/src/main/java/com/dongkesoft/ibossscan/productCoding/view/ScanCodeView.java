/*******************************************************************************
 * Copyright(c) 2018 dongke All rights reserved. / Confidential
 * 类的信息：
 * 1.程序名称：QRCodeView
 * 2.功能描述：生成二维码处理view
 * 编辑履历：
 * 作者				日期					版本				修改内容
 * dongke			2018/6/8			1.00			新建
 *******************************************************************************/
package com.dongkesoft.ibossscan.productCoding.view;

import dongkesoft.com.dkmodule.mvp.view.MvpView;

/**
 * Created by Administrator on 2018/10/30.
 */
public interface ScanCodeView extends MvpView {
    /**
     * 返回异常数据
     * @param message
     */
    void showFailedError(String message);

    void saveFailedError(String message);

    /**
     * 返回正常数据
     * @param result
     */
    void success(String result);

    /**
     * 重新登录数据
     * @param message
     */
    void getRelogin(String message);


}
