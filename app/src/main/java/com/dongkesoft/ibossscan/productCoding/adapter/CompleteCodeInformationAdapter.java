package com.dongkesoft.ibossscan.productCoding.adapter;

import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dongkesoft.ibossscan.home.adapter.IBossBaseAdapter;
import com.dongkesoft.ibossscan.productCoding.bean.CompleteCodeInfo;
import com.dongkesoft.ibossscan.R;

import java.text.DecimalFormat;

public class CompleteCodeInformationAdapter  extends IBossBaseAdapter<CompleteCodeInfo> {
    public CompleteCodeInformationAdapter(Context context) {
        super(context);
    }
    private CompleteCodeInformationAdapter.onRevokeClick click;

    public void setClick(CompleteCodeInformationAdapter.onRevokeClick click) {
        this.click = click;
    }
    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public int getItemLayoutId(int getItemViewType) {
        return R.layout.adapter_complete_product;
    }

    @Override
    public void handleItem(int itemViewType, int position, CompleteCodeInfo item, ViewHolder holder, boolean isRecycle) {
        final CompleteCodeInfo list = mData.get(position);
        TextView tv_only_code = holder.get(R.id.tv_only_code);//唯一编码
        TextView tv_code = holder.get(R.id.tv_code);//商品编码
        TextView tv_product_name = holder.get(R.id.tv_product_name);//商品名称
        TextView tv_random = holder.get(R.id.tv_random);//随机码
        TextView tv_data = holder.get(R.id.tv_data);//打码日期
        TextView et_grade = holder.get(R.id.et_grade);//等级
        TextView tv_warehouse = holder.get(R.id.tv_warehouse);//库区
        TextView tv_position = holder.get(R.id.tv_position);//仓位
        TextView tv_specifications = holder.get(R.id.tv_specifications);//规格
        TextView tv_color_number = holder.get(R.id.tv_color_number);//色号
        TextView tv_number_of_cases = holder.get(R.id.tv_number_of_cases);//箱数
        TextView tv_slice = holder.get(R.id.tv_slice);//片数
        Button btn_copy = holder.get(R.id.btn_copy);//复制原码
        Button btn_edit = holder.get(R.id.btn_edit);//重新打码

        LinearLayout content = holder.get(R.id.content);
        content.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                click.onItemClick(position);
            }
        });
        tv_only_code.setText(list.getOnlyCode());
        tv_code.setText(list.getCode());
        tv_random.setText(list.getRandomCode());
        try {
            tv_data.setText(list.getProductCodeDate());
        } catch (Exception e) {
            e.printStackTrace();
        }
        et_grade.setText(list.getGradeName());
        tv_product_name.setText(list.getGoodsName());
        tv_warehouse.setText(list.getWarehouseName());
        tv_position.setText(list.getPositionNumber());
        tv_specifications.setText(list.getSpecification());
        tv_color_number.setText(list.getColorNumber());
        tv_number_of_cases.setText(new DecimalFormat("0").format(Double.parseDouble(list.getBox())));
        tv_slice.setText(new DecimalFormat("0").format(Double.parseDouble(list.getPiece())));

        btn_copy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                click.copyData( position);


            }
        });
        btn_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                click.editData( position);


            }
        });
    }

    public interface onRevokeClick {
        void onItemClick(int postion);
        void copyData(int position);
        void editData(int position);
    }
}
