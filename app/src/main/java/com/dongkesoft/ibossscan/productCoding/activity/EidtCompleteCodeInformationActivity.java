/*******************************************************************************
 * Copyright(c) 2018 dongke All rights reserved. / Confidential
 * 类的信息：
 * 1.程序名称：EidtCompleteCodeInformationActivity
 * 2.功能描述：编辑商品打码
 * 编辑履历：
 * 作者				日期					版本				修改内容
 * dongke			2018/6/8			1.00			新建
 *******************************************************************************/
package com.dongkesoft.ibossscan.productCoding.activity;

import android.content.Intent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.dongkesoft.ibossscan.productCoding.bean.ChooseProductInformationInfo;
import com.dongkesoft.ibossscan.productCoding.bean.CompleteCodeInfo;
import com.dongkesoft.ibossscan.productCoding.presenter.EidtCompleteCodeInformationPresenter;
import com.dongkesoft.ibossscan.productCoding.view.ChooseProductInformationView;
import com.dongkesoft.ibossscan.R;
import com.dongkesoft.ibossscan.common.IBossBasePopupWindow;
import com.dongkesoft.ibossscan.utils.AlertAnimateUtil;
import com.dongkesoft.ibossscan.utils.ProcessDialogUtils;
import com.dongkesoft.ibossscan.utils.StringUtils;
import com.dongkesoft.ibossscan.utils.TimePickerInfo;
import com.dongkesoft.ibossscan.utils.ToastUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import dongkesoft.com.dkmodule.ui.activity.BaseActivity;
import dongkesoft.com.dkmodule.utils.CommonUtils;

public class EidtCompleteCodeInformationActivity extends BaseActivity<ChooseProductInformationView, EidtCompleteCodeInformationPresenter>
        implements ChooseProductInformationView {
    /*
     * 标题
     * */
    @BindView(R.id.tv_center)
    TextView tvTitle;
    /*
     * 返回按钮
     * */
    @BindView(R.id.iv_left)
    ImageView ivLeft;

    /*
     * 日期布局
     * */
    @BindView(R.id.llay_date)
    LinearLayout llay_date;
    /*
     * 日期
     * */
    @BindView(R.id.tv_date)
    TextView tv_date;
    /*
     * 商品扫码布局
     * */
    @BindView(R.id.llay_commodity_bar_code)
    LinearLayout llay_commodity_bar_code;
    /*
     * 商品扫码
     * */
    @BindView(R.id.et_commodity_bar_code)
    EditText et_commodity_bar_code;
    /*
     * 箱数
     * */
    @BindView(R.id.edt_number_of_cases)
    EditText edt_number_of_cases;
    /*
     * 片数
     * */
    @BindView(R.id.et_number_of_films)
    EditText et_number_of_films;
    /*
     * 打印按钮
     * */
    @BindView(R.id.btn_scan)
    RelativeLayout btn_scan;
    /*
     * 筛查
     * */
    @BindView(R.id.tv_right)
    TextView tvRight;
    /*
     * 商品名称
     * */
    @BindView(R.id.tv_trade_name)
    TextView tv_trade_name;
    /*
     * 商品编码
     * */
    @BindView(R.id.tv_commodity_code)
    TextView tv_commodity_code;
    /*
     * 唯一编码
     * */
    @BindView(R.id.tv_only_code)
    TextView tv_only_code;
    /*
     * 规格
     * */
    @BindView(R.id.tv_specifications)
    TextView tv_specifications;
    /*
     * 色号
     * */
    @BindView(R.id.tv_color_code)
    TextView tv_color_code;
    /*
     * 等级
     * */
    @BindView(R.id.tv_grade)
    TextView tv_grade;
    /*
     * 商品名称
     * */
    @BindView(R.id.rlay_product_name)
    RelativeLayout rlay_product_name;
    /*
     *仓库
     * */
    @BindView(R.id.tv_warehouse)
    TextView tv_warehouse;
    /*
     * 仓位
     * */
    @BindView(R.id.tv_position)
    TextView tv_position;
    /*
     * 仓位
     * */
    @BindView(R.id.tv_package1)
    TextView tv_package1;
    /**
     * 日期控件
     */
    private TimePickerInfo mTimePickerInfo;
    /**
     * 当前时间
     */
    private long mCurrentTimeMillis;
    /**
     * 包装
     */
    private String packageQuantity;
    /**
     * 箱数
     */
    private String boxQuantity;
    /**
     * 排数
     */
    private int racketQuantity = 1;
    /**
     * 条码
     */
    private String barcode;


    String mCodeID = "";
    String mCode = "";
    String mOnlyCode = "";
    String mGoodsName = "";
    String mSpecification = "";
    String mColorNumber = "";
    String mGradeID = "";
    String mPackage = "";
    String mBox = "";
    String mPiece = "";
    String mNewBatchNo = "";
    String mRandomCode = "";
    String mProductCodeDate = "";
    String mDetailID = "";
    String mWarehouseID = "";
    String mPositionNumber = "";
    String mInventoryID = "";
    private IBossBasePopupWindow iBossBasePopupWindowDelete;

    @Override
    public void showFailedError(String message) {
        ProcessDialogUtils.closeProgressDilog();
        btn_scan.setEnabled(true);
        CommonUtils.setVibrate(
                getApplicationContext());
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void saveFailedError(String message) {

        ToastUtil.showLongToast(this,message);
        ProcessDialogUtils.closeProgressDilog();
    }

    @Override
    public void success(String result) {
        ProcessDialogUtils.closeProgressDilog();
        try {
            JSONObject obj = new JSONObject(result);
            String message = obj.optString("Message");
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date currentDate = new Date();
        String currentDateStr = formatter.format(currentDate);


        tv_date.setText(currentDateStr);
        et_commodity_bar_code.setText("");
        tv_trade_name.setText("");
        tv_commodity_code.setText("");
        tv_only_code.setText("");
        tv_specifications.setText("");
        tv_color_code.setText("");
        tv_grade.setText("");
        edt_number_of_cases.setText("");
        et_number_of_films.setText("0");
        btn_scan.setEnabled(true);
        finish();
    }

    @Override
    public void getRelogin(String message) {
        ProcessDialogUtils.closeProgressDilog();
        AlertAnimateUtil.showReLoginDialog(
                EidtCompleteCodeInformationActivity.this, "异常登录",
                message);
    }

    @Override
    protected EidtCompleteCodeInformationPresenter createPresenter() {
        return new EidtCompleteCodeInformationPresenter();
    }

    @Override
    protected int setMvpView() {
        return R.layout.activity_edit_complete_qrcade;
    }

    @Override
    protected void initView() {
        initDate();
        tvTitle.setVisibility(View.VISIBLE);
        ivLeft.setVisibility(View.VISIBLE);
        tvRight.setVisibility(View.GONE);
        // tvRight.setText("列表");
        tvTitle.setText("编辑商品打码");
        DecimalFormat format = new DecimalFormat("0");
        List<CompleteCodeInfo> list_moudle = new ArrayList<CompleteCodeInfo>();
        list_moudle = (ArrayList<CompleteCodeInfo>) getIntent().getSerializableExtra("itemlist");
        mDetailID = list_moudle.get(0).getDetailID();
        tv_trade_name.setText(list_moudle.get(0).getGoodsName());
        tv_commodity_code.setText(list_moudle.get(0).getCode());
        tv_only_code.setText(list_moudle.get(0).getOnlyCode());
        tv_specifications.setText(list_moudle.get(0).getSpecification());
        tv_color_code.setText(list_moudle.get(0).getColorNumber());
        tv_grade.setText(list_moudle.get(0).getGradeName());
        tv_warehouse.setText(list_moudle.get(0).getWarehouseName());
        tv_package1.setText(list_moudle.get(0).getPACKAGE());
        tv_position.setText(list_moudle.get(0).getPositionNumber());
        edt_number_of_cases.setText(format.format(Double.parseDouble(list_moudle.get(0).getBox())));
        et_number_of_films.setText(format.format(Double.parseDouble(list_moudle.get(0).getPiece())));
        mCodeID =list_moudle.get(0).getCodeID();
        mCode =list_moudle.get(0).getCode();
        mOnlyCode =list_moudle.get(0).getOnlyCode();
        mGoodsName ="";
        mSpecification =list_moudle.get(0).getSpecification();
        mColorNumber =list_moudle.get(0).getColorNumber();
        mGradeID =list_moudle.get(0).getGradeID();
        mPackage =list_moudle.get(0).getPACKAGE();
        mWarehouseID =list_moudle.get(0).getWarehouseID();
        mPositionNumber =list_moudle.get(0).getPositionNumber();
        mInventoryID =list_moudle.get(0).getInventoryID();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

        tv_date.setText(list_moudle.get(0).getProductCodeDate());


    }

    @Override
    protected void initData() {

    }


    /**
     * 点击事件
     *
     * @param view
     */
    @OnClick({R.id.llay_date, R.id.iv_left, R.id.rlay_product_name, R.id.btn_scan,
            R.id.tv_right})
    public void OnClick(View view) {
        int i = view.getId();
        if (i == R.id.llay_date) {
            mTimePickerInfo.show(new Date(mCurrentTimeMillis));
        }
        if (i == R.id.rlay_product_name) {
            Intent intent = new Intent(EidtCompleteCodeInformationActivity.this, ChooseProductInformationActivity.class);
            startActivityForResult(intent, 101);
        }
        if (i == R.id.btn_scan) {
            /*校验*/
            if (!check()) {
                return;
            }
            if(iBossBasePopupWindowDelete==null){
                iBossBasePopupWindowDelete = new IBossBasePopupWindow(
                        EidtCompleteCodeInformationActivity.this, R.layout.popup_window_exit);

            }
            iBossBasePopupWindowDelete
                    .setPopUpWindowCallBack(new IBossBasePopupWindow.IPopUpWindowCallBack() {

                        @Override
                        public void popUpWindowCallBack(View view) {
                            TextView tvPopupWindowMessage = (TextView) view
                                    .findViewById(R.id.tv_popup_window_message);
                            TextView tvPopupWindowTitle = (TextView) view
                                    .findViewById(R.id.tv_popup_window_title);
                            tvPopupWindowTitle.setText("提示");
                            try {
                                tvPopupWindowMessage.setText("请确认是否要打印？");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            // 对布局文件进行初始化
                            RelativeLayout llCancel = (RelativeLayout) view
                                    .findViewById(R.id.ll_cancel);
                            // 对布局中的控件添加事件监听
                            llCancel.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    iBossBasePopupWindowDelete.dismiss();
                                }
                            });
                            RelativeLayout llOK = (RelativeLayout) view
                                    .findViewById(R.id.ll_ok);
                            // 对布局中的控件添加事件监听
                            llOK.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    //edtFocus.requestFocus();
                                    /*校验*/
                                    if (!check()) {
                                        return;
                                    }

                                    ProcessDialogUtils.showProcessDialog(EidtCompleteCodeInformationActivity.this);
                                    mBox = edt_number_of_cases.getText().toString();
                                    mPiece = et_number_of_films.getText().toString();
                                    // 当前时间点的毫秒和随机数 2019-07-19
                                    int randomnum = (int) (Math.random() * 100);
                                    mRandomCode = String.valueOf(System.currentTimeMillis()) + String.valueOf(randomnum);
                                    mProductCodeDate = tv_date.getText().toString();
                                    StringBuilder sb = new StringBuilder();
                                    String separator = "→";
                                    sb.append(mProductCodeDate);
                                    sb.append(separator);
                                    sb.append(mCodeID);
                                    sb.append(separator);
                                    sb.append(mCode);
                                    sb.append(separator);
                                    sb.append(mOnlyCode);
                                    sb.append(separator);
                                    sb.append(mSpecification);
                                    sb.append(separator);
                                    sb.append(mColorNumber);
                                    sb.append(separator);
                                    sb.append(mGradeID);
                                    sb.append(separator);
                                    sb.append(mBox == "" ? "0" : mBox);
                                    sb.append(separator);
                                    sb.append(mPiece == "" ? "0" : mPiece);
                                    sb.append(separator);
                                    sb.append(mRandomCode);
                                    mNewBatchNo = sb.toString();
                                    // 传递参数
                                    HashMap<String, String> map = new HashMap<>();
                                    map.put("Action", "EditQrGoodsCodeForMobile");
                                    map.put("AccountCode", mAccountCode);
                                    map.put("UserCode", mUserCode);
                                    map.put("UserPassword", mPassword);
                                    map.put("SessionKey", mSessionKey);
                                    // map.put("LicenseCode", mLicenseCode);
                                    map.put("CodeID", mCodeID);//CodeID 商品id
                                    map.put("Code", mCode);//Code 商品编码
                                    map.put("OnlyCode", mOnlyCode);//OnlyCode 唯一编码
                                    map.put("GoodsName", mGoodsName);//GoodsName 商品名称
                                    map.put("Specification", mSpecification);//Specification 规格
                                    map.put("ColorNumber", mColorNumber);// 色号
                                    map.put("GradeID", mGradeID);//等级id
                                    map.put("Package", mPackage);//保装
                                    map.put("Box", mBox);//箱数
                                    map.put("Piece", mPiece);//片数
                                    map.put("NewBatchNo", mNewBatchNo);//新二维码批次号
                                    map.put("RandomCode", mRandomCode);// RandomCode 随机码
                                    map.put("ProductCodeDate", mProductCodeDate);//  打码日期
                                    map.put("DetailID", mDetailID);//  明细id
                                    map.put("WarehouseID", mWarehouseID);//  库区id
                                    map.put("PositionNumber", mPositionNumber);// 仓位
                                    map.put("InventoryID", mInventoryID);//

                                    presenter.getData(EidtCompleteCodeInformationActivity.this, map, mServerAddressIp, mServerAddressPort);
                                    iBossBasePopupWindowDelete.dismiss();
                                }
                            });
                        }
                    });
            iBossBasePopupWindowDelete.show(false, findViewById(R.id.content), 0, 0);





        }
        if (i == R.id.iv_left) {
            finish();
        }
        if (i == R.id.tv_right) {
            Intent intent = new Intent(EidtCompleteCodeInformationActivity.this, CompleteCodeInformationActivity.class);
            startActivity(intent);
        }

    }

    /**
     * 初始化日期变量
     */
    public void initDate() {
        mCurrentTimeMillis = System.currentTimeMillis();
        mTimePickerInfo = new TimePickerInfo(this, TimePickerInfo.Type.YEAR_MONTH_DAY);
        mTimePickerInfo.setCyclic(true);
        mTimePickerInfo.setCancelable(true);
        mTimePickerInfo.setOnTimeSelectListener(new TimePickerInfo.OnTimeSelectListener() {

            @Override
            public void onTimeSelect(Date date) {
                SimpleDateFormat dateFormater = new SimpleDateFormat("yyyy-MM-dd");
                Date mDate = date;
                String strDate = dateFormater.format(mDate);

                tv_date.setText(strDate);

            }
        });
    }

    /**
     * 数据校验
     *
     * @return
     */
    private boolean check() {

        if("".equals(tv_commodity_code.getText().toString())||"".equals(tv_only_code.getText().toString()))
        {
            Toast.makeText(this, "请选择商品信息", Toast.LENGTH_SHORT).show();
            return false;
        }
        if ("0".equals(edt_number_of_cases.getText().toString()) && "0".equals(et_number_of_films.getText().toString())) {
            Toast.makeText(this, "请输入箱数或片数", Toast.LENGTH_SHORT).show();
            return false;
        }
        if ("".equals(edt_number_of_cases.getText().toString()) && "0".equals(et_number_of_films.getText().toString())) {
            Toast.makeText(this, "请输入箱数或片数", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (StringUtils.isEmpty(edt_number_of_cases.getText().toString()) && StringUtils.isEmpty(et_number_of_films.getText().toString())) {
            Toast.makeText(this, "请输入箱数或片数", Toast.LENGTH_SHORT).show();
            return false;
        }
        if ("0".equals(edt_number_of_cases.getText().toString()) && StringUtils.isEmpty(et_number_of_films.getText().toString())) {
            Toast.makeText(this, "请输入箱数或片数", Toast.LENGTH_SHORT).show();
            return false;
        }
        // 片数不能大于包装
        if (Double.parseDouble(StringUtils.isEmpty(et_number_of_films.getText().toString())?"0":et_number_of_films.getText().toString()) >= Double.parseDouble(mPackage)) {
            Toast.makeText(this, "片数不能大于包装", Toast.LENGTH_SHORT).show();
            return false;
        }



        return true;
    }

    /**
     * 返回主页
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 101 && resultCode == 3) {
            List<ChooseProductInformationInfo>  mRetureList = new ArrayList<ChooseProductInformationInfo>();
            mRetureList=  (ArrayList<ChooseProductInformationInfo>) data.getSerializableExtra("itemlist");
            tv_trade_name.setText(mRetureList.get(0).getGoodsName());
            tv_commodity_code.setText(mRetureList.get(0).getCode());
            tv_only_code.setText(mRetureList.get(0).getOnlyCode());
            tv_specifications.setText(mRetureList.get(0).getSpecification());
            tv_color_code.setText(mRetureList.get(0).getColorNumber());
            tv_grade.setText(mRetureList.get(0).getGradeName());
            tv_warehouse.setText(mRetureList.get(0).getWarehouseName());
            tv_position.setText(mRetureList.get(0).getPositionNumber());
            tv_package1.setText(mRetureList.get(0).getPACKAGE());

            mCodeID =mRetureList.get(0).getCodeID();
            mCode =mRetureList.get(0).getCode();
            mOnlyCode =mRetureList.get(0).getOnlyCode();
            mGoodsName =mRetureList.get(0).getGoodsName();
            mSpecification =mRetureList.get(0).getSpecification();
            mColorNumber =mRetureList.get(0).getColorNumber();
            mGradeID =mRetureList.get(0).getGradeID();
            mPackage =mRetureList.get(0).getPACKAGE();
            mWarehouseID =mRetureList.get(0).getWarehouseID();
            mPositionNumber =mRetureList.get(0).getPositionNumber();
            mInventoryID =mRetureList.get(0).getInventoryID();

        }


    }
}
