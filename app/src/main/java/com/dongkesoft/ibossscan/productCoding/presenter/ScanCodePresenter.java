/*******************************************************************************
 * Copyright(c) 2018 dongke All rights reserved. / Confidential
 * 类的信息：
 * 1.程序名称：QRCodePresenter
 * 2.功能描述：生成二维码处理Presenter
 * 编辑履历：
 * 作者				日期					版本				修改内容
 * dongke			2018/6/8			1.00			新建
 *******************************************************************************/
package com.dongkesoft.ibossscan.productCoding.presenter;

import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;


import com.dongkesoft.ibossscan.productCoding.view.ScanCodeView;
import com.dongkesoft.ibossscan.common.RightsSet;
import com.dongkesoft.ibossscan.utils.DataBaseOpenHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import dongkesoft.com.dkmodule.bean.ReqCallBack;
import dongkesoft.com.dkmodule.common.Constants;
import dongkesoft.com.dkmodule.mvp.presenter.BasePresenter;
import dongkesoft.com.dkmodule.utils.OkAsyncHttpClient;

/**
 * Created by Administrator on 2018/10/30.
 * 生成二维码处理Presenter
 */
public class ScanCodePresenter extends BasePresenter<ScanCodeView> {

    private HandlerThread thread;
    private Handler mHandler;
    private Context context;
    /*
     * 数据库帮助器
     */
    public DataBaseOpenHelper dataBaseOpenHelper;

    /**
     * 得到商品信息
     *
     * @param context
     * @param map
     */
    public void getData(Context context, HashMap<String, String> map, String ip, String port) {

        OkAsyncHttpClient.getInstance(context).requestPostByAsynWithForm(String.format(Constants.PRINT_QR_URL,
                ip, port), map, new ReqCallBack<String>() {
            @Override
            public void onReqSuccess(final String result) {
                {
                    //需要在UI线程执行
                    mViewHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            // 逻辑判断
                            try {
                                JSONObject obj = new JSONObject(result);

                                int status = obj.optInt("Status");
                                String message = obj.optString("Message");
                                //
                                if (status == 0) {
                                    getView().success(result);
                                } else if (status == RightsSet.NegativeOne) {
                                    getView().getRelogin(message);
                                } else {
                                    getView().saveFailedError(message);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            }

            @Override
            public void onReqFailed(final String errorMsg) {
                {
                    mViewHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            getView().showFailedError(errorMsg);
                        }
                    });
                }
            }
        });

    }

}
