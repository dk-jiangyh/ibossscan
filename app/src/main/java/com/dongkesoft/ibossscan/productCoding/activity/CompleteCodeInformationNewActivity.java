/*******************************************************************************
 * Copyright(c) 2018 dongke All rights reserved. / Confidential
 * 类的信息：
 * 1.程序名称：CompleteCodeInformationActivity
 * 2.功能描述：已完成的商品信息
 * 编辑履历：
 * 作者				日期					版本				修改内容
 * dongke			2018/6/8			1.00			新建
 *******************************************************************************/
package com.dongkesoft.ibossscan.productCoding.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dongkesoft.ibossscan.R;
import com.dongkesoft.ibossscan.productCoding.adapter.CompleteCodeInformationAdapter;
import com.dongkesoft.ibossscan.productCoding.adapter.CompleteCodeInformationNewAdapter;
import com.dongkesoft.ibossscan.productCoding.bean.CompleteCodeInfo;
import com.dongkesoft.ibossscan.productCoding.presenter.CompleteCodeInformationPresenter;
import com.dongkesoft.ibossscan.productCoding.view.ChooseProductInformationView;
import com.dongkesoft.ibossscan.utils.AlertAnimateUtil;
import com.dongkesoft.ibossscan.utils.CommonUtil;
import com.dongkesoft.ibossscan.utils.CostUtils;
import com.dongkesoft.ibossscan.utils.ProcessDialogUtils;
import com.dongkesoft.ibossscan.utils.TimePickerInfo;
import com.dongkesoft.ibossscan.utils.XListViewNew;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import dongkesoft.com.dkmodule.ui.activity.BaseActivity;
import dongkesoft.com.dkmodule.widget.GenericDrawerLayout;

public class CompleteCodeInformationNewActivity extends BaseActivity<ChooseProductInformationView, CompleteCodeInformationPresenter>
        implements ChooseProductInformationView, CompleteCodeInformationNewAdapter.onRevokeClick {
    public CompleteCodeInformationNewAdapter mCompleteCodeInformationAdapter;
    /*
     * 标题
     * */
    @BindView(R.id.tv_center)
    TextView tvTitle;
    /*
     * 返回按钮
     * */
    @BindView(R.id.iv_left)
    ImageView ivLeft;

    /*
     * 筛查
     * */
    @BindView(R.id.tv_right)
    TextView tvRight;
    /*
     * list
     * */
    @BindView(R.id.select_list)
    ListView selectList;
    /*
     * 搜索框
     * */
    @BindView(R.id.et_search)
    EditText edtSearch;
    /*
     * 搜索框
     * */
    @BindView(R.id.ll_no_data)
    LinearLayout ll_no_data;
    /**
     * 当前页数
     */
    private int pageNum = 1;
    private List<CompleteCodeInfo> CompleteCodeList;
    private List<CompleteCodeInfo> mList;
    private GenericDrawerLayout mDrawerLayout;
    private View mDrawerLayoutView;
    /**
     * 侧拉菜单——取消
     */
    private TextView tv_cancle;
    /**
     * 侧拉菜单——重置
     */
    private TextView tv_reset;
    /**
     * 侧拉菜单——确定
     */
    private TextView tv_sure;
    /**
     * 侧拉菜单——商品编码
     */
    private EditText et_code;
    /**
     * 侧拉菜单——唯一编码
     */
    private EditText et_only_code;
    /**
     * 侧拉菜单——商品名称
     */
    private EditText et_product_name;
    /**
     * 侧拉菜单——规格
     */
    private EditText et_specifications;
    /**
     * 侧拉菜单——色号
     */
    private EditText et_color_code;
    /**
     * 侧拉菜单——等级
     */
    private EditText et_grade;

    /**
     * 打码日期
     */
    private TextView tv_code_data;
    private String mCode = "";
    private String mOnlyCode = "";
    private String mGoodsName = "";
    private String mSpecification = "";
    private String mColorNumber = "";
    private String mGradeName = "";
    public boolean mDrawerLayoutStatus;

    /**
     * 当前时间
     */
    private long mCurrentTimeMillis;
    /**
     * 判断是否是上推加载数据
     */
    private boolean ispull = false;
    private String currentDate;

    @Override
    public void showFailedError(String message) {
        ProcessDialogUtils.closeProgressDilog();
    }

    @Override
    public void saveFailedError(String message) {
        ProcessDialogUtils.closeProgressDilog();
    }

    @Override
    public void success(String result) {
        ProcessDialogUtils.closeProgressDilog();

        try {
            JSONObject obj = new JSONObject(result);
            JSONObject json = obj.getJSONObject("Result");
            JSONArray array = json.getJSONArray("Table");
            if (array != null && array.length() > 0) {
                for (int i = 0; i < array.length(); i++) {
                    JSONObject jsonObjectDetail = array
                            .getJSONObject(i);
                    CompleteCodeInfo info = new CompleteCodeInfo();
                    info.setDetailID(jsonObjectDetail.optString("DetailID"));
                    info.setCodeID(jsonObjectDetail.optString("CodeID"));
                    info.setCode(jsonObjectDetail.optString("Code"));
                    info.setOnlyCode(jsonObjectDetail.optString("OnlyCode"));
                    info.setPACKAGE(jsonObjectDetail.optString("Package"));
                    info.setGradeID(jsonObjectDetail.optString("GradeID"));
                    info.setGradeName(jsonObjectDetail.optString("GradeName"));
                    info.setSpecification(jsonObjectDetail.optString("Specification"));
                    info.setColorNumber(jsonObjectDetail.optString("ColorNumber"));
                    info.setInputQuantity(jsonObjectDetail.optString("InputQuantity"));
                    info.setBatchNo(jsonObjectDetail.optString("BatchNo"));

                    info.setAccountOrganizationID(jsonObjectDetail.optString("AccountOrganizationID"));
                    info.setAccountID(jsonObjectDetail.optString("AccountID"));
                    info.setInventoryID(jsonObjectDetail.optString("InventoryID"));
                    info.setWarehouseID(jsonObjectDetail.optString("WarehouseID"));
                    info.setWarehouseName(jsonObjectDetail.optString("WarehouseName"));
                    info.setPositionNumber(jsonObjectDetail.optString("PositionNumber"));
                    info.setGoodsName(jsonObjectDetail.optString("GoodsName"));
                    CompleteCodeList.add(info);
                }

                    mCompleteCodeInformationAdapter.setData(CompleteCodeList);
                    selectList.setAdapter(mCompleteCodeInformationAdapter);



                ll_no_data.setVisibility(View.GONE);
            }
            else {


                if (CompleteCodeList != null && CompleteCodeList.size() == 0) {
                    ll_no_data.setVisibility(View.VISIBLE);
                    AlertAnimateUtil.alertShow(CompleteCodeInformationNewActivity.this, "提示", "未找到匹配结果");
                    ProcessDialogUtils.closeProgressDilog();
                    return;
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void getRelogin(String message) {
        ProcessDialogUtils.closeProgressDilog();
        AlertAnimateUtil.showReLoginDialog(
                CompleteCodeInformationNewActivity.this, "异常登录",
                message);
    }

    @Override
    public void onResume() {
        // CompleteCodeList.get(position)
        super.onResume();

    }



    /**
     * 加载
     */
//    @Override
//    public void onLoadMore() {
//        ispull = true;
//        pageNum++;
//        getData();
//        selectList.stopLoadMore();
//    }

    @Override
    protected CompleteCodeInformationPresenter createPresenter() {
        return new CompleteCodeInformationPresenter();
    }

    @Override
    protected int setMvpView() {
        return R.layout.activity_complete_qrcade_new;
    }

    @Override
    protected void initView() {

        CompleteCodeList = new ArrayList<CompleteCodeInfo>();
        tvTitle.setVisibility(View.VISIBLE);
        ivLeft.setVisibility(View.VISIBLE);
        tvRight.setVisibility(View.VISIBLE);
        tvRight.setText("筛查");
        tvTitle.setText("已打码商品信息");
        mCompleteCodeInformationAdapter = new CompleteCodeInformationNewAdapter(this);
        mCompleteCodeInformationAdapter.setClick(this);
        mDrawerLayout = (GenericDrawerLayout) findViewById(R.id.drawerlayout);
        mDrawerLayoutView = View.inflate(CompleteCodeInformationNewActivity.this, R.layout.drawerlayout_complete_information_new, null);
        //取消
        tv_cancle = (TextView) mDrawerLayoutView.findViewById(R.id.tv_cancle);
        //重置
        tv_reset = (TextView) mDrawerLayoutView.findViewById(R.id.tv_reset);
        //确定
        tv_sure = (TextView) mDrawerLayoutView.findViewById(R.id.tv_sure);
        //商品编码
        et_code = (EditText) mDrawerLayoutView.findViewById(R.id.et_code);
        //唯一编码
        et_only_code = (EditText) mDrawerLayoutView.findViewById(R.id.et_only_code);
        //商品名称
        et_product_name = (EditText) mDrawerLayoutView.findViewById(R.id.et_product_name);
        //规格
        et_specifications = (EditText) mDrawerLayoutView.findViewById(R.id.et_specifications);
        //色号
        et_color_code = (EditText) mDrawerLayoutView.findViewById(R.id.et_color_code);
        //等级
        et_grade = (EditText) mDrawerLayoutView.findViewById(R.id.et_grade);

        tv_cancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                mDrawerLayout.switchStatus();
            }
        });
        tv_reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                et_code.setText("");
                et_only_code.setText("");
                et_product_name.setText("");
                et_specifications.setText("");
                et_color_code.setText("");
                et_grade.setText("");
            }
        });
        tv_sure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if(CompleteCodeList!=null&&CompleteCodeList.size()>0)
                {
                    CompleteCodeList.clear();
                    if(mCompleteCodeInformationAdapter!=null)
                    {
                        mCompleteCodeInformationAdapter.setData(CompleteCodeList);

                        mCompleteCodeInformationAdapter.notifyDataSetChanged();
                    }
                }

                mCode = et_code.getText().toString();
                mOnlyCode = et_only_code.getText().toString();
                mGoodsName = et_product_name.getText().toString();
                mSpecification = et_specifications.getText().toString();
                mColorNumber = et_color_code.getText().toString();
                mGradeName = et_grade.getText().toString();
                getData();
                mDrawerLayout.switchStatus();
            }
        });


        edtSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                mList = new ArrayList<CompleteCodeInfo>();
                String batchNo = edtSearch.getText().toString()
                        .trim().replaceAll("\\n", "");
                batchNo = edtSearch.getText().toString()
                        .trim().replaceAll("\\r\\n", "");
                for (int i = 0; i < CompleteCodeList.size(); i++) {
                    if (CompleteCodeList.get(i)
                                    .getBatchNo()
                                    .indexOf(batchNo) >= 0) {
                        mList.add(CompleteCodeList.get(i));
                    }
                }
                selectList.setVisibility(View.VISIBLE);
                mCompleteCodeInformationAdapter.setData(mList);
                selectList.setAdapter(mCompleteCodeInformationAdapter);
            }

        });
    }

    @Override
    protected void initData() {
        initDrawerLayout();
        getData();

    }

    /**
     * 点击事件
     *
     * @param view
     */
    @OnClick({R.id.iv_left, R.id.tv_right})
    public void OnClick(View view) {
        int i = view.getId();
        if (i == R.id.iv_left) {
            finish();
        }
        if (i == R.id.tv_right) {
            mDrawerLayout.setOpennable(true);
            mDrawerLayout.switchStatus();
            mDrawerLayout.requestDisallowInterceptTouchEvent(true);
        }
    }

    /**
     * 得到数据
     */
    public void getData() {
        ProcessDialogUtils.showProcessDialog(this);
        // 传递参数
        HashMap<String, String> map = new HashMap<>();
        map.put("Action", "GetQrGoodsCodeForMobile");
        map.put("AccountCode", mAccountCode);
        map.put("UserCode", mUserCode);
        map.put("UserPassword", mPassword);
        map.put("SessionKey", mSessionKey);
        // map.put("LicenseCode", mLicenseCode);
        map.put("Code", mCode);//商品编码
        map.put("OnlyCode", mOnlyCode);//唯一编码
        map.put("GoodsName", mGoodsName);//商品名称
        map.put("Specification", mSpecification);//规格
        map.put("ColorNumber", mColorNumber);//色号
        map.put("GradeName", mGradeName);//等级
        map.put("BatchNo", "");//二维码批次号

        presenter.getData(this, map, mServerAddressIp, mServerAddressPort);
    }

    /**
     * 初始化DrawerLayout
     */
    private void initDrawerLayout() {
        // 可以设置打开时响应Touch的区域范围
        mDrawerLayout.setContentLayout(mDrawerLayoutView);
        mDrawerLayout.setTouchSizeOfOpened(dip2px(CompleteCodeInformationNewActivity.this, 500));
        mDrawerLayout.setTouchSizeOfClosed(dip2px(CompleteCodeInformationNewActivity.this, 0));
        // 设置随着位置的变更，背景透明度也改变
        mDrawerLayout.setOpaqueWhenTranslating(true);
        // 设置抽屉是否可以打开
        mDrawerLayout.setOpennable(false);
        // 设置抽屉的空白区域大小
        float v = getResources().getDisplayMetrics().density * 50 + 0.5f; // 100DIP
        mDrawerLayout.setDrawerEmptySize((int) v);

        // 设置事件回调
        mDrawerLayout.setDrawerCallback(new GenericDrawerLayout.DrawerCallback() {

            @Override
            public void onTranslating(int gravity, float translation, float fraction) {
            }

            @Override
            public void onStartOpen() {
            }

            @Override
            public void onStartClose() {
            }

            @Override
            public void onPreOpen() {
            }

            @Override
            public void onEndOpen() {
                mDrawerLayoutStatus = true;
            }

            @Override
            public void onEndClose() {
                mDrawerLayoutStatus = false;
            }
        });
    }

    private int dip2px(Context context, float dipValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dipValue * scale + 0.5f);
    }

    /**
     * @param postion
     */
    @Override
    public void onItemClick(int postion) {

    }

    @Override
    public void copyData(int position) {
        // CompleteCodeList.get(position)
    }

    @Override
    public void editData(int position) {
        List<CompleteCodeInfo> list_moudle = new ArrayList<CompleteCodeInfo>();
        list_moudle.add(CompleteCodeList.get(position));
        Intent intent = new Intent(CompleteCodeInformationNewActivity.this, EidtCompleteCodeInformationNewActivity.class);
        Bundle bundle = new Bundle();
        intent.putExtra("itemlist", (Serializable) list_moudle);
        intent.putExtras(bundle);
        startActivity(intent);

    }




}
