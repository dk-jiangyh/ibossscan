package com.dongkesoft.ibossscan.productCoding.bean;

import java.io.Serializable;


public class ChooseProductInformationInfo  implements Serializable {


    /**
     * GoodsName : 连体坐便器
     * CodeID : 1
     * Code : 连体坐便器
     * OnlyCode : 巧匠不锈钢齿形刮板 6*6MM
     * Specification : 820*460*850
     * Package : 4
     * ColorNumber : -
     * GradeID : 1
     * GradeName : 特等品
     *
     */

    private String GoodsName;
    private String InventoryID;
    private String CodeID;
    private String Code;
    private String OnlyCode;
    private String Specification;
    private String PACKAGE;
    private String WarehouseID;
    private String WarehouseName;
    private String ColorNumber;
    private String GradeID;
    private String GradeName;
    private String PositionNumber;

    public String getInventoryID() {
        return InventoryID;
    }

    public void setInventoryID(String inventoryID) {
        InventoryID = inventoryID;
    }

    public String getPACKAGE() {
        return PACKAGE;
    }

    public void setPACKAGE(String PACKAGE) {
        this.PACKAGE = PACKAGE;
    }

    public String getWarehouseName() {
        return WarehouseName;
    }

    public void setWarehouseName(String warehouseName) {
        WarehouseName = warehouseName;
    }

    public String getWarehouseID() {
        return WarehouseID;
    }

    public void setWarehouseID(String warehouseID) {
        WarehouseID = warehouseID;
    }

    public String getPositionNumber() {
        return PositionNumber;
    }

    public void setPositionNumber(String positionNumber) {
        PositionNumber = positionNumber;
    }

    public String getGoodsName() {
        return GoodsName;
    }

    public void setGoodsName(String goodsName) {
        GoodsName = goodsName;
    }

    public String getCodeID() {
        return CodeID;
    }

    public void setCodeID(String codeID) {
        CodeID = codeID;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String code) {
        Code = code;
    }

    public String getOnlyCode() {
        return OnlyCode;
    }

    public void setOnlyCode(String onlyCode) {
        OnlyCode = onlyCode;
    }

    public String getSpecification() {
        return Specification;
    }

    public void setSpecification(String specification) {
        Specification = specification;
    }



    public String getColorNumber() {
        return ColorNumber;
    }

    public void setColorNumber(String colorNumber) {
        ColorNumber = colorNumber;
    }

    public String getGradeID() {
        return GradeID;
    }

    public void setGradeID(String gradeID) {
        GradeID = gradeID;
    }

    public String getGradeName() {
        return GradeName;
    }

    public void setGradeName(String gradeName) {
        GradeName = gradeName;
    }
}
