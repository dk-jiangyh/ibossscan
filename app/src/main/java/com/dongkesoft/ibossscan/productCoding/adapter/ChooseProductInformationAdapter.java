package com.dongkesoft.ibossscan.productCoding.adapter;

import android.content.Context;
import android.widget.TextView;

import com.dongkesoft.ibossscan.home.adapter.IBossBaseAdapter;
import com.dongkesoft.ibossscan.productCoding.bean.ChooseProductInformationInfo;
import com.dongkesoft.ibossscan.R;

public class ChooseProductInformationAdapter  extends IBossBaseAdapter<ChooseProductInformationInfo> {
    public ChooseProductInformationAdapter(Context context) {
        super(context);
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public int getItemLayoutId(int getItemViewType) {
        return R.layout.adapter_choose_product;
    }

    @Override
    public void handleItem(int itemViewType, int position, ChooseProductInformationInfo item, ViewHolder holder, boolean isRecycle) {
        final ChooseProductInformationInfo list = mData.get(position);
        TextView tv_only_code = holder.get(R.id.tv_only_code);//唯一编码
        TextView tv_product_code = holder.get(R.id.tv_product_code);//商品编码
        TextView tv_specifications = holder.get(R.id.tv_specifications);//规格
        TextView tv_numble_color = holder.get(R.id.tv_numble_color);//色号
        TextView tv_grade = holder.get(R.id.tv_grade);//等级
        TextView ll_tv_warehouse = holder.get(R.id.ll_tv_warehouse);//仓库
        TextView ll_tv_position = holder.get(R.id.ll_tv_position);//仓位
        tv_only_code.setText(list.getOnlyCode());
        tv_product_code.setText(list.getCode());
        tv_specifications.setText(list.getSpecification());
        tv_numble_color.setText(list.getColorNumber());
        tv_grade.setText(list.getGradeName());
        ll_tv_warehouse.setText(list.getWarehouseName());
        ll_tv_position.setText(list.getPositionNumber());

    }
}
