package com.dongkesoft.ibossscan.productCoding.bean;

import java.io.Serializable;


public class CompleteCodeInfo  implements Serializable {


    /**
     * DetailID : 4
     * CodeID : 1
     * Code : 连体坐便器
     * OnlyCode : 巧匠不锈钢齿形刮板 6*6MM
     * Package : 4
     * GradeID : 1
     * GradeName : 特等品
     * Specification : 820*460*850
     * ColorNumber : -
     * InputQuantity : 802
     * Box : 200
     * Piece : 2
     * BatchNo : 2021-03-10→1→连体坐便器→巧匠不锈钢齿形刮板 6*6MM→820*460*850→-→1→200→2→161534276797950
     * RandomCode : 161534276797950
     * ProductCodeDate : 2021-03-10 00:00:00
     * AccountOrganizationID : 2
     * AccountID : 1
     */

    private String DetailID;
    private String CodeID;
    private String Code;
    private String OnlyCode;
    private String GradeID;
    private String GradeName;
    private String Specification;
    private String ColorNumber;
    private String InputQuantity;
    private String Box;
    private String Piece;
    private String BatchNo;
    private String RandomCode;
    private String ProductCodeDate;
    private String AccountOrganizationID;
    private String AccountID;
    private String PACKAGE;
    private String WarehouseID;
    private String WarehouseName;
    private String InventoryID;
    private String PositionNumber;
    private String GoodsName;

    public String getGoodsName() {
        return GoodsName;
    }

    public void setGoodsName(String goodsName) {
        GoodsName = goodsName;
    }

    public String getPACKAGE() {
        return PACKAGE;
    }

    public void setPACKAGE(String PACKAGE) {
        this.PACKAGE = PACKAGE;
    }

    public String getWarehouseID() {
        return WarehouseID;
    }

    public void setWarehouseID(String warehouseID) {
        WarehouseID = warehouseID;
    }

    public String getWarehouseName() {
        return WarehouseName;
    }

    public void setWarehouseName(String warehouseName) {
        WarehouseName = warehouseName;
    }

    public String getInventoryID() {
        return InventoryID;
    }

    public void setInventoryID(String inventoryID) {
        InventoryID = inventoryID;
    }

    public String getPositionNumber() {
        return PositionNumber;
    }

    public void setPositionNumber(String positionNumber) {
        PositionNumber = positionNumber;
    }

    public String getDetailID() {
        return DetailID;
    }

    public void setDetailID(String detailID) {
        DetailID = detailID;
    }

    public String getCodeID() {
        return CodeID;
    }

    public void setCodeID(String codeID) {
        CodeID = codeID;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String code) {
        Code = code;
    }

    public String getOnlyCode() {
        return OnlyCode;
    }

    public void setOnlyCode(String onlyCode) {
        OnlyCode = onlyCode;
    }


    public String getGradeID() {
        return GradeID;
    }

    public void setGradeID(String gradeID) {
        GradeID = gradeID;
    }

    public String getGradeName() {
        return GradeName;
    }

    public void setGradeName(String gradeName) {
        GradeName = gradeName;
    }

    public String getSpecification() {
        return Specification;
    }

    public void setSpecification(String specification) {
        Specification = specification;
    }

    public String getColorNumber() {
        return ColorNumber;
    }

    public void setColorNumber(String colorNumber) {
        ColorNumber = colorNumber;
    }

    public String getInputQuantity() {
        return InputQuantity;
    }

    public void setInputQuantity(String inputQuantity) {
        InputQuantity = inputQuantity;
    }

    public String getBox() {
        return Box;
    }

    public void setBox(String box) {
        Box = box;
    }

    public String getPiece() {
        return Piece;
    }

    public void setPiece(String piece) {
        Piece = piece;
    }

    public String getBatchNo() {
        return BatchNo;
    }

    public void setBatchNo(String batchNo) {
        BatchNo = batchNo;
    }

    public String getRandomCode() {
        return RandomCode;
    }

    public void setRandomCode(String randomCode) {
        RandomCode = randomCode;
    }

    public String getProductCodeDate() {
        return ProductCodeDate;
    }

    public void setProductCodeDate(String productCodeDate) {
        ProductCodeDate = productCodeDate;
    }

    public String getAccountOrganizationID() {
        return AccountOrganizationID;
    }

    public void setAccountOrganizationID(String accountOrganizationID) {
        AccountOrganizationID = accountOrganizationID;
    }

    public String getAccountID() {
        return AccountID;
    }

    public void setAccountID(String accountID) {
        AccountID = accountID;
    }
}
