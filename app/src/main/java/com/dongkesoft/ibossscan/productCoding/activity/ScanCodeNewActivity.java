/*******************************************************************************
 * Copyright(c) 2018 dongke All rights reserved. / Confidential
 * 类的信息：
 * 1.程序名称：ScanCodeActivity
 * 2.功能描述：打印二维码
 * 编辑履历：
 * 作者				日期					版本				修改内容
 * dongke			2018/6/8			1.00			新建
 *******************************************************************************/
package com.dongkesoft.ibossscan.productCoding.activity;

import android.bluetooth.BluetoothAdapter;
import android.content.Intent;

import android.text.Editable;
import android.text.InputFilter;

import android.text.Spanned;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;

import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import androidx.annotation.Nullable;

import com.dongkesoft.ibossscan.productCoding.bean.ChooseProductInformationInfo;
import com.dongkesoft.ibossscan.productCoding.presenter.ScanCodePresenter;
import com.dongkesoft.ibossscan.productCoding.view.ScanCodeView;
import com.dongkesoft.ibossscan.R;
import com.dongkesoft.ibossscan.common.IBossBasePopupWindow;
import com.dongkesoft.ibossscan.utils.AlertAnimateUtil;
import com.dongkesoft.ibossscan.utils.CommonUtil;
import com.dongkesoft.ibossscan.utils.ProcessDialogUtils;
import com.dongkesoft.ibossscan.utils.StringUtils;
import com.dongkesoft.ibossscan.utils.TimePickerInfo;
import com.dongkesoft.ibossscan.utils.UtilHelpers;


import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import dongkesoft.com.dkmodule.ui.activity.BaseActivity;
import dongkesoft.com.dkmodule.utils.CommonUtils;

/**
 * @Description : QRCodeActivity
 * @ClassName : 打印二维码
 * @Author : zhaoyu
 * @Date :
 */
public class ScanCodeNewActivity extends BaseActivity<ScanCodeView, ScanCodePresenter>
        implements ScanCodeView {
    /*
     * 标题
     * */
    @BindView(R.id.tv_center)
    TextView tvTitle;
    /*
     * 返回按钮
     * */
    @BindView(R.id.iv_left)
    ImageView ivLeft;


    /*
     * 商品扫码布局
     * */
    @BindView(R.id.llay_commodity_bar_code)
    LinearLayout llay_commodity_bar_code;
    /*
     * 商品扫码
     * */
    @BindView(R.id.et_commodity_bar_code)
    EditText et_commodity_bar_code;

    /*
     * 打印按钮
     * */
    @BindView(R.id.btn_scan)
    RelativeLayout btn_scan;
    /*
     * 筛查
     * */
    @BindView(R.id.tv_right)
    TextView tvRight;
    /*
     * 商品名称
     * */
    @BindView(R.id.tv_trade_name)
    TextView tv_trade_name;
    /*
     * 商品编码
     * */
    @BindView(R.id.tv_commodity_code)
    TextView tv_commodity_code;
    /*
     * 唯一编码
     * */
    @BindView(R.id.tv_only_code)
    TextView tv_only_code;
    /*
     * 规格
     * */
    @BindView(R.id.tv_specifications)
    TextView tv_specifications;
    /*
     * 色号
     * */
    @BindView(R.id.tv_color_code)
    TextView tv_color_code;
    /*
     * 等级
     * */
    @BindView(R.id.tv_grade)
    TextView tv_grade;
    /*
     *仓库
     * */
    @BindView(R.id.tv_warehouse)
    TextView tv_warehouse;
    /*
     * 仓位
     * */
    @BindView(R.id.tv_position)
    TextView tv_position;

    /*
     * 商品名称
     * */
    @BindView(R.id.rlay_product_name)
    RelativeLayout rlay_product_name;

    /**
     * 排数
     */
    private int racketQuantity = 1;
    /**
     * 条码
     */
    private String barcode;

    // 打印正常flag
    private boolean abnormalFlag = false;
    // 打印类型
    private int abnormalType = -1;

    List<ChooseProductInformationInfo> mRetureList;
    String mCodeID = "";
    String mCode = "";
    String mOnlyCode = "";
    String mGoodsName = "";
    String mSpecification = "";
    String mColorNumber = "";
    String mGradeID = "";
    String mWarehouseID = "";
    String mPositionNumber = "";
    String mInventoryID = "";
    String mBatchNo = "";
    String mProductCodeDate = "";
    private IBossBasePopupWindow iBossBasePopupWindowDelete;

    /**
     * 处理Presenter
     *
     * @return
     */
    @Override
    protected ScanCodePresenter createPresenter() {
        return new ScanCodePresenter();
    }

    /**
     * view
     *
     * @return
     */
    @Override
    protected int setMvpView() {
        return R.layout.activity_qrcode_new;
    }




    /**
     * 标题
     */
    @Override
    protected void initView() {
        tvTitle.setVisibility(View.VISIBLE);
        ivLeft.setVisibility(View.VISIBLE);
        tvRight.setVisibility(View.VISIBLE);
        tvRight.setText("列表");
        tvTitle.setText("商品打码");
        allListeners();


    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        switch (ev.getAction()) {
            case MotionEvent.ACTION_DOWN:
                View view = getCurrentFocus();
                UtilHelpers.hideKeyboard(ev, view, ScanCodeNewActivity.this);//调用方法判断是否需要隐藏键盘
                break;

            default:
                break;
        }
        return super.dispatchTouchEvent(ev);
    }

    private void allListeners() {

        et_commodity_bar_code.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (et_commodity_bar_code.getText().toString().contains("\\n")) {
                    barcode = et_commodity_bar_code.getText().toString().trim().replaceAll("\\n", "");
                    barcode = barcode.substring(0, barcode.length() - 2);
                    et_commodity_bar_code.setFocusable(false);
                    et_commodity_bar_code.setEnabled(false);
                    et_commodity_bar_code.setFilters(new InputFilter[]{new InputFilter() {
                        @Override
                        public CharSequence filter(
                                CharSequence source, int start,
                                int end, Spanned dest,
                                int dstart, int dend) {
                            return source.length() < 1 ? dest
                                    .subSequence(dstart, dend)
                                    : "";
                        }
                    }});

                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("BarCode", barcode);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    ProcessDialogUtils.showProcessDialog(ScanCodeNewActivity.this);
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("token", token);
                    map.put("module", "DataBasicInfo");
                    map.put("action", "GetCode");
                    map.put("data", jsonObject.toString());
                    //   presenter.ValidateBarcode(scanCodeActivity.this, map, mServerAddressIp, mServerAddressPort);

                }

            }
        });
    }


    /**
     * 点击事件
     *
     * @param view
     */
    @OnClick({ R.id.iv_left, R.id.rlay_product_name, R.id.btn_scan,
            R.id.tv_right})
    public void OnClick(View view) {
        int i = view.getId();

        if (i == R.id.rlay_product_name) {
            Intent intent = new Intent(ScanCodeNewActivity.this, ChooseProductInformationActivity.class);
            startActivityForResult(intent, 101);
        }
        if (i == R.id.btn_scan) {
            /*校验*/
            if (!check()) {
                return;
            }
            if (iBossBasePopupWindowDelete == null) {
                iBossBasePopupWindowDelete = new IBossBasePopupWindow(
                        ScanCodeNewActivity.this, R.layout.popup_window_exit);

            }
            iBossBasePopupWindowDelete
                    .setPopUpWindowCallBack(new IBossBasePopupWindow.IPopUpWindowCallBack() {

                        @Override
                        public void popUpWindowCallBack(View view) {
                            TextView tvPopupWindowMessage = (TextView) view
                                    .findViewById(R.id.tv_popup_window_message);
                            TextView tvPopupWindowTitle = (TextView) view
                                    .findViewById(R.id.tv_popup_window_title);
                            tvPopupWindowTitle.setText("提示");
                            try {
                                tvPopupWindowMessage.setText("请确认是否要打印？");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            // 对布局文件进行初始化
                            RelativeLayout llCancel = (RelativeLayout) view
                                    .findViewById(R.id.ll_cancel);
                            // 对布局中的控件添加事件监听
                            llCancel.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    iBossBasePopupWindowDelete.dismiss();
                                }
                            });
                            RelativeLayout llOK = (RelativeLayout) view
                                    .findViewById(R.id.ll_ok);
                            // 对布局中的控件添加事件监听
                            llOK.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    //edtFocus.requestFocus();



                                    ProcessDialogUtils.showProcessDialog(ScanCodeNewActivity.this);


                                    StringBuilder sb = new StringBuilder();
                                    String separator = "→";

                                    sb.append(mCodeID);
                                    sb.append(separator);
                                    sb.append(mCode);
                                    sb.append(separator);
                                    sb.append(mOnlyCode);
                                    sb.append(separator);
                                    sb.append(mSpecification);
                                    sb.append(separator);
                                    sb.append(mColorNumber);
                                    sb.append(separator);
                                    sb.append(mGradeID);
                                    sb.append(separator);
                                    sb.append(mWarehouseID);
                                    sb.append(separator);
                                    sb.append(mPositionNumber);
                                    mBatchNo = sb.toString();
                                    // 传递参数
                                    HashMap<String, String> map = new HashMap<>();
                                    map.put("Action", "SaveQrGoodsCodeForMobile");
                                    map.put("AccountCode", mAccountCode);
                                    map.put("UserCode", mUserCode);
                                    map.put("UserPassword", mPassword);
                                    map.put("SessionKey", mSessionKey);
                                    // map.put("LicenseCode", mLicenseCode);
                                    map.put("CodeID", mCodeID);//CodeID 商品id
                                    map.put("Code", mCode);//Code 商品编码
                                    map.put("OnlyCode", mOnlyCode);//OnlyCode 唯一编码
                                    map.put("GoodsName", mGoodsName);//GoodsName 商品名称
                                    map.put("Specification", mSpecification);//Specification 规格
                                    map.put("ColorNumber", mColorNumber);// 色号
                                    map.put("GradeID", mGradeID);//等级id
                                    map.put("BatchNo", mBatchNo);//二维码批次号
                                    map.put("WarehouseID", mWarehouseID);//  库区id
                                    map.put("PositionNumber", mPositionNumber);// 仓位
                                    map.put("InventoryID", mInventoryID);//
                                    map.put("Box", "0");//箱数
                                    map.put("Piece", "0");//片数
                                    map.put("ProductCodeDate", CommonUtil.getCurrentDate());
                                    map.put("RandomCode", "0");
                                    presenter.getData(ScanCodeNewActivity.this, map, mServerAddressIp, mServerAddressPort);
                                    iBossBasePopupWindowDelete.dismiss();
                                }
                            });
                        }
                    });
            iBossBasePopupWindowDelete.show(false, findViewById(R.id.content), 0, 0);

        }
        if (i == R.id.iv_left) {
            finish();
        }
        if (i == R.id.tv_right) {
            Intent intent = new Intent(ScanCodeNewActivity.this, CompleteCodeInformationNewActivity.class);
            startActivity(intent);
        }

    }


    /*
     * 条码控件设置为可编辑
     */
    public void setEdt() {
        et_commodity_bar_code.setText("");
        et_commodity_bar_code.setEnabled(true);
        et_commodity_bar_code.setFocusable(true);
        et_commodity_bar_code.setFocusableInTouchMode(true);
        et_commodity_bar_code.requestFocus();
        et_commodity_bar_code.findFocus();
        et_commodity_bar_code.setFilters(new InputFilter[]{new InputFilter() {
            @Override
            public CharSequence filter(CharSequence source,
                                       int start, int end, Spanned dest,
                                       int dstart, int dend) {

                return null;
            }
        }});


    }


    /**
     * 初始化数据
     */
    @Override
    protected void initData() {

    }


    /**
     * 成功消息
     *
     * @param result
     */
    @Override
    public void success(String result) {
        ProcessDialogUtils.closeProgressDilog();
        try {
            JSONObject obj = new JSONObject(result);
            String message = obj.optString("Message");
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        } catch (JSONException e) {
            e.printStackTrace();
        }


        et_commodity_bar_code.setText("");
        tv_trade_name.setText("");
        tv_commodity_code.setText("");
        tv_only_code.setText("");
        tv_specifications.setText("");
        tv_color_code.setText("");
        tv_grade.setText("");
        tv_warehouse.setText("");
        tv_position.setText("");
        btn_scan.setEnabled(true);


    }

    /**
     * 异常登录
     *
     * @param message
     */
    @Override
    public void getRelogin(String message) {
        ProcessDialogUtils.closeProgressDilog();
        AlertAnimateUtil.showReLoginDialog(
                ScanCodeNewActivity.this, "异常登录",
                message);
    }

    /**
     * 返回错误消息
     *
     * @param message
     */
    @Override
    public void showFailedError(String message) {
        ProcessDialogUtils.closeProgressDilog();
        btn_scan.setEnabled(true);
        CommonUtils.setVibrate(
                getApplicationContext());
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    /**
     * 有遗漏的条件导致没通过
     */
    @Override
    public void saveFailedError(String message) {
        ProcessDialogUtils.closeProgressDilog();
        CommonUtils.setVibrate(
                getApplicationContext());
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    /**
     * 数据校验
     *
     * @return
     */
    private boolean check() {

        if (StringUtils.isEmpty(tv_commodity_code.getText().toString()) ||StringUtils.isEmpty(tv_only_code.getText().toString())) {
            Toast.makeText(this, "请选择商品信息", Toast.LENGTH_SHORT).show();
            return false;
        }


        return true;
    }


    /**
     * 返回主页
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 101 && resultCode == 3) {
            mRetureList = new ArrayList<ChooseProductInformationInfo>();
            mRetureList = (ArrayList<ChooseProductInformationInfo>) data.getSerializableExtra("itemlist");
            tv_trade_name.setText(mRetureList.get(0).getGoodsName());
            tv_commodity_code.setText(mRetureList.get(0).getCode());
            tv_only_code.setText(mRetureList.get(0).getOnlyCode());
            tv_specifications.setText(mRetureList.get(0).getSpecification());
            tv_color_code.setText(mRetureList.get(0).getColorNumber());
            tv_grade.setText(mRetureList.get(0).getGradeName());
            tv_warehouse.setText(mRetureList.get(0).getWarehouseName());
            tv_position.setText(mRetureList.get(0).getPositionNumber());
            mCodeID = mRetureList.get(0).getCodeID();
            mCode = mRetureList.get(0).getCode();
            mOnlyCode = mRetureList.get(0).getOnlyCode();
            mGoodsName = mRetureList.get(0).getGoodsName();
            mSpecification = mRetureList.get(0).getSpecification();
            mColorNumber = mRetureList.get(0).getColorNumber();
            mGradeID = mRetureList.get(0).getGradeID();
            mWarehouseID = mRetureList.get(0).getWarehouseID();
            mPositionNumber = mRetureList.get(0).getPositionNumber();
            mInventoryID = mRetureList.get(0).getInventoryID();
        }


    }

}

