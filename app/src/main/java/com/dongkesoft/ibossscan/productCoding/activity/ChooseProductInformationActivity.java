/*******************************************************************************
 * Copyright(c) 2018 dongke All rights reserved. / Confidential
 * 类的信息：
 * 1.程序名称：ChooseProductInformationActivity
 * 2.功能描述：商品信息
 * 编辑履历：
 * 作者				日期					版本				修改内容
 * dongke			2018/6/8			1.00			新建
 *******************************************************************************/
package com.dongkesoft.ibossscan.productCoding.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.dongkesoft.ibossscan.productCoding.adapter.ChooseProductInformationAdapter;
import com.dongkesoft.ibossscan.productCoding.bean.ChooseProductInformationInfo;
import com.dongkesoft.ibossscan.productCoding.presenter.ChooseProductInformationPresenter;

import com.dongkesoft.ibossscan.productCoding.view.ChooseProductInformationView;
import com.dongkesoft.ibossscan.R;
import com.dongkesoft.ibossscan.utils.AlertAnimateUtil;
import com.dongkesoft.ibossscan.utils.CommonUtil;
import com.dongkesoft.ibossscan.utils.ProcessDialogUtils;

import com.dongkesoft.ibossscan.utils.XListViewNew;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import dongkesoft.com.dkmodule.ui.activity.BaseActivity;
import dongkesoft.com.dkmodule.utils.CommonUtils;
import dongkesoft.com.dkmodule.widget.GenericDrawerLayout;

public class ChooseProductInformationActivity extends BaseActivity<ChooseProductInformationView, ChooseProductInformationPresenter>
        implements ChooseProductInformationView, XListViewNew.IXListViewListener {

    public ChooseProductInformationAdapter mChooseProductInformationAdapter;
    /*
     * 标题
     * */
    @BindView(R.id.tv_center)
    TextView tvTitle;
    /*
     * 返回按钮
     * */
    @BindView(R.id.iv_left)
    ImageView ivLeft;
    /*
     * 搜索框
     * */
    @BindView(R.id.et_search)
    EditText edtSearch;
    /*
     * 搜索框
     * */
    @BindView(R.id.ll_no_data)
    LinearLayout ll_no_data;
    /*
     * list
     * */
    @BindView(R.id.select_list)
    XListViewNew selectList;
    /*
     * 跳转到已打码商品信息
     * */
    @BindView(R.id.tv_right)
    TextView tvRight;
    private View mDrawerLayoutView;
    public boolean mDrawerLayoutStatus;
    /**
     * 数据源
     */
    private List<ChooseProductInformationInfo> chooseProductList;

    private List<ChooseProductInformationInfo> chooseProductListNew;
    private List<ChooseProductInformationInfo> bundleList;
    private List<ChooseProductInformationInfo> mList;
    /**
     * 侧拉菜单——取消
     */
    private TextView tv_cancle;
    /**
     * 侧拉菜单——重置
     */
    private TextView tv_reset;
    /**
     * 侧拉菜单——确定
     */
    private TextView tv_sure;
    /**
     * 侧拉菜单——商品编码
     */
    private EditText et_code;
    /**
     * 侧拉菜单——唯一编码
     */
    private EditText et_only_code;
    /**
     * 侧拉菜单——商品名称
     */
    private EditText et_product_name;
    /**
     * 侧拉菜单——规格
     */
    private EditText et_specifications;
    /**
     * 侧拉菜单——色号
     */
    private EditText et_color_code;
    /**
     * 侧拉菜单——等级
     */
    private EditText et_grade;

    /**
     * 当前页数
     */
    private int pageNum = 1;
    private String mCode = "";
    private String mOnlyCode = "";
    private String mGoodsName = "";
    private String mSpecification = "";
    private String mColorNumber = "";
    private String mGradeName = "";
    private GenericDrawerLayout mDrawerLayout;

    private String currentDate; /**
     * 判断是否是上推加载数据
     */
    private boolean ispull = false;

    @Override
    public void showFailedError(String message) {
        ProcessDialogUtils.closeProgressDilog();
        CommonUtils.setVibrate(
                getApplicationContext());
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void saveFailedError(String message) {
        ProcessDialogUtils.closeProgressDilog();

    }

    @Override
    public void success(String result) {
        ProcessDialogUtils.closeProgressDilog();
        if(ispull){

        }else{
            chooseProductList.clear();
            chooseProductListNew.clear();
        }
        try {
            JSONObject obj = new JSONObject(result);
            JSONObject json = obj.getJSONObject("Result");
            JSONArray array = json.getJSONArray("Table");

            if (array != null && array.length() > 0) {
                for (int i = 0; i < array.length(); i++) {
                    JSONObject jsonObjectDetail = array
                            .getJSONObject(i);
                    ChooseProductInformationInfo info = new ChooseProductInformationInfo();
                    info.setCode(jsonObjectDetail.optString("Code"));
                    info.setGoodsName(jsonObjectDetail.optString("GoodsName"));
                    info.setCodeID(jsonObjectDetail.optString("CodeID"));
                    info.setOnlyCode(jsonObjectDetail.optString("OnlyCode"));
                    info.setSpecification(jsonObjectDetail.optString("Specification"));
                    info.setPACKAGE(jsonObjectDetail.optString("PACKAGE"));
                    info.setInventoryID(jsonObjectDetail.optString("InventoryID"));
                    info.setWarehouseID(jsonObjectDetail.optString("WarehouseID"));
                    info.setWarehouseName(jsonObjectDetail.optString("WarehouseName"));
                    info.setColorNumber(jsonObjectDetail.optString("ColorNumber"));
                    info.setPositionNumber(jsonObjectDetail.optString("PositionNumber"));
                    info.setGradeID(jsonObjectDetail.optString("GradeID"));
                    info.setGradeName(jsonObjectDetail.optString("GradeName"));
                    chooseProductList.add(info);
                }

                chooseProductListNew.addAll(chooseProductList);
                if (ispull) {
                    mChooseProductInformationAdapter.notifyDataSetChanged();

                } else {

                    selectList.setVisibility(View.VISIBLE);
                    mChooseProductInformationAdapter.setData(chooseProductListNew);
                    selectList.setAdapter(mChooseProductInformationAdapter);
                }
                selectList .setPullLoadEnable(true);
                onLoad();
                ispull =false;
                ll_no_data.setVisibility(View.GONE);


            } else {
                onLoad();
                selectList.setPullLoadEnable(false);
                if (chooseProductList != null && chooseProductList.size() == 0) {
                    ll_no_data.setVisibility(View.VISIBLE);
                    AlertAnimateUtil.alertShow(ChooseProductInformationActivity.this, "提示", "未找到匹配结果");
                    ProcessDialogUtils.closeProgressDilog();
                    return;
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void getRelogin(String message) {
        ProcessDialogUtils.closeProgressDilog();
        AlertAnimateUtil.showReLoginDialog(
                ChooseProductInformationActivity.this, "异常登录",
                message);
    }

    @Override
    protected ChooseProductInformationPresenter createPresenter() {
        return new ChooseProductInformationPresenter();
    }

    @Override
    protected int setMvpView() {
        return R.layout.activity_choose_qrcade;
    }

    @Override
    protected void initView() {
        chooseProductList = new ArrayList<ChooseProductInformationInfo>();
        chooseProductListNew=new ArrayList<ChooseProductInformationInfo>();
        mChooseProductInformationAdapter = new ChooseProductInformationAdapter(ChooseProductInformationActivity.this);
        tvTitle.setVisibility(View.VISIBLE);
        ivLeft.setVisibility(View.VISIBLE);
        tvRight.setVisibility(View.VISIBLE);
        tvRight.setText("筛查");
        tvTitle.setText("商品信息");
        mDrawerLayout = (GenericDrawerLayout) findViewById(R.id.drawerlayout);
        mDrawerLayoutView = View.inflate(ChooseProductInformationActivity.this, R.layout.drawerlayout_choose_information, null);
        //取消
        tv_cancle = (TextView) mDrawerLayoutView.findViewById(R.id.tv_cancle);
        //重置
        tv_reset = (TextView) mDrawerLayoutView.findViewById(R.id.tv_reset);
        //确定
        tv_sure = (TextView) mDrawerLayoutView.findViewById(R.id.tv_sure);
        //商品编码
        et_code = (EditText) mDrawerLayoutView.findViewById(R.id.et_code);
        //唯一编码
        et_only_code = (EditText) mDrawerLayoutView.findViewById(R.id.et_only_code);
        //商品名称
        et_product_name = (EditText) mDrawerLayoutView.findViewById(R.id.et_product_name);
        //规格
        et_specifications = (EditText) mDrawerLayoutView.findViewById(R.id.et_specifications);
        //色号
        et_color_code = (EditText) mDrawerLayoutView.findViewById(R.id.et_color_code);
        //等级
        et_grade = (EditText) mDrawerLayoutView.findViewById(R.id.et_grade);


        tv_cancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                mDrawerLayout.switchStatus();
            }
        });
        tv_reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                et_code.setText("");
                et_only_code.setText("");
                et_product_name.setText("");
                et_specifications.setText("");
                et_color_code.setText("");
                et_grade.setText("");
            }
        });
        tv_sure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                pageNum = 1;
                mCode = et_code.getText().toString();
                mOnlyCode = et_only_code.getText().toString();
                mGoodsName = et_product_name.getText().toString();
                mSpecification = et_specifications.getText().toString();
                mColorNumber = et_color_code.getText().toString();
                mGradeName = et_grade.getText().toString();
                if(chooseProductList!=null&&chooseProductList.size()>0)
                {
                    chooseProductList.clear();

                    if(chooseProductListNew!=null&&chooseProductListNew.size()>0)
                    {
                        chooseProductListNew.clear();
                    }
                    if(mChooseProductInformationAdapter!=null)
                    {
                        mChooseProductInformationAdapter.notifyDataSetChanged();
                    }
                }
                getData();
                mDrawerLayout.switchStatus();
            }
        });
        edtSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                mList = new ArrayList<ChooseProductInformationInfo>();
                chooseProductListNew.clear();

                for (int i = 0; i < chooseProductList.size(); i++) {
                    if ((chooseProductList.get(i)
                            .getGoodsName().toUpperCase()
                            .indexOf(edtSearch.getText().toString().toUpperCase()) >= 0) || (chooseProductList.get(i)
                            .getOnlyCode().toUpperCase()
                            .indexOf(edtSearch.getText().toString().toUpperCase()) >= 0)) {
                        mList.add(chooseProductList.get(i));
                    }
                }
                chooseProductListNew.addAll(mList);


                mChooseProductInformationAdapter.setData(chooseProductListNew);
                selectList.setAdapter(mChooseProductInformationAdapter);
            }

        });

        selectList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                if (position > chooseProductListNew.size()) {
                    return;
                }
                bundleList = new ArrayList<ChooseProductInformationInfo>();
                bundleList.add(chooseProductListNew.get(position - 1));
                Intent intent = new Intent();
                Bundle bundle = new Bundle();
                intent.putExtra("itemlist", (Serializable) bundleList);
                intent.putExtras(bundle);
                setResult(3, intent);
                finish();

            }
        });


    }

    @Override
    protected void initData() {
        initDrawerLayout();
        pageNum = 1;
        getData();
    }

    @Override
    public void onRefresh() {
        pageNum = 1;
        selectList.setRefreshTime(CommonUtil.getCurrentDateTime());
        if (chooseProductList != null && chooseProductList.size() > 0) {
            chooseProductList.clear();
            if (mChooseProductInformationAdapter != null) {
                mChooseProductInformationAdapter.notifyDataSetChanged();
            }
        }
        getData();
        selectList.stopRefresh();
    }

    /**
     * 上拉加载
     */
    @Override
    public void onLoadMore() {
        pageNum++;
        ispull =true;
        getData();
        selectList.stopLoadMore();
    }

    /**
     * 得到数据
     */
    public void getData() {
        ProcessDialogUtils.showProcessDialog(this);
        // 传递参数
        HashMap<String, String> map = new HashMap<>();
        map.put("Action", "GetGoodsCodeForMobile");
        map.put("AccountCode", mAccountCode);
        map.put("UserCode", mUserCode);
        map.put("UserPassword", mPassword);
        map.put("SessionKey", mSessionKey);
        // map.put("LicenseCode", mLicenseCode);
        map.put("Code", mCode);
        map.put("OnlyCode", mOnlyCode);
        map.put("GoodsName", mGoodsName);
        map.put("Specification", mSpecification);
        map.put("ColorNumber", mColorNumber);
        map.put("GradeName", mGradeName);
        map.put("PageSize", "20");
        map.put("PageNum", pageNum + "");

        presenter.getData(this, map, mServerAddressIp, mServerAddressPort);
    }

    /**
     * 点击事件
     *
     * @param view
     */
    @OnClick({R.id.iv_left, R.id.tv_right})
    public void OnClick(View view) {
        int i = view.getId();
        if (i == R.id.iv_left) {
            finish();
        }
        if (i == R.id.tv_right) {
            mDrawerLayout.setOpennable(true);
            mDrawerLayout.switchStatus();
            mDrawerLayout.requestDisallowInterceptTouchEvent(true);
        }
    }

    /**
     * 初始化DrawerLayout
     */
    private void initDrawerLayout() {
        // 可以设置打开时响应Touch的区域范围
        mDrawerLayout.setContentLayout(mDrawerLayoutView);
        mDrawerLayout.setTouchSizeOfOpened(dip2px(ChooseProductInformationActivity.this, 500));
        mDrawerLayout.setTouchSizeOfClosed(dip2px(ChooseProductInformationActivity.this, 0));
        // 设置随着位置的变更，背景透明度也改变
        mDrawerLayout.setOpaqueWhenTranslating(true);
        // 设置抽屉是否可以打开
        mDrawerLayout.setOpennable(false);
        // 设置抽屉的空白区域大小
        float v = getResources().getDisplayMetrics().density * 50 + 0.5f; // 100DIP
        mDrawerLayout.setDrawerEmptySize((int) v);

        // 设置事件回调
        mDrawerLayout.setDrawerCallback(new GenericDrawerLayout.DrawerCallback() {

            @Override
            public void onTranslating(int gravity, float translation, float fraction) {
            }

            @Override
            public void onStartOpen() {
            }

            @Override
            public void onStartClose() {
            }

            @Override
            public void onPreOpen() {
            }

            @Override
            public void onEndOpen() {
                mDrawerLayoutStatus = true;
            }

            @Override
            public void onEndClose() {
                mDrawerLayoutStatus = false;
            }
        });
    }

    private int dip2px(Context context, float dipValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dipValue * scale + 0.5f);
    }

    private void onLoad() {
        selectList.stopRefresh();
        selectList.stopLoadMore();
        currentDate = CommonUtil.getCurrentDateTime();
        selectList.setRefreshTime(currentDate);
    }

}
