package com.dongkesoft.ibossscan.utils;

/*******************************************************
 * Copyright(c) 2016 DongkeSoft All rights reserved. / Confidential
 *
 * @Title : IBossBasePopupWindow.java
 * @Package : com.dongkesoft.iboss.activity.common
 * @Description : PopupWindow基类
 * @Author : jiangyh
 * @Date : 2016年8月23日 下午12:04:12
 * @Version : 1.00
 ********************************************************/

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.PopupWindow;

/**
 * @Description : PopupWindow基类
 * @ClassName : IBossBasePopupWindow
 * @Author : jiangyh
 * @Date : 2016年8月23日 下午4:13:55
 */
public class UpgradePopUpWindow extends PopupWindow {
    private LayoutInflater inflater;
    private View contentView;// myPopupWindows所在的布局
    private final int h;// 显示的高度
    private final int w;// 显示的宽度
    private IPopUpWindowCallBack iPopUpWindowCallBack;// 用来初始化mypopupwindows布局中的控件和添加事件监听

    @SuppressLint("WrongConstant")
    @SuppressWarnings({ "deprecation" })
    public UpgradePopUpWindow(Activity context, int layout) {
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);// 获取inflater
        contentView = inflater.inflate(layout, null);// 加载布局文件并保存在View中
        h = context.getWindowManager().getDefaultDisplay().getHeight();// 获取屏幕的高度并赋值给h
        w = context.getWindowManager().getDefaultDisplay().getWidth();// 获取屏幕的宽度并赋值给w
        this.setContentView(contentView);// 添加到popupwindows中
        this.setFocusable(true);// 设置可获取焦点
        this.setOutsideTouchable(true);// 设置外部可触摸
        this.update();
        this.setBackgroundDrawable(new BitmapDrawable(context.getResources(),
                (Bitmap) null));// 必须设置背景否则报错
        this.setSoftInputMode(PopupWindow.INPUT_METHOD_NEEDED);
        this.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        /*
         * 触摸拦截
         */
        this.setTouchInterceptor(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return false;
            }
        });

    }

    /*
     * isShowAsDropDown ：true显示在parent控件的下面 false:显示在屏幕中间 parent：activity布局中的控件
     * width：显示的宽度 height；显示的高度
     */
    public void show(boolean isShowAsDropDown, View parent, int width,
                     int height) {
        /*
         * 判断接口是否为空，不为空则和绑定事件监听
         */
        if (iPopUpWindowCallBack != null) {
            iPopUpWindowCallBack.popUpWindowCallBack(contentView);
        }
        /*
         * 如果形参width和height小于或等于0则用获取到的屏幕宽高
         */
        if (width <= 0 || height <= 0) {
            this.setWidth(w);
            this.setHeight(h);
        } else {
            this.setWidth(width);
            this.setHeight(height);
        }
        /*
         * isShowAsDropDown ：true显示在parent控件的下面 false:显示在屏幕中间
         */
        if (isShowAsDropDown) {
            this.showAsDropDown(parent,0,h/4-parent.getHeight());
        } else {
            this.showAtLocation(parent, Gravity.CENTER, 0,0);
        }

    }

    public void setPopUpWindowCallBack(IPopUpWindowCallBack iPopUpWindowCallBack) {
        this.iPopUpWindowCallBack = iPopUpWindowCallBack;
    }

    public interface IPopUpWindowCallBack {
        public void popUpWindowCallBack(View view);
    }

}
