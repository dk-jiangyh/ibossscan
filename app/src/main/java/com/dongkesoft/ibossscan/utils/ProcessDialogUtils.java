package com.dongkesoft.ibossscan.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnKeyListener;
import android.view.KeyEvent;

import com.dongkesoft.ibossscan.R;


/*
 * Copyright(c) 2016 dongke All rights reserved. / Confidential
 * 类的信息：
 *		1.程序名称： PicUtil
 *		2.功能描述：网络加载dialog
 * 编辑履历：
 *		作者				日期					版本				修改内容
 *		dongke			2016/12/07			1.00			新建
 *******************************************************************************/
public class ProcessDialogUtils {
	/**
	 * Dialog
	 */
	public static AlertDialog mDialog;

	/**
	 * 显示dialog
	 * 
	 * @param context
	 */
	public static void showProcessDialog(Context context) {

		try {
			if (mDialog != null) {
				mDialog.cancel();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		/**
		 * 键盘点击事件
		 */
		OnKeyListener keyListener = new OnKeyListener() {
			@Override
			public boolean onKey(DialogInterface dialog, int keyCode,
					KeyEvent event) {
				if (keyCode == KeyEvent.KEYCODE_HOME
						|| keyCode == KeyEvent.KEYCODE_SEARCH) {
					return true;
				}
				return false;
			}
		};
		try {
			mDialog = new AlertDialog.Builder(context, R.style.dialog_progress).create();
			mDialog.setCancelable(false); // 点击返回键 不关掉dialog
			mDialog.setOnKeyListener(keyListener);
			mDialog.setCanceledOnTouchOutside(false);
			mDialog.show();
			mDialog.setContentView(R.layout.round_process_dialog);

		} catch (Exception e) {
		}

	}




	public static void closeProgressDilog() {
		if (mDialog != null) {
			try {
				mDialog.dismiss();
				mDialog = null;
			} catch (Exception e) {
			}

		}
	}

}
