package com.dongkesoft.ibossscan.utils;

public interface OnItemSelectedListener {
	void onItemSelected(int index);
}
