/**
 * 
 */
package com.dongkesoft.ibossscan.utils;

/**
 * @author Administrator
 * @since 2017年1月12日
 */
public interface OnItemClickListener {
	public void onItemClick(Object o, int position);
}
