package com.dongkesoft.ibossscan.utils;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.dongkesoft.ibossscan.R;

/**
 * 时间选择器
 * 
 * @author Administrator
 *
 */
public class TimePickerInfo extends BasePickerInfo implements
		View.OnClickListener {
	public enum Type {
		ALL, YEAR_MONTH_DAY, HOURS_MINS, MONTH_DAY_HOUR_MIN, YEAR_MONTH
	}

	private WheelTimeInfo wheelTime;
	private View btnSubmit, btnCancel;
	private TextView tvTitle;
	private static final String TAG_SUBMIT = "submit";
	private static final String TAG_CANCEL = "cancel";

	/**
	 * 滚动监听
	 */
	private OnTimeSelectListener timeSelectListener;

	public TimePickerInfo(Context context, Type type) {
		super(context);

		LayoutInflater.from(context).inflate(R.layout.pickerview_time,
				contentContainer);

		btnSubmit = findViewById(R.id.btnSubmit);
		btnSubmit.setTag(TAG_SUBMIT);
		btnCancel = findViewById(R.id.btnCancel);
		btnCancel.setTag(TAG_CANCEL);
		btnSubmit.setOnClickListener(this);
		btnCancel.setOnClickListener(this);
		tvTitle = (TextView) findViewById(R.id.tvTitle);
		final View timepickerview = findViewById(R.id.timepicker);
		wheelTime = new WheelTimeInfo(timepickerview, type);

		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(System.currentTimeMillis());
		int year = calendar.get(Calendar.YEAR);
		int month = calendar.get(Calendar.MONTH);
		int day = calendar.get(Calendar.DAY_OF_MONTH);
		int hours = calendar.get(Calendar.HOUR_OF_DAY);
		int minute = calendar.get(Calendar.MINUTE);
		wheelTime.setPicker(year, month, day, hours, minute);

	}

	/**
	 * 设置时间范围
	 * 
	 * @param startYear
	 * @param endYear
	 */
	public void setRange(int startYear, int endYear) {
		wheelTime.setStartYear(startYear);
		wheelTime.setEndYear(endYear);
	}

	/**
	 * 设置当前时间
	 * 
	 * @param date
	 */
	public void setTime(Date date) {
		Calendar calendar = Calendar.getInstance();
		if (date == null)
			calendar.setTimeInMillis(System.currentTimeMillis());
		else
			calendar.setTime(date);
		int year = calendar.get(Calendar.YEAR);
		int month = calendar.get(Calendar.MONTH);
		int day = calendar.get(Calendar.DAY_OF_MONTH);
		int hours = calendar.get(Calendar.HOUR_OF_DAY);
		int minute = calendar.get(Calendar.MINUTE);
		wheelTime.setPicker(year, month, day, hours, minute);
	}

	/**
	 * 定位到指定日期
	 */

	@Override
	public void show(Date date) {

		setTime(date);

		super.show(date);
	}

	public void setCyclic(boolean cyclic) {
		wheelTime.setCyclic(cyclic);
	}

	@Override
	public void onClick(View v) {
		String tag = (String) v.getTag();
		if (tag.equals(TAG_CANCEL)) {
			dismiss();
			return;
		} else {
			if (timeSelectListener != null) {
				try {
					Date date = WheelTimeInfo.dateFormat.parse(wheelTime
							.getTime());
					timeSelectListener.onTimeSelect(date);
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
			dismiss();
			return;
		}
	}

	public interface OnTimeSelectListener {
		public void onTimeSelect(Date date);
	}

	public void setOnTimeSelectListener(OnTimeSelectListener timeSelectListener) {
		this.timeSelectListener = timeSelectListener;
	}

	public void setTitle(String title) {
		tvTitle.setText(title);
	}
}
