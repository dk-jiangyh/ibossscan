package com.dongkesoft.ibossscan.utils;

public interface OnDismissListener {
	public void onDismiss(Object o);
}
