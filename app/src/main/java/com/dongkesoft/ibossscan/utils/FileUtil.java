package com.dongkesoft.ibossscan.utils;

/**
 * Created by guanhonghou on 2018/9/26.
 */

import android.graphics.BitmapFactory;
import android.os.Environment;

import java.io.File;

import java.io.FileInputStream;
import java.io.FileOutputStream;
        import java.io.IOException;
        import java.util.UUID;

        import android.content.Context;
        import android.graphics.Bitmap;
        import android.graphics.Bitmap.Config;
        import android.graphics.Canvas;
        import android.graphics.Paint;
        import android.graphics.PorterDuff.Mode;
        import android.graphics.PorterDuffXfermode;
        import android.graphics.Rect;
        import android.graphics.RectF;
import android.util.Log;

/*
 * Copyright(c) 2016 dongke All rights reserved. / Confidential
 * 类的信息：
 *		1.程序名称： FileUtil
 *		2.功能描述：文件工具类
 * 编辑履历：
 *		作者				日期					版本				修改内容
 *		dongke			2016/12/07			1.00			新建
 *******************************************************************************/
public class FileUtil {
    public static final String RECORD_SAVE_PATH = "/ibossscan/record";
    public static final String TAKE_CAHCHE = "/ibossscan/cache";
    public static final String DATA_CRASH_PATH = "/ibossscan/crash/";
    private static final String TAG = "FileUtil";

    public static String getRecordFilePath(String filename) {
        File f = new File(filename);
        if (f.exists()) {
            return filename;
        }

        return Environment.getExternalStorageDirectory()
                + FileUtil.RECORD_SAVE_PATH + "/" + filename;
    }

    public static String getTimeFromInt(int time) {

        if (time <= 0) {
            return "0′0″";
        }
        int secondnd = (time / 1000) / 60;

        String f = String.valueOf(secondnd);

        if (secondnd < 1) {
            int million = (time / 1000) % 60;
            String m = String.valueOf(million);
            return m + "″";
        } else {
            int million = (time / 1000) % 60;
            String m = String.valueOf(million);
            if (million == 0) {
                return f + "′";
            } else {
                return f + "′" + m + "″";
            }

        }
    }

    public static File getCacheFile(String imageUri) {
        File cacheFile = null;
        try {
            if (Environment.getExternalStorageState().equals(
                    Environment.MEDIA_MOUNTED)) {
                File sdCardDir = Environment.getExternalStorageDirectory();
                String fileName = getFileName(imageUri);
                File dir = new File(sdCardDir.getCanonicalPath() + "yyuts");
                if (!dir.exists()) {
                    dir.mkdirs();
                }
                cacheFile = new File(dir, fileName);
                Log.i(TAG, "exists:" + cacheFile.exists() + ",dir:" + dir
                        + ",file:" + fileName);
            }
        } catch (IOException e) {
            e.printStackTrace();
            Log.e(TAG, "getCacheFileError:" + e.getMessage());
        }

        return cacheFile;
    }

    public static String getFileName(String path) {
        int index = path.lastIndexOf("/");
        return path.substring(index + 1);
    }

    /**
     * @param folderPath
     *            删除文件夹
     */
    public static void delFolder(String folderPath) {
        try {
            delAllFile(folderPath); // 删除完里面所有内容
            String filePath = folderPath;
            filePath = filePath.toString();
            File myFilePath = new File(filePath);
            myFilePath.delete(); // 删除空文件夹
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    /**
     * 根据bitmap创建文件
     * @param context
     * @param mSignBitmap
     * @return
     */
    public static String createFile(Context context, Bitmap mSignBitmap) {
        // ByteArrayOutputStream baos = null;
        String _path = null;
        FileOutputStream out;
        try {
            String pPath = getRootFilePath();
            String fileDir = pPath + TAKE_CAHCHE;
            File file = new File(fileDir);
            if (!file.exists()) {
                file.mkdir();
            }
            String filep = getRandomFilename(context) + ".jpg";
            File f = new File(file.getAbsolutePath(), filep);
            if (!f.exists()) {
                try {
                    f.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            _path = f.getAbsolutePath();

            out = new FileOutputStream(f);
            mSignBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);

            out.flush();
            out.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return _path;
    }

    public static boolean hasSDCard() {
        String status = Environment.getExternalStorageState();
        if (!status.equals(Environment.MEDIA_MOUNTED)) {
            return false;
        }
        return true;
    }

    public static String getRootFilePath() {
        if (hasSDCard()) {
            return Environment.getExternalStorageDirectory().getAbsolutePath()
                    + "";
        } else {
            return Environment.getDataDirectory().getAbsolutePath() + "/data";
        }
    }

    public static String getRandomFilename(Context context) {

        return UUID.randomUUID().toString();

    }

    /**
     * @param path
     * @return 删除文件夹下内容
     */
    public static boolean delAllFile(String path) {
        boolean flag = false;
        File file = new File(path);
        if (!file.exists()) {
            return flag;
        }
        if (!file.isDirectory()) {
            return flag;
        }
        String[] tempList = file.list();
        File temp = null;
        for (int i = 0; i < tempList.length; i++) {
            if (path.endsWith(File.separator)) {
                temp = new File(path + tempList[i]);
            } else {
                temp = new File(path + File.separator + tempList[i]);
            }
            if (temp.isFile()) {
                temp.delete();
            }
            if (temp.isDirectory()) {
                delAllFile(path + "/" + tempList[i]);// 先删除文件夹里面的文件
                delFolder(path + "/" + tempList[i]);// 再删除空文件夹
                flag = true;
            }
        }
        return flag;
    }


    public static Bitmap decodeFile(String filePath) throws IOException{
        Bitmap b = null;
        int IMAGE_MAX_SIZE = 600;

        File f = new File(filePath);
        if (f == null){
            return null;
        }
        //Decode image size
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;

        FileInputStream fis = new FileInputStream(f);
        BitmapFactory.decodeStream(fis, null, o);
        fis.close();

        int scale = 1;
        if (o.outHeight > IMAGE_MAX_SIZE || o.outWidth > IMAGE_MAX_SIZE) {
            scale = (int) Math.pow(2, (int) Math.round(Math.log(IMAGE_MAX_SIZE / (double) Math.max(o.outHeight, o.outWidth)) / Math.log(0.5)));
        }

        //Decode with inSampleSize
        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        fis = new FileInputStream(f);
        b = BitmapFactory.decodeStream(fis, null, o2);
        fis.close();
        return b;
    }


    /**
     * 创建文件夹
     * @param path
     */
    public static void createFolder(String path) {
        File file = new File(path);
        if(!file.exists()) {
            file.mkdir();
        }
    }

    /**
     * 将图片截取为圆角图片
     *
     * @param bitmap
     *            原图片
     * @param ratio
     *            截取比例，如果是8，则圆角半径是宽高的1/8，如果是2，则是圆形图片
     * @return 圆角矩形图片
     */
    public static Bitmap getRoundedCornerBitmap(Bitmap bitmap, float ratio) {

        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getHeight(), Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        canvas.drawRoundRect(rectF, bitmap.getWidth() / ratio,
                bitmap.getHeight() / ratio, paint);

        paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);
        return output;
    }
    /**
     * 获取文件夹大小
     * @param file File实例
     * @return long
     */
    public static long getFolderSize(File file){

        long size = 0;
        try {
            File[] fileList = file.listFiles();
            for (int i = 0; i < fileList.length; i++)
            {
                if (fileList[i].isDirectory())
                {
                    size = size + getFolderSize(fileList[i]);

                }else{
                    size = size + fileList[i].length();

                }
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return size;

    }
}
