package com.dongkesoft.ibossscan.utils;

import java.util.concurrent.atomic.AtomicBoolean;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.dongkesoft.ibossscan.animation.Animator;
import com.dongkesoft.ibossscan.animation.AnimatorListenerAdapter;
import com.dongkesoft.ibossscan.animation.ObjectAnimator;
import com.dongkesoft.ibossscan.animation.ValueAnimator;

import dongkesoft.com.dkmodule.widget.ViewHelper;


@SuppressLint("NewApi")
public class GenericDrawerLayout extends FrameLayout {

	@SuppressWarnings("unused")
	private static final String TAG = GenericDrawerLayout.class.getSimpleName();

	private enum AnimStatus {
		OPENING, CLOSING, CLOSED, OPENED
	}

	/**
	 * 打开或者关闭抽屉时的DURATION
	 * */
	private static final int DURATION_OPEN_CLOSE = 300;
	/**
	 * 默认的响应触摸事件的宽度值，单位DIP
	 */
	private static final int TOUCH_VIEW_SIZE_DIP = 25;
	/**
	 * 打开抽屉时，默认响应关闭事件的宽度
	 */
	private static final int TOUCH_VIEW_SIZE_DIP_OPENED = ViewGroup.LayoutParams.MATCH_PARENT;
	/**
	 * 用来判断的最小消费事件触发距离，单位DIP
	 */
	private static final int MIN_CONSUME_SIZE_DIP = 15;
	/**
	 * 响应打开或者关闭的临界值与内容区域的宽度比例
	 */
	private static final float SCALE_AUTO_OPEN_CLOSE = 0.3f;
	/**
	 * 响应打开或者关闭的速率
	 */
	private static final int VEL = 800;

	private VelocityTracker mVelocityTracker;

	private Context mContext;

	/**
	 * 用来响应触摸事件的透明控件
	 */
	private TouchView mTouchView;
	/**
	 * 关闭状态下，响应触摸事件的控件宽度
	 */
	private int mClosedTouchViewSize;
	/**
	 * 打开状态下，响应Touch事件的宽度
	 */
	private int mOpenedTouchViewSize;
	/**
	 * 抽屉的Gravity
	 */
	private int mTouchViewGravity = Gravity.RIGHT;
	/**
	 * 用来防止内容部分视图的容器
	 */
	private ContentLayout mContentLayout;
	/**
	 * 事件回调
	 */
	private DrawerCallback mDrawerCallback;
	/**
	 * 当前正在播放的动画
	 */
	private ValueAnimator mAnimator;
	/**
	 * 是否正在播放动画
	 */
	private AtomicBoolean mAnimating = new AtomicBoolean(false);
	/**
	 * 当前的动画状态
	 */
	private AnimStatus mAnimStatus = AnimStatus.CLOSING;
	/**
	 * 当前触摸的位置相对屏幕左上角的X,Y轴值
	 */
	private float mCurTouchX, mCurTouchY;
	/**
	 * 用来判断是否消费Touch事件的最小滑动距离
	 */
	private float mMinDisallowDispatch;
	/**
	 * 是否被子View消费Touch事件
	 */
	private boolean isChildConsumeTouchEvent = false;
	/**
	 * 是否消费Touch事件
	 */
	private boolean isConsumeTouchEvent = false;
	/**
	 * 是否在滑动时改变背景透明度
	 */
	private boolean mIsOpaqueWhenTranslating = false;
	/**
	 * 是否可以打开抽屉
	 */
	private boolean mIsOpenable = true;
	/**
	 * 绘制背景透明度的View控件
	 */
	private DrawView mDrawView;
	/**
	 * 最大的不透明度
	 */
	private float mMaxOpaque = 1.0f;
	/**
	 * 抽屉空白区域的大小
	 */
	private int mDrawerEmptySize;
	/**
	 * 用来表示抽屉是否被打开过
	 */
	private boolean mIsDrawerOpenned = false;

	/**
	 * 抽屉在关闭的时候露出的宽度
	 */
	private int mRevealSize;

	public GenericDrawerLayout(Context context) {
		this(context, null);
	}

	public GenericDrawerLayout(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public GenericDrawerLayout(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		this.mContext = context;
		initView();
	}

	private void initView() {
		// 初始化背景色变化控件
		mDrawView = new DrawView(mContext);
		addView(mDrawView, generateDefaultLayoutParams());
		// 初始化用来相应触摸的透明View
		mTouchView = new TouchView(mContext);
		mClosedTouchViewSize = dip2px(mContext, TOUCH_VIEW_SIZE_DIP);
		mOpenedTouchViewSize = TOUCH_VIEW_SIZE_DIP_OPENED;
		// 初始化用来存放布局的容器
		mContentLayout = new ContentLayout(mContext);
		mContentLayout.setVisibility(View.INVISIBLE);
		// 添加视图
		addView(mTouchView, generateTouchViewLayoutParams());
		addView(mContentLayout, new LayoutParams(
				ViewGroup.LayoutParams.WRAP_CONTENT,
				ViewGroup.LayoutParams.WRAP_CONTENT));
		// 用来判断事件下发的临界距离
		mMinDisallowDispatch = dip2px(mContext, MIN_CONSUME_SIZE_DIP);
		mTouchView.setLayoutParams(generateTouchViewLayoutParams());
		mContentLayout.requestLayout();
	}

	public static int dip2px(Context context, float dipValue) {
		final float scale = context.getResources().getDisplayMetrics().density;
		return (int) (dipValue * scale + 0.5f);
	}

	private class DrawView extends View {

		Paint paint = new Paint();

		public DrawView(Context context) {
			super(context);
			paint.setColor(Color.BLACK);
			paint.setAlpha(0);
		}

		/**
		 * 设置透明度（0-1）
		 */
		public void setAlpha(float alpha) {
			paint.setAlpha((int) (alpha * 255));
			invalidate();
		}

		@Override
		protected void onDraw(Canvas canvas) {
			super.onDraw(canvas);
			canvas.drawPaint(paint);
		}

	}

	/**
	 * 设置抽屉在关闭的时候露出的部分大小
	 * 
	 * @param revealSize
	 */
	public void setDrawerRevealSize(int revealSize) {
		if (revealSize < 0) {
			return;
		}
		this.mRevealSize = revealSize;
	}

	/**
	 * 设置抽屉打开后的空白区域大小
	 * 
	 * @param emptySize
	 */
	public void setDrawerEmptySize(int emptySize) {
		if (emptySize < 0) {
			emptySize = 0;
		}
		this.mDrawerEmptySize = emptySize;
	}

	/**
	 * 抽屉容器
	 */
	private class ContentLayout extends FrameLayout {

		private float mDownX, mDownY;
		private boolean isTouchDown;

		public ContentLayout(Context context) {
			super(context);
		}

		@Override
		protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

			setMeasuredDimension(MeasureSpec.getSize(widthMeasureSpec),
					MeasureSpec.getSize(heightMeasureSpec));
			checkChildCount();

			int size;
			int childMeasureWidth;
			int childMeasureHeight;
			if (isHorizontalGravity()) {
				size = MeasureSpec.getSize(widthMeasureSpec);
				size -= mDrawerEmptySize;
				childMeasureWidth = size;
				childMeasureHeight = MeasureSpec.getSize(heightMeasureSpec);
			} else {
				size = MeasureSpec.getSize(heightMeasureSpec);
				size -= mDrawerEmptySize;
				childMeasureHeight = size;
				childMeasureWidth = MeasureSpec.getSize(widthMeasureSpec);
			}
			View child = getChildAt(0);
			child.measure(
					MeasureSpec.makeMeasureSpec(childMeasureWidth,
							MeasureSpec.getMode(widthMeasureSpec)),
					MeasureSpec.makeMeasureSpec(childMeasureHeight,
							MeasureSpec.getMode(heightMeasureSpec)));
		}

		@Override
		protected void onLayout(boolean changed, int left, int top, int right,
				int bottom) {
			super.onLayout(changed, left, top, right, bottom);
			checkChildCount();
			View child = getChildAt(0);

			child.layout(left + mDrawerEmptySize, top, right, bottom);

		}

		/**
		 * 检测自身的子View是否是只有一个
		 */
		private void checkChildCount() {
			if (getChildCount() > 1) {
				throw new RuntimeException("content child views must be one");
			}
		}

		@Override
		public void requestDisallowInterceptTouchEvent(boolean disallowIntercept) {
			super.requestDisallowInterceptTouchEvent(disallowIntercept);
		}

		@Override
		public boolean dispatchTouchEvent(MotionEvent event) {
			if (getVisibility() != View.VISIBLE) {
				// 抽屉不可见
				return super.dispatchTouchEvent(event);
			}

			// TOUCH_DOWN的时候未消化事件
			if (MotionEvent.ACTION_DOWN != event.getAction() && !isTouchDown) {
				isChildConsumeTouchEvent = true;
			}

			// ����Լ���û����¼���������view�Ƿ���Ҫ����¼�
			boolean goToConsumeTouchEvent = false;

			// 如果自己还没消化掉事件，看看子view是否需要消费事件
			switch (event.getAction()) {
			case MotionEvent.ACTION_DOWN:
				if (mAnimating.get()) {
					mAnimating.set(false);
					// ֹͣ���Ŷ���
					mAnimator.end();
					isTouchDown = true;
					isConsumeTouchEvent = true;
					goToConsumeTouchEvent = true;

					MotionEvent obtain = MotionEvent.obtain(event);
					obtain.setAction(MotionEvent.ACTION_CANCEL);
					super.dispatchTouchEvent(obtain);
				} else {

					isTouchDown = isDownInRespondArea(event);
				}
				if (isTouchDown) {
					mDownX = event.getRawX();
					mDownY = event.getRawY();
					performDispatchTouchEvent(event);
				} else {

					isChildConsumeTouchEvent = true;
				}
				if (!goToConsumeTouchEvent) {

					super.dispatchTouchEvent(event);
				}
				// �����¼�
				return true;
			case MotionEvent.ACTION_MOVE:
				if (!isConsumeTouchEvent && !isChildConsumeTouchEvent) {

					boolean b = super.dispatchTouchEvent(event);

					if ((Math.abs(event.getRawY() - mDownY) >= mMinDisallowDispatch)
							&& b) {

						isChildConsumeTouchEvent = true;
					} else if (event.getRawX() - mDownX > mMinDisallowDispatch) {

						isConsumeTouchEvent = true;
						goToConsumeTouchEvent = true;
					}

					if (goToConsumeTouchEvent) {

						MotionEvent obtain = MotionEvent.obtain(event);
						obtain.setAction(MotionEvent.ACTION_CANCEL);
						super.dispatchTouchEvent(obtain);
					}
				}
				break;
			}

			if (isChildConsumeTouchEvent || !isConsumeTouchEvent) {

				super.dispatchTouchEvent(event);
			} else if (isConsumeTouchEvent && !isChildConsumeTouchEvent) {

				performDispatchTouchEvent(event);
			}

			switch (event.getAction()) {
			case MotionEvent.ACTION_UP:
			case MotionEvent.ACTION_CANCEL:
				if (!isConsumeTouchEvent && !isChildConsumeTouchEvent) {

					performDispatchTouchEvent(event);
				}
				isConsumeTouchEvent = false;
				isChildConsumeTouchEvent = false;
				isTouchDown = false;
				break;
			}

			return true;
		}

	}

	/**
	 * 是否点击在响应区域
	 */
	private boolean isDownInRespondArea(MotionEvent event) {
		float curTranslation = getCurTranslation();
		int touchSize;
		touchSize = mOpenedTouchViewSize == ViewGroup.LayoutParams.MATCH_PARENT ? mContentLayout
				.getWidth() : mOpenedTouchViewSize;
		float x = event.getX();
	
		if (x > curTranslation && x < curTranslation + touchSize) {
			return true;
		}

		return false;
	}

	/**
	 * 设置关闭状态下，响应触摸事件的控件宽度
	 */
	public void setTouchSizeOfClosed(int width) {
		if (width == 0 || width < 0) {
			mClosedTouchViewSize = dip2px(mContext, TOUCH_VIEW_SIZE_DIP);
		} else {
			mClosedTouchViewSize = width;
		}
		ViewGroup.LayoutParams lp = mTouchView.getLayoutParams();
		if (lp != null) {
			if (isHorizontalGravity()) {
				lp.width = mClosedTouchViewSize;
				lp.height = ViewGroup.LayoutParams.MATCH_PARENT;
			} else {
				lp.height = mClosedTouchViewSize;
				lp.width = ViewGroup.LayoutParams.MATCH_PARENT;
			}
			mTouchView.requestLayout();
		}
	}

	/**
	 * 设置打开状态下，响应触摸事件的控件宽度
	 */
	public void setTouchSizeOfOpened(int width) {
		if (width <= 0) {
			mOpenedTouchViewSize = TOUCH_VIEW_SIZE_DIP_OPENED;
		} else {
			mOpenedTouchViewSize = width;
		}
	}

	/**
	 * 用来响应拖拽事件的透明View
	 */
	private class TouchView extends View {
		// private boolean isChildConsume;

		public TouchView(Context context) {
			super(context);
		}

		@Override
		public boolean dispatchTouchEvent(MotionEvent event) {
			if (!mIsOpenable) {
				// 如果禁用了抽屉
				return super.dispatchTouchEvent(event);
			}
			switch (event.getAction()) {
			case MotionEvent.ACTION_DOWN:
				if (getVisibility() == View.INVISIBLE) {

					return super.dispatchTouchEvent(event);
				}

				mContentLayout.setVisibility(View.VISIBLE);

				adjustContentLayout();
				if (mDrawerCallback != null) {

					mDrawerCallback.onPreOpen();
				}

				setVisibility(View.INVISIBLE);

				break;
			default:

				break;
			}

			performDispatchTouchEvent(event);
			return true;
		}
	}

	private void performDispatchTouchEvent(MotionEvent event) {
		if (mVelocityTracker == null) {

			mVelocityTracker = VelocityTracker.obtain();
		}

		MotionEvent trackerEvent = MotionEvent.obtain(event);
		trackerEvent.setLocation(event.getRawX(), event.getRawY());
		mVelocityTracker.addMovement(trackerEvent);
		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:

			mCurTouchX = event.getRawX();
			mCurTouchY = event.getRawY();
			break;
		case MotionEvent.ACTION_MOVE:
			float moveX = event.getRawX() - mCurTouchX;
			float moveY = event.getRawY() - mCurTouchY;

			translateContentLayout(moveX, moveY);
			mCurTouchX = event.getRawX();
			mCurTouchY = event.getRawY();
			break;
		case MotionEvent.ACTION_UP:
		case MotionEvent.ACTION_CANCEL:

			handleTouchUp();
			break;
		}
	}

	@SuppressLint("NewApi")
	private void translationCallback(float sliding) {

		sliding = sliding > mDrawerEmptySize ? sliding - mDrawerEmptySize : 0;
		if (mDrawerCallback != null) {
			float fraction;
			if (isHorizontalGravity()) {
				fraction = sliding
						/ (mContentLayout.getWidth() - mDrawerEmptySize);
			} else {
				fraction = sliding
						/ (mContentLayout.getHeight() - mDrawerEmptySize);
			}
			mDrawerCallback.onTranslating(mTouchViewGravity, sliding, fraction);
		}
		if (mIsOpaqueWhenTranslating) {
			if (isHorizontalGravity()) {
				mDrawView.setAlpha(Math.min(
						sliding / mContentLayout.getWidth(), mMaxOpaque));
			} else {
				mDrawView.setAlpha(Math.min(
						sliding / mContentLayout.getHeight(), mMaxOpaque));
			}
		}
	}

	/**
	 * 设置在移动抽屉时改变背景透明度
	 * 
	 * @param isOpaque
	 *            是否同时改变背景透明度
	 */
	public void setOpaqueWhenTranslating(boolean isOpaque) {
		this.mIsOpaqueWhenTranslating = isOpaque;
	}

	/**
	 * 设置最大的不透明度
	 * 
	 * @param maxOpaque
	 *            0 - 1， 0全透明，1完全不透明
	 */
	public void setMaxOpaque(float maxOpaque) {
		this.mMaxOpaque = maxOpaque;
	}

	public void setContentLayout(View view) {
		mContentLayout.removeAllViews();
		ViewGroup.LayoutParams lp = view.getLayoutParams();
		if (lp != null && LayoutParams.class.isInstance(lp)) {
			mContentLayout.addView(view, lp);
		} else {
			mContentLayout.addView(view);
		}

		ViewGroup.LayoutParams childLp = view.getLayoutParams();
		if (childLp != null) {
			mContentLayout.setLayoutParams(childLp);
		}
	}

	public void setContentLayout(View view,
			LayoutParams layoutParams) {
		mContentLayout.removeAllViews();
		mContentLayout.addView(view, layoutParams);
		mContentLayout.setLayoutParams(layoutParams);
	}

	/**
	 * 切换当前抽屉的状态（打开切换成关闭，关闭切换成打开）
	 */
	public void switchStatus() {
		if (AnimStatus.CLOSED.equals(mAnimStatus)
				|| AnimStatus.CLOSING.equals(mAnimStatus)) {
			if (AnimStatus.CLOSED.equals(mAnimStatus)) {
				confirmOpenViewStatus();
				adjustContentLayout();
			} else {
				if (AnimStatus.CLOSING.equals(mAnimStatus)) {
					if (mAnimating.get()) {
						mAnimating.set(false);

						mAnimator.end();
					}
				}
			}
			open();
		} else if (AnimStatus.OPENED.equals(mAnimStatus)
				|| AnimStatus.OPENING.equals(mAnimStatus)) {
			confirmOpenViewStatus();
			if (AnimStatus.OPENING.equals(mAnimStatus)) {
				if (mAnimating.get()) {
					mAnimating.set(false);
					mAnimator.end();
				}
			}
			close();
		}
	}

	private void handleTouchUp() {
		if (getCurTranslation() == getCloseTranslation()
				|| getCurTranslation() == getOpenTranslation()) {
			return;
		}

		final VelocityTracker velocityTracker = mVelocityTracker;
		velocityTracker.computeCurrentVelocity(1000);
		int velocityX = (int) velocityTracker.getXVelocity();
	
		if (mVelocityTracker != null) {
			mVelocityTracker.recycle();
			mVelocityTracker = null;
		}

		if (velocityX < -VEL
				|| (getCurTranslation() < mContentLayout.getWidth()
						* (1 - SCALE_AUTO_OPEN_CLOSE) && velocityX < VEL)) {

			autoOpenDrawer();
		} else {
			autoCloseDrawer();
		}

	}

	private void confirmOpenViewStatus() {
		mTouchView.setVisibility(View.INVISIBLE);
		mContentLayout.setVisibility(View.VISIBLE);
	}

	/**
	 * 自动打开抽屉
	 */
	private void autoOpenDrawer() {
		mAnimating.set(true);
		mAnimator = ObjectAnimator.ofFloat(getCurTranslation(),
				getOpenTranslation());
		mAnimator.setDuration(DURATION_OPEN_CLOSE);
		mAnimator.addUpdateListener(new MyAnimatorUpdateListener());
		mAnimator.addListener(new AnimatorListenerAdapter() {
			@Override
			public void onAnimationStart(Animator animation) {

				if (!AnimStatus.OPENING.equals(mAnimStatus)
						&& !AnimStatus.OPENED.equals(mAnimStatus)) {
					if (mDrawerCallback != null) {
						mDrawerCallback.onStartOpen();
					}
				}

				if (mContentLayout.getVisibility() != View.VISIBLE) {
					mContentLayout.setVisibility(View.VISIBLE);
				}

				mAnimStatus = AnimStatus.OPENING;
			}

			@Override
			public void onAnimationEnd(Animator animation) {
				if (!mAnimating.get()) {

					return;
				}
				if (mDrawerCallback != null) {
					mDrawerCallback.onEndOpen();
				}
				mAnimating.set(false);
				mAnimStatus = AnimStatus.OPENED;
			}
		});
		mAnimator.start();
	}

	public void close() {
		autoCloseDrawer();
	}

	public void open() {
		autoOpenDrawer();
	}

	public boolean isOpened() {
		return mAnimStatus == AnimStatus.OPENED;
	}

	private class MyAnimatorUpdateListener implements
			ValueAnimator.AnimatorUpdateListener {
		@Override
		public void onAnimationUpdate(ValueAnimator animation) {
			if (!mAnimating.get()) {
				return;
			}
			Float animatedValue = (Float) animation.getAnimatedValue();
			if (isHorizontalGravity()) {
				ViewHelper.setTranslationX(mContentLayout, animatedValue);
				translationCallback(mContentLayout.getWidth()
						- Math.abs(animatedValue));
			} else if (isVerticalGravity()) {
				ViewHelper.setTranslationY(mContentLayout, animatedValue);
				translationCallback(mContentLayout.getHeight()
						- Math.abs(animatedValue));
			}
		}
	}

	public void setOpennable(boolean openable) {
		this.mIsOpenable = openable;
	}

	private void autoCloseDrawer() {
		mAnimating.set(true);
		float closeTranslation = getCloseTranslation();
		mAnimator = ObjectAnimator.ofFloat(getCurTranslation(),
				closeTranslation);
		mAnimator.setDuration(DURATION_OPEN_CLOSE);
		mAnimator.addUpdateListener(new MyAnimatorUpdateListener());
		mAnimator.addListener(new AnimatorListenerAdapter() {
			@Override
			public void onAnimationStart(Animator animation) {
				if (!AnimStatus.CLOSING.equals(mAnimStatus)
						&& !AnimStatus.CLOSED.equals(mAnimStatus)) {
					if (mDrawerCallback != null) {
						mDrawerCallback.onStartClose();
					}
				}
				mAnimStatus = AnimStatus.CLOSING;
			}

			@Override
			public void onAnimationEnd(Animator animation) {
				if (!mAnimating.get()) {
					return;
				}
				if (mDrawerCallback != null) {
					mDrawerCallback.onEndClose();
					mAnimStatus = AnimStatus.CLOSED;
				}

				mTouchView.setVisibility(View.VISIBLE);
				mAnimating.set(false);
			}
		});
		mAnimator.start();
	}

	private boolean isHorizontalGravity() {
		return mTouchViewGravity == Gravity.LEFT
				|| mTouchViewGravity == Gravity.RIGHT;
	}

	private boolean isVerticalGravity() {
		return mTouchViewGravity == Gravity.TOP
				|| mTouchViewGravity == Gravity.BOTTOM;
	}

	private void translateContentLayout(float moveX, float moveY) {
		float move;

		if (getCurTranslation() + moveX > mContentLayout.getWidth()) {
			move = mContentLayout.getWidth();
		} else if (getCurTranslation() + moveX < 0) {
			move = 0;
		} else {
			move = getCurTranslation() + moveX;
		}

		if (isHorizontalGravity()) {

			ViewHelper.setTranslationX(mContentLayout, move);

			translationCallback(mContentLayout.getWidth() - Math.abs(move));
		} else {

			ViewHelper.setTranslationY(mContentLayout, move);

			translationCallback(mContentLayout.getHeight() - Math.abs(move));
		}
	}

	/**
	 * 拖拽开始前，调整内容视图位置
	 */
	private void adjustContentLayout() {
		float mStartTranslationX = 0;
		float mStartTranslationY = 0;

		mStartTranslationX = mContentLayout.getWidth() - mDrawerEmptySize
				- mRevealSize;
		mStartTranslationY = 0;

		ViewHelper.setTranslationX(mContentLayout, mStartTranslationX);
		ViewHelper.setTranslationY(mContentLayout, mStartTranslationY);
	}

	/**
	 * 获取关闭时，移动的距离
	 */
	private float getCloseTranslation() {

		return mContentLayout.getWidth() - mRevealSize;

	}

	/**
	 * 获取当前移动距离
	 */
	private float getCurTranslation() {
		checkDrawerInit();
		float curTranslation = 0;

		curTranslation = ViewHelper.getTranslationX(mContentLayout);

		return curTranslation;
	}

	private void checkDrawerInit() {
		if (mIsDrawerOpenned) {
			return;
		}
		adjustContentLayout();
		mIsDrawerOpenned = true;
	}

	/**
	 * 获取打开时候，移动距离
	 */
	private float getOpenTranslation() {
		return 0f;
	}

	/**
	 * 根据Gravity获取触摸视图LayoutParams
	 */
	protected LayoutParams generateTouchViewLayoutParams() {
		LayoutParams lp = (LayoutParams) mTouchView
				.getLayoutParams();
		if (lp == null) {
			lp = new LayoutParams(
					ViewGroup.LayoutParams.WRAP_CONTENT,
					ViewGroup.LayoutParams.WRAP_CONTENT);
		}

		lp.width = mClosedTouchViewSize;
		lp.height = ViewGroup.LayoutParams.MATCH_PARENT;

		lp.gravity = mTouchViewGravity;
		return lp;
	}

	public void setDrawerCallback(DrawerCallback drawerCallback) {
		this.mDrawerCallback = drawerCallback;
	}

	public interface DrawerCallback {

		void onStartOpen();

		void onEndOpen();

		void onStartClose();

		void onEndClose();

		void onPreOpen();

		/**
		 * 正在移动回调
		 *
		 * @param gravity
		 * @param translation
		 *            移动的距离（当前移动位置到边界的距离，永远为正数）
		 * @param fraction
		 *            移动的距离占总距离的百分比，0 - 1
		 */
		void onTranslating(int gravity, float translation, float fraction);
	}

	public static class DrawerCallbackAdapter implements DrawerCallback {

		@Override
		public void onStartOpen() {

		}

		@Override
		public void onEndOpen() {

		}

		@Override
		public void onStartClose() {

		}

		@Override
		public void onEndClose() {

		}

		@Override
		public void onPreOpen() {

		}

		@Override
		public void onTranslating(int gravity, float translation, float fraction) {

		}
	}

}
