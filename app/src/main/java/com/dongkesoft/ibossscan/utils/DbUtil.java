package com.dongkesoft.ibossscan.utils;

import android.content.Context;
import android.os.Environment;

import java.io.File;

public class DbUtil {

  //创建液奶表
  // deliveryID送货单id,codeId产品编码id,code产品编码,codeName编码名称,deliveryQuantity送货数量,
  // batchNo批次号,orderQuantity订单数量,warehouseName仓库名称,warehouseId仓库id,
  //onlyCode唯一编码,referenceId引用id,productArea产区,tankNo罐号,abutmentNo基台号,startTime开始时间,endTime结束时间,
  //periodCategoryId号期id,periodCategoryName号期名称,saveFlag保存标识,userCode用户编码,invoiceStatus单据状态,
  // outQuantity出库数量,remainQuantity剩余数量,boxQuantity箱数,oddQuantity散数,package包数,
  public static final  String sqlString = "create table if not exists LiquidMilkTb(id INTEGER PRIMARY KEY AUTOINCREMENT," +
          "deliveryID VARCHAR,codeId VARCHAR,code VARCHAR,codeName VARCHAR,deliveryQuantity VARCHAR," +
          "batchNo VARCHAR,orderQuantity VARCHAR,warehouseName VARCHAR,warehouseId VARCHAR,onlyCode VARCHAR," +
          "referenceId VARCHAR,productArea VARCHAR,tankNo VARCHAR,abutmentNo VARCHAR,startTime VARCHAR," +
          "endTime VARCHAR,currentDate VARCHAR,collectDetailId VARCHAR, periodCategoryId VARCHAR,periodCategoryName VARCHAR,saveFlag VARCHAR,userCode VARCHAR, " +
          "invoiceStatus VARCHAR,outQuantity VARCHAR,remainQuantity VARCHAR,boxQuantity VARCHAR,oddQuantity VARCHAR," +
          "package VARCHAR,remark2 VARCHAR,remark3 VARCHAR,remark4 VARCHAR )";

  //创建奶粉表
  //outNo出库单号,codeId编码id,code产品编码,codeName编码名称,onlyCode唯一编码,batchNo批次号,boxCode箱码,tankCode罐码,boxNumber箱号,dayTiff天数偏差,
  //outQuantity出库数量,warrantyPeriod保质期,saveFlag保存标识,palletCode垛码,palletNumber垛号,boxQuantity箱数,oddQuantity散数,package包装
  public static final String  milkPowerSqlString="create table if not exists MilkPowerTb(id INTEGER PRIMARY KEY AUTOINCREMENT," +
          "outNo VARCHAR,codeId VARCHAR,code VARCHAR,codeName VARCHAR,onlyCode VARCHAR,batchNo VARCHAR," +
          "boxCode VARCHAR,tankCode VARCHAR,boxNumber VARCHAR,dayTiff VARCHAR,outQuantity VARCHAR,warrantyPeriod VARCHAR," +
          "warehouseId VARCHAR, warehouseName VARCHAR,userCode VARCHAR,periodCategoryId VARCHAR, periodCategoryName VARCHAR," +
          "saveFlag VARCHAR,palletCode VARCHAR,palletNumber VARCHAR,boxQuantity VARCHAR,oddQuantity VARCHAR," +
          "package VARCHAR,remark1 VARCHAR,remark2 VARCHAR,remark3 VARCHAR)";

  //奶粉表
  public static final String milkPowerTb="MilkPowerTb";

  public static final String milkPowerReturnStorageTb="MilkPowerReturnStorageTb";

  //创建奶粉退货入库表
  //inNo入库单号,codeId编码id,code产品编码,codeName编码名称,onlyCode唯一编码,batchNo批次号,boxCode箱码,tankCode罐码,
  //boxNumber箱号,dayTiff天数偏差,inQuantity入库数量,warrantyPeriod保质期,warehouseId仓库id,warehouseName仓库名称,
  // userCode用户编码,periodCategoryId号期id,periodCategoryName号期名称,saveFlag保存标识,palletCode垛码,palletNumber垛号,
  //boxQuantity箱数,oddQuantity散数
  public static final String  milkPowerReturnStorageSqlString="create table if not exists MilkPowerReturnStorageTb(id INTEGER PRIMARY KEY AUTOINCREMENT," +
          "inNo VARCHAR,codeId VARCHAR,code VARCHAR,codeName VARCHAR,onlyCode VARCHAR,batchNo VARCHAR,boxCode VARCHAR," +
          "tankCode VARCHAR,boxNumber VARCHAR,dayTiff VARCHAR,inQuantity VARCHAR,warrantyPeriod VARCHAR," +
          "warehouseId VARCHAR, warehouseName VARCHAR,userCode VARCHAR,periodCategoryId VARCHAR, periodCategoryName VARCHAR," +
          "saveFlag VARCHAR,palletCode VARCHAR,palletNumber VARCHAR,boxQuantity VARCHAR,oddQuantity VARCHAR," +
          "remark1 VARCHAR,remark2 VARCHAR,remark3 VARCHAR)";

  public static final  String milkStockingPickUpSql = "create table if not exists LiquidMilkPickUpTb(id INTEGER PRIMARY KEY AUTOINCREMENT," +
          "orderID VARCHAR,codeId VARCHAR,code VARCHAR,codeName VARCHAR,deliveryQuantity VARCHAR," +
          "batchNo VARCHAR,orderQuantity VARCHAR,warehouseName VARCHAR,warehouseId VARCHAR,onlyCode VARCHAR," +
          "referenceId VARCHAR,productArea VARCHAR,tankNo VARCHAR,abutmentNo VARCHAR,startTime VARCHAR," +
          "endTime VARCHAR,currentDate VARCHAR,collectDetailId VARCHAR, periodCategoryId VARCHAR,periodCategoryName VARCHAR,saveFlag VARCHAR,userCode VARCHAR, " +
          "invoiceStatus VARCHAR,outQuantity VARCHAR,remainQuantity VARCHAR,boxQuantity VARCHAR,oddQuantity VARCHAR," +
          "package VARCHAR,remark2 VARCHAR,remark3 VARCHAR,remark4 VARCHAR )";

  public static final String qrPrintSql="create table if not exists QRPrintTb(id INTEGER PRIMARY KEY AUTOINCREMENT,"+"productArea VARCHAR,"
          +"platformNumber VARCHAR,currentDate VARCHAR,currentDateTimeFrom VARCHAR,currentDateTimeTo VARCHAR,"+
          "productionTankNumber VARCHAR,productionTankCount VARCHAR,ERPBatchNo VARCHAR,goodsCode VARCHAR,codeId VARCHAR, "+
          "code VARCHAR,codeName VARCHAR,mnemonicCode VARCHAR,quantity VARCHAR,box VARCHAR,piece VARCHAR,package VARCHAR,"+
          "racket VARCHAR,currentMillionRandom VARCHAR,QRTypeID VARCHAR,saveFlag VARCHAR,userCode VARCHAR)";
}


