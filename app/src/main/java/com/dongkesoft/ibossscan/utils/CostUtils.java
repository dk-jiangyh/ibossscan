package com.dongkesoft.ibossscan.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;

public class CostUtils {
    public static String stringSpilt(String string) {
        if (string.contains(".")) {
            String[] strings = string.split(Pattern.quote("."));
            String string1 = strings[1];
            if (strings[1].length() > 2) {
                string1 = strings[1].substring(0, 2);
                return strings[0] + "." + string1;
            } else if (strings[1].length() == 1) {
                return string + "0";
            } else {
                return string;
            }
        } else {
            return string + ".00";
        }
    }

    public static String stringSpiltadd(String string) {
        if (string.contains(".")) {
            String[] strings = string.split(Pattern.quote("."));
            String string1 = strings[1];
            if (strings[1].length() > 6) {
                string1 = strings[1].substring(0, 6);
                return strings[0] + "." + string1;
            } else if (strings[1].length() == 1) {
                return string + "00000";
            } else if (strings[1].length() == 2) {
                return string + "0000";
            } else if (strings[1].length() == 3) {
                return string + "000";
            } else if (strings[1].length() == 4) {
                return string + "00";
            } else if (strings[1].length() == 5) {
                return string + "0";
            } else {
                return string;
            }
        } else {
            return string + ".000000";
        }
    }

    public static String AwayDot(String string) {
        if (string.contains(".")) {
            String[] strings = string.split(Pattern.quote("."));
            String string1 = strings[0];
            return string1;
        } else {
            return string;
        }
    }

    //把字符串转为日期
    public static Date ConverToDate(String strDate) throws Exception {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        return df.parse(strDate);
    }

    //把日期转为字符串
    public static String ConverToString(Date date) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

        return df.format(date);
    }
}
