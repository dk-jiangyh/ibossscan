package com.dongkesoft.ibossscan.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.Thread.UncaughtExceptionHandler;
import java.lang.reflect.Field;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Build;
import android.os.Environment;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

/**
 * UncaughtException处理类,当程序发生Uncaught异常的时候,有该类来接管程序,并记录发送错误报告.
 *
 * @author hongy
 *
 */
public class CrashHandler implements UncaughtExceptionHandler {

	/** The Constant TAG. */
	public static final String TAG = "CrashHandler";

	/** The Constant savePath. */
	public static final String savePath = Environment
			.getExternalStorageDirectory() +"/ibossscan/crash/" ;

	// 系统默认的UncaughtException处理类
	/** The m default handler. */
	private Thread.UncaughtExceptionHandler mDefaultHandler;
	// CrashHandler实例
	/** The instance. */
	private static CrashHandler INSTANCE = new CrashHandler();
	// 程序的Context对象
	/** The m context. */
	private Context mContext;
	// 用来存储设备信息和异常信息
	/** The infos. */
	private Map<String, String> infos = new HashMap<String, String>();

	// 用于格式化日期,作为日志文件名的一部分
	/** The formatter. */
	private DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
	private String userCode;
	/**
	 * 保证只有一个CrashHandler实例.
	 */
	private CrashHandler() {
	}

	/**
	 * 获取CrashHandler实例 ,单例模式.
	 *
	 * @return single instance of CrashHandler
	 */
	public static CrashHandler getInstance() {
		return INSTANCE;
	}

	/**
	 * 初始化.
	 *
	 * @param context the context
	 */
	public void init(Context context) {
		mContext = context;
		// 获取系统默认的UncaughtException处理器
		mDefaultHandler = Thread.getDefaultUncaughtExceptionHandler();
		// 设置该CrashHandler为程序的默认处理器
		Thread.setDefaultUncaughtExceptionHandler(this);
	}

	/**
	 * 当UncaughtException发生时会转入该函数来处理.
	 *
	 * @param thread the thread
	 * @param ex the ex
	 */
	@Override
	public void uncaughtException(Thread thread, Throwable ex) {
		if (!handleException(ex) && mDefaultHandler != null) {
			// 如果用户没有处理则让系统默认的异常处理器来处理
			mDefaultHandler.uncaughtException(thread, ex);
		} else {

			mDefaultHandler.uncaughtException(thread, ex);
		}
	}

	/**
	 * 自定义错误处理,收集错误信息 发送错误报告等操作均在此完成.
	 *
	 * @param ex the ex
	 * @return true:如果处理了该异常信息;否则返回false.
	 */
	private boolean handleException(Throwable ex) {
		if (ex == null) {
			new Thread() {
				@Override
				public void run() {
					Looper.prepare();
					Toast.makeText(mContext, "异常为空.", Toast.LENGTH_LONG).show();
					Looper.loop();
				}
			}.start();

			return false;
		}
		// 使用Toast来显示异常信息
		new Thread() {
			@Override
			public void run() {
				Looper.prepare();
				Toast.makeText(mContext, "很抱歉,程序出现异常,即将退出.", Toast.LENGTH_LONG).show();
				Looper.loop();
			}
		}.start();
		// 收集设备参数信息
		collectDeviceInfo(mContext);
		// 保存日志文件
		saveCrashInfo2File(ex);
		return true;
	}

	/**
	 * 收集设备参数信息.
	 *
	 * @param ctx the ctx
	 */
	public void collectDeviceInfo(Context ctx) {
		try {
			PackageManager pm = ctx.getPackageManager();
			PackageInfo pi = pm.getPackageInfo(ctx.getPackageName(),
					PackageManager.GET_ACTIVITIES);
			if (pi != null) {
				String versionName = pi.versionName == null ? "null"
						: pi.versionName;
				String versionCode = pi.versionCode + "";
				infos.put("versionName", versionName);
				infos.put("versionCode", versionCode);
			}
		} catch (NameNotFoundException e) {
			infos.put("collectPackageInfo","an error occured when collect package info");
			Log.e(TAG, "an error occured when collect package info", e);
		}
		Field[] fields = Build.class.getDeclaredFields();
		for (Field field : fields) {
			try {
				field.setAccessible(true);
				infos.put(field.getName(), field.get(null).toString());
				Log.d(TAG, field.getName() + " : " + field.get(null));
			} catch (Exception e) {
				Log.e(TAG, "an error occured when collect crash info", e);
				infos.put("collectPackageInfo","an error occured when collect crash info");
			}
		}
	}

	/**
	 * 保存错误信息到文件中.
	 *
	 * @param ex the ex
	 * @return 返回文件名称,便于将文件传送到服务器
	 */
	private String saveCrashInfo2File(Throwable ex) {

		StringBuffer sb = new StringBuffer();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String currentTime = dateFormat.format(new java.util.Date());
		sb.append("当前时间:"+currentTime+"\n");
		sb.append("操作员工:"+userCode+"\n");
		SimpleDateFormat sDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String date = sDateFormat.format(new java.util.Date());
		RandomAccessFile raf = null;
		String fileName = "crash-"+date+ ".txt";
		for (Map.Entry<String, String> entry : infos.entrySet()) {
			String key = entry.getKey();
			String value = entry.getValue();
			sb.append(key + "=" + value + "\n");
		}
		Writer writer = new StringWriter();
		PrintWriter printWriter = new PrintWriter(writer);
		try {

			ex.printStackTrace(printWriter);
			Throwable cause = ex.getCause();
			while (cause != null) {
				cause.printStackTrace(printWriter);
				cause = cause.getCause();
			}
			printWriter.close();
			String result = writer.toString();
			sb.append(result);

			if (Environment.getExternalStorageState().equals(
					Environment.MEDIA_MOUNTED)) {
				File dir = new File(savePath);
				if (!dir.exists()) {
					dir.mkdirs();
				}

				File file=new File(savePath+fileName);
				if(file.length()>0) {
					raf = new RandomAccessFile(file, "rw");
					raf.seek(file.length());
					raf.write("\n".getBytes());
					raf.write(sb.toString().getBytes());
					raf.close();
				}
				else {
					FileOutputStream fos = new FileOutputStream(savePath + fileName);
					fos.write(sb.toString().getBytes());
					fos.close();
				}
			}
			return fileName;
		}
		catch (Exception e) {
			try {
				sb.append("保存文件异常"+"\n");
				if (Environment.getExternalStorageState().equals(
						Environment.MEDIA_MOUNTED)) {
					File dir = new File(savePath);
					if (!dir.exists()) {
						dir.mkdirs();
					}

					File file=new File(savePath+fileName);
					if(file.length()>0) {
						raf = new RandomAccessFile(file, "rw");
						raf.seek(file.length());
						raf.write("\n".getBytes());
						raf.write(sb.toString().getBytes());
						raf.close();
					}

					else {
						FileOutputStream fos = new FileOutputStream(savePath + fileName);
						fos.write(sb.toString().getBytes());
						fos.close();
					}
				}
			}
			catch(Exception ex1 ) {
				ex1.printStackTrace();
			}

		}
		return null;
	}
}
