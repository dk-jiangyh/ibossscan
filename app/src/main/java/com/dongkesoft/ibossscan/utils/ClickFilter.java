package com.dongkesoft.ibossscan.utils;

public class ClickFilter {
    public static final  long INTERVAL=500L;
    private static long lastClickTime=0;

    public static boolean filter()
    {
        long time=System.currentTimeMillis();

        if((time-lastClickTime)<INTERVAL)
        {
            return true;

        }
        lastClickTime=time;
        return false;
    }
}
