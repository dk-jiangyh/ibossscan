package com.dongkesoft.ibossscan.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;

import android.content.Context;
import android.view.View;

import com.dongkesoft.ibossscan.R;

public class WheelTimeInfo {
	public static DateFormat dateFormat = new SimpleDateFormat(
			"yyyy-MM-dd HH:mm");
	private View view;
	private WheelView wv_year;
	private WheelView wv_month;
	private WheelView wv_day;
	private WheelView wv_hours;
	private WheelView wv_mins;

	private TimePickerInfo.Type type;
	public static final int DEFULT_START_YEAR = 1999;
	public static final int DEFULT_END_YEAR = 2100;
	private int startYear = DEFULT_START_YEAR;
	private int endYear = DEFULT_END_YEAR;
	private String month;
	private String day;

	public WheelTimeInfo(View view) {
		super();
		this.view = view;
		type = TimePickerInfo.Type.ALL;
		setView(view);
	}

	public WheelTimeInfo(View view, TimePickerInfo.Type type) {
		super();
		this.view = view;
		this.type = type;
		setView(view);
	}

	public void setPicker(int year, int month, int day) {
		this.setPicker(year, month, day, 0, 0);
	}

	/**
	 * 
	 */
	public void setPicker(int year, int month, int day, int h, int m) { 
		String[] months_big = { "1", "3", "5", "7", "8", "10", "12" };
		String[] months_little = { "4", "6", "9", "11" };

		final List<String> list_big = Arrays.asList(months_big);
		final List<String> list_little = Arrays.asList(months_little);

		Context context = view.getContext();
	 
		wv_year = (WheelView) view.findViewById(R.id.year);
		wv_year.setAdapter(new NumericWheelAdapter(startYear, endYear));
		wv_year.setLabel(context.getString(R.string.pickerview_year));
		wv_year.setCurrentItem(year - startYear); 	 
		wv_month = (WheelView) view.findViewById(R.id.month);
		wv_month.setAdapter(new NumericWheelAdapter(1, 12, "%02d"));
		wv_month.setLabel(context.getString(R.string.pickerview_month));
		wv_month.setCurrentItem(month); 
		wv_day = (WheelView) view.findViewById(R.id.day); 
		 
		if (list_big.contains(String.valueOf(month + 1))) {
			wv_day.setAdapter(new NumericWheelAdapter(1, 31, "%02d"));
		} else if (list_little.contains(String.valueOf(month + 1))) {
			wv_day.setAdapter(new NumericWheelAdapter(1, 30, "%02d"));
		} else {
			if ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0)
				wv_day.setAdapter(new NumericWheelAdapter(1, 29, "%02d"));
			else
				wv_day.setAdapter(new NumericWheelAdapter(1, 28, "%02d"));
		}
		wv_day.setLabel(context.getString(R.string.pickerview_day));
		wv_day.setCurrentItem(day - 1);

		wv_hours = (WheelView) view.findViewById(R.id.hour);
		wv_hours.setAdapter(new NumericWheelAdapter(0, 23, "%02d"));
		wv_hours.setLabel(context.getString(R.string.pickerview_hours)); 
		wv_hours.setCurrentItem(h);

		wv_mins = (WheelView) view.findViewById(R.id.min);
		wv_mins.setAdapter(new NumericWheelAdapter(0, 59, "%02d"));
		wv_mins.setLabel(context.getString(R.string.pickerview_minutes)); 
		wv_mins.setCurrentItem(m);

 
		OnItemSelectedListener wheelListener_year = new OnItemSelectedListener() {
			@Override
			public void onItemSelected(int index) {
				int year_num = index;
				System.out.println("year_num--->" + year_num);
				 
				int maxItem = 30;
				if (list_big
						.contains(String.valueOf(wv_month.getCurrentItem()))) {
					wv_day.setAdapter(new NumericWheelAdapter(1, 31, "%02d"));
					maxItem = 31;
				} 
				else if (list_little.contains(String.valueOf(wv_month
						.getCurrentItem()))) {
					wv_day.setAdapter(new NumericWheelAdapter(1, 30, "%02d"));
					maxItem = 30;
				} else {
					if ((year_num % 4 == 0 && year_num % 100 != 0)
							|| year_num % 400 == 0) {
						wv_day.setAdapter(new NumericWheelAdapter(1, 29, "%02d"));
						maxItem = 29;
					} else {
						wv_day.setAdapter(new NumericWheelAdapter(1, 28, "%02d"));
						maxItem = 28;
					}
				}
				if (wv_day.getCurrentItem() > maxItem - 1) {
					wv_day.setCurrentItem(maxItem - 1);
				}
			}
		};
		
		//  
		OnItemSelectedListener wheelListener_month = new OnItemSelectedListener() {
			@Override
			public void onItemSelected(int index) {
				int month_num = index;
				System.out.println("month_num--->" + month_num);
				int maxItem = 30;
				//  
				if (list_big.contains(String.valueOf(month_num))) {
					wv_day.setAdapter(new NumericWheelAdapter(1, 31, "%02d"));
					maxItem = 31;
				} else if (list_little.contains(String.valueOf(month_num))) {
					wv_day.setAdapter(new NumericWheelAdapter(1, 30, "%02d"));
					maxItem = 30;
				} else {
					if (((wv_year.getCurrentItem()) % 4 == 0 && (wv_year
							.getCurrentItem()) % 100 != 0)
							|| (wv_year.getCurrentItem()) % 400 == 0) {
						wv_day.setAdapter(new NumericWheelAdapter(1, 29, "%02d"));
						maxItem = 29;
					} else {
						wv_day.setAdapter(new NumericWheelAdapter(1, 28, "%02d"));
						maxItem = 28;
					}
				}
				if (wv_day.getCurrentItem() > maxItem - 1) {
					wv_day.setCurrentItem(maxItem - 1);
				}
				
			

			}
		};
		
		OnItemSelectedListener wheelListener_day = new OnItemSelectedListener() {
			@Override
			public void onItemSelected(int index) {
				int day_num = index;
			
				int maxItem = 30;
				//  
				if (list_big.contains(String.valueOf(wv_month.getCurrentItem()))) {
					wv_day.setCurrentItem(day_num-1);;
					maxItem = 31;
				} else if (list_little.contains(String.valueOf(wv_month.getCurrentItem()))) {
					wv_day.setCurrentItem(day_num-1);
					maxItem = 30;
				} else {
					if (((wv_year.getCurrentItem()) % 4 == 0 && (wv_year
							.getCurrentItem()) % 100 != 0)
							|| (wv_year.getCurrentItem()) % 400 == 0) {
						wv_day.setCurrentItem(day_num-1);;
						maxItem = 29;
					} else {
						wv_day.setCurrentItem(day_num-1);
						maxItem = 28;
					}
				}
				if (day_num > maxItem - 1) {
					wv_day.setCurrentItem(maxItem - 1);
				}
				
			

			}
		};
		wv_year.setOnItemSelectedListener(wheelListener_year);
		wv_month.setOnItemSelectedListener(wheelListener_month);
         wv_day.setOnItemSelectedListener(wheelListener_day);
		//  
		int textSize = 6;
		switch (type) {
		case ALL:
			textSize = textSize * 3;
			break;
		case YEAR_MONTH_DAY:
			textSize = textSize * 3;
			wv_hours.setVisibility(View.GONE);
			wv_mins.setVisibility(View.GONE);
			break;
		case HOURS_MINS:
			textSize = textSize * 4;
			wv_year.setVisibility(View.GONE);
			wv_month.setVisibility(View.GONE);
			wv_day.setVisibility(View.GONE);
			break;
		case MONTH_DAY_HOUR_MIN:
			textSize = textSize * 3;
			wv_year.setVisibility(View.GONE);
			break;
		case YEAR_MONTH:
			textSize = textSize * 4;
			wv_day.setVisibility(View.GONE);
			wv_hours.setVisibility(View.GONE);
			wv_mins.setVisibility(View.GONE);
		}
		wv_day.setTextSize(textSize);
		wv_month.setTextSize(textSize);
		wv_year.setTextSize(textSize);
		wv_hours.setTextSize(textSize);
		wv_mins.setTextSize(textSize);

	}

	/**
	 *  
	 * 
	 * @param cyclic
	 */
	public void setCyclic(boolean cyclic) {
		wv_year.setCyclic(cyclic);
		wv_month.setCyclic(cyclic);
		wv_day.setCyclic(cyclic);
		wv_hours.setCyclic(cyclic);
		wv_mins.setCyclic(cyclic);
	}

	public String getTime() {
		String sb = "";

		// sb.append((wv_year.getCurrentItem() +
		// startYear)).append("-").append((wv_month.getCurrentItem() +
		// 1)).append("-").append((wv_day.getCurrentItem() +
		// 1)).append(" ").append(wv_hours.getCurrentItem()).append(":").append(wv_mins.getCurrentItem());

		if (String.valueOf(wv_month.getCurrentItem()).length() < 2) {
			month = "0" + String.valueOf(wv_month.getCurrentItem());
		} else {
			month = String.valueOf(wv_month.getCurrentItem());
		}
		if (String.valueOf(wv_day.getCurrentItem()).length() < 2) {
			day = "0" + String.valueOf(wv_day.getCurrentItem());
		} else {
			day = String.valueOf(wv_day.getCurrentItem());
		}
		sb = wv_year.getCurrentItem() + "-" + month + "-" + day + " "
				+ wv_hours.getCurrentItem() + ":" + wv_mins.getCurrentItem();

		return sb;
	}

	public View getView() {
		return view;
	}

	public void setView(View view) {
		this.view = view;
	}

	public int getStartYear() {
		return startYear;
	}

	public void setStartYear(int startYear) {
		this.startYear = startYear;
	}

	public int getEndYear() {
		return endYear;
	}

	public void setEndYear(int endYear) {
		this.endYear = endYear;
	}
}
