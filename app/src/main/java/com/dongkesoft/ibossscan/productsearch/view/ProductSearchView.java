package com.dongkesoft.ibossscan.productsearch.view;

import dongkesoft.com.dkmodule.mvp.view.MvpView;

public interface ProductSearchView extends MvpView {
    void success(String result);
    void showFailedError(String message);
    void getRelogin(String message);
}
