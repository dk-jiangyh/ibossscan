package com.dongkesoft.ibossscan.productsearch.presenter;

import android.content.Context;

import com.dongkesoft.ibossscan.common.RightsSet;
import com.dongkesoft.ibossscan.productsearch.view.ProductSearchView;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import dongkesoft.com.dkmodule.bean.ReqCallBack;
import dongkesoft.com.dkmodule.common.Constants;
import dongkesoft.com.dkmodule.mvp.presenter.BasePresenter;
import dongkesoft.com.dkmodule.utils.OkAsyncHttpClient;

public class ProductSearchPresenter extends BasePresenter<ProductSearchView> {

    public void getData(Context context, HashMap<String, String> map, String ip, String port){

        OkAsyncHttpClient.getInstance(context).requestPostByAsynWithForm(String.format(Constants.PRINT_QR_URL,ip,port), map, new ReqCallBack<String>() {
            @Override
            public void onReqSuccess(final String result) {
                {
                    //需要在UI线程执行
                    mViewHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            // 逻辑判断
                            try {
                                JSONObject obj = new JSONObject(result);
                                int status = obj.optInt("Status");
                                String message = obj.optString("Message");
                                //
                                if (status == 0) {
                                    getView().success(result);
                                }
                                else if(status== RightsSet.NegativeOne)
                                {
                                    getView().getRelogin(message);
                                }

                                else {
                                    getView().showFailedError(message);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            }
            @Override
            public void onReqFailed(final String errorMsg) {
                {
                    mViewHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            getView().showFailedError(errorMsg);
                        }
                    });
                }
            }
        });
    }

}
