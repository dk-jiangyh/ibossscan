/*******************************************************************************
 * Copyright(c) 2018 dongke All rights reserved. / Confidential
 * 类的信息：
 * 1.程序名称：MilkStockingRemainDetailModel
 * 2.功能描述：产品剩余查询的bean类
 * 编辑履历：
 * 作者				日期					版本				修改内容
 * dongke			2018/6/8			1.00			新建
 *******************************************************************************/
package com.dongkesoft.ibossscan.productsearch.bean;

import java.io.Serializable;


public class ProductSearchModel implements Serializable {


    /**
     * table1
     * DetailID : 16
     * CodeID : 5
     * Code : 连体坐便器
     * InventoryID : 17208
     * WarehouseID : 96
     * WarehouseName : xxxx
     * PositionNumber : 5
     * OnlyCode : AB1113MD
     * Package : 1
     * GradeID : 1
     * GradeName : 特等品
     * Specification : -
     * ColorNumber : 1
     * InputQuantity : 5000
     * Box : 5000
     * Piece : 0
     * DeliveryQuantity : 0
     * AllNoSalesQuantity : 0
     * BatchNo : 2021-03-12→5→连体坐便器→AB1113MD→-→1→1→96→5→5000→0→161553668108856
     * RandomCode : 161553668108856
     * ProductCodeDate : 2021-03-12 00:00:00
     * AccountOrganizationID : 2
     * AccountID : 1
     */

    private String DetailID;
    private String CodeID;
    private String Code;
    private String InventoryID;
    private String WarehouseID;
    private String WarehouseName;
    private String PositionNumber;
    private String OnlyCode;
    private String Package;
    private String GradeID;
    private String GradeName;
    private String Specification;
    private String ColorNumber;
    private String InputQuantity;
    private String Box;
    private String Piece;
    private String DeliveryQuantity;
    private String AllNoSalesQuantity;
    private String BatchNo;
    private String RandomCode;
    private String ProductCodeDate;
    private String AccountOrganizationID;
    private String AccountID;

    public String getDetailID() {
        return DetailID;
    }

    public void setDetailID(String detailID) {
        DetailID = detailID;
    }

    public String getCodeID() {
        return CodeID;
    }

    public void setCodeID(String codeID) {
        CodeID = codeID;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String code) {
        Code = code;
    }

    public String getInventoryID() {
        return InventoryID;
    }

    public void setInventoryID(String inventoryID) {
        InventoryID = inventoryID;
    }

    public String getWarehouseID() {
        return WarehouseID;
    }

    public void setWarehouseID(String warehouseID) {
        WarehouseID = warehouseID;
    }

    public String getWarehouseName() {
        return WarehouseName;
    }

    public void setWarehouseName(String warehouseName) {
        WarehouseName = warehouseName;
    }

    public String getPositionNumber() {
        return PositionNumber;
    }

    public void setPositionNumber(String positionNumber) {
        PositionNumber = positionNumber;
    }

    public String getOnlyCode() {
        return OnlyCode;
    }

    public void setOnlyCode(String onlyCode) {
        OnlyCode = onlyCode;
    }

    public String getPackage() {
        return Package;
    }

    public void setPackage(String aPackage) {
        Package = aPackage;
    }

    public String getGradeID() {
        return GradeID;
    }

    public void setGradeID(String gradeID) {
        GradeID = gradeID;
    }

    public String getGradeName() {
        return GradeName;
    }

    public void setGradeName(String gradeName) {
        GradeName = gradeName;
    }

    public String getSpecification() {
        return Specification;
    }

    public void setSpecification(String specification) {
        Specification = specification;
    }

    public String getColorNumber() {
        return ColorNumber;
    }

    public void setColorNumber(String colorNumber) {
        ColorNumber = colorNumber;
    }

    public String getInputQuantity() {
        return InputQuantity;
    }

    public void setInputQuantity(String inputQuantity) {
        InputQuantity = inputQuantity;
    }

    public String getBox() {
        return Box;
    }

    public void setBox(String box) {
        Box = box;
    }

    public String getPiece() {
        return Piece;
    }

    public void setPiece(String piece) {
        Piece = piece;
    }

    public String getDeliveryQuantity() {
        return DeliveryQuantity;
    }

    public void setDeliveryQuantity(String deliveryQuantity) {
        DeliveryQuantity = deliveryQuantity;
    }

    public String getAllNoSalesQuantity() {
        return AllNoSalesQuantity;
    }

    public void setAllNoSalesQuantity(String allNoSalesQuantity) {
        AllNoSalesQuantity = allNoSalesQuantity;
    }

    public String getBatchNo() {
        return BatchNo;
    }

    public void setBatchNo(String batchNo) {
        BatchNo = batchNo;
    }

    public String getRandomCode() {
        return RandomCode;
    }

    public void setRandomCode(String randomCode) {
        RandomCode = randomCode;
    }

    public String getProductCodeDate() {
        return ProductCodeDate;
    }

    public void setProductCodeDate(String productCodeDate) {
        ProductCodeDate = productCodeDate;
    }

    public String getAccountOrganizationID() {
        return AccountOrganizationID;
    }

    public void setAccountOrganizationID(String accountOrganizationID) {
        AccountOrganizationID = accountOrganizationID;
    }

    public String getAccountID() {
        return AccountID;
    }

    public void setAccountID(String accountID) {
        AccountID = accountID;
    }


    /**
     * table2
     * */
    String QRTypeID;
    String QRTypeName;
    String NoSalesQuantity;
    String Remarks;
    String CreateTime;

    public String getQRTypeID() {
        return QRTypeID;
    }

    public void setQRTypeID(String QRTypeID) {
        this.QRTypeID = QRTypeID;
    }

    public String getQRTypeName() {
        return QRTypeName;
    }

    public void setQRTypeName(String QRTypeName) {
        this.QRTypeName = QRTypeName;
    }

    public String getNoSalesQuantity() {
        return NoSalesQuantity;
    }

    public void setNoSalesQuantity(String noSalesQuantity) {
        NoSalesQuantity = noSalesQuantity;
    }

    public String getRemarks() {
        return Remarks;
    }

    public void setRemarks(String remarks) {
        Remarks = remarks;
    }

    public String getCreateTime() {
        return CreateTime;
    }

    public void setCreateTime(String createTime) {
        CreateTime = createTime;
    }
}
