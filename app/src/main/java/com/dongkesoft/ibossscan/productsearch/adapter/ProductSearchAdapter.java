package com.dongkesoft.ibossscan.productsearch.adapter;

import android.content.Context;
import android.widget.TextView;

import com.dongkesoft.ibossscan.home.adapter.IBossBaseAdapter;
import com.dongkesoft.ibossscan.R;
import com.dongkesoft.ibossscan.productsearch.bean.ProductSearchModel;
import com.dongkesoft.ibossscan.utils.CostUtils;

import java.text.DecimalFormat;

public class ProductSearchAdapter extends IBossBaseAdapter<ProductSearchModel> {

    public ProductSearchAdapter(Context context) {
        super(context);
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public int getItemLayoutId(int getItemViewType) {
        return R.layout.adapter_search_product;
    }

    @Override
    public void handleItem(int itemViewType, int position, ProductSearchModel item, ViewHolder holder, boolean isRecycle) {
        final ProductSearchModel list = mData.get(position);
        DecimalFormat format = new DecimalFormat("0");

        TextView tv_CreateTime = holder.get(R.id.tv_CreateTime);//创建时间
        TextView tv_QRTypeName = holder.get(R.id.tv_QRTypeName);//修正原因
        TextView tv_NoSalesQuantity = holder.get(R.id.tv_NoSalesQuantity);//数量
        TextView tv_Remarks = holder.get(R.id.tv_Remarks);//备注
        try {
            tv_CreateTime.setText(CostUtils.ConverToString(CostUtils.ConverToDate(list.getCreateTime())));
        } catch (Exception e) {
            e.printStackTrace();
        }
        tv_QRTypeName.setText(list.getQRTypeName());
        tv_NoSalesQuantity.setText(format.format(Double.parseDouble(list.getNoSalesQuantity())));
        tv_Remarks.setText(list.getRemarks());
    }
}