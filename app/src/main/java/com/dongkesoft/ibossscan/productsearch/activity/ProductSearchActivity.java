/*******************************************************************************
 * Copyright(c) 2018 dongke All rights reserved. / Confidential
 * 类的信息：
 * 1.程序名称：MilkStockingRemainSearchActivity
 * 2.功能描述：产品剩余查询
 * 编辑履历：
 * 作者				日期					版本				修改内容
 * dongke			2018/6/8			1.00			新建
 *******************************************************************************/
package com.dongkesoft.ibossscan.productsearch.activity;

import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.dongkesoft.ibossscan.R;
import com.dongkesoft.ibossscan.productsearch.adapter.ProductSearchAdapter;
import com.dongkesoft.ibossscan.productsearch.bean.ProductSearchModel;
import com.dongkesoft.ibossscan.productsearch.presenter.ProductSearchPresenter;
import com.dongkesoft.ibossscan.productsearch.view.ProductSearchView;
import com.dongkesoft.ibossscan.utils.AlertAnimateUtil;
import com.dongkesoft.ibossscan.utils.CostUtils;
import com.dongkesoft.ibossscan.utils.ProcessDialogUtils;
import com.dongkesoft.ibossscan.utils.StringUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import dongkesoft.com.dkmodule.ui.activity.BaseActivity;

public class ProductSearchActivity extends BaseActivity<ProductSearchView,
        ProductSearchPresenter> implements ProductSearchView {
    public ProductSearchAdapter mMilkStockingRemainSearchAdapter;

    /*
     * 标题
     * */
    @BindView(R.id.tv_center)
    TextView tvTitle;
    /*
     * 返回按钮
     * */
    @BindView(R.id.iv_left)
    ImageView ivLeft;

    @BindView(R.id.twoDimenEdt)
    EditText twoDimenEdt;

    @BindView(R.id.goodsLst)
    ListView goodsLst;
    /**
     * 商品编码
     */
    @BindView(R.id.tv_code)
    TextView tv_code;
    /**
     * 唯一编码
     */
    @BindView(R.id.tv_only_code)
    TextView tv_only_code;
    /**
     * 包装
     */
    @BindView(R.id.tv_package)
    TextView tv_package;
    /**
     * 规格
     */
    @BindView(R.id.tv_specification)
    TextView tv_specification;
    /**
     * 出库数量
     */
    @BindView(R.id.tv_delivery_quantity)
    TextView tv_delivery_quantity;
    /**
     * 色号
     */
    @BindView(R.id.tv_color_number)
    TextView tv_color_number;
    /**
     * 打码数量
     */
    @BindView(R.id.tv_input_quantity)
    TextView tv_input_quantity;
    /**
     * 剩余数量
     */
    @BindView(R.id.tv_RemainQuantity)
    TextView tv_RemainQuantity;
    /**
     * 打码箱数
     */
    @BindView(R.id.tv_box)
    TextView tv_box;
    /**
     * 打码片数
     */
    @BindView(R.id.tv_piece)
    TextView tv_piece;

    /**
     * 随机数
     */
    @BindView(R.id.tv_random_code)
    TextView tv_random_code;
    /**
     * 打码日期
     */
    @BindView(R.id.tv_ProductCodeDate)
    TextView tv_ProductCodeDate;
    /**
     * 破损
     */
    @BindView(R.id.tv_AllNoSalesQuantity)
    TextView tv_AllNoSalesQuantity;
    /**
     * 剩余箱数
     */
 @BindView(R.id.tv_RemainBoxQuantity)
    TextView tv_RemainBoxQuantity;
    /**
     * 剩余散数
     */
@BindView(R.id.tv_RemainOddQuantity)
    TextView tv_RemainOddQuantity;
    /**
     *库区
     */
    @BindView(R.id.tv_Warehouse)
    TextView tv_Warehouse;
/**
     *仓位号
     */
    @BindView(R.id.tv_Position)
    TextView tv_Position;

    private String batchNo;
    private List<ProductSearchModel> milkStockingRemainOneList;
    private List<ProductSearchModel> milkStockingRemainTwoList;

    /**
     * @param result
     */
    @Override
    public void success(String result) {
        ProcessDialogUtils.closeProgressDilog();
        setTwoDimenFocus();
        DecimalFormat format = new DecimalFormat("0");
        try {
            JSONObject obj = new JSONObject(result);
            JSONObject json = obj.optJSONObject("Result");
            JSONArray array = json.optJSONArray("Table");
            JSONArray array1 = json.optJSONArray("dtable");
            milkStockingRemainOneList = new ArrayList<ProductSearchModel>();
            milkStockingRemainTwoList = new ArrayList<ProductSearchModel>();
            if (array != null && array.length() > 0) {
                for (int i = 0; i < array.length(); i++) {
                    JSONObject jsonObjectDetail = array
                            .getJSONObject(i);
                    ProductSearchModel info = new ProductSearchModel();
                    info.setDetailID(jsonObjectDetail.optString("DetailID"));
                    info.setCodeID(jsonObjectDetail.optString("CodeID"));
                    info.setCode(jsonObjectDetail.optString("Code"));
                    info.setInventoryID(jsonObjectDetail.optString("InventoryID"));
                    info.setWarehouseID(jsonObjectDetail.optString("WarehouseID"));
                    info.setWarehouseName(jsonObjectDetail.optString("WarehouseName"));
                    info.setPositionNumber(jsonObjectDetail.optString("PositionNumber"));
                    info.setOnlyCode(jsonObjectDetail.optString("OnlyCode"));
                    info.setPackage(jsonObjectDetail.optString("Package"));
                    info.setGradeID(jsonObjectDetail.optString("GradeID"));
                    info.setGradeName(jsonObjectDetail.optString("GradeName"));
                    info.setSpecification(jsonObjectDetail.optString("Specification"));
                    info.setColorNumber(jsonObjectDetail.optString("ColorNumber"));
                    info.setInputQuantity(jsonObjectDetail.optString("InputQuantity"));
                    info.setBox(jsonObjectDetail.optString("Box"));
                    info.setPiece(jsonObjectDetail.optString("Piece"));
                    info.setDeliveryQuantity(jsonObjectDetail.optString("DeliveryQuantity"));
                    info.setAllNoSalesQuantity(jsonObjectDetail.optString("AllNoSalesQuantity"));
                    info.setBatchNo(jsonObjectDetail.optString("BatchNo"));
                    info.setRandomCode(jsonObjectDetail.optString("RandomCode"));
                    info.setProductCodeDate(jsonObjectDetail.optString("ProductCodeDate"));
                    info.setAccountOrganizationID(jsonObjectDetail.optString("AccountOrganizationID"));
                    info.setAccountID(jsonObjectDetail.optString("AccountID"));

                    milkStockingRemainOneList.add(info);
                }
                tv_code.setText(StringUtils.isEmpty(milkStockingRemainOneList.get(0).getCode()) ? "" : milkStockingRemainOneList.get(0).getCode());
                tv_only_code.setText(StringUtils.isEmpty(milkStockingRemainOneList.get(0).getOnlyCode()) ? "" : milkStockingRemainOneList.get(0).getOnlyCode());
                tv_package.setText(StringUtils.isEmpty(milkStockingRemainOneList.get(0).getPackage()) ? "0" : milkStockingRemainOneList.get(0).getPackage());
                tv_specification.setText(StringUtils.isEmpty(milkStockingRemainOneList.get(0).getSpecification()) ? "" : milkStockingRemainOneList.get(0).getSpecification());
                tv_color_number.setText(StringUtils.isEmpty(milkStockingRemainOneList.get(0).getColorNumber()) ? "" : milkStockingRemainOneList.get(0).getColorNumber());
                tv_delivery_quantity.setText(StringUtils.isEmpty(milkStockingRemainOneList.get(0).getDeliveryQuantity()) ? "0" : format.format(Double.parseDouble(milkStockingRemainOneList.get(0).getDeliveryQuantity())));
                tv_input_quantity.setText(StringUtils.isEmpty(milkStockingRemainOneList.get(0).getInputQuantity()) ? "0" : format.format(Double.parseDouble(milkStockingRemainOneList.get(0).getInputQuantity())));
                tv_box.setText(StringUtils.isEmpty(milkStockingRemainOneList.get(0).getBox()) ? "0" : format.format(Double.parseDouble(milkStockingRemainOneList.get(0).getBox())));
                tv_piece.setText(StringUtils.isEmpty(milkStockingRemainOneList.get(0).getPiece()) ? "0" : format.format(Double.parseDouble(milkStockingRemainOneList.get(0).getPiece())));
                tv_random_code.setText(StringUtils.isEmpty(milkStockingRemainOneList.get(0).getRandomCode()) ? "" : milkStockingRemainOneList.get(0).getRandomCode());
                tv_Warehouse.setText(StringUtils.isEmpty(milkStockingRemainOneList.get(0).getWarehouseName()) ? "" : milkStockingRemainOneList.get(0).getWarehouseName());
                tv_Position.setText(StringUtils.isEmpty(milkStockingRemainOneList.get(0).getPositionNumber()) ? "" : milkStockingRemainOneList.get(0).getPositionNumber());
                try {
                    tv_ProductCodeDate.setText(StringUtils.isEmpty(milkStockingRemainOneList.get(0).getProductCodeDate()) ? "" : CostUtils.ConverToString(CostUtils.ConverToDate(milkStockingRemainOneList.get(0).getProductCodeDate())));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                tv_AllNoSalesQuantity.setText(StringUtils.isEmpty(milkStockingRemainOneList.get(0).getAllNoSalesQuantity()) ? "" : format.format(Double.parseDouble(milkStockingRemainOneList.get(0).getAllNoSalesQuantity())));
                //打码数量-出库数量-破损
                String RemainQuantity="0";
                 RemainQuantity = String.valueOf(format.format(Double.parseDouble(milkStockingRemainOneList.get(0).getInputQuantity()) - Double.parseDouble(milkStockingRemainOneList.get(0).getDeliveryQuantity()) - Double.parseDouble(milkStockingRemainOneList.get(0).getAllNoSalesQuantity())));
                //剩余数量
                tv_RemainQuantity.setText(RemainQuantity);

                String RemainBoxQuantity="0";
                RemainBoxQuantity=format.format(Math.floor(Double.parseDouble(RemainQuantity)/Double.parseDouble(tv_package.getText().toString())));
                //剩余箱数
                tv_RemainBoxQuantity.setText(RemainBoxQuantity);
                String RemainOddQuantity ="0";
                RemainOddQuantity=format.format(Double.parseDouble(RemainQuantity)%Double.parseDouble(tv_package.getText().toString()));
                //剩余片数
                tv_RemainOddQuantity.setText(RemainOddQuantity);


            } else {
                tv_code.setText("");
                tv_only_code.setText("");
                tv_package.setText("");
                tv_specification.setText("");
                tv_color_number.setText("");
                tv_delivery_quantity.setText("");
                tv_input_quantity.setText("");
                tv_RemainQuantity.setText("");
                tv_box.setText("");
                tv_piece.setText("");
                tv_random_code.setText("");
                tv_ProductCodeDate.setText("");
                tv_AllNoSalesQuantity.setText("");
                tv_RemainBoxQuantity.setText("");
                tv_RemainOddQuantity.setText("");
                tv_Warehouse.setText("");
                tv_Position.setText("");
                return;
            }
            if (array1 != null && array1.length() > 0) {
                for (int i = 0; i < array1.length(); i++) {
                    JSONObject jsonObjectDetail = array1
                            .getJSONObject(i);
                    ProductSearchModel info = new ProductSearchModel();
                    info.setQRTypeID(jsonObjectDetail.optString("QRTypeID"));
                    info.setQRTypeName(jsonObjectDetail.optString("QRTypeName"));
                    info.setNoSalesQuantity(jsonObjectDetail.optString("NoSalesQuantity"));
                    info.setRemarks(jsonObjectDetail.optString("Remarks"));
                    info.setCreateTime(jsonObjectDetail.optString("CreateTime"));

                    milkStockingRemainTwoList.add(info);
                }
            } else {
                //清空文字布局


            }
            goodsLst.setVisibility(View.VISIBLE);
            mMilkStockingRemainSearchAdapter.setData(milkStockingRemainTwoList);
            goodsLst.setAdapter(mMilkStockingRemainSearchAdapter);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void showFailedError(String message) {
        ProcessDialogUtils.closeProgressDilog();
        setTwoDimenFocus();
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void getRelogin(String message) {
        ProcessDialogUtils.closeProgressDilog();
        AlertAnimateUtil.showReLoginDialog(
                ProductSearchActivity.this, "异常登录",
                message);
    }

    @Override
    protected ProductSearchPresenter createPresenter() {
        return new ProductSearchPresenter();
    }

    @Override
    protected int setMvpView() {
        return R.layout.activity_milkstocking_remain_search;
    }

    @Override
    protected void initView() {
        tvTitle.setVisibility(View.VISIBLE);
        ivLeft.setVisibility(View.VISIBLE);

        tvTitle.setText("商品剩余查询");
        mMilkStockingRemainSearchAdapter = new ProductSearchAdapter(ProductSearchActivity.this);
    }

    @Override
    protected void initData() {

        allListeners();
    }

    /**
     * 点击事件
     *
     * @param view
     */
    @OnClick({R.id.iv_left})
    public void OnClick(View view) {
        int i = view.getId();
        if (i == R.id.iv_left) {
            finish();
        }
    }



    /*
     * 条码输入框重新获得焦点
     */
    private void setTwoDimenFocus() {
        twoDimenEdt.setText("");
        twoDimenEdt.setEnabled(true);
        twoDimenEdt.setFocusable(true);
        twoDimenEdt.setFocusableInTouchMode(true);
        twoDimenEdt.requestFocus();
        twoDimenEdt.findFocus();
        twoDimenEdt
                .setFilters(new InputFilter[]{new InputFilter() {
                    @Override
                    public CharSequence filter(
                            CharSequence source,
                            int start, int end,
                            Spanned dest, int dstart,
                            int dend) {

                        return null;
                    }
                }});
    }

    /**
     * 扫码文本事件变化监听
     */
    private void allListeners() {
        //扫描条码事件
        twoDimenEdt.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable arg0) {

                if (twoDimenEdt.getText().toString().contains("\n")) {


                    batchNo = twoDimenEdt.getText().toString()
                            .trim().replaceAll("\\n", "");
                    batchNo = twoDimenEdt.getText().toString()
                            .trim().replaceAll("\\r\\n", "");
                    //  batchNo = batchNo.substring(0, batchNo.length() - 2);


                    if (!batchNo.contains("→")) {
                        Toast.makeText(ProductSearchActivity.this, "二维码信息错误", Toast.LENGTH_SHORT).show();
                        twoDimenEdt.setText("");
                        twoDimenEdt.requestFocus();
                        twoDimenEdt.findFocus();
                        return;
                    }
                    twoDimenEdt.setFocusable(false);
                    twoDimenEdt.setEnabled(false);
                    twoDimenEdt.setFilters(new InputFilter[]{new InputFilter() {
                        @Override
                        public CharSequence filter(
                                CharSequence source, int start,
                                int end, Spanned dest,
                                int dstart, int dend) {
                            return source.length() < 1 ? dest
                                    .subSequence(dstart, dend)
                                    : "";
                        }
                    }});

                    ProcessDialogUtils.showProcessDialog(ProductSearchActivity.this);
                    // 传递参数
                    HashMap<String, String> map = new HashMap<>();
                    map.put("Action", "GetQrGoodsCodeForMobile");
                    map.put("AccountCode", mAccountCode);
                    map.put("UserCode", mUserCode);
                    map.put("UserPassword", mPassword);
                    map.put("SessionKey", mSessionKey);
                    map.put("BatchNo", batchNo);
                    presenter.getData(ProductSearchActivity.this, map, mServerAddressIp, mServerAddressPort);

                }

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
            }
        });
    }

}
