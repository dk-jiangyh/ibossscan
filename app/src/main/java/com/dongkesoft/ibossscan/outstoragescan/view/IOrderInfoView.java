package com.dongkesoft.ibossscan.outstoragescan.view;

import dongkesoft.com.dkmodule.mvp.view.MvpView;

public interface IOrderInfoView extends MvpView {

    void driverLoadSuccess(String result);
    void driverLoadFail(String message);
    void departmentLoadSuccess(String result);
    void departmentLoadFail(String message);
}
