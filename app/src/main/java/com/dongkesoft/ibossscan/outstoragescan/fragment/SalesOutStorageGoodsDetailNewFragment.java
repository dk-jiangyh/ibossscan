package com.dongkesoft.ibossscan.outstoragescan.fragment;

import android.content.Intent;
import android.view.View;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dongkesoft.ibossscan.R;
import com.dongkesoft.ibossscan.common.IBossBasePopupWindow;
import com.dongkesoft.ibossscan.outstoragescan.adapter.SalesOutStoragedOrderGoodsNewAdapter;
import com.dongkesoft.ibossscan.outstoragescan.bean.SalesOutStorageOrderGoodsModel;
import com.dongkesoft.ibossscan.outstoragescan.presenter.SalesOutStorageGoodsDetailNewPresenter;
import com.dongkesoft.ibossscan.outstoragescan.presenter.SalesOutStorageGoodsDetailPresenter;
import com.dongkesoft.ibossscan.outstoragescan.view.ISalesOutStorageGoodsView;
import com.dongkesoft.ibossscan.outstoragescan.view.ISalesOutStorageGoodsViewNew;
import com.dongkesoft.ibossscan.utils.CommonUtil;
import com.dongkesoft.ibossscan.utils.ProcessDialogUtils;
import com.dongkesoft.ibossscan.utils.ToastUtil;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import dongkesoft.com.dkmodule.ui.fragment.BaseFragment;

public class SalesOutStorageGoodsDetailNewFragment extends BaseFragment <ISalesOutStorageGoodsViewNew, SalesOutStorageGoodsDetailNewPresenter>

implements ISalesOutStorageGoodsViewNew {

    public List<SalesOutStorageOrderGoodsModel> orderGoodsList;

    public SalesOutStoragedOrderGoodsNewAdapter adapter;
    private IBossBasePopupWindow cancelPopupWindow;

    @BindView(R.id.goodsLst)
    public ListView goodsLst;
    @Override
    protected SalesOutStorageGoodsDetailNewPresenter createPresenter() {
        return new SalesOutStorageGoodsDetailNewPresenter();
    }

    @Override
    protected int setMvpView() {
        return R.layout.activity_sales_out_detailed;
    }

    @Override
    protected void initView() {

    }

    @Override
    protected void initData() {

    }

    public void updateData(List<SalesOutStorageOrderGoodsModel> goodsList)
    {
        if(adapter==null) {
            adapter = new SalesOutStoragedOrderGoodsNewAdapter(getActivity(), goodsList);
            goodsLst.setAdapter(adapter);
        }
        else
        {
            adapter.notifyDataSetChanged();
        }

        adapter.setOnClickListener(new SalesOutStoragedOrderGoodsNewAdapter.onClickListener(){

            @Override
            public void cancel(int position) {

                if (cancelPopupWindow == null) {
                    cancelPopupWindow = new IBossBasePopupWindow(
                            getActivity(),
                            R.layout.popup_window_exit);
                }
                cancelPopupWindow
                        .setPopUpWindowCallBack(new IBossBasePopupWindow.IPopUpWindowCallBack() {

                            @Override
                            public void popUpWindowCallBack(View view) {
                                TextView tvPopupWindowMessage = (TextView) view
                                        .findViewById(R.id.tv_popup_window_message);
                                TextView tvPopupWindowTitle = (TextView) view
                                        .findViewById(R.id.tv_popup_window_title);
                                tvPopupWindowTitle.setText("提示");
                                try {
                                    tvPopupWindowMessage
                                            .setText("是否撤销商品 ");
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                // 对布局文件进行初始化
                                RelativeLayout llCancel = (RelativeLayout) view
                                        .findViewById(R.id.ll_cancel);
                                // 对布局中的控件添加事件监听
                                TextView tvCancel = (TextView) llCancel
                                        .findViewById(R.id.tv_popup_window_cancel);
                                tvCancel.setText("取消");

                                llCancel.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        cancelPopupWindow
                                                .dismiss();
                                    }
                                });
                                RelativeLayout llOK = (RelativeLayout) view
                                        .findViewById(R.id.ll_ok);
                                // 对布局中的控件添加事件监听

                                TextView tvOk = (TextView) llOK
                                        .findViewById(R.id.tv_popup_window_ok);
                                tvOk.setText("确定");
                                llOK.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        SalesOutStorageOrderGoodsModel bean= goodsList.get(position);
                                        cancelData(bean.getDetailId());
                                    }
                                });
                            }
                        });
                cancelPopupWindow.show(false,goodsLst
                        , 0, 0);

            }
        });

    }

    private void cancelData(String deliveryScanDetailId)
    {
        ProcessDialogUtils.showProcessDialog(getActivity());
        HashMap<String,String> map=new HashMap<String,String>();
        map.put("Action", "PasteDeliveryScanForMobile");
        map.put("AccountCode", mAccountCode);
        map.put("UserCode", mUserCode);
        map.put("UserPassword", mPassword);
        map.put("SessionKey", mSessionKey);
        map.put("DetailID", deliveryScanDetailId);

        presenter.cancelOutStorageData(getActivity(),map,mServerAddressIp,mServerAddressPort);
    }


    @Override
    public void refreshFragmentView() {

    }

    @Override
    public void cancelOutStorageSuccess(String result) {
        ProcessDialogUtils.closeProgressDilog();
        try {
            JSONObject resultObj=new JSONObject(result) ;
            int status=  resultObj.optInt("Status");
            String message=resultObj.optString("Message");
            if(status==0)
            {
                CommonUtil.refresh=true;
                ToastUtil.showShortToast(getActivity(),"撤销出库成功");
                cancelPopupWindow.dismiss();
                Intent intent=new Intent();
                getActivity().setResult(101,intent);
                getActivity().finish();
            }
            else
            {
                CommonUtil.refresh=false;
                ToastUtil.showShortToast(getActivity(),message);
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void cancelOutStorageFail(String message) {
        ProcessDialogUtils.closeProgressDilog();
        ToastUtil.showShortToast(getActivity(),message);
    }
}
