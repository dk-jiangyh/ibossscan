/*
 * Copyright(c) 2020 dongke All rights reserved. / Confidential
 * 类的信息：
 *		1.程序名称： SalesInventoryCostBean
 *		2.功能描述：  销售出库明细里费用信息的bean类
 * 编辑履历：
 *		作者				日期					版本				修改内容
 *		dongke			2020/12/01			1.00			新建
 *******************************************************************************/
package com.dongkesoft.ibossscan.outstoragescan.bean;

public class SalesInventoryCostBean {
    // 费用单号
    private String FeeNo;
    // 对象名称
    private String ObjectName;
    // 对象类型
    private String ObjectTypeName;
    // 业财科目
    private String FeeItemName;
    // 费用金额
    private String FeeSum;
    // 确认费用金额
    private String ConfirmFeeSum;
    // 确认标识
    private String FeeAuditFlag;
    // 审核人
    private String CheckUserName;
    // 状态
    private String InvoiceStatusName;

    public String getObjectName() {
        return ObjectName;
    }

    public void setObjectName(String objectName) {
        ObjectName = objectName;
    }

    // 备注
    private String Remarks;

    public String getFeeNo() {
        return FeeNo;
    }

    public void setFeeNo(String feeNo) {
        FeeNo = feeNo;
    }


    public String getObjectTypeName() {
        return ObjectTypeName;
    }

    public void setObjectTypeName(String objectTypeName) {
        ObjectTypeName = objectTypeName;
    }

    public String getFeeItemName() {
        return FeeItemName;
    }

    public void setFeeItemName(String feeItemName) {
        FeeItemName = feeItemName;
    }

    public String getFeeSum() {
        return FeeSum;
    }

    public void setFeeSum(String feeSum) {
        FeeSum = feeSum;
    }

    public String getConfirmFeeSum() {
        return ConfirmFeeSum;
    }

    public void setConfirmFeeSum(String confirmFeeSum) {
        ConfirmFeeSum = confirmFeeSum;
    }

    public String getFeeAuditFlag() {
        return FeeAuditFlag;
    }

    public void setFeeAuditFlag(String feeAuditFlag) {
        FeeAuditFlag = feeAuditFlag;
    }

    public String getCheckUserName() {
        return CheckUserName;
    }

    public void setCheckUserName(String checkUserName) {
        CheckUserName = checkUserName;
    }

    public String getInvoiceStatusName() {
        return InvoiceStatusName;
    }

    public void setInvoiceStatusName(String invoiceStatusName) {
        InvoiceStatusName = invoiceStatusName;
    }

    public String getRemarks() {
        return Remarks;
    }

    public void setRemarks(String remarks) {
        Remarks = remarks;
    }


}
