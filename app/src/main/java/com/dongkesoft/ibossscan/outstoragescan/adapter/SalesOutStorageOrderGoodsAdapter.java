package com.dongkesoft.ibossscan.outstoragescan.adapter;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.text.InputType;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dongkesoft.ibossscan.R;
import com.dongkesoft.ibossscan.outstoragescan.activity.SalesOutStorageScanActivity;
import com.dongkesoft.ibossscan.outstoragescan.bean.SalesOutStorageOrderGoodsModel;
import com.dongkesoft.ibossscan.utils.CommonUtil;
import com.dongkesoft.ibossscan.utils.DecimalInputTextWatcher;
import com.dongkesoft.ibossscan.utils.StringUtils;

import java.io.Serializable;
import java.util.List;

public class SalesOutStorageOrderGoodsAdapter extends BaseAdapter {

    private List<SalesOutStorageOrderGoodsModel> productList;

    private Context context;
    private int mDeliveryQuantityCurrentIndex = -1;

  //  private int mOddNumberCurrentIndex = -1;

    private onClickListener onClickListener;

    public void setOnClickListener(onClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }


    public interface onClickListener {
        void goColorNumber(int position);

    }

    public SalesOutStorageOrderGoodsAdapter(Context context) {

        this.context=context;
    }

    public void setData(List<SalesOutStorageOrderGoodsModel> productList)
    {
        this.productList = productList;
    }

    @Override
    public int getCount() {
        return productList.size();
    }

    @Override
    public Object getItem(int position) {
        return productList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class ViewHolder implements Serializable {

        TextView tvCode;
        TextView tvOnlyCode;// 唯一编码
        TextView tvColorNumber;
        TextView tvSpecification;
        TextView tvGrade;
        TextView tvPositionNumber;
        TextView tvSalesQuantity;
        TextView tvSalesBox;
        TextView tvSalesPiece;
        TextView tvWarehouse;
        TextView tvOutQuantity;
      //  TextView tvOutBox;
     //   TextView tvOutPiece;
        EditText edtDeliveryQuantity;
        EditText edtDeliveryBox;
        EditText edtDeliveryPiece;
        LinearLayout llCheck;
       // TextView tvUploadedQuantity;

       // TextView tvUploadQuantity;

        LinearLayout llOutBox;

      //  LinearLayout llUploadQuantity;

        ImageView goodsCheck;

    }

    private class OnDeliveryQuantityEditTextTouched implements View.OnTouchListener {
        private int position;

        public OnDeliveryQuantityEditTextTouched(int position) {
            this.position = position;
        }

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_UP) {
                mDeliveryQuantityCurrentIndex = position;
            }

            return false;
        }

    }

//    private class OnOddNumEditTextTouched implements View.OnTouchListener {
//        private int position;
//
//        public OnOddNumEditTextTouched(int position) {
//            this.position = position;
//        }
//
//        @Override
//        public boolean onTouch(View v, MotionEvent event) {
//            if (event.getAction() == MotionEvent.ACTION_UP) {
//                mOddNumberCurrentIndex = position;
//            }
//
//            return false;
//        }
//
//    }


    //修改送货数量
    private class DeliveryQuantityEditTextListener implements
            View.OnFocusChangeListener {
        private int position;
        private EditText edtDeliveryQuantity;
        private EditText edtBox;
        private EditText edtPiece;

        public DeliveryQuantityEditTextListener(int position, EditText edtDeliveryQuantity, EditText edtBox,EditText edtPiece) {
            this.position = position;
            this.edtDeliveryQuantity = edtDeliveryQuantity;
            this.edtBox=edtBox;
            this.edtPiece=edtPiece;

        }

        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            if (!hasFocus) {
                int pos = (Integer) edtDeliveryQuantity.getTag();
                int boxpos= (int) edtBox.getTag();
                int piecepos= (int) edtPiece.getTag();
                if (pos == position&&pos==boxpos&&pos==piecepos) {
                    String deliveryQuantity = edtDeliveryQuantity.getText().toString();
                    if (productList.size() > 0) {
                        if (position < productList.size()) {
                            if (!TextUtils.isEmpty(deliveryQuantity)) {

                                SalesOutStorageOrderGoodsModel goodsModel = productList.get(position);

                              deliveryQuantity=  CommonUtil.calculateOrderQuantity(goodsModel.getCirculateType(),String.valueOf(goodsModel.getDecimalPlaces()),deliveryQuantity,goodsModel.getAcreage());
                                CommonUtil. calculateBoxAndPiece(deliveryQuantity,goodsModel);
                                productList.set(position, goodsModel);
                                edtDeliveryQuantity.setText(deliveryQuantity);
                                edtBox.setText(goodsModel.getBox());
                                edtPiece.setText(goodsModel.getPiece());

                            }
                        }
                    }
                }
            }

            else{
                edtDeliveryQuantity.requestFocus();
                if (!TextUtils.isEmpty(edtDeliveryQuantity.getText().toString())) {

                    edtDeliveryQuantity.setSelection(edtDeliveryQuantity.getText().length());
                }



            }
        }
    }


//    private class OddNumQuantityEditTextListener implements
//            View.OnFocusChangeListener {
//        private int position;
//        private EditText edtOddQuantity;
//
//        public OddNumQuantityEditTextListener(int position, EditText edtOddQuantity) {
//            this.position = position;
//            this.edtOddQuantity = edtOddQuantity;
//        }
//
//        @Override
//        public void onFocusChange(View v, boolean hasFocus) {
//            if (!hasFocus) {
//                int pos = (Integer) edtOddQuantity.getTag();
//                if (pos == position) {
//
//
//                    if (productList.size() > 0) {
//                        if (position < productList.size() ) {
//
//                          SalesOutStorageOrderGoodsModel goodsBean = productList.get(position);
//                            String oddQuantity = edtOddQuantity.getText().toString();
//                            if(TextUtils.isEmpty(oddQuantity))
//                            {
//                                oddQuantity="0";
//                            }
//                            if (Double.parseDouble(oddQuantity) >=goodsBean.getPackageValue()) {
//                                edtOddQuantity.setText("0");
//                                oddQuantity = "0";
//                            }
//                            if (TextUtils.isEmpty(goodsBean.getBox())) {
//                                goodsBean.setBox("0");
//                            }
//                            int deliveryQuantity = (int) (Double.parseDouble(goodsBean.getBox()) * goodsBean.getPackageValue() + Double.parseDouble(oddQuantity));
//                            goodsBean.setDeliveryQuantity(String.valueOf(deliveryQuantity));
//                            goodsBean.setPiece(oddQuantity);
//                            productList.set(position, goodsBean);
//
//                        }
//                    }
//                }
//            }
//        }
//    }




    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder viewhold;
        LayoutInflater _LayoutInflater = LayoutInflater.from(context);
        viewhold = new ViewHolder();
        convertView = _LayoutInflater.inflate(
                R.layout.adapter_sales_out_storage_order_goods_list_item, null);

        viewhold.tvCode=convertView.findViewById(R.id.tvCode);
        viewhold.tvSalesQuantity=convertView.findViewById(R.id.tvSalesQuantity);
        viewhold.tvOutQuantity=convertView.findViewById(R.id.tvOutQuantity);
        viewhold.edtDeliveryQuantity=convertView.findViewById(R.id.edtDeliveryQuantity);
        viewhold.goodsCheck=convertView.findViewById(R.id.goodsCheck);
        viewhold.llCheck=convertView.findViewById(R.id.llcheck);
        viewhold.tvOnlyCode=convertView.findViewById(R.id.tvOnlyCode);
        viewhold.tvColorNumber=convertView.findViewById(R.id.tvColorNumber);
        viewhold.tvSpecification=convertView.findViewById(R.id.tvSpecification);
        viewhold.tvGrade=convertView.findViewById(R.id.tvGrade);
        viewhold.tvPositionNumber=convertView.findViewById(R.id.tvPositionNumber);
        viewhold.tvWarehouse=convertView.findViewById(R.id.tvWarehouse);
        viewhold.tvSalesBox=convertView.findViewById(R.id.tvSalesBox);
        viewhold.tvSalesPiece=convertView.findViewById(R.id.tvSalesPiece);
        viewhold.tvOutQuantity=convertView.findViewById(R.id.tvOutQuantity);
        viewhold.edtDeliveryQuantity.setTag(position);
     //   viewhold.tvOutPiece=convertView.findViewById(R.id.tvOutPiece);
      //  viewhold.tvUploadedQuantity=convertView.findViewById(R.id.tvUploadedQuantity);
        //viewhold.tvUploadQuantity=convertView.findViewById(R.id.tvUploadQuantity);
        viewhold.edtDeliveryBox=convertView.findViewById(R.id.edtBox);
        viewhold.edtDeliveryPiece=convertView.findViewById(R.id.edtPiece);
        viewhold.edtDeliveryBox.setTag(position);
        viewhold.edtDeliveryPiece.setTag(position);
        viewhold.llOutBox=convertView.findViewById(R.id.llOutBox);
       // viewhold.llUploadQuantity=convertView.findViewById(R.id.llUploadQuantity);

        SalesOutStorageOrderGoodsModel goodsModel=productList.get(position);

        if(goodsModel.getProductCodeFlag()==1)
        {
            viewhold.tvOnlyCode.setTextColor(Color.RED);

        }
        else
        {
            viewhold.tvOnlyCode.setTextColor(Color.BLACK);

        }

        if(goodsModel.isCheckedStatus())
        {
            viewhold.goodsCheck.setBackgroundResource(R.drawable.checked);
        }
        else
        {
            viewhold.goodsCheck.setBackgroundResource(R.drawable.checknormal);
        }
          viewhold.llCheck.setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(View v) {
                  if(goodsModel.getProductCodeFlag()==0) {
                      goodsModel.setCheckedStatus(!goodsModel.isCheckedStatus());
                      if (goodsModel.isCheckedStatus()) {
                          viewhold.goodsCheck.setBackgroundResource(R.drawable.checked);
                      } else {
                          viewhold.goodsCheck.setBackgroundResource(R.drawable.checknormal);
                      }
                  }
                  else
                  {
                  int pos= (int) viewhold.edtDeliveryQuantity.getTag();
                  if(pos==position)
                  {
                      viewhold.edtDeliveryQuantity.setText("0");
                  }
                      if (goodsModel.isCheckedStatus()) {
                          goodsModel.setCheckedStatus(false);
                          goodsModel.setDeliveryQuantity("0");
                          goodsModel.setBox("0");
                          goodsModel.setPiece("0");
                          productList.set(position,goodsModel);
                          viewhold.goodsCheck.setBackgroundResource(R.drawable.checknormal);
                          notifyDataSetChanged();
                      }
                      }


              }
          });

        viewhold.tvSalesQuantity.setText(goodsModel.getSalesQuantity());
        viewhold.tvOutQuantity.setText(goodsModel.getOutQuantity());
        viewhold.tvCode.setText(goodsModel.getCode());
        viewhold.tvOnlyCode.setText(goodsModel.getOnlyCode());
        viewhold.tvGrade.setText(goodsModel.getGrade());
        viewhold.tvPositionNumber.setText(goodsModel.getPositionNumber());
                     //王英杰 2020 11
        viewhold.tvColorNumber.setText(goodsModel.getColorNumber());
        viewhold.tvSpecification.setText(goodsModel.getSpecification());
        viewhold.tvSalesBox.setText(goodsModel.getSalesBox());
        viewhold.tvSalesPiece.setText(goodsModel.getSalesPiece());
      //  viewhold.tvOutBox.setText(goodsModel.getOutBox());
     //   viewhold.tvOutPiece.setText(goodsModel.getOutPiece());
        viewhold.edtDeliveryQuantity.setText(goodsModel.getDeliveryQuantity());

      //  viewhold.edtDeliveryBox.setOnTouchListener(new OnBoxNumEditTextTouched(position));
        viewhold.edtDeliveryBox.setText(goodsModel.getBox());
     //   viewhold.edtDeliveryPiece.setOnTouchListener(new OnOddNumEditTextTouched(position));
        viewhold.edtDeliveryPiece.setText(goodsModel.getPiece());


        if (mDeliveryQuantityCurrentIndex != -1 && position == mDeliveryQuantityCurrentIndex) {
            viewhold.edtDeliveryQuantity.requestFocus();
            viewhold.edtDeliveryQuantity.setSelection(goodsModel.getDeliveryQuantity().length());

        } else {
            viewhold.edtDeliveryQuantity.clearFocus();
        }
          viewhold.edtDeliveryQuantity.setOnFocusChangeListener(new DeliveryQuantityEditTextListener(position, viewhold.edtDeliveryQuantity,viewhold.edtDeliveryBox,viewhold.edtDeliveryPiece));

      //  viewhold.edtDeliveryBox.setOnFocusChangeListener(new BoxNumQuantityEditTextListener(position, viewhold.edtDeliveryBox));
       // viewhold.edtDeliveryPiece.setOnFocusChangeListener(new OddNumQuantityEditTextListener(position,viewhold.edtDeliveryPiece));
       // viewhold.tvUploadedQuantity.setText(goodsModel.getOutQuantity());
      //  viewhold.tvUploadQuantity.setText(StringUtils.isEmpty(goodsModel.getUploadQuantity())?"0":goodsModel.getUploadQuantity());
        viewhold.tvWarehouse.setText(goodsModel.getWarehouseName());

      //  viewhold.tvSalesBox.setTag(position);
       // viewhold.tvSalesPiece.setTag(position);


        viewhold.tvColorNumber.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                if(goodsModel.isCheckedStatus()&&goodsModel.getProductCodeFlag()==1)
                {
                    return;
                }
                onClickListener.goColorNumber(position);

            }
        });
        viewhold.edtDeliveryQuantity.setInputType(InputType.TYPE_CLASS_NUMBER
                | InputType.TYPE_NUMBER_FLAG_DECIMAL);
//        viewhold.edtDeliveryPiece.setInputType(InputType.TYPE_CLASS_NUMBER
//                | InputType.TYPE_NUMBER_FLAG_DECIMAL);


//        if (mBoxNumberCurrentIndex!=-1&&position == mBoxNumberCurrentIndex) {
//            viewhold.edtDeliveryBox.requestFocus();
//            viewhold.edtDeliveryBox.setSelection(viewhold.edtDeliveryBox.getText().toString().length());
//        }
//
//
//        if (mOddNumberCurrentIndex!=-1&&position == mOddNumberCurrentIndex) {
//            viewhold.edtDeliveryPiece.requestFocus();
//            viewhold.edtDeliveryPiece.setSelection(viewhold.edtDeliveryPiece.getText().toString().length());
//        }


        return convertView;
    }
}
