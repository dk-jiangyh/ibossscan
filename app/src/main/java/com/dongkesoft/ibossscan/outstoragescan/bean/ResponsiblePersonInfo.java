/**
 * 
 */
package com.dongkesoft.ibossscan.outstoragescan.bean;


/**
 * @author Administrator
 * @since 2016年7月7日
 */
public class ResponsiblePersonInfo {
	private int staffID;
	private String staffName;
	private String staffCode;

	public int getStaffID() {
		return staffID;
	}

	public void setStaffID(int staffID) {
		this.staffID = staffID;
	}

	public String getStaffName() {
		return staffName;
	}

	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}

	public String getStaffCode() {
		return staffCode;
	}

	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}

}
