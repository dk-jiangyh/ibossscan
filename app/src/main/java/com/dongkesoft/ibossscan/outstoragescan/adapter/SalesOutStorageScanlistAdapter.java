/*
 * Copyright(c) 2020 dongke All rights reserved. / Confidential
 * 类的信息：
 *		1.程序名称： SalesOutStorageScanlistAdapter
 *		2.功能描述：  销售出库列表的适配器
 * 编辑履历：
 *		作者				日期					版本				修改内容
 *		dongke			2020/12/01			1.00			新建
 *******************************************************************************/
package com.dongkesoft.ibossscan.outstoragescan.adapter;

import android.content.Context;
import android.graphics.Color;
import android.widget.TextView;

import com.dongkesoft.ibossscan.R;
import com.dongkesoft.ibossscan.outstoragescan.bean.SalesOutListBean;


public class SalesOutStorageScanlistAdapter extends IBossBaseAdapter<SalesOutListBean> {

    private onClickListener onClickListener;

    public void setOnClickListener(onClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    ViewHolder holder = null;
    private Context context;

    public SalesOutStorageScanlistAdapter(Context context) {
        super(context);
    }

    public interface onClickListener {
        void goCorrect(int position);

        void goDetail(int position);
    }

    private void setEnabled(TextView textView, boolean enable) {
        if (enable) {
            textView.setTextColor(Color.RED);
            textView.setBackgroundResource(R.drawable.btn_red_corner_shape);
        } else {
            textView.setTextColor(Color.parseColor("#EBEBEB"));
            textView.setBackgroundResource(R.drawable.btn_gray_corner_shape);
        }
        textView.setEnabled(enable);
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public int getItemLayoutId(int getItemViewType) {
        return R.layout.item_sales_out;
    }

    @Override
    public void handleItem(int itemViewType, final int position, SalesOutListBean item,
                         IBossBaseAdapter.ViewHolder holder, boolean isRecycle) {
        TextView tv_out_no = holder.get(R.id.tv_out_no);
        TextView tv_customer_name = holder.get(R.id.tv_customer_name);
        TextView tv_telephone = holder.get(R.id.tv_telephone);
        TextView tv_customer_code = holder.get(R.id.tv_customer_code);

        TextView tv_account_date = holder.get(R.id.tv_date);
        TextView tv_address = holder.get(R.id.tv_customer_address);


        //出库单号
        tv_out_no.setText(item.getDeliveryNo());

        //客户名称
        tv_customer_name.setText(item.getCustomerName());
        //联系电话
        tv_telephone.setText(item.getTelephone());
        //客户编码
        tv_customer_code.setText(item.getCustomerCode());
        //业务部门名
        tv_account_date.setText(item.getAccountDate());

        //客户地址
        tv_address.setText(item.getAddress());


    }


}
