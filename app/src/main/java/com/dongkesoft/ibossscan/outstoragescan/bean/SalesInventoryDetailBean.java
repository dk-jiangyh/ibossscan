/*
 * Copyright(c) 2020 dongke All rights reserved. / Confidential
 * 类的信息：
 *		1.程序名称： SalesInventoryDetailBean
 *		2.功能描述：  销售出库里的 商品明细实体bean类
 * 编辑履历：
 *		作者				日期					版本				修改内容
 *		dongke			2020/12/01			1.00			新建
 *******************************************************************************/
package com.dongkesoft.ibossscan.outstoragescan.bean;

public class SalesInventoryDetailBean {
    public SalesInventoryDetailBean() {
        super();
    }

    public String getFeeItemID() {
        return FeeItemID;
    }

    public void setFeeItemID(String feeItemID) {
        FeeItemID = feeItemID;
    }

    //员工ID
    private String StaffID;
    //业财科目id
    public String FeeItemID;

    public String getStaffID() {
        return StaffID;
    }

    public void setStaffID(String staffID) {
        StaffID = staffID;
    }

    public String getCompanyName() {
        return CompanyName;
    }

    public void setCompanyName(String companyName) {
        CompanyName = companyName;
    }

    public String getCompanyID() {
        return CompanyID;
    }

    public void setCompanyID(String companyID) {
        CompanyID = companyID;
    }

    //委外公司
    private String CompanyName;
    //委外公司id
    private String CompanyID;

    // 业务科目名称
    private String FeeItemName;

    public String getFeeItemName() {
        return FeeItemName;
    }

    public void setFeeItemName(String feeItemName) {
        FeeItemName = feeItemName;
    }

    // 销售单号
    private String SalesNo;
    // 订单单号
    private String DeliveryNo;
    // 业务部门
    private String OrganizationName;
    // 业务员
    private String StaffName;
    // 合同号
    private String ContractNumber;
    // 商品编码
    private String Code;
    // 唯一编码
    private String OnlyCode;
    // 商品备注
    private String GoodsRemarks;
    // 商品品牌
    private String BrandName;
    // 商品种类
    private String KindName;
    // 商品品种
    private String VarietyName;
    // 商品系列
    private String SeriesName;
    // 商品规格
    private String Specification;
    // 色号
    private String ColorNumber;
    // 库区名称
    private String WarehouseName;
    // 仓位号
    private String PositionNumber;
    // 出库数量
    private String DeliveryQuantity;
    // 出库单价
    private String DeliveryPrice;
    // 销售价格
    private String SalesPrice;
    // 账务日期
    private String AccountDate;
    // 计量单位
    private String UnitName;
    // 包装
    private String Package;
    // 重量
    private String Weight;
    // 流通方式
    private String CirculateType;
    // 面积
    private String Acreage;
    // 体积
    private String Volume;
    // 体积合计
    private String VolumeTotal;
    // 平米数
    private String M2;
    // 出库量箱
    private String Box;
    // 出库量片
    private String Piece;

    public String getSalesNo() {
        return SalesNo;
    }

    public void setSalesNo(String salesNo) {
        SalesNo = salesNo;
    }

    public String getDeliveryNo() {
        return DeliveryNo;
    }

    public void setDeliveryNo(String deliveryNo) {
        DeliveryNo = deliveryNo;
    }

    public String getOrganizationName() {
        return OrganizationName;
    }

    public void setOrganizationName(String organizationName) {
        OrganizationName = organizationName;
    }

    public String getStaffName() {
        return StaffName;
    }

    public void setStaffName(String staffName) {
        StaffName = staffName;
    }

    public String getContractNumber() {
        return ContractNumber;
    }

    public void setContractNumber(String contractNumber) {
        ContractNumber = contractNumber;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String code) {
        Code = code;
    }

    public String getOnlyCode() {
        return OnlyCode;
    }

    public void setOnlyCode(String onlyCode) {
        OnlyCode = onlyCode;
    }

    public String getGoodsRemarks() {
        return GoodsRemarks;
    }

    public void setGoodsRemarks(String goodsRemarks) {
        GoodsRemarks = goodsRemarks;
    }

    public String getBrandName() {
        return BrandName;
    }

    public void setBrandName(String brandName) {
        BrandName = brandName;
    }

    public String getKindName() {
        return KindName;
    }

    public void setKindName(String kindName) {
        KindName = kindName;
    }

    public String getVarietyName() {
        return VarietyName;
    }

    public void setVarietyName(String varietyName) {
        VarietyName = varietyName;
    }

    public String getSeriesName() {
        return SeriesName;
    }

    public void setSeriesName(String seriesName) {
        SeriesName = seriesName;
    }

    public String getSpecification() {
        return Specification;
    }

    public void setSpecification(String specification) {
        Specification = specification;
    }

    public String getColorNumber() {
        return ColorNumber;
    }

    public void setColorNumber(String colorNumber) {
        ColorNumber = colorNumber;
    }

    public String getWarehouseName() {
        return WarehouseName;
    }

    public void setWarehouseName(String warehouseName) {
        WarehouseName = warehouseName;
    }

    public String getPositionNumber() {
        return PositionNumber;
    }

    public void setPositionNumber(String positionNumber) {
        PositionNumber = positionNumber;
    }

    public String getDeliveryQuantity() {
        return DeliveryQuantity;
    }

    public void setDeliveryQuantity(String deliveryQuantity) {
        DeliveryQuantity = deliveryQuantity;
    }

    public String getDeliveryPrice() {
        return DeliveryPrice;
    }

    public void setDeliveryPrice(String deliveryPrice) {
        DeliveryPrice = deliveryPrice;
    }

    public String getSalesPrice() {
        return SalesPrice;
    }

    public void setSalesPrice(String salesPrice) {
        SalesPrice = salesPrice;
    }

    public String getAccountDate() {
        return AccountDate;
    }

    public void setAccountDate(String accountDate) {
        AccountDate = accountDate;
    }

    public String getUnitName() {
        return UnitName;
    }

    public void setUnitName(String unitName) {
        UnitName = unitName;
    }

    public String getPackage() {
        return Package;
    }

    public void setPackage(String aPackage) {
        Package = aPackage;
    }

    public String getWeight() {
        return Weight;
    }

    public void setWeight(String weight) {
        Weight = weight;
    }

    public String getCirculateType() {
        return CirculateType;
    }

    public void setCirculateType(String circulateType) {
        CirculateType = circulateType;
    }

    public String getAcreage() {
        return Acreage;
    }

    public void setAcreage(String acreage) {
        Acreage = acreage;
    }

    public String getVolume() {
        return Volume;
    }

    public void setVolume(String volume) {
        Volume = volume;
    }

    public String getVolumeTotal() {
        return VolumeTotal;
    }

    public void setVolumeTotal(String volumeTotal) {
        VolumeTotal = volumeTotal;
    }

    public String getM2() {
        return M2;
    }

    public void setM2(String m2) {
        M2 = m2;
    }

    public String getBox() {
        return Box;
    }

    public void setBox(String box) {
        Box = box;
    }

    public String getPiece() {
        return Piece;
    }

    public void setPiece(String piece) {
        Piece = piece;
    }

    public String getGradeName() {
        return GradeName;
    }

    public void setGradeName(String gradeName) {
        GradeName = gradeName;
    }

    public String getGoodsName() {
        return GoodsName;
    }

    public void setGoodsName(String goodsName) {
        GoodsName = goodsName;
    }

    public String getRemarks() {
        return Remarks;
    }

    public void setRemarks(String remarks) {
        Remarks = remarks;
    }

    // 商品等级
    private String GradeName;
    //商品名称
    private String GoodsName;
    //备注
    private String Remarks;


}
