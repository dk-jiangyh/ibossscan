package com.dongkesoft.ibossscan.outstoragescan.adapter;

import java.util.ArrayList;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.dongkesoft.ibossscan.R;
import com.dongkesoft.ibossscan.outstoragescan.bean.ColorNumberBean;

public class ColorNumberAdapter extends BaseAdapter {
	private Context context;
	private ArrayList<ColorNumberBean> list;
	ViewHolder holder = null;

	public ColorNumberAdapter(Context context) {
		super();
		this.context = context;
	}

	public void setList(ArrayList<ColorNumberBean> list) {
	this.list = list;
}
	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater _LayoutInflater = LayoutInflater.from(context);
		if (convertView == null) {
			convertView = _LayoutInflater.inflate(R.layout.item_color_number,
					null);
			holder = new ViewHolder();
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		holder.tv_code = (TextView) convertView.findViewById(R.id.tv_code);
		holder.tv_code.setText(list.get(position).getCode());
		holder.tv_OnlyCode = (TextView) convertView.findViewById(R.id.tv_OnlyCode);
		holder.tv_OnlyCode.setText(list.get(position).getOnlyCode());
		holder.tv_BrandName = (TextView) convertView.findViewById(R.id.tv_BrandName);
		holder.tv_BrandName.setText(list.get(position).getBrandName());
		holder.tv_GradeName = (TextView) convertView.findViewById(R.id.tv_GradeName);
		holder.tv_GradeName.setText(list.get(position).getGradeName());
		holder.tv_ColorNumber = (TextView) convertView.findViewById(R.id.tv_ColorNumber);
		holder.tv_ColorNumber.setText(list.get(position).getColorNumber());
		holder.tv_Specification = (TextView) convertView.findViewById(R.id.tv_Specification);
		holder.tv_Specification.setText(list.get(position).getSpecification());
		holder.tv_WarehouseName = (TextView) convertView.findViewById(R.id.tv_WarehouseName);
		holder.tv_WarehouseName .setText(list.get(position).getWarehouseName());
		holder.tv_PositionNumber = (TextView) convertView.findViewById(R.id.tv_PositionNumber);
		holder.tv_PositionNumber.setText(list.get(position).getPositionNumber());
		holder.tv_InventoryQuantity = (TextView) convertView.findViewById(R.id.tv_InventoryQuantity);
		holder.tv_InventoryQuantity.setText(list.get(position).getInventoryQuantity()); 
		holder.tv_BalanceQuantity = (TextView) convertView.findViewById(R.id.tv_BalanceQuantity);
		holder.tv_BalanceQuantity.setText(list.get(position).getBalanceQuantity());
		return convertView;
	}

	class ViewHolder {
		public TextView tv_code;
		public TextView tv_OnlyCode;
		public TextView tv_BrandName;
		public TextView tv_GradeName;
		public TextView tv_ColorNumber;
		public TextView tv_Specification;
		public TextView tv_WarehouseName;
		public TextView tv_PositionNumber;
		public TextView tv_InventoryQuantity;
		public TextView tv_BalanceQuantity;
	}
}
