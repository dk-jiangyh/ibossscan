/*
 * Copyright(c) 2020 dongke All rights reserved. / Confidential
 * 类的信息：
 *		1.程序名称： SalseOutlistBean
 *		2.功能描述：  销售出库列表的数据bean
 * 编辑履历：
 *		作者				日期					版本				修改内容
 *		dongke			2020/12/01			1.00			新建
 *******************************************************************************/
package com.dongkesoft.ibossscan.outstoragescan.bean;

import java.io.Serializable;

public class SalesOutListBean implements Serializable {

    public SalesOutListBean() {
        super();
    }
    // 出库ID
    private String DeliveryID;
    // 出库单号
    private String DeliveryNo;
    // 出库类型名
    private String InventoryDeliveryTypeName;
    // 出库类型
    private String DeliveryType;
    // 出库单号
    private String mtv_code;
    // 导分销商系String
    private String  ToDistributorFlag;
    // 分销商标识
    private String  DistributorFlag;
    // 冲正单号
    private String  ReversedDeliveryNo;
    // 出库类型
    private String  out_type;
    // 客户编码
    private String  CustomerCode;
    // 客户名称
    private String  CustomerName;
    // 业务员
    private String  StaffName;
    // 业务部门
    private String  OrganizationName;
    // 联系人
    private String  Contacts;
    // 联系电话
    private String  Telephone;
    // 运输司机
    private String  Driver;
    // 客户地址
    private String  Address;
    // 装卸工人
    private String  Docker;
    // 出库总数量
    private String  DeliveryQuantitySum;
    // 费用金额
    private String  FeeAmount;
    //车牌号
    private String  TruckNumber;
    //合计体积
    private String  TotalVolume;

    public String getMtv_code() {
        return mtv_code;
    }

    public void setMtv_code(String mtv_code) {
        this.mtv_code = mtv_code;
    }

    public String getToDistributorFlag() {
        return ToDistributorFlag;
    }

    public void setToDistributorFlag(String toDistributorFlag) {
        ToDistributorFlag = toDistributorFlag;
    }

    public String getDistributorFlag() {
        return DistributorFlag;
    }

    public void setDistributorFlag(String distributorFlag) {
        DistributorFlag = distributorFlag;
    }

    public String getOut_type() {
        return out_type;
    }

    public void setOut_type(String out_type) {
        this.out_type = out_type;
    }

    public String getDeliveryQuantitySum() {
        return DeliveryQuantitySum;
    }

    public void setDeliveryQuantitySum(String deliveryQuantitySum) {
        DeliveryQuantitySum = deliveryQuantitySum;
    }

    public String getTruckNumber() {
        return TruckNumber;
    }

    public void setTruckNumber(String truckNumber) {
        TruckNumber = truckNumber;
    }

    public String getTotalVolume() {
        return TotalVolume;
    }

    public void setTotalVolume(String totalVolume) {
        TotalVolume = totalVolume;
    }

    public String getPrintTimes() {
        return PrintTimes;
    }

    public void setPrintTimes(String printTimes) {
        PrintTimes = printTimes;
    }

    public String getAttachmentCount() {
        return AttachmentCount;
    }

    public void setAttachmentCount(String attachmentCount) {
        AttachmentCount = attachmentCount;
    }

    //单据状态
    private String  InvoiceStatusName;
    // 创建者
    private String  CreateUserName;
    //账务日期
    private String  AccountDate;
    //打印次数
    private String  PrintTimes;
    //附件数量
    private String  AttachmentCount;
    //备注
    private String  Remarks;

    // 客户ID
    private String CustomerID;

    // 业务员ID
    private String StaffID;

    // 部门ID
    private String OrganizationID;

    // 运输司机名
    private String DriverName;

    // 装卸工人名
    private String DockerName;
    // 出库单状态
    private String Status;

    // 冲正出库单ID
    private String ReversedDeliveryID;
    // 供应商
    private String SupplierName;



    // 票据样式ID
    private String InvoiceLayoutID;
    // 票据样式名
    private String InvoiceLayoutName;

    private String SourceNo;
    public String getDeliveryID() {
        return DeliveryID;
    }

    public void setDeliveryID(String deliveryID) {
        DeliveryID = deliveryID;
    }

    public String getDeliveryNo() {
        return DeliveryNo;
    }

    public void setDeliveryNo(String deliveryNo) {
        DeliveryNo = deliveryNo;
    }

    public String getInventoryDeliveryTypeName() {
        return InventoryDeliveryTypeName;
    }

    public void setInventoryDeliveryTypeName(String inventoryDeliveryTypeName) {
        InventoryDeliveryTypeName = inventoryDeliveryTypeName;
    }

    public String getDeliveryType() {
        return DeliveryType;
    }

    public void setDeliveryType(String deliveryType) {
        DeliveryType = deliveryType;
    }

    public String getCustomerID() {
        return CustomerID;
    }

    public void setCustomerID(String customerID) {
        CustomerID = customerID;
    }

    public String getCustomerCode() {
        return CustomerCode;
    }

    public void setCustomerCode(String customerCode) {
        CustomerCode = customerCode;
    }

    public String getCustomerName() {
        return CustomerName;
    }

    public void setCustomerName(String customerName) {
        CustomerName = customerName;
    }

    public String getStaffID() {
        return StaffID;
    }

    public void setStaffID(String staffID) {
        StaffID = staffID;
    }

    public String getStaffName() {
        return StaffName;
    }

    public void setStaffName(String staffName) {
        StaffName = staffName;
    }

    public String getOrganizationID() {
        return OrganizationID;
    }

    public void setOrganizationID(String organizationID) {
        OrganizationID = organizationID;
    }

    public String getOrganizationName() {
        return OrganizationName;
    }

    public void setOrganizationName(String organizationName) {
        OrganizationName = organizationName;
    }

    public String getContacts() {
        return Contacts;
    }

    public void setContacts(String contacts) {
        Contacts = contacts;
    }

    public String getTelephone() {
        return Telephone;
    }

    public void setTelephone(String telephone) {
        Telephone = telephone;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getDriver() {
        return Driver;
    }

    public void setDriver(String driver) {
        Driver = driver;
    }

    public String getDriverName() {
        return DriverName;
    }

    public void setDriverName(String driverName) {
        DriverName = driverName;
    }

    public String getDocker() {
        return Docker;
    }

    public void setDocker(String docker) {
        Docker = docker;
    }

    public String getDockerName() {
        return DockerName;
    }

    public void setDockerName(String dockerName) {
        DockerName = dockerName;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getInvoiceStatusName() {
        return InvoiceStatusName;
    }

    public void setInvoiceStatusName(String invoiceStatusName) {
        InvoiceStatusName = invoiceStatusName;
    }

    public String getReversedDeliveryNo() {
        return ReversedDeliveryNo;
    }

    public void setReversedDeliveryNo(String reversedDeliveryNo) {
        ReversedDeliveryNo = reversedDeliveryNo;
    }

    public String getReversedDeliveryID() {
        return ReversedDeliveryID;
    }

    public void setReversedDeliveryID(String reversedDeliveryID) {
        ReversedDeliveryID = reversedDeliveryID;
    }

    public String getSupplierName() {
        return SupplierName;
    }

    public void setSupplierName(String supplierName) {
        SupplierName = supplierName;
    }

    public String getFeeAmount() {
        return FeeAmount;
    }

    public void setFeeAmount(String feeAmount) {
        FeeAmount = feeAmount;
    }

    public String getAccountDate() {
        return AccountDate;
    }

    public void setAccountDate(String accountDate) {
        AccountDate = accountDate;
    }

    public String getInvoiceLayoutID() {
        return InvoiceLayoutID;
    }

    public void setInvoiceLayoutID(String invoiceLayoutID) {
        InvoiceLayoutID = invoiceLayoutID;
    }

    public String getInvoiceLayoutName() {
        return InvoiceLayoutName;
    }

    public void setInvoiceLayoutName(String invoiceLayoutName) {
        InvoiceLayoutName = invoiceLayoutName;
    }

    public String getCreateUserName() {
        return CreateUserName;
    }

    public void setCreateUserName(String createUserName) {
        CreateUserName = createUserName;
    }

    public String getRemarks() {
        return Remarks;
    }

    public void setRemarks(String remarks) {
        Remarks = remarks;
    }

    public String getSourceNo() {
        return SourceNo;
    }

    public void setSourceNo(String sourceNo) {
        SourceNo = sourceNo;
    }

    @Override
    public String toString() {
        return "SalseOutlistBean{" +
                "DeliveryID='" + DeliveryID + '\'' +
                ", DeliveryNo='" + DeliveryNo + '\'' +
                ", InventoryDeliveryTypeName='" + InventoryDeliveryTypeName + '\'' +
                ", DeliveryType='" + DeliveryType + '\'' +
                ", CustomerID='" + CustomerID + '\'' +
                ", CustomerCode='" + CustomerCode + '\'' +
                ", CustomerName='" + CustomerName + '\'' +
                ", StaffID='" + StaffID + '\'' +
                ", StaffName='" + StaffName + '\'' +
                ", OrganizationID='" + OrganizationID + '\'' +
                ", OrganizationName='" + OrganizationName + '\'' +
                ", Contacts='" + Contacts + '\'' +
                ", Telephone='" + Telephone + '\'' +
                ", Address='" + Address + '\'' +
                ", Driver='" + Driver + '\'' +
                ", DriverName='" + DriverName + '\'' +
                ", Docker='" + Docker + '\'' +
                ", DockerName='" + DockerName + '\'' +
                ", Status='" + Status + '\'' +
                ", InvoiceStatusName='" + InvoiceStatusName + '\'' +
                ", ReversedDeliveryNo='" + ReversedDeliveryNo + '\'' +
                ", ReversedDeliveryID='" + ReversedDeliveryID + '\'' +
                ", SupplierName='" + SupplierName + '\'' +
                ", FeeAmount='" + FeeAmount + '\'' +
                ", AccountDate='" + AccountDate + '\'' +
                ", InvoiceLayoutID='" + InvoiceLayoutID + '\'' +
                ", InvoiceLayoutName='" + InvoiceLayoutName + '\'' +
                ", CreateUserName='" + CreateUserName + '\'' +
                ", Remarks='" + Remarks + '\'' +
                ", SourceNo='" + SourceNo + '\'' +
                '}';
    }


}
