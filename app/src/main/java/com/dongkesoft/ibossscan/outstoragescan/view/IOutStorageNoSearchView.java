package com.dongkesoft.ibossscan.outstoragescan.view;

import dongkesoft.com.dkmodule.mvp.view.MvpView;

public interface IOutStorageNoSearchView extends MvpView {

    void outStorageLoadSuccess(String result);
    void outStorageLoadFail(String message);
}
