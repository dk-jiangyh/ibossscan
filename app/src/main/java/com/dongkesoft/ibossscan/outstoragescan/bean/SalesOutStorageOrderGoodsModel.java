package com.dongkesoft.ibossscan.outstoragescan.bean;

import java.io.Serializable;

public class SalesOutStorageOrderGoodsModel implements Serializable {
    private String codeName;
    private String code;
    private String onlyCode;
    private String colorNumber;
    private String specification;
    private String grade;
    private String box;
    private String piece;
    private String uploadedQuantity;
    private String salesQuantity;
    private String uploadQuantity;
    private String scanCode;
    private String outBox;
    private String outPiece;
    private String salesBox;
    private String salesPiece;
    private int circulateType;
    private int packageValue;
    private String salesNo;
    private boolean isCheckedStatus;
    private String warehouseName;
    private String gradeId;
    private String positionNumber;
    private String codeId;
    private String sourceDetailId;
    private String warehouseId;
    private String inventoryId;
    private String remarks;
    private String outQuantity;
    private String deliveryQuantity;
    private double acreage;

    public String getNoOutReturnQuantity() {
        return noOutReturnQuantity;
    }

    public void setNoOutReturnQuantity(String noOutReturnQuantity) {
        this.noOutReturnQuantity = noOutReturnQuantity;
    }

    private String noOutReturnQuantity;


    public int getDecimalPlaces() {
        return decimalPlaces;
    }

    public void setDecimalPlaces(int decimalPlaces) {
        this.decimalPlaces = decimalPlaces;
    }

    private int decimalPlaces;
    private String outStorageQuantity;

    public String getDetailId() {
        return detailId;
    }

    public void setDetailId(String detailId) {
        this.detailId = detailId;
    }

    private String detailId;

    public String getScanBox() {
        return scanBox;
    }

    public void setScanBox(String scanBox) {
        this.scanBox = scanBox;
    }

    public String getScanPiece() {
        return scanPiece;
    }

    public void setScanPiece(String scanPiece) {
        this.scanPiece = scanPiece;
    }

    private String scanBox;
    private String scanPiece;
    private String M2;

    public int getProductCodeFlag() {
        return productCodeFlag;
    }

    public void setProductCodeFlag(int productCodeFlag) {
        this.productCodeFlag = productCodeFlag;
    }

    private int productCodeFlag;

    public String getOutBox() {
        return outBox;
    }

    public void setOutBox(String outBox) {
        this.outBox = outBox;
    }

    public String getOutPiece() {
        return outPiece;
    }

    public void setOutPiece(String outPiece) {
        this.outPiece = outPiece;
    }

    public String getSalesBox() {
        return salesBox;
    }

    public void setSalesBox(String salesBox) {
        this.salesBox = salesBox;
    }

    public String getSalesPiece() {
        return salesPiece;
    }

    public void setSalesPiece(String salesPiece) {
        this.salesPiece = salesPiece;
    }

    public String getSalesNo() {
        return salesNo;
    }

    public void setSalesNo(String salesNo) {
        this.salesNo = salesNo;
    }

    public boolean isCheckedStatus() {
        return isCheckedStatus;
    }

    public void setCheckedStatus(boolean checkedStatus) {
        isCheckedStatus = checkedStatus;
    }

    public String getWarehouseName() {
        return warehouseName;
    }

    public void setWarehouseName(String warehouseName) {
        this.warehouseName = warehouseName;
    }

    public String getGradeId() {
        return gradeId;
    }

    public void setGradeId(String gradeId) {
        this.gradeId = gradeId;
    }

    public String getPositionNumber() {
        return positionNumber;
    }

    public void setPositionNumber(String positionNumber) {
        this.positionNumber = positionNumber;
    }


    public String getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(String warehouseId) {
        this.warehouseId = warehouseId;
    }

    public String getCodeId() {
        return codeId;
    }

    public void setCodeId(String codeId) {
        this.codeId = codeId;
    }

    public String getM2() {
        return M2;
    }

    public void setM2(String m2) {
        M2 = m2;
    }


    public String getSourceDetailId() {
        return sourceDetailId;
    }

    public void setSourceDetailId(String sourceDetailId) {
        this.sourceDetailId = sourceDetailId;
    }

    public String getInventoryId() {
        return inventoryId;
    }

    public void setInventoryId(String inventoryId) {
        this.inventoryId = inventoryId;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public int getCirculateType() {
        return circulateType;
    }

    public void setCirculateType(int circulateType) {
        this.circulateType = circulateType;
    }

    public int getPackageValue() {
        return packageValue;
    }

    public void setPackageValue(int packageValue) {
        this.packageValue = packageValue;
    }

    public String getOutQuantity() {
        return outQuantity;
    }

    public void setOutQuantity(String outQuantity) {
        this.outQuantity = outQuantity;
    }

    public String getDeliveryQuantity() {
        return deliveryQuantity;
    }

    public void setDeliveryQuantity(String deliveryQuantity) {
        this.deliveryQuantity = deliveryQuantity;
    }

    public double getAcreage() {
        return acreage;
    }

    public void setAcreage(double acreage) {
        this.acreage = acreage;
    }


    public String getScanCode() {
        return scanCode;
    }

    public void setScanCode(String scanCode) {
        this.scanCode = scanCode;
    }

    public String getOutStorageQuantity() {
        return outStorageQuantity;
    }

    public void setOutStorageQuantity(String outStorageQuantity) {
        this.outStorageQuantity = outStorageQuantity;
    }


    public String getCodeName() {
        return codeName;
    }

    public void setCodeName(String codeName) {
        this.codeName = codeName;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getOnlyCode() {
        return onlyCode;
    }

    public void setOnlyCode(String onlyCode) {
        this.onlyCode = onlyCode;
    }

    public String getColorNumber() {
        return colorNumber;
    }

    public void setColorNumber(String colorNumber) {
        this.colorNumber = colorNumber;
    }

    public String getSpecification() {
        return specification;
    }

    public void setSpecification(String specification) {
        this.specification = specification;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getBox() {
        return box;
    }

    public void setBox(String box) {
        this.box = box;
    }

    public String getPiece() {
        return piece;
    }

    public void setPiece(String piece) {
        this.piece = piece;
    }

    public String getUploadedQuantity() {
        return uploadedQuantity;
    }

    public void setUploadedQuantity(String uploadedQuantity) {
        this.uploadedQuantity = uploadedQuantity;
    }

    public String getSalesQuantity() {
        return salesQuantity;
    }

    public void setSalesQuantity(String salesQuantity) {
        this.salesQuantity = salesQuantity;
    }

    public String getUploadQuantity() {
        return uploadQuantity;
    }

    public void setUploadQuantity(String uploadQuantity) {
        this.uploadQuantity = uploadQuantity;
    }


}
