package com.dongkesoft.ibossscan.outstoragescan.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.dongkesoft.ibossscan.R;
import com.dongkesoft.ibossscan.outstoragescan.bean.ResponsiblePersonInfo;

public class StaffsAdapter extends BaseAdapter {
	private List<ResponsiblePersonInfo> mList;
	private Context mContext;

	public StaffsAdapter(Context pContext, List<ResponsiblePersonInfo> pList) {
		this.mContext = pContext;
		this.mList = pList;
	}

	@Override
	public int getCount() {
		return mList.size();
	}

	@Override
	public Object getItem(int position) {
		return mList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater _LayoutInflater = LayoutInflater.from(mContext);
		convertView = _LayoutInflater.inflate(R.layout.responsible_person_item, null);
		if (convertView != null) {
			TextView tvStaffID = (TextView) convertView.findViewById(R.id.staffID);
			tvStaffID.setText(String.valueOf(mList.get(position).getStaffID()));
			TextView tvStaffCode = (TextView) convertView.findViewById(R.id.staffCode);
			tvStaffCode.setText(mList.get(position).getStaffCode());
			TextView tvStaffName = (TextView) convertView.findViewById(R.id.staffName);
			tvStaffName.setText(mList.get(position).getStaffName());
		}
		return convertView;
	}

}
