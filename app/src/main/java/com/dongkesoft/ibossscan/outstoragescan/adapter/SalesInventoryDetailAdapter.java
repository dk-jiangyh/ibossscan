/*
 * Copyright(c) 2020 dongke All rights reserved. / Confidential
 * 类的信息：
 *		1.程序名称： SalesInventoryDetailAdapter
 *		2.功能描述：  销售出库商品明细适配器
 * 编辑履历：
 *		作者				日期					版本				修改内容
 *		dongke			2020/12/01			1.00			新建
 *******************************************************************************/
package com.dongkesoft.ibossscan.outstoragescan.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.dongkesoft.ibossscan.R;
import com.dongkesoft.ibossscan.outstoragescan.bean.SalesInventoryDetailBean;

import java.math.BigDecimal;
import java.util.ArrayList;

public class SalesInventoryDetailAdapter extends BaseAdapter {
    ViewHolder holder = null;
    private Context context;
    private ArrayList<SalesInventoryDetailBean> list;

    public SalesInventoryDetailAdapter(Context context) {
        super();
        this.context = context;
    }

    public ArrayList<SalesInventoryDetailBean> getList() {
        return list;
    }

    public void setList(ArrayList<SalesInventoryDetailBean> list) {
        this.list = list;
    }

    @Override
    public int getCount() {

        return list.size();
    }

    @Override
    public Object getItem(int position) {

        return list.get(position);
    }

    @Override
    public long getItemId(int position) {

        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater _LayoutInflater = LayoutInflater.from(context);
        if (convertView == null) {
            convertView = _LayoutInflater.inflate(R.layout.item_sales_out_detailed, null);
            holder = new ViewHolder();
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.tv_salesNo = (TextView) convertView.findViewById(R.id.tv_salesNo);

        holder.tv_organizationName = (TextView) convertView.findViewById(R.id.tv_organizationName);
        holder.tv_staffname = (TextView) convertView.findViewById(R.id.tv_staffName);
        holder.tv_contractNumber = (TextView) convertView.findViewById(R.id.tv_contractNumber);
        holder.tv_code = (TextView) convertView.findViewById(R.id.tv_code);
        holder.tv_onlycode = (TextView) convertView.findViewById(R.id.tv_onlyCode);
        holder.tv_goodsRemarks = (TextView) convertView.findViewById(R.id.tv_goodsRemarks);
        holder.tv_brandName = (TextView) convertView.findViewById(R.id.tv_brandName);
        holder.tv_kindName = (TextView) convertView.findViewById(R.id.tv_kindName);
        holder.tv_varietyName = (TextView) convertView.findViewById(R.id.tv_varietyName);
        holder.tv_seriesName = (TextView) convertView.findViewById(R.id.tv_seriesName);
        holder.tv_specification = (TextView) convertView.findViewById(R.id.tv_specification);
        holder.tv_colorNumber= (TextView) convertView.findViewById(R.id.tv_colorNumber);
        holder.tv_warehouseName = (TextView) convertView.findViewById(R.id.tv_warehouseName);
        holder.tv_positionNumber = (TextView) convertView.findViewById(R.id.tv_positionNumber);
        holder.tv_deliveryQuantity = (TextView) convertView.findViewById(R.id.tv_deliveryQuantity);
        holder.tv_deliveryPrice = (TextView) convertView.findViewById(R.id.tv_deliveryPrice);
        holder.tv_salesPrice = (TextView) convertView.findViewById(R.id.tv_salesPrice);
        holder.tv_accountDate = (TextView) convertView.findViewById(R.id.tv_accountDate);
        holder.tv_unitName = (TextView) convertView.findViewById(R.id.tv_unitName);
        holder.tv_package = (TextView) convertView.findViewById(R.id.tv_package);
        holder.tv_weight = (TextView) convertView.findViewById(R.id.tv_weight);
        holder.tv_circulateType= (TextView) convertView.findViewById(R.id.tv_circulateType);
        holder.tv_acreage = (TextView) convertView.findViewById(R.id.tv_acreage);
        holder.tv_volume = (TextView) convertView.findViewById(R.id.tv_volume);
        holder.tv_volumeTotal = (TextView) convertView.findViewById(R.id.tv_volumeTotal);
        holder.tv_m2 = (TextView) convertView.findViewById(R.id.tv_m2);
        holder.tv_box = (TextView) convertView.findViewById(R.id.tv_box);
        holder.tv_piece = (TextView) convertView.findViewById(R.id.tv_piece);
        holder.tv_gradeName = (TextView) convertView.findViewById(R.id.tv_gradeName);
        holder.tv_goodsName = (TextView) convertView.findViewById(R.id.tv_goodsName);
        holder.tv_remarks = (TextView) convertView.findViewById(R.id.tv_remarks);




        holder.tv_salesNo.setText(list.get(position).getSalesNo());
        holder.tv_organizationName.setText(list.get(position).getOrganizationName());
        holder.tv_staffname.setText(list.get(position).getStaffName());
        holder.tv_contractNumber.setText(list.get(position).getContractNumber());
        holder.tv_code.setText(list.get(position).getCode());
        holder.tv_onlycode .setText(list.get(position).getOnlyCode());
        holder.tv_goodsRemarks.setText(list.get(position).getGoodsRemarks());
        holder.tv_brandName .setText(list.get(position).getBrandName());
        holder.tv_kindName .setText(list.get(position).getKindName());
        holder.tv_varietyName .setText(list.get(position).getVarietyName());
        holder.tv_seriesName.setText(list.get(position).getSeriesName());
        holder.tv_specification.setText(list.get(position).getSpecification());
        holder.tv_colorNumber.setText(list.get(position).getColorNumber());
        holder.tv_warehouseName.setText(list.get(position).getWarehouseName());
        holder.tv_positionNumber.setText(list.get(position).getPositionNumber());
        holder.tv_deliveryQuantity.setText(list.get(position).getDeliveryQuantity());
        holder.tv_deliveryPrice.setText(list.get(position).getDeliveryPrice());
        holder.tv_salesPrice.setText(list.get(position).getSalesPrice());
        holder. tv_accountDate  .setText(list.get(position).getAccountDate().substring(0, 10));
        holder.tv_unitName .setText(list.get(position).getUnitName());
        holder. tv_package.setText(list.get(position).getPackage());
        holder. tv_weight  .setText(list.get(position).getWeight ());
        holder. tv_circulateType .setText(list.get(position).getCirculateType ());
        holder. tv_acreage .setText(list.get(position).getAcreage ());
        holder. tv_volume .setText(list.get(position).getVolume ());
        holder. tv_volumeTotal .setText(list.get(position).getVolumeTotal ());
        holder. tv_m2 .setText(list.get(position).getM2 ());
        holder. tv_gradeName .setText(list.get(position).getGradeName ());
        holder. tv_goodsName .setText(list.get(position).getGoodsName ());
        holder. tv_remarks .setText(list.get(position).getRemarks ());

        double boxDouble=0.0;
        double pieceDouble=0.0;
        double Package=Double.parseDouble(list.get(position).getPackage());
        double DeliveryQuantity=Double.parseDouble(list.get(position).getDeliveryQuantity());
        double acreage=Double.parseDouble(list.get(position).getAcreage());
        if (list.get(position).getCirculateType().equals("1")) {
            if (Package>0) {
                if (DeliveryQuantity>=0) {
                    boxDouble=Math.floor(DeliveryQuantity/Package);
                }else {
                    boxDouble=Math.ceil(DeliveryQuantity/Package);
                }
                pieceDouble=DeliveryQuantity%Package;
            }
        }else if (list.get(position).getCirculateType().equals("2")) {
            if (Package>0&&acreage>0) {
                double pieces=0.0;
                if (DeliveryQuantity>=0) {
                    pieces=Math.ceil(DeliveryQuantity/acreage);
                    boxDouble=Math.floor(pieces/Package);
                }else {
                    pieces=Math.floor(DeliveryQuantity/acreage);
                    boxDouble=Math.ceil(pieces/Package);
                }
                pieceDouble=pieces%Package;
            }
        }else {
            pieceDouble=1.0;
            boxDouble=0.0;
        }
        holder.tv_box .setText(String.valueOf(boxDouble));
        holder.tv_piece .setText(String.valueOf(pieceDouble));
        BigDecimal bd = new BigDecimal(list.get(position).getAcreage());
        BigDecimal bd1 = new BigDecimal(list.get(position).getVolume());
        String Acreage=bd.toPlainString();
        Double ac=Double.parseDouble(Acreage);
        Double double1=Double.parseDouble(list.get(position).getDeliveryQuantity());
        String Volume=bd1.toPlainString();
        if (list.get(position).getAcreage()!=null) {
            if (list.get(position).getCirculateType().equals("1")) {
                BigDecimal bd2 = new BigDecimal(String.valueOf(ac*double1));
                holder.tv_m2 .setText(bd2.toPlainString());
            }else {
                holder.tv_m2 .setText(list.get(position).getDeliveryQuantity());
            }
        }

        return convertView;
    }

    class ViewHolder {
        // 销售单号
        public TextView tv_salesNo;
        // 订单单号
        public TextView tv_deliveryNo;
        // 业务部门
        public TextView tv_organizationName;
        // 业务员
        public TextView tv_staffname;
        // 合同号
        public TextView tv_contractNumber;
        // 商品编码
        public TextView tv_code;
        //唯一编码
        public TextView tv_onlycode;
        //商品备注
        public TextView tv_goodsRemarks;
        // 商品品牌
        public TextView tv_brandName;
        // 商品种类
        public TextView tv_kindName;
        // 商品品种
        public TextView tv_varietyName;
        // 商品系列
        public TextView tv_seriesName;
        //商品规格
        public TextView tv_specification;
        // 色号
        public TextView tv_colorNumber;
        // 库区名称
        public TextView tv_warehouseName;
        // 仓位号
        public TextView tv_positionNumber;
        //出库数量
        public TextView tv_deliveryQuantity;
        // 出库单价
        public TextView tv_deliveryPrice;
        // 销售价格
        public TextView tv_salesPrice;
        // 账务日期
        public TextView tv_accountDate;
        // 计量单位
        public TextView tv_unitName;
        // 包装
        public TextView tv_package;
        // 重量
        public TextView tv_weight;
        // 流通方式
        public TextView tv_circulateType;
        // 面积
        public TextView tv_acreage;
        // 体积
        public TextView tv_volume;
        // 体积合计
        public TextView tv_volumeTotal;
        // 平米数
        public TextView tv_m2;
        // 出库量箱
        public TextView tv_box;
        // 出库量片
        public TextView tv_piece;
        // 商品等级
        public TextView tv_gradeName;
        //商品名称
        public TextView tv_goodsName;
        //备注
        public TextView tv_remarks;




    }
}