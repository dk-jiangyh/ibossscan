package com.dongkesoft.ibossscan.outstoragescan.view;

import dongkesoft.com.dkmodule.mvp.view.MvpView;

public interface ISalesOutStorageGoodsViewNew extends MvpView {

    void cancelOutStorageSuccess(String result);
    void cancelOutStorageFail(String message);
}
