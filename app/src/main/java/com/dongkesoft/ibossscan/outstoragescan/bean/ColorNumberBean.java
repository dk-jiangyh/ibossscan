package com.dongkesoft.ibossscan.outstoragescan.bean;

import java.io.Serializable;

public class ColorNumberBean implements Serializable {
	
public ColorNumberBean() {
		super();
	}
private String Code;
private String OnlyCode;
private String BrandName;
private String GradeName;
private String ColorNumber;
private String Specification;
private String WarehouseName;
private String PositionNumber;
private String InventoryQuantity;
private String BalanceQuantity;
private String InventoryId;
private String WarehouseId;

	public String getWarehouseId() {
		return WarehouseId;
	}

	public void setWarehouseId(String warehouseId) {
		WarehouseId = warehouseId;
	}

	public String getGradeId() {
		return GradeId;
	}

	public void setGradeId(String gradeId) {
		GradeId = gradeId;
	}

	private String GradeId;

	public String getInventoryId() {
		return InventoryId;
	}

	public void setInventoryId(String inventoryId) {
		InventoryId = inventoryId;
	}


public String getCode() {
	return Code;
}
public void setCode(String code) {
	Code = code;
}
public String getOnlyCode() {
	return OnlyCode;
}
public void setOnlyCode(String onlyCode) {
	OnlyCode = onlyCode;
}
public String getBrandName() {
	return BrandName;
}
public void setBrandName(String brandName) {
	BrandName = brandName;
}
public String getGradeName() {
	return GradeName;
}
public void setGradeName(String gradeName) {
	GradeName = gradeName;
}
public String getColorNumber() {
	return ColorNumber;
}
public void setColorNumber(String colorNumber) {
	ColorNumber = colorNumber;
}
public String getSpecification() {
	return Specification;
}
public void setSpecification(String specification) {
	Specification = specification;
}
public String getWarehouseName() {
	return WarehouseName;
}
public void setWarehouseName(String warehouseName) {
	WarehouseName = warehouseName;
}
public String getPositionNumber() {
	return PositionNumber;
}
public void setPositionNumber(String positionNumber) {
	PositionNumber = positionNumber;
}
public String getInventoryQuantity() {
	return InventoryQuantity;
}
public void setInventoryQuantity(String inventoryQuantity) {
	InventoryQuantity = inventoryQuantity;
}
public String getBalanceQuantity() {
	return BalanceQuantity;
}
public void setBalanceQuantity(String balanceQuantity) {
	BalanceQuantity = balanceQuantity;
}
}
