package com.dongkesoft.ibossscan.outstoragescan.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dongkesoft.ibossscan.R;
import com.dongkesoft.ibossscan.outstoragescan.bean.SaleOrderDetail;

import java.util.List;


/**
 * 未出库的销售订单的列表
 */
public class OutNoSearchAdapter extends BaseAdapter {

    private List<SaleOrderDetail> searchList;
    private Context context;
    private ViewHolder holder = null;

    public OutNoSearchAdapter(List<SaleOrderDetail> searchList, Context context) {
        this.searchList = searchList;
        this.context = context;
    }

    @Override
    public int getCount() {
        return searchList.size();
    }

    @Override
    public Object getItem(int position) {
        return searchList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater _LayoutInflater = LayoutInflater.from(context);
        if (convertView == null) {
            convertView = _LayoutInflater.inflate(R.layout.item_out_no,
                    null);
            holder = new ViewHolder();
            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }

        SaleOrderDetail searchBean = searchList.get(position);

      //  holder.orderCheck=convertView.findViewById(R.id.orderCheck);
       // holder.orderCheck.setTag(position);
        holder.tv_SalesNo = (TextView) convertView.findViewById(R.id.tv_SalesNo);


        holder.tvStatus=convertView.findViewById(R.id.tv_Status);

        holder.tv_CustomerName = (TextView) convertView.findViewById(R.id.tv_CustomerName);

        holder.tv_CreateDate = (TextView) convertView.findViewById(R.id.tv_CreateDate);

        holder.tv_CustomerTelephone = (TextView) convertView.findViewById(R.id.tv_CustomerTelephone);

        holder.tv_CustomerAddress = (TextView) convertView.findViewById(R.id.tv_CustomerAddress);


//        holder.orderCheck
//                .setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//
//                    @Override
//                    public void onCheckedChanged(CompoundButton buttonView,
//                                                 boolean isChecked) {
//
//                        searchBean.setChecked(isChecked);
//                        searchList.set(position, searchBean);
//
//
//                    }
//                });



        holder.tv_SalesNo.setText(searchBean.getInvoiceNo());

        holder.tvStatus.setText(searchBean.getStatus());

        holder.tv_CustomerName.setText(searchBean.getCustomerName());

        holder.tv_CustomerTelephone.setText(searchBean.getTelephone());

        holder.tv_CreateDate.setText(searchBean.getCreateDate());
       // holder.orderCheck.setChecked(searchBean.isChecked());

        holder.tv_CustomerAddress.setText(searchBean.getAddress());


        return convertView;
    }

    class ViewHolder {

        public LinearLayout orderLin;

      //  public CheckBox orderCheck;

        public TextView tvStatus;

        public TextView tv_SalesNo;

        //客户名称
        public TextView tv_CustomerName;


        public TextView tv_CreateDate;

        public TextView tv_CustomerTelephone;


        public TextView tv_CustomerAddress;
    }
}
