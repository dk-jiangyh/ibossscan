/*
 * Copyright(c) 2020 dongke All rights reserved. / Confidential
 * 类的信息：
 *		1.程序名称： SalesOutStorageScanActivity
 *		2.功能描述： 出库扫码
 * 编辑履历：
 *		作者				日期					版本				修改内容
 *		dongke			2020/06/23			1.00			新建
 *******************************************************************************/
package com.dongkesoft.ibossscan.outstoragescan.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentPagerAdapter;

import com.dongkesoft.ibossscan.R;
import com.dongkesoft.ibossscan.common.IBossBasePopupWindow;
import com.dongkesoft.ibossscan.outstoragescan.bean.SaleOrderDetail;
import com.dongkesoft.ibossscan.outstoragescan.bean.SalesOutStorageOrderGoodsModel;
import com.dongkesoft.ibossscan.outstoragescan.bean.ScanInformationBean;
import com.dongkesoft.ibossscan.outstoragescan.fragment.OrderGoodsFragment;
import com.dongkesoft.ibossscan.outstoragescan.fragment.OrderInfoFragment;
import com.dongkesoft.ibossscan.outstoragescan.fragment.ScanFragment;
import com.dongkesoft.ibossscan.outstoragescan.presenter.SalesOutStorageScanPresenter;
import com.dongkesoft.ibossscan.outstoragescan.view.ISalesOutStorageScanView;
import com.dongkesoft.ibossscan.utils.AlertAnimateUtil;
import com.dongkesoft.ibossscan.utils.CommonUtil;
import com.dongkesoft.ibossscan.utils.GenericDrawerLayout;
import com.dongkesoft.ibossscan.utils.NoScrollViewPager;
import com.dongkesoft.ibossscan.utils.ProcessDialogUtils;
import com.dongkesoft.ibossscan.utils.TimePickerInfo;
import com.dongkesoft.ibossscan.utils.ToastUtil;
import com.dongkesoft.ibossscan.utils.ViewPagerIndicator;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import dongkesoft.com.dkmodule.common.Constants;
import dongkesoft.com.dkmodule.ui.activity.BaseActivity;
import dongkesoft.com.dkmodule.utils.DateTimeUtils;

/**
 * 出库扫码
 */

public class SalesOutStorageScanNewActivity extends BaseActivity<ISalesOutStorageScanView, SalesOutStorageScanPresenter>
        implements ISalesOutStorageScanView {
    /**
     * 标题
     */
    @BindView(R.id.tv_center)
    public TextView tvTitle;
    /**
     * Fragment集合
     */
    private ArrayList<Fragment> mListFragment;
    /*
     * fragment适配器
     */
    private FragmentPagerAdapter mAdapter;
    /*
     * 订单信息fragment
     */
    private OrderInfoFragment orderInfoFragment;
    /*
     * 订单产品fragment
     */
    public OrderGoodsFragment orderGoodsFragment;
    /*
     * 扫描列表fragment
     */
    public ScanFragment scanListFragment;
    /**
     * ViewPagerIndicator
     */
    @BindView(R.id.vpi_merger)
    public ViewPagerIndicator viewPagerIndicator;
    /**
     * NoScrollViewPager
     */
    @BindView(R.id.vp_merger)
    public NoScrollViewPager viewPager;

    /**
     * 二维码
     */
    @BindView(R.id.barcodeEdt)
    public EditText edtBarcode;
    /**
     * 保存
     */
    @BindView(R.id.btn_outstorage_save)
    public Button btnOutStorageSave;


    @BindView(R.id.btn_scan_save)
    public Button btnScanSave;

    /**
     * 开始日期
     */
    private TextView tvStartDate;
    /**
     * 结束日期
     */
    private TextView tvEndDate;
    /**
     * 日期标识
     */
    private boolean startDateFlag;
    /**
     * 标识
     */

    private boolean endDateFlag;
    /**
     * 开始日期
     */
    public String mStartDate;
    /**
     * 结束日期
     */
    public String mEndDate;

    private String onlyCode;

    private String salesId;

    private String customerId;

    private String customerCode;

    private String customerName;

    private String staffId;

    private String telephone;

    private String address;
    private String warehouseName;

    private String contacts;

    private String otherContacts;

    private IBossBasePopupWindow savePopupWindow;


    /**
     * 返回
     */
    @BindView(R.id.iv_left)
    public ImageView ivLeft;
    /**
     * 抽屉
     */
    @BindView(R.id.out_storage_drawerlayout)
    public GenericDrawerLayout drawerLayout;
    /**
     * 日期
     */
    private TimePickerInfo mTimePickerInfo;

    /**
     * 销售单no
     */
    private EditText edtSaleNo;

    @BindView(R.id.salesNoEdt)
    public EditText etSalesSlipNo;
    /**
     * 编码
     */
    private EditText edtCustomerCode;
    /**
     * 客户名称
     */
    private EditText edtCustomerName;

    private EditText edtCustomerAddress;

    /**
     * 条码
     */
    private String barcode;


    private String salesNo;
    /*
     *抽屉布局
     */
    private View mDrawerLayoutView;

    private HandlerThread handlerThread;// handler线程
    private Handler mHandler;

    public List<SalesOutStorageOrderGoodsModel> orderSelectedList;


    /**
     * 关闭
     */
    private TextView tvDrawerClose;

    /**
     * 重置
     */
    private TextView tvDrawerReset;

    /**
     * 抽屉
     */
    private TextView tvDrawerSure;
    /**
     * 扫描数据列表
     */
    public List<ScanInformationBean> scanList;
    /**
     * 商品列表
     */
    public List<SalesOutStorageOrderGoodsModel> orderGoodsList;

    List<SalesOutStorageOrderGoodsModel> orderGoodsDetailList;

    /**
     * handler线程
     */
    private Handler commonHandler;

    @BindView(R.id.ivScan)
    public ImageView ivScan;
    /**
     * 详细
     */
    @BindView(R.id.tv_right1)
    public TextView tv_right1;

    private SaleOrderDetail searchBean;

    /**
     * 销售出库 右上角列表文字   王英杰 2020 11
     */
    @BindView(R.id.tv_right)
    public TextView tvRight;
    private List<ScanInformationBean> currentList;
    private String codeId;
    private String specification;
    private String colorNumber;
    private String gradeId;
    private String warehouseId;
    private String positionNumber;
    private String inventoryId;


    @BindView(R.id.focusEdt)
    EditText edtFocus;

    /*
     * 初始化抽屉布局
     */
    private void initDrawerLayout() {
        // 可以设置打开时响应Touch的区域范围
        drawerLayout.setContentLayout(mDrawerLayoutView);
        drawerLayout.setTouchSizeOfOpened(dip2px(SalesOutStorageScanNewActivity.this, 500));
        drawerLayout.setTouchSizeOfClosed(dip2px(SalesOutStorageScanNewActivity.this, 0));
        // 设置随着位置的变更，背景透明度也改变
        drawerLayout.setOpaqueWhenTranslating(true);
        // 设置抽屉是否可以打开
        drawerLayout.setOpennable(false);
        // 设置抽屉的空白区域大小
        float v = getResources().getDisplayMetrics().density * 50 + 0.5f; // 100DIP
        drawerLayout.setDrawerEmptySize((int) v);

        // 设置事件回调
        drawerLayout.setDrawerCallback(new GenericDrawerLayout.DrawerCallback() {

            @Override
            public void onTranslating(int gravity, float translation, float fraction) {
            }

            @Override
            public void onStartOpen() {
            }

            @Override
            public void onStartClose() {
            }

            @Override
            public void onPreOpen() {
            }

            @Override
            public void onEndOpen() {

            }

            @Override
            public void onEndClose() {

            }
        });
    }

    /*
     * dp转px
     */
    private int dip2px(Context context, float dipValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dipValue * scale + 0.5f);
    }

    /**
     * 初始化日期变量
     */
    private void initDate() {
        mTimePickerInfo = new TimePickerInfo(SalesOutStorageScanNewActivity.this,
                TimePickerInfo.Type.YEAR_MONTH_DAY);
        mTimePickerInfo.setCyclic(true);
        mTimePickerInfo.setCancelable(true);
        mTimePickerInfo
                .setOnTimeSelectListener(new TimePickerInfo.OnTimeSelectListener() {
                    @SuppressLint("SimpleDateFormat")
                    @Override
                    public void onTimeSelect(Date date) {
                        // 存储当前选中的date
                        SimpleDateFormat dateFormater = new SimpleDateFormat(
                                "yyyy-MM-dd");
                        if (startDateFlag) {
                            Date mDateStart = date;
                            mStartDate = dateFormater.format(mDateStart);
                            tvStartDate.setText(mStartDate);
                            startDateFlag = false;
                        }

                        if (endDateFlag) {
                            Date mDateEnd = date;
                            mEndDate = dateFormater.format(mDateEnd);
                            tvEndDate.setText(mEndDate);
                            endDateFlag = false;
                        }

                    }
                });
    }


    @Override
    protected SalesOutStorageScanPresenter createPresenter() {
        return new SalesOutStorageScanPresenter();
    }

    @Override
    protected int setMvpView() {
        return R.layout.activity_sales_out_storage;
    }

    @Override
    protected void initView() {
        tvRight.setVisibility(View.VISIBLE);
        tvRight.setText("列表");

        tvTitle.setVisibility(View.VISIBLE);
        tvTitle.setText("出库扫码");

        tv_right1.setVisibility(View.VISIBLE);
        tv_right1.setText("详细");
        etSalesSlipNo.setFocusable(true);
        etSalesSlipNo.requestFocus();
        ivLeft.setVisibility(View.VISIBLE);
        mDrawerLayoutView = View.inflate(SalesOutStorageScanNewActivity.this,
                R.layout.activity_sales_out_storage_scan_drawer_layout, null);

        edtSaleNo = (EditText) mDrawerLayoutView.findViewById(R.id.edt_SaleNo);

        edtCustomerCode = (EditText) mDrawerLayoutView.findViewById(R.id.et_CustomerCode);

        edtCustomerName = (EditText) mDrawerLayoutView.findViewById(R.id.et_CustomerName);

        edtCustomerAddress = (EditText) mDrawerLayoutView.findViewById(R.id.et_CustomerAddress);

        tvStartDate = (TextView) mDrawerLayoutView.findViewById(R.id.tv_start_date);

        tvEndDate = (TextView) mDrawerLayoutView.findViewById(R.id.tv_end_date);

        tvDrawerClose = (TextView) mDrawerLayoutView.findViewById(R.id.tv_sales_out_storage_scan_drawer_layout_close);
        tvDrawerReset = (TextView) mDrawerLayoutView.findViewById(R.id.tv_sales_out_storage_scan_layout_btn_reset);
        tvDrawerSure = (TextView) mDrawerLayoutView.findViewById(R.id.tv_sales_out_storage_scan_drawer_layout_btn_sure);
        orderGoodsList = new ArrayList<SalesOutStorageOrderGoodsModel>();
        orderGoodsDetailList = new ArrayList<SalesOutStorageOrderGoodsModel>();
        scanList = new ArrayList<ScanInformationBean>();
        mStartDate=CommonUtil.getOldDate(-1);
        tvStartDate.setText(mStartDate);
        mEndDate=CommonUtil.getCurrentDate();
        tvEndDate.setText(mEndDate);
        initDrawerLayout();
        initDate();
        initData();

        List<String> mDatas = new ArrayList<String>();
        mDatas.add("商品列表");
        mDatas.add("销售单信息");
        viewPagerIndicator.setTabItemTitles(mDatas);
        viewPagerIndicator.setQuadrangle(false);
        mListFragment = new ArrayList<Fragment>();
        orderInfoFragment = new OrderInfoFragment();
        orderGoodsFragment = new OrderGoodsFragment();
        mListFragment.add(orderGoodsFragment);
        mListFragment.add(orderInfoFragment);

        viewPager.setOffscreenPageLimit(mListFragment.size());
        mAdapter = new FragmentPagerAdapter(getSupportFragmentManager()) {
            @Override
            public int getCount() {
                return mListFragment.size();
            }

            @Override
            public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
            }

            @Override
            public Fragment getItem(int position) {
                return mListFragment.get(position);
            }
        };
        viewPager.setAdapter(mAdapter);

        viewPagerIndicator.setViewPager(viewPager, 0);

        setOnClick();
    }

    @Override
    protected void initData() {

    }

    /**
     * 判断是否是同一个商品
     * @param inventoryId
     * @param orderGoodsList
     * @return
     */
    private boolean isExistsSameGoods(String inventoryId,List<SalesOutStorageOrderGoodsModel> orderGoodsList)
    {
        boolean isExists=false;
        for(int i=0;i<orderGoodsList.size();i++)
        {
            SalesOutStorageOrderGoodsModel goodsModel=  orderGoodsList.get(i) ;
            if(goodsModel.isCheckedStatus()&&goodsModel.getProductCodeFlag()==1&&Double.parseDouble(inventoryId)==Double.parseDouble(goodsModel.getInventoryId()))
            {
                isExists=true;
                break;
            }
        }

        return isExists;

    }

    /**
     * 按钮点击事件
     */
    protected void setOnClick() {

        //出库单详细页面
        tv_right1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!TextUtils.isEmpty(salesId)) {
                    Intent intent = new Intent();
                    Bundle b = new Bundle();
                    b.putString("salesId", salesId);
                    b.putString("flag", "1");
                    intent.putExtras(b);
                    intent.setClass(SalesOutStorageScanNewActivity.this, SalesOutListFragmentNewActivity.class);
                    startActivity(intent);
                }

            }
        });

        //销售单查询
        ivScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.switchStatus();
            }
        });

        //销售单号扫描
        etSalesSlipNo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
               
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (etSalesSlipNo.getText().toString().contains("\n")||etSalesSlipNo.getText().toString().contains("\\n")) {
                    salesNo = etSalesSlipNo.getText().toString().trim().replaceAll("\\n", "");
                    if (salesNo.endsWith("\\n")) {
                        salesNo = salesNo.substring(0, salesNo.length() - 2);
                    }
                    etSalesSlipNo.setText("");
                    salesId="";
                    clearData();
                    ProcessDialogUtils.showProcessDialog(SalesOutStorageScanNewActivity.this);
                    loadSalesDetailData("",salesNo);


                }
            }
        });

        //条码扫描
        edtBarcode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (edtBarcode.getText().toString().endsWith("\n")||edtBarcode.getText().toString().endsWith("\\n")) {
                    barcode = edtBarcode.getText().toString().trim();
                    if (barcode.endsWith("\\n")) {
                        barcode = barcode.substring(0, barcode.length() - 2);
                    }

                    if (orderGoodsList == null || orderGoodsList.size() == 0) {
                        edtBarcode.setText("");
                        Toast.makeText(getApplicationContext(), "请选择销售单", Toast.LENGTH_SHORT).show();
                        return;
                    }

                   String[] barcodeArray=   barcode.split("→");
                    String inventoryId="";
                     if(barcodeArray.length>=9)
                     {
                        inventoryId=barcodeArray[8] ;
                   boolean isExists= isExistsSameGoods(inventoryId,orderGoodsList);
                   if(isExists)
                   {
                      setEdt();
                      Toast.makeText(SalesOutStorageScanNewActivity.this,"商品已经存在",Toast.LENGTH_SHORT).show();
                    CommonUtil.setDefaultSound(SalesOutStorageScanNewActivity.this);
                      return;
                   }
                     }


                    edtBarcode.setFocusable(false);
                    edtBarcode.setEnabled(false);
                    edtBarcode.setFilters(new InputFilter[]{new InputFilter() {
                        @Override
                        public CharSequence filter(
                                CharSequence source, int start,
                                int end, Spanned dest,
                                int dstart, int dend) {
                            return source.length() < 1 ? dest
                                    .subSequence(dstart, dend)
                                    : "";
                        }
                    }});

                    ProcessDialogUtils.showProcessDialog(SalesOutStorageScanNewActivity.this);
                    HashMap<String, String> map = new HashMap<>();
                    map.put("Action", "GetQrGoodsCodeForMobile");
                    map.put("AccountCode", mAccountCode);
                    map.put("UserCode", mUserCode);
                    map.put("UserPassword", mPassword);
                    map.put("SessionKey", mSessionKey);
                    map.put("BatchNo", barcode);
                    presenter.loadScanData(SalesOutStorageScanNewActivity.this, map, mServerAddressIp, mServerAddressPort);

                }


            }
        });

        //返回按钮
        ivLeft.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                finish();

            }
        });

        //出库保存
        btnOutStorageSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(salesId)) {
                    Toast.makeText(SalesOutStorageScanNewActivity.this, "请选择或扫描销售单", Toast.LENGTH_SHORT).show();

                    return;
                }

                orderSelectedList = new ArrayList<SalesOutStorageOrderGoodsModel>();
                for (int i = 0; i < orderGoodsList.size(); i++) {
                    SalesOutStorageOrderGoodsModel orderModel = orderGoodsList.get(i);
                    if (orderModel.isCheckedStatus() && orderModel.getProductCodeFlag() == 0) {
                        orderSelectedList.add(orderModel);
                    }

                }
                if (orderSelectedList!=null&&orderSelectedList.size()==0)  {
                    Toast.makeText(getApplicationContext(), "请选择商品", Toast.LENGTH_SHORT).show();
                    return;
                }



                edtFocus.requestFocus();
                edtFocus.findFocus();

                for (int i = 0; i < orderSelectedList.size(); i++) {
                    SalesOutStorageOrderGoodsModel orderModel = orderSelectedList.get(i);
                    if (TextUtils.isEmpty(orderModel.getDeliveryQuantity())) {
                        Toast.makeText(getApplicationContext(), "出库数量不能为空", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (Double.parseDouble(orderModel.getDeliveryQuantity()) == 0) {
                        Toast.makeText(getApplicationContext(), "出库数量不能为0", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if(Double.parseDouble(orderModel.getDeliveryQuantity())>Double.parseDouble(orderModel.getSalesQuantity())-Double.parseDouble(orderModel.getOutQuantity())-Double.parseDouble(orderModel.getNoOutReturnQuantity()))
                    {
                        Toast.makeText(SalesOutStorageScanNewActivity.this, "出库数量不能大于销售未出库数量", Toast.LENGTH_SHORT).show();
                        return;
                    }

                }

                if (savePopupWindow == null) {
                    savePopupWindow = new IBossBasePopupWindow(
                            SalesOutStorageScanNewActivity.this,
                            R.layout.popup_window_exit);
                }
                savePopupWindow
                        .setPopUpWindowCallBack(new IBossBasePopupWindow.IPopUpWindowCallBack() {

                            @Override
                            public void popUpWindowCallBack(View view) {
                                TextView tvPopupWindowMessage = (TextView) view
                                        .findViewById(R.id.tv_popup_window_message);
                                TextView tvPopupWindowTitle = (TextView) view
                                        .findViewById(R.id.tv_popup_window_title);
                                tvPopupWindowTitle.setText("提示");
                                try {
                                    tvPopupWindowMessage
                                            .setText("是否保存商品 ");
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                // 对布局文件进行初始化
                                RelativeLayout llCancel = (RelativeLayout) view
                                        .findViewById(R.id.ll_cancel);
                                // 对布局中的控件添加事件监听
                                TextView tvCancel = (TextView) llCancel
                                        .findViewById(R.id.tv_popup_window_cancel);
                                tvCancel.setText("取消");

                                llCancel.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        savePopupWindow
                                                .dismiss();
                                    }
                                });
                                RelativeLayout llOK = (RelativeLayout) view
                                        .findViewById(R.id.ll_ok);
                                // 对布局中的控件添加事件监听

                                TextView tvOk = (TextView) llOK
                                        .findViewById(R.id.tv_popup_window_ok);
                                tvOk.setText("确定");
                                llOK.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        ProcessDialogUtils.showProcessDialog(SalesOutStorageScanNewActivity.this);
                                        savePopupWindow
                                                .dismiss();
                                        saveOutStorageData();
                                    }
                                });
                            }
                        });
                savePopupWindow.show(false, btnOutStorageSave
                        , 0, 0);







            }
        });

        //扫描保存
        btnScanSave.setOnClickListener(new View.OnClickListener() {
                                           @Override
                                           public void onClick(View v) {

                                               if (TextUtils.isEmpty(salesId)) {
                                                   Toast.makeText(SalesOutStorageScanNewActivity.this, "请先选择或扫描销售单", Toast.LENGTH_SHORT).show();

                                                   return;
                                               }

                                               orderSelectedList = new ArrayList<SalesOutStorageOrderGoodsModel>();
                                               for (int i = 0; i < orderGoodsList.size(); i++) {
                                                   SalesOutStorageOrderGoodsModel orderModel = orderGoodsList.get(i);
                                                   if (orderModel.isCheckedStatus() && orderModel.getProductCodeFlag() == 1) {
                                                       orderSelectedList.add(orderModel);
                                                   }

                                               }
                                               if (orderSelectedList!=null&&orderSelectedList.size()==0)  {
                                                   Toast.makeText(getApplicationContext(), "请扫描商品", Toast.LENGTH_SHORT).show();
                                                   return;
                                               }


//                                               if(TextUtils.isEmpty(orderInfoFragment.staffId))
//                                               {
//                                                   Toast.makeText(SalesOutStorageScanNewActivity.this, "请选择运输司机", Toast.LENGTH_SHORT).show();
//                                                   return;
//                                               }
//
//                                               if(TextUtils.isEmpty(orderInfoFragment.dockId))
//                                               {
//                                                   Toast.makeText(SalesOutStorageScanNewActivity.this, "请选择装卸工人", Toast.LENGTH_SHORT).show();
//                                                   return;
//                                               }
//
//                                               if(TextUtils.isEmpty(orderInfoFragment.mDepartmentId))
//                                               {
//                                                   Toast.makeText(SalesOutStorageScanNewActivity.this, "请选择业务部门", Toast.LENGTH_SHORT).show();
//                                                   return;
//                                               }

                                               edtFocus.requestFocus();
                                               edtFocus.findFocus();

                                               for (int i = 0; i < orderSelectedList.size(); i++) {
                                                   SalesOutStorageOrderGoodsModel orderModel = orderSelectedList.get(i);
                                                     if(TextUtils.isEmpty(orderModel.getDeliveryQuantity()))
                                                     {
                                                         Toast.makeText(SalesOutStorageScanNewActivity.this, "出库数量不能为空", Toast.LENGTH_SHORT).show();
                                                         return;
                                                     }

                                                     if(Double.parseDouble(orderModel.getDeliveryQuantity())==0)
                                                   {
                                                       Toast.makeText(SalesOutStorageScanNewActivity.this, "出库数量不能为0", Toast.LENGTH_SHORT).show();
                                                       return;
                                                   }
                                                    if(Double.parseDouble(orderModel.getDeliveryQuantity())>Double.parseDouble(orderModel.getSalesQuantity())-Double.parseDouble(orderModel.getOutQuantity())-Double.parseDouble(orderModel.getNoOutReturnQuantity()))
                                                    {
                                                        Toast.makeText(SalesOutStorageScanNewActivity.this, "出库数量不能大于销售未出库数量", Toast.LENGTH_SHORT).show();
                                                             return;
                                                    }

                                               }

                                               if (savePopupWindow == null) {
                                                   savePopupWindow = new IBossBasePopupWindow(
                                                           SalesOutStorageScanNewActivity.this,
                                                           R.layout.popup_window_exit);
                                               }
                                               savePopupWindow
                                                       .setPopUpWindowCallBack(new IBossBasePopupWindow.IPopUpWindowCallBack() {

                                                           @Override
                                                           public void popUpWindowCallBack(View view) {
                                                               TextView tvPopupWindowMessage = (TextView) view
                                                                       .findViewById(R.id.tv_popup_window_message);
                                                               TextView tvPopupWindowTitle = (TextView) view
                                                                       .findViewById(R.id.tv_popup_window_title);
                                                               tvPopupWindowTitle.setText("提示");
                                                               try {
                                                                   tvPopupWindowMessage
                                                                           .setText("是否保存商品 ");
                                                               } catch (Exception e) {
                                                                   e.printStackTrace();
                                                               }
                                                               // 对布局文件进行初始化
                                                               RelativeLayout llCancel = (RelativeLayout) view
                                                                       .findViewById(R.id.ll_cancel);
                                                               // 对布局中的控件添加事件监听
                                                               TextView tvCancel = (TextView) llCancel
                                                                       .findViewById(R.id.tv_popup_window_cancel);
                                                               tvCancel.setText("取消");

                                                               llCancel.setOnClickListener(new View.OnClickListener() {
                                                                   @Override
                                                                   public void onClick(View v) {
                                                                       savePopupWindow.dismiss();
                                                                   }
                                                               });
                                                               RelativeLayout llOK = (RelativeLayout) view
                                                                       .findViewById(R.id.ll_ok);
                                                               // 对布局中的控件添加事件监听

                                                               TextView tvOk = (TextView) llOK
                                                                       .findViewById(R.id.tv_popup_window_ok);
                                                               tvOk.setText("确定");
                                                               llOK.setOnClickListener(new View.OnClickListener() {
                                                                   @Override
                                                                   public void onClick(View v) {
                                                                       ProcessDialogUtils.showProcessDialog(SalesOutStorageScanNewActivity.this);
                                                                       savePopupWindow.dismiss();
                                                                       saveScanData();
                                                                   }
                                                               });
                                                           }
                                                       });
                                               savePopupWindow.show(false,btnOutStorageSave
                                                       , 0, 0);




                                           }
                                       }
        );


       //出库单查询
        tvRight.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SalesOutStorageScanNewActivity.this, SalesOutStorageScanListActivity.class);
                startActivityForResult(intent, 100);
            }
        });

        //关闭抽屉
        tvDrawerClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.switchStatus();
            }
        });

        //重置数据
        tvDrawerReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                edtSaleNo.setText("");
                edtCustomerCode.setText("");
                edtCustomerName.setText("");
                edtCustomerAddress.setText("");
                mStartDate=CommonUtil.getOldDate(-1);
                tvStartDate.setText(mStartDate);
                mEndDate=CommonUtil.getCurrentDate();
                tvEndDate.setText(mEndDate);
            }
        });
        //侧拉抽屉确定按钮监听事件
        tvDrawerSure.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                salesNo = edtSaleNo.getText().toString();
                String startDate = tvStartDate.getText().toString();
                String endDate = tvEndDate.getText().toString();
                String code = edtCustomerCode.getText().toString();
                String name = edtCustomerName.getText().toString();
                String address = edtCustomerAddress.getText().toString();
               clearData();
               CommonUtil.refresh=false;
                Bundle b = new Bundle();
                salesId="";
                salesNo="";
                b.putString("saleNo", salesNo);
                b.putString("startDate", startDate);
                b.putString("endDate", endDate);
                b.putString("customerCode", code);
                b.putString("customerName", name);
                b.putString("address", address);
                intent.putExtras(b);
                intent.setClass(SalesOutStorageScanNewActivity.this, OutInventoryNoSearchActivity.class);
                startActivityForResult(intent, 100);
                drawerLayout.switchStatus();

            }
        });

        //开始日期点击事件
        tvStartDate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //修改bug，只能弹出一个日厉，存在一个true；
                startDateFlag = true;
                endDateFlag = false;
                if (TextUtils.isEmpty(mStartDate)) {
                    mTimePickerInfo.show(new Date());
                } else {
                    mTimePickerInfo.show(DateTimeUtils.stringToDate(mStartDate, "yyyy-MM-dd"));
                }
            }
        });

        //结束日期点击事件
        tvEndDate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //修改bug，只能弹出一个日厉，存在一个true；
                startDateFlag = false;
                endDateFlag = true;
                if (TextUtils.isEmpty(mEndDate)) {
                    mTimePickerInfo.show(new Date());
                } else {
                    mTimePickerInfo.show(DateTimeUtils.stringToDate(mEndDate, "yyyy-MM-dd"));
                }
            }
        });

    }

    /**
     * 保存出库
     */
    private void saveOutStorageData() {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("Action", "SaveDeliveryScanForMobile");
        map.put("AccountCode", mAccountCode);
        map.put("UserCode", mUserCode);
        map.put("UserPassword", mPassword);
        map.put("SessionKey", mSessionKey);

        try {
            JSONArray orderArray = new JSONArray();
            JSONObject orderInfoObj = new JSONObject();
            orderInfoObj.put("ScanFlag", "0");
            orderInfoObj.put("SalesID", salesId);
            orderInfoObj.put("CustomerID", customerId);
            orderInfoObj.put("CustomerCode", customerCode);
            orderInfoObj.put("CustomerName", customerName);
            orderInfoObj.put("StaffID", staffId);
            orderInfoObj.put("OrganizationID", orderInfoFragment.mDepartmentId);
            orderInfoObj.put("OrganizationCode", orderInfoFragment.mDepartmentCode);
            orderInfoObj.put("Contacts", contacts);
            orderInfoObj.put("OtherContact", otherContacts);
            orderInfoObj.put("Telephone", telephone);
            orderInfoObj.put("Address", address);
            orderInfoObj.put("Driver", orderInfoFragment.staffId);
            orderInfoObj.put("Docker", orderInfoFragment.dockId);
            orderInfoObj.put("Remarks", orderInfoFragment.edtRemarks.getText().toString());
            orderInfoObj.put("FeeAmount", "0");
            orderArray.put(orderInfoObj);
            map.put("SalesEntity", orderArray.toString());

            JSONArray detailArray = new JSONArray();
            if (orderGoodsDetailList != null && orderGoodsDetailList.size() > 0) {
                for (int i = 0; i < orderGoodsDetailList.size(); i++) {
                    JSONObject detailObj = new JSONObject();
                    SalesOutStorageOrderGoodsModel goodsModel = orderGoodsDetailList.get(i);
                    detailObj.put("DetailID", goodsModel.getDetailId());
                    detailObj.put("InventoryID", goodsModel.getInventoryId());
                    detailObj.put("SalesQuantity", goodsModel.getSalesQuantity());
                    detailObj.put("OutQuantity", goodsModel.getOutQuantity());
                    detailObj.put("NoOutReturnQuantity", goodsModel.getNoOutReturnQuantity());
                    detailObj.put("Code", goodsModel.getCode());
                    detailObj.put("Remarks", goodsModel.getRemarks());
                    detailArray.put(detailObj);
                }
                map.put("SalesDetailEntity", detailArray.toString());
            }

            JSONArray detailTotalArray = new JSONArray();
            if (orderSelectedList != null && orderSelectedList.size() > 0) {
                for (int i = 0; i < orderSelectedList.size(); i++) {
                    JSONObject detailObj = new JSONObject();
                    SalesOutStorageOrderGoodsModel goodsModel = orderSelectedList.get(i);
                    if (goodsModel.getProductCodeFlag() == 0) {

                        detailObj.put("InventoryID", goodsModel.getInventoryId());
                        detailObj.put("Code", goodsModel.getCode());
                        detailObj.put("OnlyCode", goodsModel.getOnlyCode());
                        detailObj.put("GradeID", goodsModel.getGradeId());
                        detailObj.put("ColorNumber", goodsModel.getColorNumber());
                        detailObj.put("WarehouseID", goodsModel.getWarehouseId());
                        detailObj.put("Specification", goodsModel.getSpecification());
                        detailObj.put("PositionNumber", goodsModel.getPositionNumber());
                        detailObj.put("Box", goodsModel.getBox());
                        detailObj.put("Piece", goodsModel.getPiece());
                        detailObj.put("Package", goodsModel.getPackageValue() + "");
                        detailObj.put("Quantity", goodsModel.getDeliveryQuantity());
                        detailTotalArray.put(detailObj);
                    }

                }

                map.put("CodeEntity", detailTotalArray.toString());
            }

            JSONArray goodsDetailArray = new JSONArray();
            map.put("ScanCodeEntity", goodsDetailArray.toString());

            presenter.saveOutStorageData(this, map, mServerAddressIp, mServerAddressPort);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 保存扫描数据
     */
    private void saveScanData() {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("Action", "SaveDeliveryScanForMobile");
        map.put("AccountCode", mAccountCode);
        map.put("UserCode", mUserCode);
        map.put("UserPassword", mPassword);
        map.put("SessionKey", mSessionKey);
        map.put("LicenseCode", mLicenseCode);

        try {
            JSONArray salesArray = new JSONArray();
            JSONObject orderInfoObj = new JSONObject();
            orderInfoObj.put("ScanFlag", "1");
            orderInfoObj.put("SalesID", salesId);
            orderInfoObj.put("CustomerID", customerId);
            orderInfoObj.put("CustomerCode", customerCode);
            orderInfoObj.put("CustomerName", customerName);
            orderInfoObj.put("StaffID", staffId);
            orderInfoObj.put("OrganizationID", orderInfoFragment.mDepartmentId);
            orderInfoObj.put("OrganizationCode", orderInfoFragment.mDepartmentCode);
            orderInfoObj.put("Contacts", contacts);
            orderInfoObj.put("OtherContact", otherContacts);
            orderInfoObj.put("Telephone", telephone);
            orderInfoObj.put("Address", address);
            orderInfoObj.put("Driver", orderInfoFragment.staffId);
            orderInfoObj.put("Docker", orderInfoFragment.dockId);
            orderInfoObj.put("Remarks", orderInfoFragment.edtRemarks.getText().toString());
            orderInfoObj.put("FeeAmount", "0");
            salesArray.put(orderInfoObj);
            map.put("SalesEntity", salesArray.toString());

            JSONArray detailArray = new JSONArray();
            if (orderGoodsDetailList != null && orderGoodsDetailList.size() > 0) {
                for (int i = 0; i < orderGoodsDetailList.size(); i++) {
                    JSONObject detailObj = new JSONObject();
                    SalesOutStorageOrderGoodsModel goodsModel = orderGoodsDetailList.get(i);
                    detailObj.put("DetailID", goodsModel.getDetailId());
                    detailObj.put("InventoryID", goodsModel.getInventoryId());
                    detailObj.put("SalesQuantity", goodsModel.getSalesQuantity());
                    detailObj.put("OutQuantity", goodsModel.getOutQuantity());
                    detailObj.put("NoOutReturnQuantity", goodsModel.getNoOutReturnQuantity());
                    detailObj.put("Code", goodsModel.getCode());
                    detailObj.put("Remarks", goodsModel.getRemarks());
                    detailArray.put(detailObj);
                }
                map.put("SalesDetailEntity", detailArray.toString());
            }

            JSONArray detailTotalArray = new JSONArray();
            if (orderSelectedList != null && orderSelectedList.size() > 0) {
                for (int i = 0; i < orderSelectedList.size(); i++) {
                    JSONObject detailObj = new JSONObject();
                    SalesOutStorageOrderGoodsModel goodsModel = orderSelectedList.get(i);
                    if (goodsModel.getProductCodeFlag() == 1) {

                        detailObj.put("InventoryID", goodsModel.getInventoryId());
                        detailObj.put("Code", goodsModel.getCode());
                        detailObj.put("OnlyCode", goodsModel.getOnlyCode());
                        detailObj.put("GradeID", goodsModel.getGradeId());
                        detailObj.put("ColorNumber", goodsModel.getColorNumber());
                        detailObj.put("WarehouseID", goodsModel.getWarehouseId());
                        detailObj.put("Specification", goodsModel.getSpecification());
                        detailObj.put("PositionNumber", goodsModel.getPositionNumber());
                        detailObj.put("Box", goodsModel.getBox());
                        detailObj.put("Piece", goodsModel.getPiece());
                        detailObj.put("Package", goodsModel.getPackageValue() + "");
                        detailObj.put("Quantity", goodsModel.getDeliveryQuantity());
                        detailTotalArray.put(detailObj);
                    }

                }

                map.put("CodeEntity", detailTotalArray.toString());
            }
            JSONArray goodsDetailArray = new JSONArray();
            map.put("ScanCodeEntity", goodsDetailArray.toString());

          //  JSONArray goodsDetailArray = new JSONArray();
//            if (scanList != null && scanList.size() > 0) {
//                for (int i = 0; i < scanList.size(); i++) {
//                    JSONObject detailObj = new JSONObject();
//                    ScanInformationBean scanModel = scanList.get(i);
//
//                    detailObj.put("InventoryID", scanModel.getInventoryID());
//                    detailObj.put("CodeID", scanModel.getCodeID());
//                    detailObj.put("Specification", scanModel.getSpecification());
//                    detailObj.put("ColorNumber", scanModel.getColorNumber());
//                    detailObj.put("WarehouseID", scanModel.getWarehouseID());
//                    detailObj.put("PositionNumber", scanModel.getPositionNumber());
//                    detailObj.put("Box", scanModel.getBoxQuantity());
//                    detailObj.put("Piece", scanModel.getOddQuantity());
//                    detailObj.put("DeliveryQuantity", scanModel.getDeliveryQuantity());
//                    detailObj.put("GoodQrCodeDetailID", scanModel.getDetailId());
//                    detailObj.put("BatchNo", scanModel.getBatchNo());
//                    detailObj.put("RandomCode", scanModel.getRandomCode());
//                    detailObj.put("ProductCodeDate", scanModel.getProductCodeDate());
//                    goodsDetailArray.put(detailObj);
//                }
//                map.put("ScanCodeEntity", goodsDetailArray.toString());
//            }
            presenter.saveData(this, map, mServerAddressIp, mServerAddressPort);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /*
     * 条码控件设置为可编辑
     */
    public void setEdt() {
        edtBarcode.setText("");
        edtBarcode.setEnabled(true);
        edtBarcode.setFocusable(true);
        edtBarcode.setFocusableInTouchMode(true);
        edtBarcode.requestFocus();
        edtBarcode.findFocus();
        edtBarcode.setFilters(new InputFilter[]{new InputFilter() {
            @Override
            public CharSequence filter(CharSequence source,
                                       int start, int end, Spanned dest,
                                       int dstart, int dend) {
                return null;
            }
        }});


    }

    /**
     * 加载明细数据
     * @param salesId
     * @param salesNo
     */
    private void loadSalesDetailData(String salesId,String salesNo) {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("Action", "GetSalesDetailForMobile");
        map.put("AccountCode", mAccountCode);
        map.put("UserCode", mUserCode);
        map.put("UserPassword", mPassword);
        map.put("SessionKey", mSessionKey);
        map.put("SalesID", salesId);
        map.put("SalesNo", salesNo);
        presenter.loadSalesDetailData(this, map, mServerAddressIp, mServerAddressPort);
    }

    /**
     * 明细数据回调事件
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //获取产品明细数据
        if (requestCode == 100 && resultCode == 101) {
            edtBarcode.setFocusable(true);
            edtBarcode.setFocusableInTouchMode(true);
            edtBarcode.requestFocus();
            edtBarcode.findFocus();
            salesId = data.getExtras().getString("SalesId");
           clearData();

            ProcessDialogUtils.showProcessDialog(SalesOutStorageScanNewActivity.this);
            loadSalesDetailData(salesId,"");

        }


    }

    @Override
    public void onResume()
    {
        if(CommonUtil.refresh)
        {
           clearData();
            if(!TextUtils.isEmpty(salesId)||!TextUtils.isEmpty(salesNo)) {
                ProcessDialogUtils.showProcessDialog(SalesOutStorageScanNewActivity.this);
                loadSalesDetailData(salesId, salesNo);
            }
        }
        super.onResume();
    }

    /**
     * 销售单明细加载成功
     * @param result
     */
    @Override
    public void outStorageDetailLoadSuccess(String result) {
        ProcessDialogUtils.closeProgressDilog();
        CommonUtil.refresh=false;
        try {
            JSONObject jobj = new JSONObject(result);
            int status = jobj.optInt("Status");
            String message = jobj.optString("Message");

            if (status == 0) {
                JSONObject resultObj = jobj.optJSONObject("Result");
                JSONArray tableArray = resultObj.optJSONArray("Table");
                JSONArray detailArray1 = resultObj.optJSONArray("Table1");
                JSONArray detailArray2 = resultObj.optJSONArray("Table2");
                if (tableArray != null && tableArray.length() > 0) {
                    JSONObject tableObj = (JSONObject) tableArray.opt(0);
                      customerId = tableObj.optString("CustomerID");
                    customerCode = tableObj.optString("CustomerCode");
                    customerName = tableObj.optString("CustomerName");
                    warehouseName=tableObj.optString("WarehouseName");
                    int salesOrderStatus=tableObj.optInt("Status");
                    if(salesOrderStatus==7)
                    {
                        Toast.makeText(SalesOutStorageScanNewActivity.this,"该销售单已出库完成",Toast.LENGTH_SHORT).show();
                        salesId="";
                        etSalesSlipNo.setText("");
                        etSalesSlipNo.setFocusable(true);
                        etSalesSlipNo.requestFocus();
                        clearData();
                         return;
                    }
                    if(warehouseName!=null&&warehouseName.length()>0)
                    {
                        warehouseName=  warehouseName.substring(0,warehouseName.length()-1);
                    }
                    etSalesSlipNo.setText(customerName);
                    staffId = tableObj.optString("StaffID");
                    telephone = tableObj.optString("Telephone");
                    address = tableObj.optString("Address");
                    contacts = tableObj.optString("Contacts");
                    otherContacts = tableObj.optString("OtherContact");
                    salesId = tableObj.optString("SalesID");
                    String organizationId=tableObj.optString("OrganizationID");
                    String organizationCode=tableObj.optString("organizationCode");
                    String organizationName=tableObj.optString("OrganizationName");
                    String salesNo=tableObj.optString("SalesNo");
                    String createDate=tableObj.optString("AccountDate");
                    createDate = CommonUtil.commonDateConverter(createDate);
                    searchBean = new SaleOrderDetail();
                    searchBean.setCustomerCode(customerCode);
                    searchBean.setWarehouseName(warehouseName);
                    searchBean.setCustomerName(customerName);
                    searchBean.setTelephone(telephone);
                    searchBean.setAddress(address);
                    searchBean.setSalesNo(salesNo);
                    searchBean.setCreateDate(createDate);
                    orderInfoFragment.mDepartmentId=organizationId;
                    orderInfoFragment.mDepartmentCode=organizationCode;
                    orderInfoFragment.mDepartmentName=organizationName;
                    orderInfoFragment.loadData(searchBean);
                    edtBarcode.setFocusable(true);
                    edtBarcode.requestFocus();

                }
                else
                {
                    etSalesSlipNo.setFocusable(true);
                    etSalesSlipNo.requestFocus();
                    Toast.makeText(getApplicationContext(),"此销售单不存在",Toast.LENGTH_SHORT).show();
                    CommonUtil.setDefaultSound(SalesOutStorageScanNewActivity.this);
                    return;
                }

                if (detailArray1 != null && detailArray1.length() > 0) {
                    for (int i = 0; i < detailArray1.length(); i++) {
                        JSONObject obj = (JSONObject) detailArray1.opt(i);

                        String inventoryId = obj.optString("InventoryID");
                        String detailId = obj.optString("DetailID");
                        String code = obj.optString("Code");
                        String codeId = obj.optString("CodeID");
                        String onlyCode = obj.optString("OnlyCode");
                        String colorNumber = obj.optString("ColorNumber");
                        String warehouseId = obj.optString("WarehouseID");
                        String warehouseName = obj.optString("WarehouseName");
                        String specification = obj.optString("Specification");
                        String gradeId = obj.optString("GradeID");
                        String gradeName = obj.optString("GradeName");
                        String positionNumber = obj.optString("PositionNumber");
                        String remarks = obj.optString("Remarks");
                        int packageValue = obj.optInt("Package");
                        double salesQuantity = obj.optDouble("SalesQuantity");
                        int outQuantity = obj.optInt("OutQuantity");
                        int noOutReturnQuantity=obj.optInt("NoOutReturnQuantity");
                        double deliveryQuantity = salesQuantity - outQuantity-noOutReturnQuantity;
                        int salesBox = (int) (salesQuantity / packageValue);
                        int salesPiece = (int) (salesQuantity % packageValue);
                        int outBox = (int) (outQuantity / packageValue);
                        int outPiece = (int) (outQuantity % packageValue);
                        int deliveryBox = (int) (deliveryQuantity / packageValue);
                        int deliveryPiece = (int) (deliveryQuantity % packageValue);

                        SalesOutStorageOrderGoodsModel orderGoodsModel = new SalesOutStorageOrderGoodsModel();
                        orderGoodsModel.setDetailId(detailId);
                        orderGoodsModel.setInventoryId(inventoryId);
                        orderGoodsModel.setCode(code);
                        orderGoodsModel.setCheckedStatus(true);
                        orderGoodsModel.setCodeId(codeId);
                        orderGoodsModel.setOnlyCode(onlyCode);
                        orderGoodsModel.setGradeId(gradeId);
                        orderGoodsModel.setGrade(gradeName);
                        orderGoodsModel.setPositionNumber(positionNumber);
                        orderGoodsModel.setColorNumber(colorNumber);
                        orderGoodsModel.setSpecification(specification);
                        orderGoodsModel.setWarehouseId(warehouseId);
                        orderGoodsModel.setNoOutReturnQuantity(String.valueOf(noOutReturnQuantity));
                        orderGoodsModel.setWarehouseName(warehouseName);
                        orderGoodsModel.setPackageValue(packageValue);
                        orderGoodsModel.setSalesQuantity(String.valueOf(salesQuantity));
                        orderGoodsModel.setOutQuantity(String.valueOf(outQuantity));   //已出库数量
                        orderGoodsModel.setSalesBox(String.valueOf(salesBox));
                        orderGoodsModel.setSalesPiece(String.valueOf(salesPiece));
                        if(orderGoodsModel.getProductCodeFlag()==0) {
                            orderGoodsModel.setBox(String.valueOf(deliveryBox));
                        }
                        else
                        {
                            orderGoodsModel.setBox("0") ;
                        }

                        if(orderGoodsModel.getProductCodeFlag()==0) {
                            orderGoodsModel.setPiece(String.valueOf(deliveryPiece));
                        }
                        else
                        {
                            orderGoodsModel.setPiece("0");
                        }
                        orderGoodsModel.setDeliveryQuantity(String.valueOf(deliveryQuantity));
                        orderGoodsModel.setRemarks(remarks);
                        orderGoodsDetailList.add(orderGoodsModel);

                    }
                }

                if (detailArray2 != null && detailArray2.length() > 0) {
                    for (int i = 0; i < detailArray2.length(); i++) {
                        JSONObject obj = (JSONObject) detailArray2.opt(i);

                        String inventoryId = obj.optString("InventoryID");
                        String code = obj.optString("Code");
                        String codeId = obj.optString("CodeID");
                        String onlyCode = obj.optString("OnlyCode");
                        String colorNumber = obj.optString("ColorNumber");
                        String warehouseId = obj.optString("WarehouseID");
                        String warehouseName = obj.optString("WarehouseName");
                        String specification = obj.optString("Specification");
                        String gradeId = obj.optString("GradeID");
                        String gradeName = obj.optString("GradeName");
                        String positionNumber = obj.optString("PositionNumber");
                        int productCodeFlag = obj.optInt("ProductCodeFlag");
                        int packageValue = obj.optInt("Package");
                        int circulateType=obj.optInt("CirculateType");
                        int decimalPlaces=obj.optInt("DecimalPlaces");
                        double acreage=obj.optDouble("Acreage");
                        double salesQuantity = obj.optDouble("SalesQuantity");
                        int outQuantity = obj.optInt("OutQuantity");
                        int noOutReturnQuantity=obj.optInt("NoOutReturnQuantity");
                        double deliveryQuantity = salesQuantity - outQuantity-noOutReturnQuantity;
                        int salesBox = (int) (salesQuantity / packageValue);
                        int salesPiece = (int) (salesQuantity % packageValue);
                       // int deliveryBox = (int) (deliveryQuantity / packageValue);
                      //  int deliveryPiece = (int) (deliveryQuantity % packageValue);
                      String   deliveryQuantityStr= CommonUtil.calculateOrderQuantity(circulateType,decimalPlaces+"",deliveryQuantity+"",acreage);

                        SalesOutStorageOrderGoodsModel orderGoodsModel = new SalesOutStorageOrderGoodsModel();
                        orderGoodsModel.setProductCodeFlag(productCodeFlag);
                        orderGoodsModel.setInventoryId(inventoryId);
                        orderGoodsModel.setCode(code);
                        if(orderGoodsModel.getProductCodeFlag()==0)
                        {
                            orderGoodsModel.setCheckedStatus(true);
                        }
                        else
                        {
                            orderGoodsModel.setCheckedStatus(false);
                        }


                        if(orderGoodsModel.getProductCodeFlag()==0) {

                            CommonUtil.calculateBoxAndPiece(deliveryQuantityStr,orderGoodsModel);
                        }
                        else
                        {
                            orderGoodsModel.setDeliveryQuantity("0");
                            orderGoodsModel.setBox("0") ;
                            orderGoodsModel.setPiece("0");
                        }

                        orderGoodsModel.setNoOutReturnQuantity(String.valueOf(noOutReturnQuantity));
                        orderGoodsModel.setCodeId(codeId);
                        orderGoodsModel.setOnlyCode(onlyCode);
                        orderGoodsModel.setGradeId(gradeId);
                        orderGoodsModel.setGrade(gradeName);
                        orderGoodsModel.setCirculateType(circulateType);
                        orderGoodsModel.setAcreage(acreage);
                        orderGoodsModel.setDecimalPlaces(decimalPlaces);
                        orderGoodsModel.setPositionNumber(positionNumber);
                        orderGoodsModel.setColorNumber(colorNumber);
                        orderGoodsModel.setSpecification(specification);
                        orderGoodsModel.setWarehouseId(warehouseId);
                        orderGoodsModel.setWarehouseName(warehouseName);
                        orderGoodsModel.setPackageValue(packageValue);
                        orderGoodsModel.setSalesQuantity(String.valueOf(salesQuantity));
                        orderGoodsModel.setOutQuantity(String.valueOf(outQuantity));   //已出库数量
                        orderGoodsModel.setSalesBox(String.valueOf(salesBox));
                        orderGoodsModel.setSalesPiece(String.valueOf(salesPiece));

                        //销售量-已出量-待上传量
                        if ((salesQuantity - outQuantity-noOutReturnQuantity ) == 0) {
                            continue;
                        }
                        orderGoodsList.add(orderGoodsModel);

                    }
                    orderGoodsFragment.setData(orderGoodsList);
                }
                else
                {
                    Toast.makeText(getApplicationContext(),"此销售单已出库",Toast.LENGTH_SHORT).show();
                    CommonUtil.setDefaultSound(SalesOutStorageScanNewActivity.this);
                    etSalesSlipNo.setFocusable(true);
                    etSalesSlipNo.requestFocus();
                    return;
                }

            }

            else if (status == Constants.ACTION_RESULT_STATUS
                    ||status == Constants.NegativeThree
                    ||status == Constants.NegativeTwo
                    ||status == Constants.ACTION_INVALID_STATUS) {
                AlertAnimateUtil.showReLoginDialog(
                        SalesOutStorageScanNewActivity.this, "异常登录",
                        message);
            } else {
                ToastUtil.showShortToast(
                        SalesOutStorageScanNewActivity.this,
                        message);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 销售单明细加载失败
     * @param message
     */
    @Override
    public void outStorageDetailLoadFail(String message) {
        ProcessDialogUtils.closeProgressDilog();
        CommonUtil.setDefaultSound(SalesOutStorageScanNewActivity.this);
        ToastUtil.showShortToast(getApplicationContext(), message);
    }


    /*
     * 判断送货单是否包含当前扫描的产品
     */
    private boolean isContainsDeliveryInfo(ScanInformationBean currentBean, List<SalesOutStorageOrderGoodsModel> pickList) {
        boolean isHasSameData = false;
        for (int i = 0; i < pickList.size(); i++) {
            SalesOutStorageOrderGoodsModel scanBean = pickList.get(i);
            if ((Double.parseDouble(currentBean.getInventoryID())) == Double.parseDouble(scanBean.getInventoryId())&& scanBean.getProductCodeFlag() == 1

            )
            {
                isHasSameData = true;
                currentBean.setOutQuantity(scanBean.getOutQuantity());

            }

        }
        return isHasSameData;

    }


    /**
     * 计算出库数量
     * @param scanInfo
     * @param list
     * @param goodsList
     */
    private void calculateDeliveryQuantity(ScanInformationBean scanInfo, List<ScanInformationBean> list, List<SalesOutStorageOrderGoodsModel> goodsList) {

        long totalDeliveryQuantity = 0;

        for (int i = 0; i < goodsList.size(); i++) {
            SalesOutStorageOrderGoodsModel goodsBean = goodsList.get(i);
            if ((scanInfo.getInventoryID().equals(goodsBean.getInventoryId())) && goodsBean.getProductCodeFlag() == 1) {
           double salesQuantity=Double.parseDouble(goodsBean.getSalesQuantity());
           double outQuantity=Double.parseDouble(goodsBean.getOutQuantity());
           double deliveryQuantity=salesQuantity-outQuantity;
           String    deliveryQuantityStr=  CommonUtil.calculateOrderQuantity(goodsBean.getCirculateType(),String.valueOf(goodsBean.getDecimalPlaces()),deliveryQuantity+"",goodsBean.getAcreage());
           CommonUtil. calculateBoxAndPiece(deliveryQuantityStr,goodsBean);
           goodsBean.setCheckedStatus(true);

            }
        }



    }

    /**
     * 清空数据
     */
    private void clearData() {

        if( orderGoodsList!=null&&  orderGoodsList.size()>0)
        {
            orderGoodsList.clear();
        }

        if(orderGoodsDetailList!=null&&orderGoodsDetailList.size()>0)
        {
            orderGoodsDetailList.clear();
        }

     orderGoodsFragment.setData(orderGoodsList);


        orderInfoFragment.tvCustomerAddress.setText("");
        orderInfoFragment.tvCustomerName.setText("");
        orderInfoFragment.tvTelephone.setText("");
       orderInfoFragment.tvCustomerCode.setText("");
        orderInfoFragment.tvWarehouse.setText("");
        orderInfoFragment.tvSalesNo.setText("");
        orderInfoFragment.tvCreateDate.setText("");
//        orderInfoFragment.tvDriver.setText("");
//        orderInfoFragment.staffId = "";
//        orderInfoFragment.tvWorker.setText("");
//        orderInfoFragment.dockId = "";
//        orderInfoFragment.tvDepartment.setText("");
//        orderInfoFragment.mDepartmentId = "";
//        orderInfoFragment.mDepartmentCode = "";
//        orderInfoFragment.mDepartmentName = "";
        orderInfoFragment.edtRemarks.setText("");
    }


    /**
     * 扫描成功
     * @param result
     */
    @Override
    public void outStorageScanLoadSuccess(String result) {
        boolean isMaximumQuantity = false;
        currentList = new ArrayList<ScanInformationBean>();
        ProcessDialogUtils.closeProgressDilog();

        setEdt();
        try {
            JSONObject jobj = new JSONObject(result);
            int status = jobj.optInt("Status");
            JSONObject resultObj = (JSONObject) jobj.opt("Result");
            JSONArray detailArray = resultObj.optJSONArray("Table");
            if (detailArray != null && detailArray.length() > 0) {
                for (int i = 0; i < detailArray.length(); i++) {
                    JSONObject detailObj = (JSONObject) detailArray.opt(i);
                    codeId = detailObj.optString("CodeID");
                    String code = detailObj.optString("Code");
                    inventoryId = detailObj.optString("InventoryID");
                    warehouseId = detailObj.optString("WarehouseID");
                    String warehouseName = detailObj.optString("WarehouseName");
                    positionNumber = detailObj.optString("PositionNumber");
                    String onlyCode = detailObj.optString("OnlyCode");
                    int packageValue = detailObj.optInt("Package");
                    gradeId = detailObj.optString("GradeID");
                    String gradeName = detailObj.optString("GradeName");
                    specification = detailObj.optString("Specification");
                    colorNumber = detailObj.optString("ColorNumber");
                    String detailId = detailObj.optString("DetailID");
                    String batchNo = detailObj.optString("BatchNo");
                    ScanInformationBean bean = new ScanInformationBean();
                    bean.setDetailId(detailId);
                    bean.setCodeID(codeId);
                    bean.setCode(code);
                    bean.setInventoryID(inventoryId);
                    bean.setWarehouseID(warehouseId);
                    bean.setWarehouseName(warehouseName);
                    bean.setPositionNumber(positionNumber);
                    bean.setOnlyCode(onlyCode);
                    bean.setPackageQuantity(String.valueOf(packageValue));
                    bean.setGradeId(gradeId);
                    bean.setGradeName(gradeName);
                    bean.setSpecification(specification);
                    bean.setColorNumber(colorNumber);
                    bean.setBatchNo(batchNo);

                    currentList.add(bean);
                }

                List<SalesOutStorageOrderGoodsModel> pickList = orderGoodsList;

                if (currentList.size() > 0) {

                    ScanInformationBean scanInfoBean = currentList.get(0);

                    boolean isHasSameData = isContainsDeliveryInfo(scanInfoBean, pickList);

                    if (!isHasSameData) {
                        CommonUtil.setDefaultSound(SalesOutStorageScanNewActivity.this);
                        Toast.makeText(SalesOutStorageScanNewActivity.this, "该销售单没有该商品或已出库", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    //计算送货数量
                  calculateDeliveryQuantity(scanInfoBean, currentList, pickList);


                    for (int i = 0; i < orderGoodsFragment.orderGoodsList.size(); i++) {
                        SalesOutStorageOrderGoodsModel goodsBean = orderGoodsFragment.orderGoodsList.get(i);
                        if ((scanInfoBean.getInventoryID().equals(goodsBean.getInventoryId())) )
                            if (Double.parseDouble(goodsBean.getOutQuantity()) == Double.parseDouble(goodsBean.getSalesQuantity())) {
                                Toast.makeText(SalesOutStorageScanNewActivity.this, "该商品已出库", Toast.LENGTH_SHORT).show();
                                CommonUtil.setDefaultSound(SalesOutStorageScanNewActivity.this);
                                return;
                            }
                    }

                    //当前剩余送货数量等于0
//                    if (currentQuantity == 0) {
//                        Toast.makeText(this, "该产品出库数量已达到最大值", Toast.LENGTH_SHORT).show();
//                        return;
//                    }

//                    scanInfoBean.setDeliveryQuantity(String.valueOf(deliveryQuantity));
//
//                    String packageQuantity = scanInfoBean.getPackageQuantity();
//                    int box = (int) (deliveryQuantity / Double.parseDouble(packageQuantity));
//                    int oddNum = (int) (deliveryQuantity % Double.parseDouble(packageQuantity));
//
//                    scanInfoBean.setBoxQuantity(String.valueOf(box));
//                    scanInfoBean.setOddQuantity(String.valueOf(oddNum));
//                    //判断是否达到最大送货数量
//                    isMaximumQuantity = isMaximumQuantity(scanInfoBean, scanList, pickList);
//
//                    if (isMaximumQuantity) {
//                        Toast.makeText(SalesOutStorageScanNewActivity.this, "该商品已达到最大送货数量", Toast.LENGTH_SHORT).show();
//                        return;
//                    }
                   // scanList.add(scanInfoBean);
                   // long totalQuantity = calculateCurrentQuantity(scanInfoBean, scanList);

                    //updateGoodsScanedQuantity(totalQuantity, scanInfoBean, pickList);
                    orderGoodsFragment.setData(pickList);



                }
                  for(int i=0;i<orderGoodsFragment.orderGoodsList.size();i++){
                      if(inventoryId.equals(orderGoodsFragment.orderGoodsList.get(i).getInventoryId()) ) {

                          orderGoodsFragment.goodsLst.setSelection(i);

                      }

                  }

            }
            else
            {
                CommonUtil.setDefaultSound(SalesOutStorageScanNewActivity.this);
                Toast.makeText(SalesOutStorageScanNewActivity.this,"此商品不存在",Toast.LENGTH_SHORT).show();
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 扫描失败
     * @param message
     */
    @Override
    public void outStorageScanLoadFail(String message) {
        ProcessDialogUtils.closeProgressDilog();
        setEdt();
        CommonUtil.setDefaultSound(SalesOutStorageScanNewActivity.this);
        ToastUtil.showShortToast(getApplicationContext(), message);
    }

    @Override
    public void saveSuccess(String result) {

        ProcessDialogUtils.closeProgressDilog();
        try {
            JSONObject jobj = new JSONObject(result);
            int status = jobj.optInt("Status");
            if (status == 0) {
                Toast.makeText(getApplicationContext(), "保存成功", Toast.LENGTH_SHORT).show();
                CommonUtil.playSound(SalesOutStorageScanNewActivity.this);
                clearData();
                edtBarcode.setFocusable(true);
                edtBarcode.requestFocus();
                loadSalesDetailData(salesId,"");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 保存失败
     * @param message
     */
    @Override
    public void saveFail(String message) {
        ProcessDialogUtils.closeProgressDilog();
        CommonUtil.setDefaultSound(SalesOutStorageScanNewActivity.this);
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
    }

    /**
     * 保存成功
     * @param result
     */
    @Override
    public void saveOutStorageSuccess(String result) {

        ProcessDialogUtils.closeProgressDilog();
        try {
            JSONObject jobj = new JSONObject(result);
            int status = jobj.optInt("Status");
            if (status == 0) {
                Toast.makeText(getApplicationContext(), "保存成功", Toast.LENGTH_SHORT).show();
                CommonUtil.playSound(SalesOutStorageScanNewActivity.this);
                clearData();
                loadSalesDetailData(salesId,"");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 保存失败
     * @param message
     */
    @Override
    public void saveOutStorageFail(String message) {
        ProcessDialogUtils.closeProgressDilog();
        CommonUtil.setDefaultSound(SalesOutStorageScanNewActivity.this);
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();

    }
}
