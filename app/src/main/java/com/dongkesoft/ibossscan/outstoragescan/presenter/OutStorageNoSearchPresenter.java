package com.dongkesoft.ibossscan.outstoragescan.presenter;


import android.content.Context;

import com.dongkesoft.ibossscan.outstoragescan.view.IOutStorageNoSearchView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import dongkesoft.com.dkmodule.bean.ReqCallBack;
import dongkesoft.com.dkmodule.common.Constants;
import dongkesoft.com.dkmodule.mvp.presenter.BasePresenter;
import dongkesoft.com.dkmodule.utils.OkAsyncHttpClient;

public class OutStorageNoSearchPresenter   extends BasePresenter<IOutStorageNoSearchView> {

    public OutStorageNoSearchPresenter()
    {

    }


    public void loadSalesSlipData(Context context, HashMap<String, String> map, String ip, String port) {

        OkAsyncHttpClient.getInstance(context).requestPostByAsynWithForm(String.format(Constants.LOGIN_URL,
                ip, port), map, new ReqCallBack<String>() {
            @Override
            public void onReqSuccess(final String result) {
                {
                    //需要在UI线程执行
                    mViewHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            // 逻辑判断
                            try {
                                JSONObject obj = new JSONObject(result);

                                int status = obj.optInt("Status");
                                String message = obj.optString("Message");
                                //
                                if (status == 0) {
                                    getView().outStorageLoadSuccess(result);
                                } else {
                                    getView().outStorageLoadFail(message);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            }

            @Override
            public void onReqFailed(final String errorMsg) {
                {
                    mViewHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            getView().outStorageLoadFail(errorMsg);
                        }
                    });
                }
            }
        });
    }
}
