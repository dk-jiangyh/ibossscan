package com.dongkesoft.ibossscan.outstoragescan.bean;

import java.io.Serializable;

public class ScanInformationBean implements Serializable {
    /**
     * 追溯品 1为可追溯
     */
    private int salesTraceFlag;
    /*
    * 编码名称
    */
    private String CodeName;
    /*
    * 编码
    */
    private String Code;

    /*
    * 唯一编码
    */
    private String OnlyCode;
    /*
    * 仓库名称
    */
    private String WarehouseName;

    private String OrderNo;

    public String getUnitId() {
        return unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }

    private String unitId;

    public String getRandomCode() {
        return randomCode;
    }

    public void setRandomCode(String randomCode) {
        this.randomCode = randomCode;
    }

    private String randomCode;

    public String getDetailId() {
        return detailId;
    }

    public void setDetailId(String detailId) {
        this.detailId = detailId;
    }

    private String detailId;

    public String getPositionNumber() {
        return positionNumber;
    }

    public void setPositionNumber(String positionNumber) {
        this.positionNumber = positionNumber;
    }

    private String positionNumber;

    public String getGradeId() {
        return gradeId;
    }

    public void setGradeId(String gradeId) {
        this.gradeId = gradeId;
    }

    public String getGradeName() {
        return gradeName;
    }

    public void setGradeName(String gradeName) {
        this.gradeName = gradeName;
    }

    private String gradeId;

    private String gradeName;


    public String getSpecification() {
        return specification;
    }

    public void setSpecification(String specification) {
        this.specification = specification;
    }

    public String getColorNumber() {
        return colorNumber;
    }

    public void setColorNumber(String colorNumber) {
        this.colorNumber = colorNumber;
    }

    private String specification;

    private String colorNumber;

    public String getProductCodeDate() {
        return productCodeDate;
    }

    public void setProductCodeDate(String productCodeDate) {
        this.productCodeDate = productCodeDate;
    }

    private String productCodeDate;


    public String getNoSalesQuantity() {
        return noSalesQuantity;
    }

    public void setNoSalesQuantity(String noSalesQuantity) {
        this.noSalesQuantity = noSalesQuantity;
    }

    private String noSalesQuantity;


    public String getDeliveryNo() {
        return DeliveryNo;
    }

    public void setDeliveryNo(String deliveryNo) {
        DeliveryNo = deliveryNo;
    }

    private String DeliveryNo;



    public String getOutInventoryNo() {
        return OutInventoryNo;
    }

    public void setOutInventoryNo(String outInventoryNo) {
        OutInventoryNo = outInventoryNo;
    }

    private String OutInventoryNo;

    public String getOrderNo() {
        return OrderNo;
    }

    public void setOrderNo(String orderNo) {
        OrderNo = orderNo;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    private String remarks;

    public String getOutBoxQuantity() {
        return outBoxQuantity;
    }

    public void setOutBoxQuantity(String outBoxQuantity) {
        this.outBoxQuantity = outBoxQuantity;
    }

    public String getOutOddQuantity() {
        return outOddQuantity;
    }

    public void setOutOddQuantity(String outOddQuantity) {
        this.outOddQuantity = outOddQuantity;
    }

    private String outBoxQuantity;

    private String outOddQuantity;

    /*
    * 仓库id
    */
    private String WarehouseID;

    public String getOutInventoryId() {
        return OutInventoryId;
    }

    public void setOutInventoryId(String outInventoryId) {
        OutInventoryId = outInventoryId;
    }

    private String OutInventoryId;

    //出库批次明细ID
    private String OutBatchDetailID;

    public String getOutInventoryDetailId() {
        return OutInventoryDetailId;
    }

    public void setOutInventoryDetailId(String outInventoryDetailId) {
        OutInventoryDetailId = outInventoryDetailId;
    }

    private String OutInventoryDetailId;

    public String getOutBatchDetailID() {
        return OutBatchDetailID;
    }

    public void setOutBatchDetailID(String outBatchDetailID) {
        OutBatchDetailID = outBatchDetailID;
    }

    /*
    * 送货数量
    */
    private String DeliveryQuantity;

    public String getRemainQuantity() {
        return RemainQuantity;
    }

    public void setRemainQuantity(String remainQuantity) {
        RemainQuantity = remainQuantity;
    }

    private String RemainQuantity;

    public String getOrderTotalQuantity() {
        return OrderTotalQuantity;
    }

    public void setOrderTotalQuantity(String orderTotalQuantity) {
        OrderTotalQuantity = orderTotalQuantity;
    }

    private String OrderTotalQuantity;

    /*
    * 批次号
    */
    private String BatchNo;

    /*
    * 送货单id
    */
    private String DeliveryID;

    /*
    * 编码id
    */
    private String CodeID;

    /*
    * 仓库id
    */
    private String InventoryID;

    /*
    * 选中状态
    */
    private boolean CheckStatus;

    public String getSalesQuantity() {
        return salesQuantity;
    }

    public void setSalesQuantity(String salesQuantity) {
        this.salesQuantity = salesQuantity;
    }

    private String salesQuantity;

    public String getInputQuantity() {
        return inputQuantity;
    }

    public void setInputQuantity(String inputQuantity) {
        this.inputQuantity = inputQuantity;
    }

    private String inputQuantity;

    /*
    * 订单数量
    */
    private String OrderQuantity;

    /*
    * 出库数量
    */
    private String outQuantity;

    private String boxQuantity;


    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    private String orderId;


    public String getBoxQuantity() {
        return boxQuantity;
    }

    public void setBoxQuantity(String boxQuantity) {
        this.boxQuantity = boxQuantity;
    }

    public String getOddQuantity() {
        return oddQuantity;
    }

    public void setOddQuantity(String oddQuantity) {
        this.oddQuantity = oddQuantity;
    }

    private String oddQuantity;

    public String getPackageQuantity() {
        return packageQuantity;
    }

    public void setPackageQuantity(String packageQuantity) {
        this.packageQuantity = packageQuantity;
    }

    private String packageQuantity;

    public String getCurrentDate() {
        return currentDate;
    }

    public void setCurrentDate(String currentDate) {
        this.currentDate = currentDate;
    }

    private String currentDate;

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    private String userCode;
    public String getOutQuantity() {
        return outQuantity;
    }

    public void setOutQuantity(String outQuantity) {
        this.outQuantity = outQuantity;
    }

    public String getOrderQuantity() {
        return OrderQuantity;
    }

    public void setOrderQuantity(String orderQuantity) {
        OrderQuantity = orderQuantity;
    }

    public String getWarehouseID() {
        return WarehouseID;
    }

    public void setWarehouseID(String warehouseID) {
        WarehouseID = warehouseID;
    }

    public boolean isCheckStatus() {
        return CheckStatus;
    }

    public void setCheckStatus(boolean checkStatus) {
        CheckStatus = checkStatus;
    }

    public String getCodeName() {
        return CodeName;
    }

    public void setCodeName(String codeName) {
        CodeName = codeName;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String code) {
        Code = code;
    }

    public String getOnlyCode() {
        return OnlyCode;
    }

    public void setOnlyCode(String onlyCode) {
        OnlyCode = onlyCode;
    }

    public String getWarehouseName() {
        return WarehouseName;
    }

    public void setWarehouseName(String warehouseName) {
        WarehouseName = warehouseName;
    }

    public String getDeliveryQuantity() {
        return DeliveryQuantity;
    }

    public void setDeliveryQuantity(String deliveryQuantity) {
        DeliveryQuantity = deliveryQuantity;
    }

    public String getBatchNo() {
        return BatchNo;
    }

    public void setBatchNo(String batchNo) {
        BatchNo = batchNo;
    }

    public String getDeliveryID() {
        return DeliveryID;
    }

    public void setDeliveryID(String deliveryID) {
        DeliveryID = deliveryID;
    }

    public String getCodeID() {
        return CodeID;
    }

    public void setCodeID(String codeID) {
        CodeID = codeID;
    }

    public String getInventoryID() {
        return InventoryID;
    }

    public void setInventoryID(String inventoryID) {
        InventoryID = inventoryID;
    }
}
