package com.dongkesoft.ibossscan.outstoragescan.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.dongkesoft.ibossscan.R;
import com.dongkesoft.ibossscan.outstoragescan.bean.AchievementInfo;


/* 
 * Copyright(c) 2012 dongke All rights reserved. / Confidential
 * 类的信息：
 *		1.程序名称：AchievementAdapter
 *		2.功能描述：业绩信息列表适配
 * 编辑履历：
 *		作者				日期					版本				修改内容
 *		姜永辉				2015/08/26				1.00				新建
 *******************************************************************************/
public class AchievementAdapter extends BaseAdapter {
	private List<AchievementInfo> mList;
	private Context mContext;

	public AchievementAdapter(Context pContext, List<AchievementInfo> pList) {
		this.mContext = pContext;
		this.mList = pList;
	}

	@Override
	public int getCount() {

		return mList.size();
	}

	@Override
	public Object getItem(int position) {

		return mList.get(position);
	}

	@Override
	public long getItemId(int position) {

		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		LayoutInflater _LayoutInflater = LayoutInflater.from(mContext);
		convertView = _LayoutInflater.inflate(R.layout.achievement_item, null);
		if (convertView != null) {
			TextView _TextView1 = (TextView) convertView
					.findViewById(R.id.OrganizationName);
			TextView _TextView2 = (TextView) convertView
					.findViewById(R.id.OrganizationFullName);
			TextView _TextView3 = (TextView) convertView
					.findViewById(R.id.OrganizationID);
			TextView _TextView4 = (TextView) convertView
					.findViewById(R.id.OrganizationCode);
			_TextView1.setText(mList.get(position).getOrganizationName());
			_TextView2.setText(mList.get(position).getOrganizationFullName());
			_TextView3.setText(String.valueOf(mList.get(position)
					.getOrganizationID()));
			_TextView4.setText(mList.get(position).getOrganizationCode());

		}

		return convertView;
	}

}
