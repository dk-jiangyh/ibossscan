/*
 * Copyright(c) 2020 dongke All rights reserved. / Confidential
 * 类的信息：
 *		1.程序名称： SalesOutStorageScanListActivity
 *		2.功能描述：  销售出库列表
 * 编辑履历：
 *		作者				日期					版本				修改内容
 *		dongke			2020/12/01			1.00			新建
 *******************************************************************************/
package com.dongkesoft.ibossscan.outstoragescan.activity;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dongkesoft.ibossscan.R;
import com.dongkesoft.ibossscan.outstoragescan.adapter.SalesOutStorageScanlistAdapter;
import com.dongkesoft.ibossscan.outstoragescan.bean.SalesOutListBean;
import com.dongkesoft.ibossscan.outstoragescan.presenter.OutStoragePresenter;
import com.dongkesoft.ibossscan.outstoragescan.view.IOutStorageListView;
import com.dongkesoft.ibossscan.utils.AlertAnimateUtil;
import com.dongkesoft.ibossscan.utils.CommonUtil;
import com.dongkesoft.ibossscan.utils.GenericDrawerLayout;
import com.dongkesoft.ibossscan.utils.ProcessDialogUtils;
import com.dongkesoft.ibossscan.utils.TimePickerInfo;
import com.dongkesoft.ibossscan.utils.ToastUtil;
import com.dongkesoft.ibossscan.utils.XListViewNew;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import butterknife.BindView;
import dongkesoft.com.dkmodule.common.Constants;
import dongkesoft.com.dkmodule.ui.activity.BaseActivity;
import dongkesoft.com.dkmodule.utils.DateTimeUtils;

/*
 * 销售出库左上角 列表
 * 2020 11
 * */
public class SalesOutStorageScanListActivity extends BaseActivity<IOutStorageListView, OutStoragePresenter> implements XListViewNew.IXListViewListener, IOutStorageListView {
    /**
     * 当前页数
     */
    private int pageNum = 1;

    /**
     * 标题
     */
    @BindView(R.id.tv_center)
    TextView titleTxt;
    /**
     * 返回
     */
    @BindView(R.id.iv_left)
    ImageView iv_left;
    /**
     * 无数据显示
     */
    @BindView(R.id.ll_no_data)
    LinearLayout llNoData;

    @BindView(R.id.tv_right)
    TextView tvRight;
    /**
     * 抽屉
     */
    private GenericDrawerLayout mDrawerLayout;
    /**
     * 抽屉布局
     */
    private View mDrawerLayoutView;
    /**
     * 抽屉打开状态
     */
    private Boolean mDrawerLayoutStatus = false;
    /**
     * 是否显示的是开始时间
     */
    private boolean isStartDate = false;
    /**
     * 临时存放时间
     */
    private String mTempStartDate = "";
    /**
     * 临时存放时间
     */
    private String mTempEndDate = "";
    /**
     * 出库单号
     */
    private EditText etOutNo;
    /**
     * 客户名称
     */
    private EditText etCustomerName;
    /**
     * 开始日期布局
     */
    private RelativeLayout rlStartDate;
    /**
     * 日期空间
     */
    private TimePickerInfo mTimePickerInfo;
    /**
     * 结束日期布局
     */
    private RelativeLayout rlEndDate;

    /**
     * 客户编码
     */
    private EditText etCustomerCode;
    /**
     * 客户地址
     */
    private EditText etAddress;

    private EditText etTelephone;
    /**
     * 关闭抽屉
     */
    private TextView btnClose;
    /**
     * 确认
     */
    private TextView btnSure;
    /**
     * 重置
     */
    private TextView btnReset;
    /**
     * 开始时间
     */
    private TextView tvStartDate;
    /**
     * 结束时间
     */
    private TextView tvEndDate;
    @BindView(R.id.report_listview)
    XListViewNew mListView;
    // 出库单号
    private String mDeliveryNo = "";
    // 销售单号
    private String mSalesNo = "";
    // 客户名称
    private String mCustomerName = "";
    // 客户编码
    private String mCustomerCode = "";
    // 客户地址
    private String mAddress = "";
    //电话
    private String mTelephone = "";
    private boolean ispush = false;

    //财务开始时间
    private String mStartDate = "";
    // 财务结束时间
    private String mEndDate = "";

    // 出库类型
    private String mDeliveryType = "1";

    // 适配器
    private SalesOutStorageScanlistAdapter mAdapter;
    private ArrayList<SalesOutListBean> list;
    private boolean IsLoadMore;
    private String currentDate;


    /**
     * 初始化日期控件
     */
    private void initTimePickerInfo() {
        mTimePickerInfo = new TimePickerInfo(SalesOutStorageScanListActivity.this,
                TimePickerInfo.Type.YEAR_MONTH_DAY);
        mTimePickerInfo.setCyclic(true);
        mTimePickerInfo.setCancelable(true);
        mTimePickerInfo
                .setOnTimeSelectListener(new TimePickerInfo.OnTimeSelectListener() {

                    @SuppressLint("SimpleDateFormat")
                    @Override
                    public void onTimeSelect(Date date) {
                        SimpleDateFormat dateFormater = new SimpleDateFormat(
                                "yyyy-MM-dd");
                        if (isStartDate) {
                            mTempStartDate = dateFormater.format(date);
                            tvStartDate.setText(mTempStartDate);
                        } else {
                            mTempEndDate = dateFormater.format(date);
                            tvEndDate.setText(mTempEndDate);
                        }

                    }
                });
    }

    /**
     * 按钮点击事件
     */
    private void setOnclick() {
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Intent intent = new Intent(SalesOutStorageScanListActivity.this,
                        SalesOutListFragmentNewActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("bean", list.get(position - 1));
                bundle.putString("flag", "2");
                intent.putExtras(bundle);

                startActivityForResult(intent,100);
            }
        });
        mListView.setXListViewListener(this);
        mListView.setPullLoadEnable(true);
        // 左侧返回箭头
        iv_left.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                finish();

            }
        });

        tvRight.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                mDrawerLayout.switchStatus();
            }
        });


        // 取消按钮
        btnClose.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                mDrawerLayout.switchStatus();
            }
        });

        // 结束时间
        rlEndDate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                isStartDate = false;
                mTimePickerInfo.show(new Date());
            }
        });
        // 开始时间
        rlStartDate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                isStartDate = true;
                mTimePickerInfo.show(new Date());
            }
        });


        //侧拉确定
        btnSure.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                pageNum = 1;
                mDeliveryNo = etOutNo.getText().toString();
                mCustomerName = etCustomerName.getText().toString();
                mCustomerCode = etCustomerCode.getText().toString();
                mAddress = etAddress.getText().toString();
                mStartDate = tvStartDate.getText().toString();
                mEndDate = tvEndDate.getText().toString();
                mTelephone = etTelephone.getText().toString();
                if (list != null && list.size() > 0) {
                    list.clear();
                    if (mAdapter != null) {
                        mAdapter.notifyDataSetChanged();
                    }
                }

                loadOutStorageData();
                llNoData.setVisibility(View.GONE);
                mDrawerLayout.switchStatus();
            }
        });

        btnReset.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                etOutNo.setText("");
                etCustomerCode.setText("");
                etCustomerName.setText("");
                etTelephone.setText("");
                etAddress.setText("");
                mStartDate=CommonUtil.getOldDate(-1);
                tvStartDate.setText(mStartDate);
                mEndDate=CommonUtil.getCurrentDate();
                tvEndDate.setText(mEndDate);
            }
        });

    }

    /**
     * 停止刷新
     */
    private void onLoad() {
        mListView.stopRefresh();
        mListView.stopLoadMore();
        currentDate = CommonUtil.getCurrentDateTime();
        mListView.setRefreshTime(currentDate);
    }

    /**
     * 加载出库数据
     */
    private void loadOutStorageData() {
        ProcessDialogUtils.showProcessDialog(SalesOutStorageScanListActivity.this);
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("Action", "GetDeliveryForMobile");
        map.put("AccountCode", mAccountCode);
        map.put("UserCode", mUserCode);
        map.put("UserPassword", mPassword);
        map.put("SessionKey", mSessionKey);
        map.put("DeliveryNo", mDeliveryNo);
        map.put("Telephone", mTelephone);
        map.put("CustomerCode", mCustomerCode);
        map.put("CustomerName", mCustomerName);
        map.put("StartDate", mStartDate);
        map.put("EndDate", mEndDate);
        map.put("Address", mAddress);
        map.put("PageNo", pageNum + "");
        map.put("MaxNumber", "20");
        presenter.loadOutStorageData(this, map, mServerAddressIp, mServerAddressPort);
    }


    /*
     * 初始化抽屉布局
     */
    private void initDrawerLayout() {
        // 可以设置打开时响应Touch的区域范围
        mDrawerLayout.setContentLayout(mDrawerLayoutView);
        mDrawerLayout.setTouchSizeOfOpened(dip2px(this, 500));
        mDrawerLayout.setTouchSizeOfClosed(dip2px(this, 0));
        // 设置随着位置的变更，背景透明度也改变
        mDrawerLayout.setOpaqueWhenTranslating(true);
        // 设置抽屉是否可以打开
        mDrawerLayout.setOpennable(false);
        // 设置抽屉的空白区域大小
        float v = getResources().getDisplayMetrics().density * 50 + 0.5f; // 100DIP
        mDrawerLayout.setDrawerEmptySize((int) v);

        // 设置事件回调
        mDrawerLayout.setDrawerCallback(new GenericDrawerLayout.DrawerCallback() {

            @Override
            public void onTranslating(int gravity, float translation,
                                      float fraction) {
            }

            @Override
            public void onStartOpen() {

            }

            @Override
            public void onStartClose() {
            }

            @Override
            public void onPreOpen() {
            }

            @Override
            public void onEndOpen() {
                mDrawerLayoutStatus = true;
            }

            @Override
            public void onEndClose() {
                mDrawerLayoutStatus = false;
                mDrawerLayout.setOpennable(false);
            }
        });


    }

    /**
     * 工具类
     */
    public int dip2px(Context context, float dipValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dipValue * scale + 0.5f);
    }


    /**
     * 下拉刷新
     */
    @Override
    public void onRefresh() {
        pageNum = 1;
        mListView.setRefreshTime(CommonUtil.getCurrentDateTime());
        if (list != null && list.size() > 0) {
            list.clear();
            if (mAdapter != null) {
                mAdapter.notifyDataSetChanged();
            }
        }
        loadOutStorageData();
     
    }

    /**
     * 上拉加载更多
     */
    @Override
    public void onLoadMore() {

        if (!IsLoadMore) {
            return;
        }
        pageNum++;
        loadOutStorageData();

    }

    /**
     * 返回键处理
     */
    @Override
    public void onBackPressed() {
        if (!mTimePickerInfo.isShowing()) {
            if (!mDrawerLayoutStatus) {
                super.onBackPressed();
            } else {
                mDrawerLayout.switchStatus();
            }
        } else {
            mTimePickerInfo.dismiss();
        }

    }

    /**
     * 加载出库数据
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100 && resultCode == 101) {
            pageNum = 1;
             if(list!=null&&list.size()>0)
             {
                 list.clear();
                 if(mAdapter!=null)
                 {
                     mAdapter.notifyDataSetChanged();
                 }
             }
            loadOutStorageData();

        }
    }

    @Override
    protected OutStoragePresenter createPresenter() {
        return new OutStoragePresenter();
    }

    @Override
    protected int setMvpView() {
        return R.layout.activity_sales_out_storage_scan_list;
    }

    /**
     * 加载布局
     */
    @Override
    protected void initView() {
        titleTxt.setVisibility(View.VISIBLE);

        titleTxt.setText("出库单一览");
        tvRight.setVisibility(View.VISIBLE);
        tvRight.setText("查询");
        iv_left.setVisibility(View.VISIBLE);
        list = new ArrayList<SalesOutListBean>();
        llNoData.setVisibility(View.GONE);


    }

    /**
     * 加载数据
     */
    @Override
    protected void initData() {
        mDrawerLayout = (GenericDrawerLayout) findViewById(R.id.filing_list_activity_drawerlayout);
        mDrawerLayoutView = View.inflate(SalesOutStorageScanListActivity.this,
                R.layout.sale_out_list_drawerlayout, null);
        etOutNo = (EditText) mDrawerLayoutView.findViewById(R.id.et_out_code);
        etCustomerName = (EditText) mDrawerLayoutView
                .findViewById(R.id.et_customer_name);
        etTelephone = (EditText) mDrawerLayoutView
                .findViewById(R.id.et_telephone);
        rlStartDate = (RelativeLayout) mDrawerLayoutView
                .findViewById(R.id.rl_start_date);
        rlEndDate = (RelativeLayout) mDrawerLayoutView
                .findViewById(R.id.rl_end_date);
        tvStartDate = (TextView) mDrawerLayoutView
                .findViewById(R.id.tv_start_date);
        tvEndDate = (TextView) mDrawerLayoutView.findViewById(R.id.tv_end_date);

        mStartDate=CommonUtil.getOldDate(-1);
        tvStartDate.setText(mStartDate);
        mEndDate=CommonUtil.getCurrentDate();
        tvEndDate.setText(mEndDate);

        etCustomerCode = (EditText) mDrawerLayoutView
                .findViewById(R.id.et_customer_code);
        etAddress = (EditText) mDrawerLayoutView.findViewById(R.id.et_address);

        btnClose = (TextView) mDrawerLayoutView.findViewById(R.id.btn_close);
        btnSure = (TextView) mDrawerLayoutView.findViewById(R.id.btn_sure);
        btnReset = (TextView) mDrawerLayoutView.findViewById(R.id.btn_reset);
        pageNum = 1;
        loadOutStorageData();
        initDrawerLayout();
        initTimePickerInfo();
        setOnclick();
    }

    /**
     * 出库单加载成功
     * @param result
     */
    @Override
    public void outStorageLoadSuccess(String result) {
        ProcessDialogUtils.closeProgressDilog();

        try {
            JSONObject resultObj = new JSONObject(result);
            int status = resultObj.optInt("Status");
            String message = resultObj.optString("Message");
            if (status == 0) {
                JSONObject obj = resultObj.optJSONObject("Result");
                JSONArray tableArray = obj.optJSONArray("Table");
                if (tableArray != null && tableArray.length() > 0) {

                    for (int i = 0; i < tableArray.length(); i++) {
                        JSONObject jobj = tableArray.optJSONObject(i);
                        String deliveryId = jobj.optString("DeliveryID");
                        String deliveryNo = jobj.optString("DeliveryNo");
                        String customerCode = jobj.optString("CustomerCode");
                        String customerName = jobj.optString("CustomerName");
                        String telephone = jobj.optString("Telephone");
                        String address = jobj.optString("Address");
                        String accountDate = jobj.optString("AccountDate");
                        accountDate = CommonUtil.commonDateConverter(accountDate);
                        SalesOutListBean salesOutListBean = new SalesOutListBean();
                        salesOutListBean.setDeliveryID(deliveryId);
                        salesOutListBean.setDeliveryNo(deliveryNo);
                        salesOutListBean.setCustomerCode(customerCode);
                        salesOutListBean.setCustomerName(customerName);
                        salesOutListBean.setAddress(address);
                        salesOutListBean.setTelephone(telephone);
                        salesOutListBean.setAccountDate(accountDate);
                        list.add(salesOutListBean);
                    }
                        if(mAdapter==null) {
                            mAdapter = new SalesOutStorageScanlistAdapter(SalesOutStorageScanListActivity.this);
                            mAdapter.setData(list);
                            mListView.setAdapter(mAdapter);
                        }
                        else
                        {
                            mAdapter.setData(list);
                            mAdapter.notifyDataSetChanged();
                    }

                    IsLoadMore = true;
                    mListView.setPullLoadEnable(true);
                    llNoData.setVisibility(View.GONE);
                    onLoad();
                }
                else {
                    onLoad();
                    IsLoadMore = false;
                    mListView.setPullLoadEnable(false);
                    if (list != null && list.size() == 0) {
                        llNoData.setVisibility(View.VISIBLE);
                        AlertAnimateUtil.alertShow(SalesOutStorageScanListActivity.this, "提示", "未找到匹配结果");
                        ProcessDialogUtils.closeProgressDilog();
                        return;
                    }

                }

            } else if (status == Constants.ACTION_RESULT_STATUS
                    || status == Constants.NegativeThree
                    || status == Constants.NegativeTwo
                    || status == Constants.ACTION_INVALID_STATUS) {
                AlertAnimateUtil.showReLoginDialog(
                        SalesOutStorageScanListActivity.this, "异常登录",
                        message);
            } else {
                ToastUtil.showShortToast(
                        SalesOutStorageScanListActivity.this,
                        message);
            }

        } catch (Exception e) {
            e.printStackTrace();

        }

    }

    /**
     * 出库单加载失败
     * @param message
     */
    @Override
    public void outStorageLoadFail(String message) {
        ProcessDialogUtils.closeProgressDilog();
        ToastUtil.showShortToast(SalesOutStorageScanListActivity.this, message);
    }
}
