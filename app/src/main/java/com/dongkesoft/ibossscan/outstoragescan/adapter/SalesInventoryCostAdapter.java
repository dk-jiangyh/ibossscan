/*
 * Copyright(c) 2020 dongke All rights reserved. / Confidential
 * 类的信息：
 *		1.程序名称： SalesInventoryCostAdapter
 *		2.功能描述：  销售出库费用明细适配器
 * 编辑履历：
 *		作者				日期					版本				修改内容
 *		dongke			2020/12/01			1.00			新建
 *******************************************************************************/
package com.dongkesoft.ibossscan.outstoragescan.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.dongkesoft.ibossscan.R;
import com.dongkesoft.ibossscan.outstoragescan.bean.SalesInventoryCostBean;

import java.util.ArrayList;

/**
 *
 */
public class SalesInventoryCostAdapter extends BaseAdapter {
    ViewHolder holder = null;
    private Context context;
    private ArrayList<SalesInventoryCostBean> list;

    @Override
    public int getCount() {

        return list.size();
    }

    @Override
    public Object getItem(int position) {

        return list.get(position);
    }

    @Override
    public long getItemId(int position) {

        return position;
    }
    @Override
    public View getView(int position,View convertView, ViewGroup viewGroup) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.item_sales_out_cost, null);
            holder = new ViewHolder();
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.tv_FeeNo = (TextView) convertView.findViewById(R.id.tv_FeeNo);
        holder.tv_ObjectName = (TextView) convertView.findViewById(R.id.tv_ObjectName);
        holder.tv_ObjectTypeName = (TextView) convertView.findViewById(R.id.tv_ObjectTypeName);
        holder.tv_FeeItemName = (TextView) convertView.findViewById(R.id.tv_FeeItemName);
        holder.tv_FeeSum = (TextView) convertView.findViewById(R.id.tv_FeeSum);
        holder.tv_ConfirmFeeSum = (TextView) convertView.findViewById(R.id.tv_ConfirmFeeSum);
        holder.tv_FeeAuditFlag = (TextView) convertView.findViewById(R.id.tv_FeeAuditFlag);
        holder.tv_CheckUserName = (TextView) convertView.findViewById(R.id.tv_CheckUserName);
        holder.tv_InvoiceStatusName = (TextView) convertView.findViewById(R.id.tv_InvoiceStatusName);
        holder.tv_Remarks = (TextView) convertView.findViewById(R.id.tv_Remarks);

        holder.tv_FeeNo.setText(list.get(position).getFeeNo());
        holder.tv_ObjectName.setText(list.get(position).getObjectName());
        holder.tv_ObjectTypeName.setText(list.get(position).getObjectTypeName());
        holder.tv_FeeItemName.setText(list.get(position).getFeeItemName());
        holder.tv_FeeSum.setText(list.get(position).getFeeSum());
        holder.tv_ConfirmFeeSum.setText(list.get(position).getConfirmFeeSum());
        holder.tv_FeeAuditFlag.setText(Boolean.parseBoolean(list.get(position).getFeeAuditFlag())==true?"是":"否");
        holder.tv_CheckUserName.setText(list.get(position).getCheckUserName());
        holder.tv_InvoiceStatusName.setText(list.get(position).getInvoiceStatusName());
        holder.tv_Remarks.setText(list.get(position).getRemarks());
        return convertView;
    }

    /**
     * @param context 传进来的上下文
     */
    public SalesInventoryCostAdapter(Context context) {
        super();
        this.context = context;
    }


    public ArrayList<SalesInventoryCostBean> getList() {
        return list;
    }

    /**
     * @param list 费用详细数据的集合
     */
    public void setList(ArrayList<SalesInventoryCostBean> list) {
        this.list = list;
    }
    class ViewHolder {

        // 费用单号
        public TextView tv_FeeNo;
        // 对象类型
        public TextView tv_ObjectName;
        // 对象名称
        public TextView tv_ObjectTypeName;
        // 业财科目
        public TextView tv_FeeItemName;
        // 费用金额
        public TextView tv_FeeSum;
        // 确认费用金额
        public TextView tv_ConfirmFeeSum;
        // 确认标识
        public TextView tv_FeeAuditFlag;
        // 审核人
        public TextView tv_CheckUserName;
        // 状态
        public TextView  tv_InvoiceStatusName;
        // 备注
        public TextView tv_Remarks;




    }
}
