package com.dongkesoft.ibossscan.outstoragescan.adapter;

import android.content.Context;
import android.text.InputType;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.TextView;

import com.dongkesoft.ibossscan.R;
import com.dongkesoft.ibossscan.outstoragescan.activity.SalesOutStorageScanActivity;
import com.dongkesoft.ibossscan.outstoragescan.bean.ScanInformationBean;

import java.io.Serializable;
import java.util.List;

public class SalesOutStoragedScanGoodsAdapter extends BaseAdapter {

    private List<ScanInformationBean> productList;

    private Context context;

    private onClickListener onClickListener;

    public SalesOutStoragedScanGoodsAdapter(Context context, List<ScanInformationBean> productList) {
        this.productList = productList;
        this.context=context;
    }

    //仓库点击回调
    public interface onClickListener {

        void cancel(int position);
    }


    @Override
    public int getCount() {
        return productList.size();
    }

    @Override
    public Object getItem(int position) {
        return productList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class ViewHolder implements Serializable {

        TextView tvCancel;
        TextView tvCode;
        TextView tvOnlyCode;// 唯一编码
        TextView tvColorNumber;
        TextView tvSpecification;
        TextView tvGrade;// 等级
        TextView tvWarehouseArea;
        TextView tvPositionNumber;
        TextView tvTwoDimensionCode;
        TextView tvPiece;
        TextView tvBox;

    }

    public void setOnClickListener(onClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final ViewHolder viewhold;
        LayoutInflater _LayoutInflater = LayoutInflater.from(context);
        viewhold = new ViewHolder();
        convertView = _LayoutInflater.inflate(
                R.layout.adapter_sales_out_storaged_scan_goods_list_item, null);

       viewhold.tvCancel=convertView.findViewById(R.id.tv_cancel);
        viewhold.tvCode=convertView.findViewById(R.id.tvCode);
        viewhold.tvOnlyCode=convertView.findViewById(R.id.tv_OnlyCode);
        viewhold.tvColorNumber=convertView.findViewById(R.id.tvColorNumber);
        viewhold.tvSpecification=convertView.findViewById(R.id.tvSpecification);
        viewhold.tvGrade=convertView.findViewById(R.id.tvGrade);
        viewhold.tvTwoDimensionCode=convertView.findViewById(R.id.tvScanCode);
        viewhold.tvWarehouseArea=convertView.findViewById(R.id.tvWarehouseArea);
        viewhold.tvPositionNumber=convertView.findViewById(R.id.tvPositionNumber);
        viewhold.tvBox=convertView.findViewById(R.id.tvScanBox);
        viewhold.tvPiece=convertView.findViewById(R.id.tvPiece);

        ScanInformationBean goodsModel=productList.get(position);

        viewhold.tvCode.setText(goodsModel.getCode());
        viewhold.tvOnlyCode.setText(goodsModel.getOnlyCode());
        viewhold.tvColorNumber.setText(goodsModel.getColorNumber());
        viewhold.tvSpecification.setText(goodsModel.getSpecification());
        viewhold.tvGrade.setText(goodsModel.getGradeName());
        viewhold.tvTwoDimensionCode.setText(goodsModel.getBatchNo());
        viewhold.tvWarehouseArea.setText(goodsModel.getWarehouseName());
        viewhold.tvPositionNumber.setText(goodsModel.getPositionNumber());

        viewhold.tvBox.setText(goodsModel.getBoxQuantity());
        viewhold.tvPiece.setText(goodsModel.getOddQuantity());

        //删除产品
        viewhold.tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onClickListener.cancel(position);

            }
        });

        return convertView;
    }
}
