package com.dongkesoft.ibossscan.outstoragescan.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.dongkesoft.ibossscan.R;
import com.dongkesoft.ibossscan.outstoragescan.adapter.ColorNumberAdapter;
import com.dongkesoft.ibossscan.outstoragescan.bean.ColorNumberBean;
import com.dongkesoft.ibossscan.outstoragescan.presenter.ColorNumberPresenter;
import com.dongkesoft.ibossscan.outstoragescan.presenter.OutStorageNoSearchPresenter;
import com.dongkesoft.ibossscan.outstoragescan.view.IColorNumberView;
import com.dongkesoft.ibossscan.outstoragescan.view.IOutStorageNoSearchView;
import com.dongkesoft.ibossscan.utils.AlertAnimateUtil;
import com.dongkesoft.ibossscan.utils.ProcessDialogUtils;
import com.dongkesoft.ibossscan.utils.ToastUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;


import butterknife.BindView;
import dongkesoft.com.dkmodule.common.Constants;
import dongkesoft.com.dkmodule.ui.activity.BaseActivity;

public class ColorNumberActivity extends BaseActivity <IColorNumberView,ColorNumberPresenter> implements IColorNumberView {

     @BindView(R.id.colorNumberLstView)
     ListView colorNumberLst;
    @BindView(R.id.iv_left)
    ImageView ivLeft;
    LinearLayout llNoData;
    @BindView(R.id.tv_center)
    TextView tvCenter;
    private String codeId;
    private String code;
    private String onlyCode;
    private int position;
    private ArrayList<ColorNumberBean> list;
    public ColorNumberAdapter adapter;

    /**
     * 点击事件
     */
    protected void setOnclick() {

        ivLeft.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                finish();
            }
        });
        colorNumberLst.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Intent intent=new Intent();
                Bundle b=new Bundle();
                b.putInt("position",getIntent().getIntExtra("position",0));
                b.putSerializable("colorNumber", (Serializable) list.get(position));
                intent.putExtras(b);
                setResult(101,intent);
                finish();
            }
        });

    }


    @Override
    protected ColorNumberPresenter createPresenter() {
        return new ColorNumberPresenter();
    }

    @Override
    protected int setMvpView() {
        return R.layout.activity_color_number;
    }

    @Override
    protected void initView() {

    }

    /**
     * 初始化数据
     */
    @Override
    protected void initData() {
        tvCenter.setVisibility(View.VISIBLE);
        tvCenter.setText("修改色号");
        ivLeft.setVisibility(View.VISIBLE);
        list=new ArrayList<ColorNumberBean>();
        setOnclick();
        ProcessDialogUtils.showProcessDialog(ColorNumberActivity.this);
        HashMap<String,String> map=new HashMap<String,String>();
        map.put("Action", "GetColorNumberOfInventory");
        map.put("AccountCode", mAccountCode);
        map.put("UserCode", mUserCode);
        map.put("UserPassword", mPassword);
        map.put("SessionKey", mSessionKey);
        map.put("Code", getIntent().getStringExtra("Code"));
        map.put("OnlyCode", getIntent().getStringExtra("OnlyCode"));
        map.put("CodeID", getIntent().getStringExtra("CodeID"));
        presenter.loadColorNumberData(this,map,mServerAddressIp,mServerAddressPort);
    }

    /**
     * 色号加载成功
     * @param result
     */
    @Override
    public void colorNumberLoadSuccess(String result) {
        ProcessDialogUtils.closeProgressDilog();
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getInt("Status") == 0) {
                JSONArray array = jsonObject
                        .getJSONArray("Result");
                for (int i = 0; i < array.length(); i++) {
                    JSONObject jb = array.getJSONObject(i);

                    ColorNumberBean bean = new ColorNumberBean();
                    bean.setInventoryId(jb.optString("InventoryID"));
                    bean.setCode((jb.optString("Code")));
                    bean.setOnlyCode((jb.optString("OnlyCode")));
                    bean.setBrandName((jb.optString("BrandName")));
                    bean.setGradeId(jb.optString("GradeID"));
                    bean.setGradeName((jb.optString("GradeName")));
                    bean.setColorNumber((jb.optString("ColorNumber")));
                    bean.setSpecification((jb.optString("Specification")));
                    bean.setWarehouseId(jb.optString("WarehouseID"));
                    bean.setWarehouseName((jb.optString("WarehouseName")));
                    bean.setPositionNumber((jb.optString("PositionNumber")));
                    bean.setInventoryQuantity((jb.optString("InventoryQuantity")));
                    bean.setBalanceQuantity((jb.optString("BalanceQuantity")));
                    list.add(bean);
                }
                adapter = new ColorNumberAdapter(ColorNumberActivity.this);
                adapter.setList(list);
                colorNumberLst.setAdapter(adapter);

            } else if (jsonObject.getInt("Status") == Constants.ACTION_RESULT_STATUS
                    || jsonObject.getInt("Status") == Constants.NegativeThree
                    || jsonObject.getInt("Status") == Constants.NegativeTwo
                    || jsonObject.getInt("Status") == Constants.ACTION_INVALID_STATUS) {
                AlertAnimateUtil.showReLoginDialog(
                        ColorNumberActivity.this, "异常登录",
                        jsonObject.getString("Message"));
            } else {
                ToastUtil.showShortToast(
                        ColorNumberActivity.this,
                        jsonObject.getString("Message"));
            }
        }

        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    /**
     * 色号加载失败
     * @param message
     */
    @Override
    public void colorNumberLoadFail(String message) {
        ProcessDialogUtils.closeProgressDilog();
        ToastUtil.showShortToast(ColorNumberActivity.this,message);

    }
}
