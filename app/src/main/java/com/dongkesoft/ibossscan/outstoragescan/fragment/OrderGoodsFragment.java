package com.dongkesoft.ibossscan.outstoragescan.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.Toast;

import com.dongkesoft.ibossscan.R;
import com.dongkesoft.ibossscan.outstoragescan.activity.ColorNumberActivity;
import com.dongkesoft.ibossscan.outstoragescan.activity.SalesOutStorageScanActivity;
import com.dongkesoft.ibossscan.outstoragescan.adapter.SalesOutStorageOrderGoodsAdapter;
import com.dongkesoft.ibossscan.outstoragescan.bean.ColorNumberBean;
import com.dongkesoft.ibossscan.outstoragescan.bean.SalesOutStorageOrderGoodsModel;
import com.dongkesoft.ibossscan.outstoragescan.bean.ScanInformationBean;
import com.dongkesoft.ibossscan.outstoragescan.presenter.OrderGoodsPresenter;
import com.dongkesoft.ibossscan.outstoragescan.view.IOrderGoodsView;

import java.util.List;

import butterknife.BindView;
import dongkesoft.com.dkmodule.ui.fragment.BaseFragment;

/**
 *
 */
public class OrderGoodsFragment  extends BaseFragment<IOrderGoodsView, OrderGoodsPresenter>    implements IOrderGoodsView {

	public List<SalesOutStorageOrderGoodsModel> orderGoodsList;

	public SalesOutStorageOrderGoodsAdapter adapter;

	@BindView(R.id.orderGoodsLst)
	public ListView goodsLst;


	@Override
	protected OrderGoodsPresenter createPresenter() {
		return new OrderGoodsPresenter();
	}

	@Override
	protected int setMvpView() {
		return R.layout.fragment_order_goods;
	}

	@Override
	protected void initView() {

	}

	@Override
	protected void initData() {

	}

	@Override
	public void refreshFragmentView() {

	}

	public void setData(List<SalesOutStorageOrderGoodsModel> goodsList) {
		this.orderGoodsList = goodsList;
             if(adapter==null) {
				  adapter = new SalesOutStorageOrderGoodsAdapter(getActivity());
				  adapter.setData(orderGoodsList);
				  goodsLst.setAdapter(adapter);
			  }
              else
			  {
				 adapter.setData(orderGoodsList);
			 	adapter.notifyDataSetChanged();
			  }


		adapter.setOnClickListener(new SalesOutStorageOrderGoodsAdapter.onClickListener() {

			@Override
			public void goColorNumber(int position) {
				Intent intent = new Intent(getActivity(),
						ColorNumberActivity.class);
				intent.putExtra("position", position);
				intent.putExtra("Code", orderGoodsList.get(position).getCode());
				intent.putExtra("OnlyCode", orderGoodsList.get(position).getOnlyCode());
				intent.putExtra("CodeID", orderGoodsList.get(position).getCodeId());
				startActivityForResult(intent, 100);
			}
		});
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {

		if (requestCode == 100 && resultCode == 101) {
		 Bundle b=data.getExtras();
		int position=b.getInt("position");
			ColorNumberBean colorNumberBean= (ColorNumberBean) b.getSerializable("colorNumber");
			SalesOutStorageOrderGoodsModel goodsModel=	orderGoodsList.get(position);


				if ((goodsModel.getInventoryId().equals(colorNumberBean.getInventoryId())  && goodsModel.getProductCodeFlag() == 1 && goodsModel.isCheckedStatus())) {
					Toast.makeText(getActivity(), "扫描的产品已经存在，不能更改色号", Toast.LENGTH_SHORT).show();
					return;
				}

			goodsModel.setInventoryId(colorNumberBean.getInventoryId());
			goodsModel.setWarehouseId(colorNumberBean.getWarehouseId());
			goodsModel.setWarehouseName(colorNumberBean.getWarehouseName());
			goodsModel.setPositionNumber(colorNumberBean.getPositionNumber());
			goodsModel.setColorNumber(colorNumberBean.getColorNumber());
			goodsModel.setSpecification(colorNumberBean.getSpecification());
			goodsModel.setGradeId(colorNumberBean.getGradeId());
			goodsModel.setGrade(colorNumberBean.getGradeName());

           setData(orderGoodsList);
		}
	}
}




