/*
 * Copyright(c) 2020 dongke All rights reserved. / Confidential
 * 类的信息：
 *		1.程序名称： SalesOutStorageScanlistDetailedFragment
 *		2.功能描述：  销售出库里的 商品明细fragment
 * 编辑履历：
 *		作者				日期					版本				修改内容
 *		dongke			2020/12/01			1.00			新建
 *******************************************************************************/
package com.dongkesoft.ibossscan.outstoragescan.fragment;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dongkesoft.ibossscan.R;
import com.dongkesoft.ibossscan.common.IBossBasePopupWindow;
import com.dongkesoft.ibossscan.outstoragescan.activity.SalesOutListFragmentActivity;
import com.dongkesoft.ibossscan.outstoragescan.adapter.SalesInventoryDetailAdapter;
import com.dongkesoft.ibossscan.outstoragescan.adapter.SalesOutStoragedScanGoodsAdapter;
import com.dongkesoft.ibossscan.outstoragescan.bean.SalesInventoryDetailBean;
import com.dongkesoft.ibossscan.outstoragescan.bean.ScanInformationBean;
import com.dongkesoft.ibossscan.outstoragescan.presenter.SalesOutStorageScanGoodsPresenter;
import com.dongkesoft.ibossscan.outstoragescan.view.ISalesOutStorageScanGoodsView;
import com.dongkesoft.ibossscan.utils.CommonUtil;
import com.dongkesoft.ibossscan.utils.MyListView;
import com.dongkesoft.ibossscan.utils.ProcessDialogUtils;
import com.dongkesoft.ibossscan.utils.ToastUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import dongkesoft.com.dkmodule.mvp.presenter.BaseFragmentPresenter;
import dongkesoft.com.dkmodule.ui.fragment.BaseFragment;


public class SalesOutStorageScanlistDetailedFragment extends BaseFragment<ISalesOutStorageScanGoodsView, SalesOutStorageScanGoodsPresenter> implements ISalesOutStorageScanGoodsView  {

    //出库单ID
    private String DeliveryID = "";
    //出库单号
    private String DeliveryNo = "";
    private List<ScanInformationBean> list;

    @BindView(R.id.goodsLst)
     MyListView listView;
    //适配器
    private SalesOutStoragedScanGoodsAdapter mAdapter;
    private IBossBasePopupWindow cancelPopupWindow;

    @Override
    protected SalesOutStorageScanGoodsPresenter createPresenter() {
        return new SalesOutStorageScanGoodsPresenter();
    }

    @Override
    protected int setMvpView() {
        return R.layout.activity_sales_out_detailed;
    }

    @Override
    protected void initView() {
        list = new ArrayList<ScanInformationBean>();
        listView.setDividerHeight(0);
    }

    @Override
    protected void initData() {

    }

    public void updateData(List<ScanInformationBean> scanList)

    {
        list=scanList;
       if(mAdapter==null)
       {
           mAdapter=new SalesOutStoragedScanGoodsAdapter(getActivity(),list);
           listView.setAdapter(mAdapter);
       }
       else
       {
           mAdapter.notifyDataSetChanged();
       }

        mAdapter.setOnClickListener(new SalesOutStoragedScanGoodsAdapter.onClickListener(){
            @Override
            public void cancel(int position) {

                if (cancelPopupWindow == null) {
                    cancelPopupWindow = new IBossBasePopupWindow(
                          getActivity(),
                            R.layout.popup_window_exit);
                }
                cancelPopupWindow
                        .setPopUpWindowCallBack(new IBossBasePopupWindow.IPopUpWindowCallBack() {

                            @Override
                            public void popUpWindowCallBack(View view) {
                                TextView tvPopupWindowMessage = (TextView) view
                                        .findViewById(R.id.tv_popup_window_message);
                                TextView tvPopupWindowTitle = (TextView) view
                                        .findViewById(R.id.tv_popup_window_title);
                                tvPopupWindowTitle.setText("提示");
                                try {
                                    tvPopupWindowMessage
                                            .setText("是否撤销该商品 ");
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                // 对布局文件进行初始化
                                RelativeLayout llCancel = (RelativeLayout) view
                                        .findViewById(R.id.ll_cancel);
                                // 对布局中的控件添加事件监听
                                TextView tvCancel = (TextView) llCancel
                                        .findViewById(R.id.tv_popup_window_cancel);
                                tvCancel.setText("取消");

                                llCancel.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        cancelPopupWindow
                                                .dismiss();
                                    }
                                });
                                RelativeLayout llOK = (RelativeLayout) view
                                        .findViewById(R.id.ll_ok);
                                // 对布局中的控件添加事件监听

                                TextView tvOk = (TextView) llOK
                                        .findViewById(R.id.tv_popup_window_ok);
                                tvOk.setText("确定");
                                llOK.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                          ScanInformationBean bean= list.get(position);
                                         cancelData(bean.getDetailId());
                                    }
                                });
                            }
                        });
                cancelPopupWindow.show(false,listView
                      , 0, 0);

            }
        });


    }

    /**
     * 撤销出库
     * @param deliveryScanDetailId
     */
    private void cancelData(String deliveryScanDetailId)
    {
        ProcessDialogUtils.showProcessDialog(getActivity());
        HashMap<String,String> map=new HashMap<String,String>();
        map.put("Action", "PasteDeliveryScanForMobile");
        map.put("AccountCode", mAccountCode);
        map.put("UserCode", mUserCode);
        map.put("UserPassword", mPassword);
        map.put("SessionKey", mSessionKey);
        map.put("DeliveryScanDetailID", deliveryScanDetailId);

        presenter.cancelOutStorageData(getActivity(),map,mServerAddressIp,mServerAddressPort);
    }
    @Override
    public void refreshFragmentView() {

    }

    /**
     * 撤销出库成功
     * @param result
     */
    @Override
    public void cancelOutStorageSuccess(String result) {
        ProcessDialogUtils.closeProgressDilog();
        try {
             JSONObject resultObj=new JSONObject(result) ;
           int status=  resultObj.optInt("Status");
           String message=resultObj.optString("Message");
           if(status==0)
           {
               ToastUtil.showShortToast(getActivity(),"撤销出库成功");
               cancelPopupWindow.dismiss();
               Intent intent=new Intent();
               getActivity().setResult(101,intent);
               getActivity().finish();

           }
           else
           {

               ToastUtil.showShortToast(getActivity(),message);
           }
            }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    /**
     * 撤销出库失败
     * @param message
     */
    @Override
    public void cancelOutStorageFail(String message) {
        ProcessDialogUtils.closeProgressDilog();
        ToastUtil.showShortToast(getActivity(),message);
    }
}
