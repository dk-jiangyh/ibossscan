package com.dongkesoft.ibossscan.outstoragescan.view;

import dongkesoft.com.dkmodule.mvp.view.MvpView;

public interface ISalesOutStorageDetailView extends MvpView {

    void outStorageDetailLoadSuccess(String result);
    void outStorageDetailLoadFail(String message);

    void salesOutStorageDetailLoadSuccess(String result);
    void salesOutStorageDetailLoadFail(String message);




}
