/*
 * Copyright(c) 2020 dongke All rights reserved. / Confidential
 * 类的信息：
 *		1.程序名称： SalesOutStorageScanListInfoFragment
 *		2.功能描述：  销售出库里 出库单据fragment
 * 编辑履历：
 *		作者				日期					版本				修改内容
 *		dongke			2020/12/01			1.00			新建
 *******************************************************************************/
package com.dongkesoft.ibossscan.outstoragescan.fragment;

import android.app.Activity;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dongkesoft.ibossscan.R;
import com.dongkesoft.ibossscan.outstoragescan.activity.SalesOutListFragmentActivity;
import com.dongkesoft.ibossscan.outstoragescan.bean.OrderInfoBean;
import com.dongkesoft.ibossscan.outstoragescan.bean.SalesOutListBean;
import com.dongkesoft.ibossscan.outstoragescan.presenter.OutStoragedOrderInfoPresenter;
import com.dongkesoft.ibossscan.outstoragescan.view.IOutStoragedOrderInfoView;

import butterknife.BindView;
import dongkesoft.com.dkmodule.mvp.presenter.BaseFragmentPresenter;
import dongkesoft.com.dkmodule.ui.fragment.BaseFragment;

/**
 * 销售出库详细
 * 王
 * 2020 11
 */

public class SalesOutStorageScanListInfoFragment extends BaseFragment<IOutStoragedOrderInfoView, OutStoragedOrderInfoPresenter> implements IOutStoragedOrderInfoView {

    @BindView(R.id.tv_CustomerCode)
    public TextView tvCustomerCode;

    @BindView(R.id.tv_CustomerName)
    public TextView tvCustomerName;

    @BindView(R.id.tv_Telephone)
    public TextView tvTelephone;

    @BindView(R.id.tv_out_no)
    public TextView tvDeliveryNo;

    @BindView(R.id.tv_Address)
    public TextView tvAddress;

    @BindView(R.id.tv_Remark)
    public TextView tvRemarks;

    @BindView(R.id.tv_AccountDate)
    public TextView tvAccountDate;



    @Override
    protected OutStoragedOrderInfoPresenter createPresenter() {
        return new OutStoragedOrderInfoPresenter();
    }

    @Override
    protected int setMvpView() {
        return  R.layout.fragment_out_storaged_order_info;
    }

    @Override
    protected void initView() {

    }

    public void setData(OrderInfoBean bean)
    {
        tvDeliveryNo.setText(bean.getDeliveryNo());
        tvCustomerCode.setText(bean.getCustomerCode());
        tvCustomerName.setText(bean.getCustomerName());
        tvTelephone.setText(bean.getTelephone());
        tvAddress.setText(bean.getAddress());
        tvRemarks.setText(bean.getRemarks());
        tvAccountDate.setText(bean.getOutStorageDate());
    }

    public void clearData()
    {
        tvDeliveryNo.setText("");
        tvCustomerCode.setText("");
        tvCustomerName.setText("");
        tvTelephone.setText("");
        tvAddress.setText("");
        tvRemarks.setText("");
        tvAccountDate.setText("");
    }

    @Override
    protected void initData() {

    }

    @Override
    public void refreshFragmentView() {

    }


}
