package com.dongkesoft.ibossscan.outstoragescan.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.dongkesoft.ibossscan.R;
import com.dongkesoft.ibossscan.outstoragescan.bean.SalesOutStorageOrderGoodsModel;

import java.io.Serializable;
import java.util.List;

public class SalesOutStoragedOrderGoodsNewAdapter extends BaseAdapter {

    private List<SalesOutStorageOrderGoodsModel> productList;

    private Context context;

    private  onClickListener onClickListener;

    public void setOnClickListener(onClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    public SalesOutStoragedOrderGoodsNewAdapter(Context context, List<SalesOutStorageOrderGoodsModel> productList) {
        this.productList = productList;
        this.context=context;
    }

    public interface onClickListener {
        void cancel(int position);

    }

    @Override
    public int getCount() {
        return productList.size();
    }

    @Override
    public Object getItem(int position) {
        return productList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class ViewHolder implements Serializable {

        TextView tvCode;
        TextView tvDelete;
        TextView tvOnlyCode;// 唯一编码
        TextView tvColorNumber;
        TextView tvSpecification;
        TextView tvGrade;
        TextView tvPositionNumber;
        TextView tvWarehouse;
        TextView tvDeliveryBox;
        TextView tvDeliveryPiece;

    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder viewhold;
        LayoutInflater _LayoutInflater = LayoutInflater.from(context);
        viewhold = new ViewHolder();
        convertView = _LayoutInflater.inflate(
                R.layout.adapter_sales_out_storaged_order_goods_new_list_item, null);

        viewhold.tvCode=convertView.findViewById(R.id.tvCode);
        viewhold.tvDelete=convertView.findViewById(R.id.tv_delete);

        viewhold.tvOnlyCode=convertView.findViewById(R.id.tvOnlyCode);
        viewhold.tvColorNumber=convertView.findViewById(R.id.tvColorNumber);
        viewhold.tvSpecification=convertView.findViewById(R.id.tvSpecification);
        viewhold.tvGrade=convertView.findViewById(R.id.tvGrade);
        viewhold.tvPositionNumber=convertView.findViewById(R.id.tvPositionNumber);
        viewhold.tvWarehouse=convertView.findViewById(R.id.tvWarehouse);
        viewhold.tvDeliveryBox=convertView.findViewById(R.id.tvBox);
        viewhold.tvDeliveryPiece=convertView.findViewById(R.id.tvPiece);



        SalesOutStorageOrderGoodsModel goodsModel=productList.get(position);

        viewhold.tvDelete.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                onClickListener.cancel(position);
            }
        });

        viewhold.tvCode.setText(goodsModel.getCode());
        viewhold.tvOnlyCode.setText(goodsModel.getOnlyCode());
        viewhold.tvGrade.setText(goodsModel.getGrade());
        viewhold.tvPositionNumber.setText(goodsModel.getPositionNumber());
                     //王英杰 2020 11
        viewhold.tvColorNumber.setText(goodsModel.getColorNumber());
        viewhold.tvSpecification.setText(goodsModel.getSpecification());

        viewhold.tvDeliveryBox.setText(goodsModel.getBox());
        viewhold.tvDeliveryPiece.setText(goodsModel.getPiece());
        viewhold.tvWarehouse.setText(goodsModel.getWarehouseName());

        return convertView;
    }
}
