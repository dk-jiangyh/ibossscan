/*******************************************************
 * Copyright(c) 2016 DongkeSoft All rights reserved. / Confidential
 * 
 * @Title : AchievementInfo.java
 * @Package : com.dongkesoft.ibosshj.model
 * @Description : 业绩信息
 * @Author : jiangyh
 * @Date : 2016年8月23日 下午12:04:12
 * @Version : 1.00
 ********************************************************/
package com.dongkesoft.ibossscan.outstoragescan.bean;

import java.io.Serializable;

/**
 * @Description : 用户登录信息
 * @ClassName : AchievementInfo
 * @Author : jiangyh
 * @Date : 2016年8月23日 下午4:13:55
 */
public class AchievementInfo implements Serializable {
	private Boolean checkBoolean;

	public Boolean getCheckBoolean() {
		return checkBoolean;
	}
	public void setCheckBoolean(Boolean checkBoolean) {
		this.checkBoolean = checkBoolean;
	}
	/**
	 * @Field serialVersionUID :
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 部门编码
	 */
	private String mOrganizationCode;

	/**
	 * 部门名称
	 */
	private String mOrganizationName;

	/**
	 * 部门全名
	 */
	private String mOrganizationFullName;

	/**
	 * 部门id
	 */
	private int mOrganizationID;

	/**
	 * 业务员编码
	 */
	private String mStaffCode;

	/**
	 * 业务员名称
	 */
	private String mStaffName;

	/**
	 * 业务员id
	 */
	private int mStaffID;

	/**
	 * 中间客户
	 */
	private String mIntCustomerCode;

	/**
	 * 中间客户
	 */
	private String mIntCustomerName;

	/**
	 * 中间客户
	 */
	private int mIntCustomer;
	// 中间客户员工
	private String mIntCustomerStaffName;

	/**
	 * 中间客户员工
	 */
	private int mIntCustomerStaff;

	/**
	 * 中间员工
	 */
	private int mFatherCustomerID;

	/**
	 * 中间员工
	 */
	private String mFatherCustomerName;

	/**
	 * 中间员工
	 */
	private String mFatherCustomerCode;

	public int getFatherCustomerID() {
		return mFatherCustomerID;
	}

	public void setFatherCustomerID(int fatherCustomerID) {
		mFatherCustomerID = fatherCustomerID;
	}

	public String getFatherCustomerName() {
		return mFatherCustomerName;
	}

	public void setFatherCustomerName(String fatherCustomerName) {
		mFatherCustomerName = fatherCustomerName;
	}

	public String getFatherCustomerCode() {
		return mFatherCustomerCode;
	}

	public void setFatherCustomerCode(String fatherCustomerCode) {
		mFatherCustomerCode = fatherCustomerCode;
	}

	public String getIntCustomerStaffName() {
		return mIntCustomerStaffName;
	}

	public void setIntCustomerStaffName(String intCustomerStaffName) {
		mIntCustomerStaffName = intCustomerStaffName;
	}

	public int getIntCustomerStaff() {
		return mIntCustomerStaff;
	}

	public void setIntCustomerStaff(int intCustomerStaff) {
		mIntCustomerStaff = intCustomerStaff;
	}

	public String getIntCustomerCode() {
		return mIntCustomerCode;
	}

	public void setIntCustomerCode(String intCustomerCode) {
		mIntCustomerCode = intCustomerCode;
	}

	public String getIntCustomerName() {
		return mIntCustomerName;
	}

	public void setIntCustomerName(String intCustomerName) {
		mIntCustomerName = intCustomerName;
	}

	public int getIntCustomer() {
		return mIntCustomer;
	}

	public void setIntCustomer(int intCustomer) {
		mIntCustomer = intCustomer;
	}

	public String getStaffCode() {
		return mStaffCode;
	}

	public void setStaffCode(String staffCode) {
		mStaffCode = staffCode;
	}

	public String getStaffName() {
		return mStaffName;
	}

	public void setStaffName(String staffName) {
		mStaffName = staffName;
	}

	public int getStaffID() {
		return mStaffID;
	}

	public void setStaffID(int staffID) {
		mStaffID = staffID;
	}

	public String getOrganizationCode() {
		return mOrganizationCode;
	}

	public void setOrganizationCode(String organizationCode) {
		mOrganizationCode = organizationCode;
	}

	public String getOrganizationName() {
		return mOrganizationName;
	}

	public void setOrganizationName(String organizationName) {
		mOrganizationName = organizationName;
	}

	public String getOrganizationFullName() {
		return mOrganizationFullName;
	}

	public void setOrganizationFullName(String organizationFullName) {
		mOrganizationFullName = organizationFullName;
	}

	public int getOrganizationID() {
		return mOrganizationID;
	}

	public void setOrganizationID(int organizationID) {
		mOrganizationID = organizationID;
	}

}
