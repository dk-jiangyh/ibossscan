/*
 * Copyright(c) 2020 dongke All rights reserved. / Confidential
 * 类的信息：
 *		1.程序名称： SalesOutStorageScanActivity
 *		2.功能描述： 出库扫码
 * 编辑履历：
 *		作者				日期					版本				修改内容
 *		dongke			2020/06/23			1.00			新建
 *******************************************************************************/
package com.dongkesoft.ibossscan.outstoragescan.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentPagerAdapter;

import com.dongkesoft.ibossscan.R;
import com.dongkesoft.ibossscan.outstoragescan.bean.SaleOrderDetail;
import com.dongkesoft.ibossscan.outstoragescan.bean.SalesOutStorageOrderGoodsModel;
import com.dongkesoft.ibossscan.outstoragescan.bean.ScanInformationBean;
import com.dongkesoft.ibossscan.outstoragescan.fragment.OrderGoodsFragment;
import com.dongkesoft.ibossscan.outstoragescan.fragment.OrderInfoFragment;
import com.dongkesoft.ibossscan.outstoragescan.fragment.ScanFragment;
import com.dongkesoft.ibossscan.outstoragescan.presenter.SalesOutStorageScanPresenter;
import com.dongkesoft.ibossscan.outstoragescan.view.ISalesOutStorageScanView;
import com.dongkesoft.ibossscan.utils.AlertAnimateUtil;
import com.dongkesoft.ibossscan.utils.CommonUtil;
import com.dongkesoft.ibossscan.utils.GenericDrawerLayout;
import com.dongkesoft.ibossscan.utils.NoScrollViewPager;
import com.dongkesoft.ibossscan.utils.ProcessDialogUtils;
import com.dongkesoft.ibossscan.utils.TimePickerInfo;
import com.dongkesoft.ibossscan.utils.ToastUtil;
import com.dongkesoft.ibossscan.utils.ViewPagerIndicator;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import dongkesoft.com.dkmodule.common.Constants;
import dongkesoft.com.dkmodule.ui.activity.BaseActivity;
import dongkesoft.com.dkmodule.utils.DateTimeUtils;

/**
 * 出库扫码
 */

public class SalesOutStorageScanActivity extends BaseActivity<ISalesOutStorageScanView, SalesOutStorageScanPresenter>
        implements ISalesOutStorageScanView {
    /**
     * 标题
     */
    @BindView(R.id.tv_center)
    public TextView tvTitle;
    /**
     * Fragment集合
     */
    private ArrayList<Fragment> mListFragment;
    /*
     * fragment适配器
     */
    private FragmentPagerAdapter mAdapter;
    /*
     * 订单信息fragment
     */
    private OrderInfoFragment orderInfoFragment;
    /*
     * 订单产品fragment
     */
    public OrderGoodsFragment orderGoodsFragment;
    /*
     * 扫描列表fragment
     */
    public ScanFragment scanListFragment;
    /**
     * ViewPagerIndicator
     */
    @BindView(R.id.vpi_merger)
    public ViewPagerIndicator viewPagerIndicator;
    /**
     * NoScrollViewPager
     */
    @BindView(R.id.vp_merger)
    public NoScrollViewPager viewPager;

    /**
     * 二维码
     */
    @BindView(R.id.barcodeEdt)
    public EditText edtBarcode;
    /**
     * 保存
     */
    @BindView(R.id.btn_outstorage_save)
    public Button btnOutStorageSave;


    @BindView(R.id.btn_scan_save)
    public Button btnScanSave;

    /**
     * 开始日期
     */
    private TextView tvStartDate;
    /**
     * 结束日期
     */
    private TextView tvEndDate;
    /**
     * 日期标识
     */
    private boolean startDateFlag;
    /**
     * 标识
     */

    private boolean endDateFlag;
    /**
     * 开始日期
     */
    public String mStartDate;
    /**
     * 结束日期
     */
    public String mEndDate;

    private String onlyCode;

    private String salesId;

    private String customerId;

    private String customerCode;

    private String customerName;

    private String staffId;

    private String telephone;

    private String address;

    private String contacts;

    private String otherContacts;


    /**
     * 返回
     */
    @BindView(R.id.iv_left)
    public ImageView ivLeft;
    /**
     * 抽屉
     */
    @BindView(R.id.out_storage_drawerlayout)
    public GenericDrawerLayout drawerLayout;
    /**
     * 日期
     */
    private TimePickerInfo mTimePickerInfo;

    /**
     * 销售单no
     */
    private EditText edtSaleNo;

    @BindView(R.id.salesNoEdt)
    public EditText etSalesSlipNo;
    /**
     * 编码
     */
    private EditText edtCustomerCode;
    /**
     * 客户名称
     */
    private EditText edtCustomerName;

    private EditText edtCustomerAddress;

    /**
     * 条码
     */
    private String barcode;


    private String salesNo;
    /*
     *抽屉布局
     */
    private View mDrawerLayoutView;

    private HandlerThread handlerThread;// handler线程
    private Handler mHandler;

    public List<SalesOutStorageOrderGoodsModel> orderSelectedList;


    /**
     * 关闭
     */
    private TextView tvDrawerClose;

    /**
     * 重置
     */
    private TextView tvDrawerReset;

    /**
     * 抽屉
     */
    private TextView tvDrawerSure;
    /**
     * 扫描数据列表
     */
    public List<ScanInformationBean> scanList;
    /**
     * 商品列表
     */
    public List<SalesOutStorageOrderGoodsModel> orderGoodsList;

    List<SalesOutStorageOrderGoodsModel> orderGoodsDetailList;

    /**
     * handler线程
     */
    private Handler commonHandler;

    @BindView(R.id.ivScan)
    public ImageView ivScan;
    /**
     * 详细
     */
    @BindView(R.id.tv_right1)
    public TextView tv_right1;

    private SaleOrderDetail searchBean;

    /**
     * 销售出库 右上角列表文字   王英杰 2020 11
     */
    @BindView(R.id.tv_right)
    public TextView tvRight;
    public List<ScanInformationBean> currentList;
    private String codeId;
    private String specification;
    private String colorNumber;
    private String gradeId;
    private String warehouseId;
    private String positionNumber;

    @BindView(R.id.focusEdt)
    EditText edtFocus;

    /*
     * 初始化抽屉布局
     */
    private void initDrawerLayout() {
        // 可以设置打开时响应Touch的区域范围
        drawerLayout.setContentLayout(mDrawerLayoutView);
        drawerLayout.setTouchSizeOfOpened(dip2px(SalesOutStorageScanActivity.this, 500));
        drawerLayout.setTouchSizeOfClosed(dip2px(SalesOutStorageScanActivity.this, 0));
        // 设置随着位置的变更，背景透明度也改变
        drawerLayout.setOpaqueWhenTranslating(true);
        // 设置抽屉是否可以打开
        drawerLayout.setOpennable(false);
        // 设置抽屉的空白区域大小
        float v = getResources().getDisplayMetrics().density * 50 + 0.5f; // 100DIP
        drawerLayout.setDrawerEmptySize((int) v);

        // 设置事件回调
        drawerLayout.setDrawerCallback(new GenericDrawerLayout.DrawerCallback() {

            @Override
            public void onTranslating(int gravity, float translation, float fraction) {
            }

            @Override
            public void onStartOpen() {
            }

            @Override
            public void onStartClose() {
            }

            @Override
            public void onPreOpen() {
            }

            @Override
            public void onEndOpen() {

            }

            @Override
            public void onEndClose() {

            }
        });
    }

    /*
     * dp转px
     */
    private int dip2px(Context context, float dipValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dipValue * scale + 0.5f);
    }

    /**
     * 初始化日期变量
     */
    private void initDate() {
        mTimePickerInfo = new TimePickerInfo(SalesOutStorageScanActivity.this,
                TimePickerInfo.Type.YEAR_MONTH_DAY);
        mTimePickerInfo.setCyclic(true);
        mTimePickerInfo.setCancelable(true);
        mTimePickerInfo
                .setOnTimeSelectListener(new TimePickerInfo.OnTimeSelectListener() {
                    @SuppressLint("SimpleDateFormat")
                    @Override
                    public void onTimeSelect(Date date) {
                        // 存储当前选中的date
                        SimpleDateFormat dateFormater = new SimpleDateFormat(
                                "yyyy-MM-dd");
                        if (startDateFlag) {
                            Date mDateStart = date;
                            mStartDate = dateFormater.format(mDateStart);
                            tvStartDate.setText(mStartDate);
                            startDateFlag = false;
                        }

                        if (endDateFlag) {
                            Date mDateEnd = date;
                            mEndDate = dateFormater.format(mDateEnd);
                            tvEndDate.setText(mEndDate);
                            endDateFlag = false;
                        }

                    }
                });
    }


    @Override
    protected SalesOutStorageScanPresenter createPresenter() {
        return new SalesOutStorageScanPresenter();
    }

    @Override
    protected int setMvpView() {
        return R.layout.activity_sales_out_storage;
    }

    @Override
    protected void initView() {
        tvRight.setVisibility(View.VISIBLE);
        tvRight.setText("列表");

        tvTitle.setVisibility(View.VISIBLE);
        tvTitle.setText("出库扫码");

        tv_right1.setVisibility(View.VISIBLE);
        tv_right1.setText("详细");

        ivLeft.setVisibility(View.VISIBLE);
        mDrawerLayoutView = View.inflate(SalesOutStorageScanActivity.this,
                R.layout.activity_sales_out_storage_scan_drawer_layout, null);

        edtSaleNo = (EditText) mDrawerLayoutView.findViewById(R.id.edt_SaleNo);

        edtCustomerCode = (EditText) mDrawerLayoutView.findViewById(R.id.et_CustomerCode);

        edtCustomerName = (EditText) mDrawerLayoutView.findViewById(R.id.et_CustomerName);

        edtCustomerAddress = (EditText) mDrawerLayoutView.findViewById(R.id.et_CustomerAddress);

        tvStartDate = (TextView) mDrawerLayoutView.findViewById(R.id.tv_start_date);

        tvEndDate = (TextView) mDrawerLayoutView.findViewById(R.id.tv_end_date);

        tvDrawerClose = (TextView) mDrawerLayoutView.findViewById(R.id.tv_sales_out_storage_scan_drawer_layout_close);
        tvDrawerReset = (TextView) mDrawerLayoutView.findViewById(R.id.tv_sales_out_storage_scan_layout_btn_reset);
        tvDrawerSure = (TextView) mDrawerLayoutView.findViewById(R.id.tv_sales_out_storage_scan_drawer_layout_btn_sure);
        orderGoodsList = new ArrayList<SalesOutStorageOrderGoodsModel>();
        orderGoodsDetailList = new ArrayList<SalesOutStorageOrderGoodsModel>();
        scanList = new ArrayList<ScanInformationBean>();
        mStartDate=CommonUtil.getOldDate(-1);
        tvStartDate.setText(mStartDate);
        mEndDate=CommonUtil.getCurrentDate();
        tvEndDate.setText(mEndDate);
        initDrawerLayout();
        initDate();
        initData();

        List<String> mDatas = new ArrayList<String>();
        mDatas.add("商品列表");
        mDatas.add("销售单信息");
        mDatas.add("扫描信息");

        viewPagerIndicator.setTabItemTitles(mDatas);
        viewPagerIndicator.setQuadrangle(false);
        mListFragment = new ArrayList<Fragment>();
        orderInfoFragment = new OrderInfoFragment();
        orderGoodsFragment = new OrderGoodsFragment();
        scanListFragment = new ScanFragment();
        mListFragment.add(orderGoodsFragment);
        mListFragment.add(orderInfoFragment);
        mListFragment.add(scanListFragment);

        viewPager.setOffscreenPageLimit(mListFragment.size());
        mAdapter = new FragmentPagerAdapter(getSupportFragmentManager()) {
            @Override
            public int getCount() {
                return mListFragment.size();
            }

            @Override
            public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
            }

            @Override
            public Fragment getItem(int position) {
                return mListFragment.get(position);
            }
        };
        viewPager.setAdapter(mAdapter);

        viewPagerIndicator.setViewPager(viewPager, 0);

        setOnClick();
    }

    @Override
    protected void initData() {

    }


    protected void setOnClick() {

        tv_right1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!TextUtils.isEmpty(salesId)) {
                    Intent intent = new Intent();
                    Bundle b = new Bundle();
                    b.putString("salesId", salesId);
                    b.putString("flag", "1");
                    intent.putExtras(b);
                    intent.setClass(SalesOutStorageScanActivity.this, SalesOutListFragmentActivity.class);
                    startActivity(intent);
                }

            }
        });

        ivScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.switchStatus();
            }
        });

        etSalesSlipNo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (etSalesSlipNo.getText().toString().contains("\n")) {
                    salesNo = etSalesSlipNo.getText().toString().trim().replaceAll("\\n", "");
                    Bundle b = new Bundle();
                    b.putString("saleNo", salesNo);
                    b.putString("startDate", "");
                    b.putString("endDate", "");
                    b.putString("customerCode", "");
                    b.putString("customerName", "");
                    b.putString("address", "");
                    Intent intent = new Intent();
                    intent.putExtras(b);
                    intent.setClass(SalesOutStorageScanActivity.this, OutInventoryNoSearchActivity.class);
                    startActivityForResult(intent, 100);


                }
            }
        });

        edtBarcode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (edtBarcode.getText().toString().contains("\n")) {
                    barcode = edtBarcode.getText().toString().trim().replaceAll("\\n", "");

                    if (orderGoodsList == null || orderGoodsList.size() == 0) {
                        Toast.makeText(getApplicationContext(), "请选择销售订单", Toast.LENGTH_SHORT).show();
                        return;
                    }


                    edtBarcode.setFocusable(false);
                    edtBarcode.setEnabled(false);
                    edtBarcode.setFilters(new InputFilter[]{new InputFilter() {
                        @Override
                        public CharSequence filter(
                                CharSequence source, int start,
                                int end, Spanned dest,
                                int dstart, int dend) {
                            return source.length() < 1 ? dest
                                    .subSequence(dstart, dend)
                                    : "";
                        }
                    }});

                    ProcessDialogUtils.showProcessDialog(SalesOutStorageScanActivity.this);
                    HashMap<String, String> map = new HashMap<>();
                    map.put("Action", "GetQrGoodsCodeForMobile");
                    map.put("AccountCode", mAccountCode);
                    map.put("UserCode", mUserCode);
                    map.put("UserPassword", mPassword);
                    map.put("SessionKey", mSessionKey);
                    map.put("BatchNo", barcode);
                    presenter.loadScanData(SalesOutStorageScanActivity.this, map, mServerAddressIp, mServerAddressPort);

                }


            }
        });

        ivLeft.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                finish();

            }
        });

        btnOutStorageSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (orderGoodsList == null || orderGoodsList.size() == 0) {
                    Toast.makeText(SalesOutStorageScanActivity.this, "请选择销售单", Toast.LENGTH_SHORT).show();

                    return;
                }

                orderSelectedList = new ArrayList<SalesOutStorageOrderGoodsModel>();
                for (int i = 0; i < orderGoodsList.size(); i++) {
                    SalesOutStorageOrderGoodsModel orderModel = orderGoodsList.get(i);
                    if (orderModel.isCheckedStatus() && orderModel.getProductCodeFlag() == 0) {
                        orderSelectedList.add(orderModel);
                    }

                }
                if (orderSelectedList.size() == 0) {
                    Toast.makeText(getApplicationContext(), "请选择销售单", Toast.LENGTH_SHORT).show();
                    return;
                }

                edtFocus.requestFocus();
                edtFocus.findFocus();

                for (int i = 0; i < orderSelectedList.size(); i++) {
                    SalesOutStorageOrderGoodsModel orderModel = orderSelectedList.get(i);
                    if( TextUtils.isEmpty(orderModel.getDeliveryQuantity()))
                    {
                        Toast.makeText(getApplicationContext(), "出库数量不能为空", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if(Double.parseDouble(orderModel.getDeliveryQuantity())==0)
                    {
                        Toast.makeText(getApplicationContext(), "出库数量不能为0", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if(Double.parseDouble(orderModel.getDeliveryQuantity())>Double.parseDouble(orderModel.getSalesQuantity()))
                    {
                        Toast.makeText(getApplicationContext(), "出库数量不能大于销售数量", Toast.LENGTH_SHORT).show();
                        return;
                    }

                }



                ProcessDialogUtils.showProcessDialog(SalesOutStorageScanActivity.this);

                saveOutStorageData();
            }
        });

        btnScanSave.setOnClickListener(new View.OnClickListener() {
                                           @Override
                                           public void onClick(View v) {

                                               if (orderGoodsFragment.orderGoodsList == null || orderGoodsFragment.orderGoodsList.size() == 0) {
                                                   Toast.makeText(SalesOutStorageScanActivity.this, "请先选择销售单号", Toast.LENGTH_SHORT).show();

                                                   return;
                                               }

                                               orderSelectedList = new ArrayList<SalesOutStorageOrderGoodsModel>();
                                               for (int i = 0; i < orderGoodsList.size(); i++) {
                                                   SalesOutStorageOrderGoodsModel orderModel = orderGoodsList.get(i);
                                                   if (orderModel.isCheckedStatus() && orderModel.getProductCodeFlag() == 1) {
                                                       orderSelectedList.add(orderModel);
                                                   }

                                               }
                                               if (orderSelectedList.size() == 0) {
                                                   Toast.makeText(getApplicationContext(), "请选择销售单", Toast.LENGTH_SHORT).show();
                                                   return;
                                               }

                                               if (scanList == null
                                                       || scanList.size() == 0) {
                                                   Toast.makeText(SalesOutStorageScanActivity.this, "请先扫描条码", Toast.LENGTH_SHORT).show();
                                                   return;
                                               }

                                               edtFocus.requestFocus();
                                               edtFocus.findFocus();

                                               for (int i = 0; i < orderSelectedList.size(); i++) {
                                                   SalesOutStorageOrderGoodsModel orderModel = orderSelectedList.get(i);
                                                    if(Double.parseDouble(orderModel.getUploadQuantity())>Double.parseDouble(orderModel.getSalesQuantity())-Double.parseDouble(orderModel.getOutQuantity()))
                                                    {
                                                        Toast.makeText(SalesOutStorageScanActivity.this, "出库数量不能大于销售数量减已出库数量", Toast.LENGTH_SHORT).show();
                                                             return;
                                                    }

                                               }

                                               for (int i = 0; i < scanList.size(); i++) {
                                                   ScanInformationBean scanInfo = scanList.get(i);
                                                   String deliveryQuantity = scanInfo.getDeliveryQuantity();
                                                   if (TextUtils.isEmpty(deliveryQuantity)) {
                                                       Toast.makeText(SalesOutStorageScanActivity.this, "出库数量不能为空", Toast.LENGTH_SHORT).show();

                                                       return;
                                                   }

                                                   if (Double.parseDouble(deliveryQuantity) == 0) {
                                                       Toast.makeText(SalesOutStorageScanActivity.this, "出库数量不能为0", Toast.LENGTH_SHORT).show();
                                                       return;
                                                   }
                                               }


                                               ProcessDialogUtils.showProcessDialog(SalesOutStorageScanActivity.this);
                                               saveScanData();
                                           }
                                       }
        );


        //王英杰 2020 11
        tvRight.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SalesOutStorageScanActivity.this, SalesOutStorageScanListActivity.class);
                startActivityForResult(intent, 100);
            }
        });


        tvDrawerClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.switchStatus();
            }
        });

        tvDrawerReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                edtSaleNo.setText("");
                edtCustomerCode.setText("");
                edtCustomerName.setText("");
                edtCustomerAddress.setText("");
                mStartDate=CommonUtil.getOldDate(-1);
                tvStartDate.setText(mStartDate);
                mEndDate=CommonUtil.getCurrentDate();
                tvEndDate.setText(mEndDate);
            }
        });
        //侧拉抽屉确定按钮监听事件
        tvDrawerSure.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                salesNo = edtSaleNo.getText().toString();
                String startDate = tvStartDate.getText().toString();
                String endDate = tvEndDate.getText().toString();
                String code = edtCustomerCode.getText().toString();
                String name = edtCustomerName.getText().toString();
                String address = edtCustomerAddress.getText().toString();
                if (orderGoodsList != null && orderGoodsList.size() > 0) {
                    orderGoodsList.clear();
                }
                Bundle b = new Bundle();
                b.putString("saleNo", salesNo);
                b.putString("startDate", startDate);
                b.putString("endDate", endDate);
                b.putString("customerCode", code);
                b.putString("customerName", name);
                b.putString("address", address);
                intent.putExtras(b);
                intent.setClass(SalesOutStorageScanActivity.this, OutInventoryNoSearchActivity.class);
                startActivityForResult(intent, 100);
                drawerLayout.switchStatus();

            }
        });

        tvStartDate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //修改bug，只能弹出一个日厉，存在一个true；
                startDateFlag = true;
                endDateFlag = false;
                if (TextUtils.isEmpty(mStartDate)) {
                    mTimePickerInfo.show(new Date());
                } else {
                    mTimePickerInfo.show(DateTimeUtils.stringToDate(mStartDate, "yyyy-MM-dd"));
                }
            }
        });

        tvEndDate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //修改bug，只能弹出一个日厉，存在一个true；
                startDateFlag = false;
                endDateFlag = true;
                if (TextUtils.isEmpty(mEndDate)) {
                    mTimePickerInfo.show(new Date());
                } else {
                    mTimePickerInfo.show(DateTimeUtils.stringToDate(mEndDate, "yyyy-MM-dd"));
                }
            }
        });

    }

    private void saveOutStorageData() {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("Action", "SaveDeliveryScanForMobile");
        map.put("AccountCode", mAccountCode);
        map.put("UserCode", mUserCode);
        map.put("UserPassword", mPassword);
        map.put("SessionKey", mSessionKey);

        try {
            JSONArray orderArray = new JSONArray();
            JSONObject orderInfoObj = new JSONObject();

            orderInfoObj.put("ScanFlag", "0");
            orderInfoObj.put("SalesID", salesId);
            orderInfoObj.put("CustomerID", customerId);
            orderInfoObj.put("CustomerCode", customerCode);
            orderInfoObj.put("CustomerName", customerName);
            orderInfoObj.put("StaffID", staffId);
            orderInfoObj.put("OrganizationID", orderInfoFragment.mDepartmentId);
            orderInfoObj.put("OrganizationCode", orderInfoFragment.mDepartmentCode);
            orderInfoObj.put("Contacts", contacts);
            orderInfoObj.put("OtherContact", otherContacts);
            orderInfoObj.put("Telephone", telephone);
            orderInfoObj.put("Address", address);
            orderInfoObj.put("Driver", orderInfoFragment.staffId);
            orderInfoObj.put("Docker", orderInfoFragment.dockId);
            orderInfoObj.put("Remarks", orderInfoFragment.edtRemarks.getText().toString());
            orderInfoObj.put("FeeAmount", "0");
            orderArray.put(orderInfoObj);
            map.put("SalesEntity", orderArray.toString());

            JSONArray detailArray = new JSONArray();
            if (orderGoodsDetailList != null && orderGoodsDetailList.size() > 0) {
                for (int i = 0; i < orderGoodsDetailList.size(); i++) {
                    JSONObject detailObj = new JSONObject();
                    SalesOutStorageOrderGoodsModel goodsModel = orderGoodsDetailList.get(i);
                    detailObj.put("DetailID", goodsModel.getDetailId());
                    detailObj.put("InventoryID", goodsModel.getInventoryId());
                    detailObj.put("SalesQuantity", goodsModel.getSalesQuantity());
                    detailObj.put("OutQuantity", goodsModel.getOutQuantity());
                    detailObj.put("Remarks", goodsModel.getRemarks());
                    detailArray.put(detailObj);
                }
                map.put("SalesDetailEntity", detailArray.toString());
            }

            JSONArray detailTotalArray = new JSONArray();
            if (orderGoodsList != null && orderGoodsList.size() > 0) {
                for (int i = 0; i < orderGoodsList.size(); i++) {
                    JSONObject detailObj = new JSONObject();
                    SalesOutStorageOrderGoodsModel goodsModel = orderGoodsList.get(i);
                    if (goodsModel.getProductCodeFlag() == 0) {

                        detailObj.put("InventoryID", goodsModel.getInventoryId());
                        detailObj.put("Code", goodsModel.getCode());
                        detailObj.put("OnlyCode", goodsModel.getOnlyCode());
                        detailObj.put("GradeID", goodsModel.getGradeId());
                        detailObj.put("ColorNumber", goodsModel.getColorNumber());
                        detailObj.put("WarehouseID", goodsModel.getWarehouseId());
                        detailObj.put("Specification", goodsModel.getSpecification());
                        detailObj.put("PositionNumber", goodsModel.getPositionNumber());
                        detailObj.put("Box", goodsModel.getBox());
                        detailObj.put("Piece", goodsModel.getPiece());
                        detailObj.put("Package", goodsModel.getPackageValue() + "");
                        detailObj.put("Quantity", goodsModel.getDeliveryQuantity());
                        detailTotalArray.put(detailObj);
                    }

                }

                map.put("CodeEntity", detailTotalArray.toString());
            }

            JSONArray goodsDetailArray = new JSONArray();
            map.put("ScanCodeEntity", goodsDetailArray.toString());

            presenter.saveOutStorageData(this, map, mServerAddressIp, mServerAddressPort);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void saveScanData() {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("Action", "SaveDeliveryScanForMobile");
        map.put("AccountCode", mAccountCode);
        map.put("UserCode", mUserCode);
        map.put("UserPassword", mPassword);
        map.put("SessionKey", mSessionKey);
        map.put("LicenseCode", mLicenseCode);

        try {
            JSONArray salesArray = new JSONArray();
            JSONObject orderInfoObj = new JSONObject();
            orderInfoObj.put("ScanFlag", "1");
            orderInfoObj.put("SalesID", salesId);
            orderInfoObj.put("CustomerID", customerId);
            orderInfoObj.put("CustomerCode", customerCode);
            orderInfoObj.put("CustomerName", customerName);
            orderInfoObj.put("StaffID", staffId);
            orderInfoObj.put("OrganizationID", orderInfoFragment.mDepartmentId);
            orderInfoObj.put("OrganizationCode", orderInfoFragment.mDepartmentCode);
            orderInfoObj.put("Contacts", contacts);
            orderInfoObj.put("OtherContact", otherContacts);
            orderInfoObj.put("Telephone", telephone);
            orderInfoObj.put("Address", address);
            orderInfoObj.put("Driver", orderInfoFragment.staffId);
            orderInfoObj.put("Docker", orderInfoFragment.dockId);
            orderInfoObj.put("Remarks", orderInfoFragment.edtRemarks.getText().toString());
            orderInfoObj.put("FeeAmount", "0");
            salesArray.put(orderInfoObj);
            map.put("SalesEntity", salesArray.toString());

            JSONArray detailArray = new JSONArray();
            if (orderGoodsDetailList != null && orderGoodsDetailList.size() > 0) {
                for (int i = 0; i < orderGoodsDetailList.size(); i++) {
                    JSONObject detailObj = new JSONObject();
                    SalesOutStorageOrderGoodsModel goodsModel = orderGoodsDetailList.get(i);
                    detailObj.put("DetailID", goodsModel.getDetailId());
                    detailObj.put("InventoryID", goodsModel.getInventoryId());
                    detailObj.put("SalesQuantity", goodsModel.getSalesQuantity());
                    detailObj.put("OutQuantity", goodsModel.getOutQuantity());
                    detailObj.put("Remarks", goodsModel.getRemarks());
                    detailArray.put(detailObj);
                }
                map.put("SalesDetailEntity", detailArray.toString());
            }

            JSONArray detailTotalArray = new JSONArray();
            if (orderGoodsList != null && orderGoodsList.size() > 0) {
                for (int i = 0; i < orderGoodsList.size(); i++) {
                    JSONObject detailObj = new JSONObject();
                    SalesOutStorageOrderGoodsModel goodsModel = orderGoodsList.get(i);
                    if (goodsModel.getProductCodeFlag() == 1) {

                        detailObj.put("InventoryID", goodsModel.getInventoryId());
                        detailObj.put("Code", goodsModel.getCode());
                        detailObj.put("OnlyCode", goodsModel.getOnlyCode());
                        detailObj.put("GradeID", goodsModel.getGradeId());
                        detailObj.put("ColorNumber", goodsModel.getColorNumber());
                        detailObj.put("WarehouseID", goodsModel.getWarehouseId());
                        detailObj.put("Specification", goodsModel.getSpecification());
                        detailObj.put("PositionNumber", goodsModel.getPositionNumber());
                        detailObj.put("Box", goodsModel.getBox());
                        detailObj.put("Piece", goodsModel.getPiece());
                        detailObj.put("Package", goodsModel.getPackageValue() + "");
                        detailObj.put("Quantity", goodsModel.getUploadQuantity());
                        detailTotalArray.put(detailObj);
                    }

                }

                map.put("CodeEntity", detailTotalArray.toString());
            }


            JSONArray goodsDetailArray = new JSONArray();
            if (scanList != null && scanList.size() > 0) {
                for (int i = 0; i < scanList.size(); i++) {
                    JSONObject detailObj = new JSONObject();
                    ScanInformationBean scanModel = scanList.get(i);

                    detailObj.put("InventoryID", scanModel.getInventoryID());
                    detailObj.put("CodeID", scanModel.getCodeID());
                    detailObj.put("Specification", scanModel.getSpecification());
                    detailObj.put("ColorNumber", scanModel.getColorNumber());
                    detailObj.put("WarehouseID", scanModel.getWarehouseID());
                    detailObj.put("PositionNumber", scanModel.getPositionNumber());
                    detailObj.put("Box", scanModel.getBoxQuantity());
                    detailObj.put("Piece", scanModel.getOddQuantity());
                    detailObj.put("DeliveryQuantity", scanModel.getDeliveryQuantity());
                    detailObj.put("GoodQrCodeDetailID", scanModel.getDetailId());
                    detailObj.put("BatchNo", scanModel.getBatchNo());
                    detailObj.put("RandomCode", scanModel.getRandomCode());
                    detailObj.put("ProductCodeDate", scanModel.getProductCodeDate());
                    goodsDetailArray.put(detailObj);
                }
                map.put("ScanCodeEntity", goodsDetailArray.toString());
            }
            presenter.saveData(this, map, mServerAddressIp, mServerAddressPort);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /*
     * 条码控件设置为可编辑
     */
    public void setEdt() {
        edtBarcode.setText("");
        edtBarcode.setEnabled(true);
        edtBarcode.setFocusable(true);
        edtBarcode.setFocusableInTouchMode(true);
        edtBarcode.requestFocus();
        edtBarcode.findFocus();
        edtBarcode.setFilters(new InputFilter[]{new InputFilter() {
            @Override
            public CharSequence filter(CharSequence source,
                                       int start, int end, Spanned dest,
                                       int dstart, int dend) {
                return null;
            }
        }});


    }

    private void loadSalesDetailData(String salesId) {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("Action", "GetSalesDetailForMobile");
        map.put("AccountCode", mAccountCode);
        map.put("UserCode", mUserCode);
        map.put("UserPassword", mPassword);
        map.put("SessionKey", mSessionKey);
        map.put("SalesID", salesId);

        presenter.loadSalesDetailData(this, map, mServerAddressIp, mServerAddressPort);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //获取产品明细数据
        if (requestCode == 100 && resultCode == 101) {
            edtBarcode.setFocusable(true);
            edtBarcode.setFocusableInTouchMode(true);
            edtBarcode.requestFocus();
            edtBarcode.findFocus();
            salesId = data.getExtras().getString("SalesId");

            ProcessDialogUtils.showProcessDialog(SalesOutStorageScanActivity.this);
            loadSalesDetailData(salesId);

        }


    }


    @Override
    public void outStorageDetailLoadSuccess(String result) {
        ProcessDialogUtils.closeProgressDilog();
        orderGoodsDetailList.clear();
        try {
            JSONObject jobj = new JSONObject(result);
            int status = jobj.optInt("Status");
            String message = jobj.optString("Message");
            if (status == 0) {
                JSONObject resultObj = jobj.optJSONObject("Result");
                JSONArray tableArray = resultObj.optJSONArray("Table");
                JSONArray detailArray1 = resultObj.optJSONArray("Table1");
                JSONArray detailArray2 = resultObj.optJSONArray("Table2");
                if (tableArray != null && tableArray.length() > 0) {
                    JSONObject tableObj = (JSONObject) tableArray.opt(0);
                    customerId = tableObj.optString("CustomerID");
                    customerCode = tableObj.optString("CustomerCode");
                    customerName = tableObj.optString("CustomerName");
                    etSalesSlipNo.setText(customerName);
                    staffId = tableObj.optString("StaffID");
                    telephone = tableObj.optString("Telephone");
                    address = tableObj.optString("Address");
                    contacts = tableObj.optString("Contacts");
                    otherContacts = tableObj.optString("OtherContact");
                    salesId = tableObj.optString("SalesID");
                    searchBean = new SaleOrderDetail();
                    searchBean.setCustomerCode(customerCode);
                    searchBean.setCustomerName(customerName);
                    searchBean.setTelephone(telephone);
                    searchBean.setAddress(address);
                    orderInfoFragment.loadData(searchBean);

                }

                if (detailArray1 != null && detailArray1.length() > 0) {
                    for (int i = 0; i < detailArray1.length(); i++) {
                        JSONObject obj = (JSONObject) detailArray1.opt(i);

                        String inventoryId = obj.optString("InventoryID");
                        String detailId = obj.optString("DetailID");
                        String code = obj.optString("Code");
                        String codeId = obj.optString("CodeID");
                        String onlyCode = obj.optString("OnlyCode");
                        String colorNumber = obj.optString("ColorNumber");
                        String warehouseId = obj.optString("WarehouseID");
                        String warehouseName = obj.optString("WarehouseName");
                        String specification = obj.optString("Specification");
                        String gradeId = obj.optString("GradeID");
                        String gradeName = obj.optString("GradeName");
                        String positionNumber = obj.optString("PositionNumber");
                        String remarks = obj.optString("Remarks");
                        int packageValue = obj.optInt("Package");
                        double salesQuantity = obj.optDouble("SalesQuantity");
                        int outQuantity = obj.optInt("OutQuantity");
                        double deliveryQuantity = salesQuantity - outQuantity;
                        int salesBox = (int) (salesQuantity / packageValue);
                        int salesPiece = (int) (salesQuantity % packageValue);
                        int outBox = (int) (outQuantity / packageValue);
                        int outPiece = (int) (outQuantity % packageValue);
                        int deliveryBox = (int) (deliveryQuantity / packageValue);
                        int deliveryPiece = (int) (deliveryQuantity % packageValue);

                        SalesOutStorageOrderGoodsModel orderGoodsModel = new SalesOutStorageOrderGoodsModel();
                        orderGoodsModel.setDetailId(detailId);
                        orderGoodsModel.setInventoryId(inventoryId);
                        orderGoodsModel.setCode(code);
                        orderGoodsModel.setCheckedStatus(true);
                        orderGoodsModel.setCodeId(codeId);
                        orderGoodsModel.setOnlyCode(onlyCode);
                        orderGoodsModel.setGradeId(gradeId);
                        orderGoodsModel.setGrade(gradeName);
                        orderGoodsModel.setPositionNumber(positionNumber);
                        orderGoodsModel.setColorNumber(colorNumber);
                        orderGoodsModel.setSpecification(specification);
                        orderGoodsModel.setWarehouseId(warehouseId);
                        orderGoodsModel.setWarehouseName(warehouseName);
                        orderGoodsModel.setPackageValue(packageValue);
                        orderGoodsModel.setSalesQuantity(String.valueOf(salesQuantity));
                        orderGoodsModel.setOutQuantity(String.valueOf(outQuantity));   //已出库数量
                        orderGoodsModel.setSalesBox(String.valueOf(salesBox));
                        orderGoodsModel.setSalesPiece(String.valueOf(salesPiece));
                        orderGoodsModel.setOutBox(String.valueOf(outBox));
                        orderGoodsModel.setOutPiece(String.valueOf(outPiece));
                        orderGoodsModel.setBox(String.valueOf(deliveryBox));
                        orderGoodsModel.setPiece(String.valueOf(deliveryPiece));
                        orderGoodsModel.setDeliveryQuantity(String.valueOf(deliveryQuantity));
                        orderGoodsModel.setRemarks(remarks);
                        orderGoodsDetailList.add(orderGoodsModel);

                    }
                }

                if (detailArray2 != null && detailArray2.length() > 0) {
                    for (int i = 0; i < detailArray2.length(); i++) {
                        JSONObject obj = (JSONObject) detailArray2.opt(i);

                        String inventoryId = obj.optString("InventoryID");
                        String code = obj.optString("Code");
                        String codeId = obj.optString("CodeID");
                        String onlyCode = obj.optString("OnlyCode");
                        String colorNumber = obj.optString("ColorNumber");
                        String warehouseId = obj.optString("WarehouseID");
                        String warehouseName = obj.optString("WarehouseName");
                        String specification = obj.optString("Specification");
                        String gradeId = obj.optString("GradeID");
                        String gradeName = obj.optString("GradeName");
                        String positionNumber = obj.optString("PositionNumber");
                        int productCodeFlag = obj.optInt("ProductCodeFlag");
                        int packageValue = obj.optInt("Package");
                        double salesQuantity = obj.optDouble("SalesQuantity");
                        int outQuantity = obj.optInt("OutQuantity");
                        double deliveryQuantity = salesQuantity - outQuantity;
                        int salesBox = (int) (salesQuantity / packageValue);
                        int salesPiece = (int) (salesQuantity % packageValue);
                        int outBox = (int) (outQuantity / packageValue);
                        int outPiece = (int) (outQuantity % packageValue);
                        int deliveryBox = (int) (deliveryQuantity / packageValue);
                        int deliveryPiece = (int) (deliveryQuantity % packageValue);

                        SalesOutStorageOrderGoodsModel orderGoodsModel = new SalesOutStorageOrderGoodsModel();
                        orderGoodsModel.setProductCodeFlag(productCodeFlag);
                        orderGoodsModel.setInventoryId(inventoryId);
                        orderGoodsModel.setCode(code);
                        orderGoodsModel.setCheckedStatus(true);
                        orderGoodsModel.setCodeId(codeId);
                        orderGoodsModel.setOnlyCode(onlyCode);
                        orderGoodsModel.setGradeId(gradeId);
                        orderGoodsModel.setGrade(gradeName);
                        orderGoodsModel.setPositionNumber(positionNumber);
                        orderGoodsModel.setColorNumber(colorNumber);
                        orderGoodsModel.setSpecification(specification);
                        orderGoodsModel.setWarehouseId(warehouseId);
                        orderGoodsModel.setWarehouseName(warehouseName);
                        orderGoodsModel.setPackageValue(packageValue);
                        orderGoodsModel.setSalesQuantity(String.valueOf(salesQuantity));
                        orderGoodsModel.setOutQuantity(String.valueOf(outQuantity));   //已出库数量
                        orderGoodsModel.setSalesBox(String.valueOf(salesBox));
                        orderGoodsModel.setSalesPiece(String.valueOf(salesPiece));
                        orderGoodsModel.setOutBox(String.valueOf(outBox));
                        orderGoodsModel.setOutPiece(String.valueOf(outPiece));
                        orderGoodsModel.setBox(String.valueOf(deliveryBox));
                        orderGoodsModel.setPiece(String.valueOf(deliveryPiece));
                        orderGoodsModel.setDeliveryQuantity(String.valueOf(deliveryQuantity));
                        //销售量-已出量-待上传量
                        if ((salesQuantity - outQuantity ) == 0) {
                            continue;
                        }
                        orderGoodsList.add(orderGoodsModel);

                    }
                    orderGoodsFragment.setData(orderGoodsList);
                }

            }

            else if (status == Constants.ACTION_RESULT_STATUS
                    ||status == Constants.NegativeThree
                    ||status == Constants.NegativeTwo
                    ||status == Constants.ACTION_INVALID_STATUS) {
                AlertAnimateUtil.showReLoginDialog(
                        SalesOutStorageScanActivity.this, "异常登录",
                        message);
            } else {
                ToastUtil.showShortToast(
                        SalesOutStorageScanActivity.this,
                        message);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void outStorageDetailLoadFail(String message) {
        ProcessDialogUtils.closeProgressDilog();
        ToastUtil.showShortToast(getApplicationContext(), message);
    }


    /*
     * 计算当前出库数量
     */
    public long calculateCurrentQuantity(ScanInformationBean bean, List<ScanInformationBean> showList) {
        long totalQuantity = 0;

        for (int i = 0; i < showList.size(); i++) {
            ScanInformationBean showBean = showList.get(i);
            if (bean.getColorNumber().equals(showBean.getColorNumber())
                    && (bean.getSpecification().equals(showBean.getSpecification())) && (bean.getGradeId().equals(showBean.getGradeId())) && (bean.getWarehouseID().equals(showBean.getWarehouseID()))
                    && (bean.getPositionNumber().equals(showBean.getPositionNumber())))
                totalQuantity += Double.parseDouble(showBean.getDeliveryQuantity());
        }


        return totalQuantity;
    }

    /*
     * 判断是否达到最大送货数量
     */
    private boolean isMaximumQuantity(ScanInformationBean scanInfo, List<ScanInformationBean> list, List<SalesOutStorageOrderGoodsModel> goodsList) {
        boolean isMaximum = false;
        long currentDeliveryQuantity = 0;
        long totalDeliveryQuantity = 0;
        for (int i = 0; i < list.size(); i++) {
            ScanInformationBean currentInfo = list.get(i);
            if (scanInfo.getColorNumber().equals(currentInfo.getColorNumber())
                    && (scanInfo.getSpecification().equals(currentInfo.getSpecification())) && (scanInfo.getGradeId().equals(currentInfo.getGradeId())) && (scanInfo.getWarehouseID().equals(currentInfo.getWarehouseID()))
                    && (scanInfo.getPositionNumber().equals(currentInfo.getPositionNumber())))
                currentDeliveryQuantity += Double.parseDouble(currentInfo.getDeliveryQuantity());
        }


        for (int i = 0; i < goodsList.size(); i++) {
            SalesOutStorageOrderGoodsModel goodsBean = goodsList.get(i);
            if (scanInfo.getColorNumber().equals(goodsBean.getColorNumber())
                    && (scanInfo.getSpecification().equals(goodsBean.getSpecification())) && (scanInfo.getGradeId().equals(goodsBean.getGradeId())) && (scanInfo.getWarehouseID().equals(goodsBean.getWarehouseId()))
                    && (scanInfo.getPositionNumber().equals(goodsBean.getPositionNumber())) && goodsBean.getProductCodeFlag() == 1)
                totalDeliveryQuantity = (long) Double.parseDouble(goodsBean.getDeliveryQuantity());
        }


        if (currentDeliveryQuantity > totalDeliveryQuantity) {
            isMaximum = true;
        }

        return isMaximum;
    }

    /*
     * 判断送货单是否包含当前扫描的产品
     */
    private boolean isContainsDeliveryInfo(ScanInformationBean currentBean, List<SalesOutStorageOrderGoodsModel> pickList) {
        boolean isHasSameData = false;
        for (int i = 0; i < pickList.size(); i++) {
            SalesOutStorageOrderGoodsModel scanBean = pickList.get(i);
            if ((Double.parseDouble(currentBean.getCodeID()) == Double.parseDouble(scanBean.getCodeId())) && (currentBean.getColorNumber().equals(scanBean.getColorNumber()))
                    && (currentBean.getSpecification().equals(scanBean.getSpecification())) && (currentBean.getGradeId().equals(scanBean.getGradeId())) && (currentBean.getWarehouseID().equals(scanBean.getWarehouseId()))
                    && (currentBean.getPositionNumber().equals(scanBean.getPositionNumber())) && scanBean.getProductCodeFlag() == 1

            ) {
                isHasSameData = true;
                currentBean.setOutQuantity(scanBean.getOutQuantity());

            }

        }
        return isHasSameData;

    }


    /*
     * 更新扫描数量
     */
    public void updateGoodsScanedQuantity(double uploadQuantity, ScanInformationBean bean, List<SalesOutStorageOrderGoodsModel> goodsList) {
        for (int i = 0; i < goodsList.size(); i++) {
            SalesOutStorageOrderGoodsModel goodsBean = goodsList.get(i);
            if ((bean.getColorNumber().equals(goodsBean.getColorNumber())) && (bean.getCodeID().equals(goodsBean.getCodeId())) && (bean.getSpecification().equals(goodsBean.getSpecification())) && (bean.getGradeId().equals(goodsBean.getGradeId())) && (bean.getWarehouseID().equals(goodsBean.getWarehouseId())) && (bean.getPositionNumber().equals(goodsBean.getPositionNumber())) && goodsBean.getProductCodeFlag() == 1) {
                goodsBean.setUploadQuantity(String.valueOf(uploadQuantity));
                int box = (int) (uploadQuantity / goodsBean.getPackageValue());
                int piece = (int) (uploadQuantity % goodsBean.getPackageValue());
                goodsBean.setBox(String.valueOf(box));
                goodsBean.setPiece(String.valueOf(piece));


            }
        }
    }


    /*
     * 统计送货数量
     */
    private long calculateDeliveryQuantity(ScanInformationBean scanInfo, List<ScanInformationBean> list, List<SalesOutStorageOrderGoodsModel> goodsList) {
        long currentDeliveryQuantity = 0;
        long totalDeliveryQuantity = 0;
        if (list != null && list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ScanInformationBean currentInfo = list.get(i);
                if ((scanInfo.getColorNumber().equals(currentInfo.getColorNumber())) && (scanInfo.getCodeID().equals(currentInfo.getCodeID())) && (scanInfo.getSpecification().equals(currentInfo.getSpecification())) && (scanInfo.getGradeId().equals(currentInfo.getGradeId())) && (scanInfo.getWarehouseID().equals(currentInfo.getWarehouseID())) && (scanInfo.getPositionNumber().equals(currentInfo.getPositionNumber()))) {
                    currentDeliveryQuantity += Integer.parseInt(currentInfo.getDeliveryQuantity());
                }
            }
        }
        for (int i = 0; i < goodsList.size(); i++) {
            SalesOutStorageOrderGoodsModel goodsBean = goodsList.get(i);
            if ((scanInfo.getColorNumber().equals(goodsBean.getColorNumber())) && (scanInfo.getCodeID().equals(goodsBean.getCodeId())) && (scanInfo.getSpecification().equals(goodsBean.getSpecification())) && (scanInfo.getGradeId().equals(goodsBean.getGradeId())) && (scanInfo.getWarehouseID().equals(goodsBean.getWarehouseId())) && (scanInfo.getPositionNumber().equals(goodsBean.getPositionNumber())) && goodsBean.getProductCodeFlag() == 1) {
                totalDeliveryQuantity = (long) Double.parseDouble(goodsBean.getDeliveryQuantity());
            }
        }

        totalDeliveryQuantity -= currentDeliveryQuantity;
        if (totalDeliveryQuantity < 0) {
            totalDeliveryQuantity = 0;
        }

        return totalDeliveryQuantity;


    }

    private void clearData() {
        if (orderGoodsFragment.orderGoodsList != null && orderGoodsFragment.orderGoodsList.size() > 0) {
            orderGoodsFragment.orderGoodsList.clear();
            if (orderGoodsFragment.adapter != null) {
                orderGoodsFragment.adapter.notifyDataSetChanged();
            }
        }

        if (scanListFragment.scanList != null && scanListFragment.scanList.size() > 0) {
            scanListFragment.scanList.clear();
            if (scanListFragment.adapter != null) {
                scanListFragment.adapter.notifyDataSetChanged();
            }
        }
        orderInfoFragment.tvCustomerAddress.setText("");
        orderInfoFragment.tvCustomerName.setText("");
        orderInfoFragment.tvTelephone.setText("");
        orderInfoFragment.tvCustomerCode.setText("");
        orderInfoFragment.tvDriver.setText("");
        orderInfoFragment.staffId = "";
        orderInfoFragment.tvWorker.setText("");
        orderInfoFragment.dockId = "";
        orderInfoFragment.tvDepartment.setText("");
        orderInfoFragment.mDepartmentId = "";
        orderInfoFragment.mDepartmentCode = "";
        orderInfoFragment.mDepartmentName = "";
        orderInfoFragment.edtRemarks.setText("");
    }


    @Override
    public void outStorageScanLoadSuccess(String result) {
        boolean isMaximumQuantity = false;
        currentList = new ArrayList<ScanInformationBean>();
        ProcessDialogUtils.closeProgressDilog();


        setEdt();
        try {
            JSONObject jobj = new JSONObject(result);
            int status = jobj.optInt("Status");
            JSONObject resultObj = (JSONObject) jobj.opt("Result");
            JSONArray detailArray = resultObj.optJSONArray("Table");
            if (detailArray != null && detailArray.length() > 0) {
                for (int i = 0; i < detailArray.length(); i++) {
                    JSONObject detailObj = (JSONObject) detailArray.opt(i);
                    codeId = detailObj.optString("CodeID");
                    String code = detailObj.optString("Code");
                    String inventoryId = detailObj.optString("InventoryID");
                    warehouseId = detailObj.optString("WarehouseID");
                    String warehouseName = detailObj.optString("WarehouseName");
                    positionNumber = detailObj.optString("PositionNumber");
                    String onlyCode = detailObj.optString("OnlyCode");
                    int packageValue = detailObj.optInt("Package");
                    gradeId = detailObj.optString("GradeID");
                    String gradeName = detailObj.optString("GradeName");
                    specification = detailObj.optString("Specification");
                    colorNumber = detailObj.optString("ColorNumber");
                    double inputQuantity = detailObj.optDouble("InputQuantity");
                    double deliveryQuantity = detailObj.optDouble("DeliveryQuantity");
                    double allNoSalesQuantity = detailObj.optDouble("AllNoSalesQuantity");
                    String randomCode = detailObj.optString("RandomCode");
                    String detailId = detailObj.optString("DetailID");
                    String productCodeDate = detailObj.optString("ProductCodeDate");
                    productCodeDate = CommonUtil.commonDateConverter(productCodeDate);
                    String batchNo = detailObj.optString("BatchNo");
                    ScanInformationBean bean = new ScanInformationBean();
                    bean.setDetailId(detailId);
                    bean.setCodeID(codeId);
                    bean.setCode(code);
                    bean.setInventoryID(inventoryId);
                    bean.setWarehouseID(warehouseId);
                    bean.setWarehouseName(warehouseName);
                    bean.setPositionNumber(positionNumber);
                    bean.setOnlyCode(onlyCode);
                    bean.setPackageQuantity(String.valueOf(packageValue));
                    bean.setGradeId(gradeId);
                    bean.setGradeName(gradeName);
                    bean.setSpecification(specification);
                    bean.setColorNumber(colorNumber);
                    bean.setInputQuantity(String.valueOf(inputQuantity));
                    bean.setDeliveryQuantity(String.valueOf(deliveryQuantity));
                    bean.setNoSalesQuantity(String.valueOf(allNoSalesQuantity));
                    bean.setBatchNo(batchNo);
                    bean.setRandomCode(randomCode);
                    bean.setProductCodeDate(productCodeDate);
                    currentList.add(bean);
                }

                List<SalesOutStorageOrderGoodsModel> pickList = orderGoodsList;

                if (currentList.size() > 0) {

                    ScanInformationBean scanInfoBean = currentList.get(0);

                    boolean isHasSameData = isContainsDeliveryInfo(scanInfoBean, pickList);

                    if (!isHasSameData) {
                        Toast.makeText(SalesOutStorageScanActivity.this, "该销售单没有该商品", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    //计算送货数量
                    long deliveryQuantity = calculateDeliveryQuantity(scanInfoBean, scanList, pickList);
                    //                //拍子上的实物送货数量
                    double remainQuantity = Double.parseDouble(scanInfoBean.getInputQuantity()) - Double.parseDouble(scanInfoBean.getDeliveryQuantity()) - Double.parseDouble(scanInfoBean.getNoSalesQuantity());

                    double currentQuantity = 0;

                    if (remainQuantity == 0) {
                        Toast.makeText(SalesOutStorageScanActivity.this, "该条码已扫完,不能重复扫描", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (remainQuantity < deliveryQuantity) {
                        //当前剩余送货数量
                        currentQuantity = remainQuantity;
                    } else {
                        currentQuantity = deliveryQuantity;
                    }

                    for (int i = 0; i < orderGoodsFragment.orderGoodsList.size(); i++) {
                        SalesOutStorageOrderGoodsModel goodsBean = orderGoodsFragment.orderGoodsList.get(i);
                        if ((scanInfoBean.getColorNumber().equals(goodsBean.getColorNumber())) && (scanInfoBean.getCodeID().equals(goodsBean.getCodeId())) && (scanInfoBean.getSpecification().equals(goodsBean.getSpecification())) && (scanInfoBean.getGradeId().equals(goodsBean.getGradeId())) && (scanInfoBean.getWarehouseID().equals(goodsBean.getWarehouseId())) && (scanInfoBean.getPositionNumber().equals(goodsBean.getPositionNumber())))
                            if (Double.parseDouble(goodsBean.getOutQuantity()) == Double.parseDouble(goodsBean.getDeliveryQuantity())) {
                                Toast.makeText(SalesOutStorageScanActivity.this, "该商品已出库", Toast.LENGTH_SHORT).show();
                                return;
                            }
                    }

                    //当前剩余送货数量等于0
                    if (currentQuantity == 0) {
                        Toast.makeText(this, "该产品出库数量已达到最大值", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    scanInfoBean.setDeliveryQuantity(String.valueOf(currentQuantity));

                    String packageQuantity = scanInfoBean.getPackageQuantity();
                    int box = (int) (currentQuantity / Double.parseDouble(packageQuantity));
                    int oddNum = (int) (currentQuantity % Double.parseDouble(packageQuantity));

                    scanInfoBean.setBoxQuantity(String.valueOf(box));
                    scanInfoBean.setOddQuantity(String.valueOf(oddNum));
                    //判断是否达到最大送货数量
                    isMaximumQuantity = isMaximumQuantity(scanInfoBean, scanList, pickList);

                    if (isMaximumQuantity) {
                        Toast.makeText(SalesOutStorageScanActivity.this, "该商品已达到最大送货数量", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    scanList.add(scanInfoBean);
                    long totalQuantity = calculateCurrentQuantity(scanInfoBean, scanList);

                    updateGoodsScanedQuantity(totalQuantity, scanInfoBean, pickList);
                    orderGoodsFragment.setData(pickList);
                    scanListFragment.setData(scanList);


                }
                  for(int i=0;i<orderGoodsFragment.orderGoodsList.size();i++){
                      if(codeId.equals(orderGoodsFragment.orderGoodsList.get(i).getCodeId())&&
                              specification.equals(orderGoodsFragment.orderGoodsList.get(i).getSpecification())&&
                              colorNumber.equals(orderGoodsFragment.orderGoodsList.get(i).getColorNumber())&&
                              gradeId.equals(orderGoodsFragment.orderGoodsList.get(i).getGradeId()) &&
                              warehouseId.equals(orderGoodsFragment.orderGoodsList.get(i).getWarehouseId())&&
                              positionNumber.equals(orderGoodsFragment.orderGoodsList.get(i).getPositionNumber()) ) {


                          orderGoodsFragment.goodsLst.setSelection(i);

                      }

                  }

            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void outStorageScanLoadFail(String message) {
        ProcessDialogUtils.closeProgressDilog();
        ToastUtil.showShortToast(getApplicationContext(), message);
    }

    @Override
    public void saveSuccess(String result) {

        ProcessDialogUtils.closeProgressDilog();
        try {
            JSONObject jobj = new JSONObject(result);
            int status = jobj.optInt("Status");
            if (status == 0) {
                Toast.makeText(getApplicationContext(), "保存成功", Toast.LENGTH_SHORT).show();
                clearData();
                loadSalesDetailData(salesId);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void saveFail(String message) {
        ProcessDialogUtils.closeProgressDilog();
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void saveOutStorageSuccess(String result) {

        ProcessDialogUtils.closeProgressDilog();
        try {
            JSONObject jobj = new JSONObject(result);
            int status = jobj.optInt("Status");
            if (status == 0) {
                Toast.makeText(getApplicationContext(), "保存成功", Toast.LENGTH_SHORT).show();
                clearData();
                loadSalesDetailData(salesId);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void saveOutStorageFail(String message) {
        ProcessDialogUtils.closeProgressDilog();
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();

    }
}
