/*
 * Copyright(c) 2020 dongke All rights reserved. / Confidential
 * 类的信息：
 *		1.程序名称： OutInventoryNoSearchActivity
 *		2.功能描述： 奶粉出库单查询页面
 * 编辑履历：
 *		作者				日期					版本				修改内容
 *		dongke			2020/06/23			1.00			新建
 *******************************************************************************/
package com.dongkesoft.ibossscan.outstoragescan.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.dongkesoft.ibossscan.R;
import com.dongkesoft.ibossscan.outstoragescan.adapter.OutNoSearchAdapter;
import com.dongkesoft.ibossscan.outstoragescan.bean.SaleOrderDetail;
import com.dongkesoft.ibossscan.outstoragescan.presenter.OutStorageNoSearchPresenter;
import com.dongkesoft.ibossscan.outstoragescan.view.IOutStorageNoSearchView;
import com.dongkesoft.ibossscan.utils.AlertAnimateUtil;
import com.dongkesoft.ibossscan.utils.CommonUtil;
import com.dongkesoft.ibossscan.utils.ProcessDialogUtils;
import com.dongkesoft.ibossscan.utils.ToastUtil;
import com.dongkesoft.ibossscan.utils.XListViewNew;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import dongkesoft.com.dkmodule.common.Constants;
import dongkesoft.com.dkmodule.mvp.presenter.BasePresenter;
import dongkesoft.com.dkmodule.ui.activity.BaseActivity;


/**
 * @Description : 销售单查询页面
 * @ClassName : OutInventoryNoSearchActivity
 * @Author :
 * @Date :
 */
public class OutInventoryNoSearchActivity extends BaseActivity<IOutStorageNoSearchView,OutStorageNoSearchPresenter> implements IOutStorageNoSearchView,XListViewNew.IXListViewListener  {

    /**
     * 标题
     */
    @BindView(R.id.tv_center)
    TextView tvCenter;
    /**
     * 返回
     */
    @BindView(R.id.iv_left)
    ImageView ivLeft;

    @BindView(R.id.outNoLstView)
    XListViewNew salesLstView;

    @BindView(R.id.ll_no_data)
    LinearLayout llNoData;

    private String sNo;

    private String startDate;

    private String endDate;
    
    private String customerName;

    private String address;

    private boolean IsLoadMore;
    private String currentDate;

    /**
     * 当前页数
     */
    private int pageNum = 1;

    @BindView(R.id.tv_right)
     TextView tvRight;

    private OutNoSearchAdapter adapter;

    private List<SaleOrderDetail> saleOrderDetailList;
    private String customerCode;


    protected void setOnclick() {
        ivLeft.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                finish();

            }
        });

    }

    @Override
    protected OutStorageNoSearchPresenter createPresenter() {
        return new OutStorageNoSearchPresenter();
    }

    @Override
    protected int setMvpView() {
        return R.layout.activity_out_inventory_no_search;
    }

    /**
     * 加载ui
     */
    @Override
    protected void initView() {
        tvCenter.setVisibility(View.VISIBLE);
        tvCenter.setText("销售单列表");
        ivLeft.setVisibility(View.VISIBLE);
        salesLstView.setXListViewListener(this);
        salesLstView.setPullLoadEnable(true);
        setOnClick();
        initSalesData();
    }

    /**
     * 点击事件
     */
    private void setOnClick()
    {
        ivLeft.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    @Override
    protected void initData() {
        saleOrderDetailList = new ArrayList<SaleOrderDetail>();

    }

    /**
     * 加载数据
     */
    private void initSalesData() {
        ProcessDialogUtils.showProcessDialog(OutInventoryNoSearchActivity.this);

        Bundle b = getIntent().getExtras();
        sNo = b.getString("saleNo");
        startDate = b.getString("startDate");
        endDate = b.getString("endDate");
        customerCode = b.getString("customerCode");
        customerName = b.getString("customerName");
        address = b.getString("address");

        HashMap<String,String> map=new HashMap<String,String>();
        map.put("Action", "GetSalesForMobile");
        map.put("AccountCode", mAccountCode);
        map.put("UserCode", mUserCode);
        map.put("UserPassword", mPassword);
        map.put("SessionKey", mSessionKey);
        map.put("SalesNo", sNo);
        map.put("CustomerCode", customerCode);
        map.put("CustomerName", customerName);
        map.put("StartDate", startDate);
        map.put("EndDate", endDate);
        map.put("Address", address);
        map.put("PageNum", pageNum + "");
        map.put("PageSize", "20");
        presenter.loadSalesSlipData(this,map,mServerAddressIp,mServerAddressPort);

    }


    /**
     * 销售单数据加载成功
     * @param result
     */
    @Override
    public void outStorageLoadSuccess(String result) {

                        try {
                            JSONObject jsonObject = new JSONObject(result);
                            if (jsonObject.getInt("Status") == 0) {

                                JSONObject obj = jsonObject.optJSONObject("Result");
                                JSONArray jsonArray = obj.optJSONArray("Table");
                                if (jsonArray != null && jsonArray.length() > 0) {
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject dataObj = jsonArray.optJSONObject(i);
                                        String salesId = dataObj.optString("SalesID");
                                        String salesNo = dataObj.optString("SalesNo");
                                      //  String customerCode = dataObj.optString("CustomerCode");
                                        String customerName = dataObj.optString("CustomerName");
                                        String address = dataObj.optString("Address");
                                        String createDate = dataObj.optString("CreateTime");
                                        String  status=dataObj.optString("Status");
                                        createDate= CommonUtil.commonDateConverter(createDate);
                                        String telephone = dataObj.optString("Telephone");

                                        SaleOrderDetail sd = new SaleOrderDetail();
                                        sd.setInvoiceId(salesId);
                                        sd.setInvoiceNo(salesNo);
                                     //   sd.setCustomerCode(customerCode);
                                        sd.setCustomerName(customerName);
                                        sd.setAddress(address);
                                        sd.setCreateDate(createDate);
                                        sd.setTelephone(telephone);
                                        sd.setStatus(status);
                                        saleOrderDetailList.add(sd);

                                    }

                                    adapter = new OutNoSearchAdapter(saleOrderDetailList, OutInventoryNoSearchActivity.this);
                                    salesLstView.setAdapter(adapter);
                                    IsLoadMore = true;
                                    salesLstView.setPullLoadEnable(true);
                                    llNoData.setVisibility(View.GONE);
                                    onLoad();

                                    salesLstView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                        @Override
                                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                                            SaleOrderDetail searchBean = saleOrderDetailList.get(i-1);
                                            Intent intent = new Intent();
                                            Bundle b = new Bundle();
                                            b.putString("SalesId", searchBean.getInvoiceId());
                                            intent.putExtras(b);
                                            setResult(101, intent);
                                            finish();
                                        }
                                    });
                                }
                                else
                                {
                                    onLoad();
                                    IsLoadMore = false;
                                    salesLstView.setPullLoadEnable(false);
                                    if(saleOrderDetailList!=null&&saleOrderDetailList.size()==0) {
                                        llNoData.setVisibility(View.VISIBLE);
                                    }
                                }

                            }

                            else if (jsonObject.getInt("Status") == Constants.ACTION_RESULT_STATUS
                                    || jsonObject.getInt("Status") == Constants.NegativeThree
                                    || jsonObject.getInt("Status") == Constants.NegativeTwo
                                    || jsonObject.getInt("Status") == Constants.ACTION_INVALID_STATUS) {
                                AlertAnimateUtil.showReLoginDialog(
                                        OutInventoryNoSearchActivity.this, "异常登录",
                                        jsonObject.getString("Message"));
                            } else {
                                ToastUtil.showShortToast(
                                        OutInventoryNoSearchActivity.this,
                                        jsonObject.getString("Message"));
                            }
                        }
                        catch (Exception e)
                        {
                            e.printStackTrace();
                        }

                   ProcessDialogUtils.closeProgressDilog();

    }

    /**
     * 出库单数据加载失败
     * @param message
     */
    @Override
    public void outStorageLoadFail(String message) {
        ProcessDialogUtils.closeProgressDilog();
        ToastUtil.showShortToast(getApplicationContext(),message);
    }

    /**
     * 下拉刷新
     */
    @Override
    public void onRefresh() {
        pageNum = 1;
        salesLstView.setRefreshTime(CommonUtil.getCurrentDateTime());
        if (saleOrderDetailList != null && saleOrderDetailList.size() > 0) {
            saleOrderDetailList.clear();
            if (adapter != null) {
                adapter.notifyDataSetChanged();
            }
        }
        initSalesData();
    }

    /**
     * 停止下拉刷新
     */
    private void onLoad() {
        salesLstView.stopRefresh();
        salesLstView.stopLoadMore();
        currentDate = CommonUtil.getCurrentDateTime();
        salesLstView.setRefreshTime(currentDate);
    }

    /**
     * 加载更多数据
     */
    @Override
    public void onLoadMore() {
        if (!IsLoadMore) {
            return;
        }
        pageNum++;
        initSalesData();
    }
}
