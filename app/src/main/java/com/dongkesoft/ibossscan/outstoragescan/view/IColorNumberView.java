package com.dongkesoft.ibossscan.outstoragescan.view;

import dongkesoft.com.dkmodule.mvp.view.MvpView;

public interface IColorNumberView extends MvpView {

    void colorNumberLoadSuccess(String result);
    void colorNumberLoadFail(String message);
}
