
package com.dongkesoft.ibossscan.outstoragescan.fragment;

import android.view.View;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.dongkesoft.ibossscan.R;
import com.dongkesoft.ibossscan.outstoragescan.activity.SalesOutStorageScanActivity;
import com.dongkesoft.ibossscan.outstoragescan.adapter.SalesOutStorageScanGoodsAdapter;
import com.dongkesoft.ibossscan.outstoragescan.bean.SalesOutStorageOrderGoodsModel;
import com.dongkesoft.ibossscan.outstoragescan.bean.ScanInformationBean;
import com.dongkesoft.ibossscan.outstoragescan.presenter.ScanInfoPresenter;
import com.dongkesoft.ibossscan.outstoragescan.view.IScanView;
import com.dongkesoft.ibossscan.utils.BasePopupWindow;

import java.util.List;

import butterknife.BindView;
import dongkesoft.com.dkmodule.ui.fragment.BaseFragment;


public class ScanFragment  extends BaseFragment <IScanView, ScanInfoPresenter>    implements IScanView  {

	public SalesOutStorageScanGoodsAdapter adapter;

	public List<ScanInformationBean> scanList;

	private BasePopupWindow BasePopupWindowDelete;

    @BindView(R.id.scanLst)
	ListView scanLst;


	@Override
	protected ScanInfoPresenter createPresenter() {
		return new ScanInfoPresenter();
	}

	@Override
	protected int setMvpView() {
		return R.layout.fragment_scan;
	}

	@Override
	protected void initView() {

	}


	@Override
	protected void initData() {


	}

	@Override
	public void refreshFragmentView() {

	}

	public void setData(List<ScanInformationBean> scanList)
	{
		this.scanList=scanList;
		if(adapter==null)
		{
			adapter=new SalesOutStorageScanGoodsAdapter(getActivity(),scanList);
			scanLst.setAdapter(adapter);
		}
		else
		{
			adapter.notifyDataSetChanged();
		}

		adapter.setOnClickListener(new SalesOutStorageScanGoodsAdapter.onClickListener(){
			@Override
			public void delete(int position) {
				BasePopupWindowDelete = new BasePopupWindow(
						getActivity(), R.layout.popup_window_exit);
				BasePopupWindowDelete
						.setPopUpWindowCallBack(new BasePopupWindow.IPopUpWindowCallBack() {

							@Override
							public void popUpWindowCallBack(View view) {
								TextView tvPopupWindowMessage = (TextView) view
										.findViewById(R.id.tv_popup_window_message);
								TextView tvPopupWindowTitle = (TextView) view
										.findViewById(R.id.tv_popup_window_title);
								tvPopupWindowTitle.setText("");
								try {
									tvPopupWindowMessage.setText("是否删除当前项？");
								} catch (Exception e) {
									e.printStackTrace();
								}
								// 对布局文件进行初始化
								RelativeLayout llCancel = (RelativeLayout) view
										.findViewById(R.id.ll_cancel);
								// 对布局中的控件添加事件监听
								llCancel.setOnClickListener(new View.OnClickListener() {
									@Override
									public void onClick(View v) {
										BasePopupWindowDelete.dismiss();
									}
								});
								RelativeLayout llOK = (RelativeLayout) view
										.findViewById(R.id.ll_ok);
								// 对布局中的控件添加事件监听
								llOK.setOnClickListener(new View.OnClickListener() {
									@Override
									public void onClick(View v) {

										//删除数据库信息
										ScanInformationBean scanInfo= scanList.get(position);

										scanList.remove(scanInfo);
										adapter.notifyDataSetChanged();
										updateGoodsScanedQuantity(scanInfo, ((SalesOutStorageScanActivity) getActivity()).orderGoodsFragment.orderGoodsList);

										((SalesOutStorageScanActivity) getActivity()). orderGoodsFragment.setData(((SalesOutStorageScanActivity) getActivity()).orderGoodsFragment.orderGoodsList);

										if (BasePopupWindowDelete != null) {
											BasePopupWindowDelete.dismiss();
										}
									}
								});
							}
						});
				BasePopupWindowDelete.show(false,
						scanLst, 0, 0);
			}
		});
	}


	/*
	 * 更新扫描数量
	 */
	public void updateGoodsScanedQuantity(ScanInformationBean bean, List<SalesOutStorageOrderGoodsModel> goodsList) {
		for (int i = 0; i < goodsList.size(); i++) {
			SalesOutStorageOrderGoodsModel goodsBean = goodsList.get(i);
			if (bean.getColorNumber().equals(goodsBean.getColorNumber())
					&&(bean.getSpecification().equals(goodsBean.getSpecification()))&&(bean.getGradeId().equals(goodsBean.getGradeId()))&&(bean.getWarehouseID().equals(goodsBean.getWarehouseId()))
					&&(bean.getPositionNumber().equals(goodsBean.getPositionNumber())))
			{
				String deliveryQuantity=  bean.getDeliveryQuantity();
				 String uploadQuantity=goodsBean.getUploadQuantity();
				int packageValue=goodsBean.getPackageValue();
				double scanedQuantity=Double.parseDouble( uploadQuantity)-Double.parseDouble(deliveryQuantity);
				int scanedBoxQuantity= (int) (scanedQuantity/packageValue);
				int scanedOddQuantity=(int)(scanedQuantity%packageValue);
				goodsBean.setBox(String.valueOf(scanedBoxQuantity));
				goodsBean.setPiece(String.valueOf(scanedOddQuantity));
				goodsBean.setUploadQuantity(String.valueOf(scanedQuantity));

			}
		}
	}

}
