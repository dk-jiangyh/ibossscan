package com.dongkesoft.ibossscan.outstoragescan.adapter;

import android.content.Context;
import android.text.InputType;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.TextView;

import com.dongkesoft.ibossscan.R;
import com.dongkesoft.ibossscan.outstoragescan.activity.SalesOutStorageScanActivity;
import com.dongkesoft.ibossscan.outstoragescan.bean.SalesOutStorageOrderGoodsModel;
import com.dongkesoft.ibossscan.outstoragescan.bean.ScanInformationBean;

import java.io.Serializable;
import java.util.List;

public class SalesOutStorageScanGoodsAdapter extends BaseAdapter {

    private List<ScanInformationBean> productList;

    private Context context;

    private int mBoxNumberCurrentIndex = -1;

    private int mOddNumberCurrentIndex = -1;

    private onClickListener onClickListener;

    public SalesOutStorageScanGoodsAdapter(Context context, List<ScanInformationBean> productList) {
        this.productList = productList;
        this.context=context;
    }

    //仓库点击回调
    public interface onClickListener {

        void delete(int position);
    }

    private class OnBoxNumEditTextTouched implements View.OnTouchListener {
        private int position;

        public OnBoxNumEditTextTouched(int position) {
            this.position = position;
        }

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_UP) {
                mBoxNumberCurrentIndex = position;
            }

            return false;
        }

    }

    private class OnOddNumEditTextTouched implements View.OnTouchListener {
        private int position;

        public OnOddNumEditTextTouched(int position) {
            this.position = position;
        }

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_UP) {
                mOddNumberCurrentIndex = position;
            }

            return false;
        }

    }



    public void setOnClickListener(onClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }


    //修改送货数量
    private class BoxNumQuantityEditTextListener implements
            View.OnFocusChangeListener {
        private int position;
        private EditText edtBoxQuantity;

        public BoxNumQuantityEditTextListener(int position, EditText edtBoxQuantity) {
            this.position = position;
            this.edtBoxQuantity = edtBoxQuantity;
        }

        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            if (!hasFocus) {
                int pos = (Integer) edtBoxQuantity.getTag();
                if (pos == position) {
                    String boxQuantity = edtBoxQuantity.getText().toString();
                    if (productList.size() > 0) {
                        if (position < productList.size()) {
                            if(TextUtils.isEmpty(boxQuantity))
                            {
                                boxQuantity="0" ;
                            }
                            ScanInformationBean goodsModel = productList.get(position);
                            double deliveryQuantity;
                            if (TextUtils.isEmpty(goodsModel.getOddQuantity())) {
                                deliveryQuantity = (int) (Double.parseDouble(boxQuantity) *Double.parseDouble(goodsModel.getPackageQuantity()) );
                            } else {
                                deliveryQuantity = (int) (Double.parseDouble(boxQuantity) *Double.parseDouble(goodsModel.getPackageQuantity())) + Double.parseDouble(goodsModel.getOddQuantity());
                            }
                            goodsModel.setDeliveryQuantity(String.valueOf(deliveryQuantity));
                            goodsModel.setBoxQuantity(boxQuantity);
                            productList.set(position, goodsModel);
                            edtBoxQuantity.setText(boxQuantity);
                            long totalQuantity = ((SalesOutStorageScanActivity)context).calculateCurrentQuantity(goodsModel, productList);

                            ((SalesOutStorageScanActivity)context).updateGoodsScanedQuantity(totalQuantity, goodsModel,  ((SalesOutStorageScanActivity)context).orderGoodsList);
                            ((SalesOutStorageScanActivity)context).orderGoodsFragment.setData( ((SalesOutStorageScanActivity)context).orderGoodsList);

                        }
                    }
                }
            }
        }
    }


    private class OddNumQuantityEditTextListener implements
            View.OnFocusChangeListener {
        private int position;
        private EditText edtOddQuantity;

        public OddNumQuantityEditTextListener(int position, EditText edtOddQuantity) {
            this.position = position;
            this.edtOddQuantity = edtOddQuantity;
        }

        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            if (!hasFocus) {
                int pos = (Integer) edtOddQuantity.getTag();
                if (pos == position) {


                    if (productList.size() > 0) {
                        if (position < productList.size() ) {

                            ScanInformationBean scanBean = productList.get(position);
                            String oddQuantity = edtOddQuantity.getText().toString();
                            if(TextUtils.isEmpty(oddQuantity))
                            {
                                oddQuantity="0";
                            }
                            if (Double.parseDouble(oddQuantity) >=Double.parseDouble(scanBean.getPackageQuantity())) {
                                edtOddQuantity.setText("0");
                                oddQuantity = "0";
                            }
                            if (TextUtils.isEmpty(scanBean.getBoxQuantity())) {
                                scanBean.setBoxQuantity("0");
                            }
                            int deliveryQuantity = (int) (Double.parseDouble(scanBean.getBoxQuantity()) * Double.parseDouble(scanBean.getPackageQuantity()) + Double.parseDouble(oddQuantity));
                            scanBean.setDeliveryQuantity(String.valueOf(deliveryQuantity));
                            scanBean.setOddQuantity(oddQuantity);
                            productList.set(position, scanBean);

                        }
                    }
                }
            }
        }
    }



    @Override
    public int getCount() {
        return productList.size();
    }

    @Override
    public Object getItem(int position) {
        return productList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class ViewHolder implements Serializable {

        TextView tvDelete;
        TextView tvCode;
        TextView tvOnlyCode;// 唯一编码
        TextView tvColorNumber;
        TextView tvSpecification;
        TextView tvGrade;// 等级
        TextView tvWarehouseArea;
        TextView tvPositionNumber;
        TextView tvTwoDimensionCode;
        EditText edtPiece;
        EditText edtbox;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final ViewHolder viewhold;
        LayoutInflater _LayoutInflater = LayoutInflater.from(context);
        viewhold = new ViewHolder();
        convertView = _LayoutInflater.inflate(
                R.layout.adapter_sales_out_storage_scan_goods_list_item, null);

       viewhold.tvDelete=convertView.findViewById(R.id.tv_delete);
        viewhold.tvCode=convertView.findViewById(R.id.tvCode);
        viewhold.tvOnlyCode=convertView.findViewById(R.id.tv_OnlyCode);
        viewhold.tvColorNumber=convertView.findViewById(R.id.tvColorNumber);
        viewhold.tvSpecification=convertView.findViewById(R.id.tvSpecification);
        viewhold.tvGrade=convertView.findViewById(R.id.tvGrade);
        viewhold.tvTwoDimensionCode=convertView.findViewById(R.id.tvScanCode);
        viewhold.tvWarehouseArea=convertView.findViewById(R.id.tvWarehouseArea);
        viewhold.tvPositionNumber=convertView.findViewById(R.id.tvPositionNumber);
        viewhold.edtbox=convertView.findViewById(R.id.edtScanBox);
        viewhold.edtbox.setTag(position);
        viewhold.edtPiece=convertView.findViewById(R.id.edtPiece);
        viewhold.edtPiece.setTag(position);
        viewhold.edtbox.setInputType(InputType.TYPE_CLASS_NUMBER
                | InputType.TYPE_NUMBER_FLAG_DECIMAL);

        viewhold.edtPiece.setInputType(InputType.TYPE_CLASS_NUMBER
                | InputType.TYPE_NUMBER_FLAG_DECIMAL);


        viewhold.edtbox.setOnTouchListener(new OnBoxNumEditTextTouched(position));

        viewhold.edtPiece.setOnTouchListener(new OnOddNumEditTextTouched(position));


        if (mBoxNumberCurrentIndex != -1 && position == mBoxNumberCurrentIndex) {
            viewhold.edtbox.requestFocus();

        } else {
            viewhold.edtbox.clearFocus();
        }

        if (mOddNumberCurrentIndex != -1 && position == mOddNumberCurrentIndex) {
            viewhold.edtPiece.requestFocus();

        } else {
            viewhold.edtPiece.clearFocus();
        }

        ScanInformationBean goodsModel=productList.get(position);

        viewhold.tvCode.setText(goodsModel.getCode());
        viewhold.tvOnlyCode.setText(goodsModel.getOnlyCode());
        viewhold.tvColorNumber.setText(goodsModel.getColorNumber());
        viewhold.tvSpecification.setText(goodsModel.getSpecification());
        viewhold.tvGrade.setText(goodsModel.getGradeName());
        viewhold.tvTwoDimensionCode.setText(goodsModel.getBatchNo());
        viewhold.tvWarehouseArea.setText(goodsModel.getWarehouseName());
        viewhold.tvPositionNumber.setText(goodsModel.getPositionNumber());
        viewhold.edtbox.setOnFocusChangeListener(new BoxNumQuantityEditTextListener(position, viewhold.edtbox));
        viewhold.edtPiece.setOnFocusChangeListener(new OddNumQuantityEditTextListener(position,viewhold.edtPiece));
        viewhold.edtbox.setText(goodsModel.getBoxQuantity());
        viewhold.edtPiece.setText(goodsModel.getOddQuantity());

        //删除产品
        viewhold.tvDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onClickListener.delete(position);

            }
        });

        return convertView;
    }
}
