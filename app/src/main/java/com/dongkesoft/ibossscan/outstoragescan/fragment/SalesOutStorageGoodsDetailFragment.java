package com.dongkesoft.ibossscan.outstoragescan.fragment;

import android.widget.ListView;

import com.dongkesoft.ibossscan.R;
import com.dongkesoft.ibossscan.outstoragescan.adapter.SalesOutStorageOrderGoodsAdapter;
import com.dongkesoft.ibossscan.outstoragescan.adapter.SalesOutStoragedOrderGoodsAdapter;
import com.dongkesoft.ibossscan.outstoragescan.bean.SalesOutStorageOrderGoodsModel;
import com.dongkesoft.ibossscan.outstoragescan.presenter.SalesOutStorageGoodsDetailPresenter;
import com.dongkesoft.ibossscan.outstoragescan.view.ISalesOutStorageGoodsView;

import java.util.List;

import butterknife.BindView;
import dongkesoft.com.dkmodule.mvp.presenter.BaseFragmentPresenter;
import dongkesoft.com.dkmodule.ui.fragment.BaseFragment;

public class SalesOutStorageGoodsDetailFragment extends BaseFragment <ISalesOutStorageGoodsView, SalesOutStorageGoodsDetailPresenter>

implements ISalesOutStorageGoodsView {

    public List<SalesOutStorageOrderGoodsModel> orderGoodsList;

    public SalesOutStoragedOrderGoodsAdapter adapter;

    @BindView(R.id.goodsLst)
    public ListView goodsLst;
    @Override
    protected SalesOutStorageGoodsDetailPresenter createPresenter() {
        return new SalesOutStorageGoodsDetailPresenter();
    }

    @Override
    protected int setMvpView() {
        return R.layout.activity_sales_out_detailed;
    }

    @Override
    protected void initView() {

    }

    @Override
    protected void initData() {

    }

    public void updateData(List<SalesOutStorageOrderGoodsModel> goodsList)
    {
        if(adapter==null) {
            adapter = new SalesOutStoragedOrderGoodsAdapter(getActivity(), goodsList);
            goodsLst.setAdapter(adapter);
        }
        else
        {
            adapter.notifyDataSetChanged();
        }

    }

    @Override
    public void refreshFragmentView() {

    }

}
