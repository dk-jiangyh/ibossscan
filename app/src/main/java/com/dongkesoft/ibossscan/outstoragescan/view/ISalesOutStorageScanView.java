package com.dongkesoft.ibossscan.outstoragescan.view;

import dongkesoft.com.dkmodule.mvp.view.MvpView;

public interface ISalesOutStorageScanView extends MvpView {

    void outStorageDetailLoadSuccess(String result);
    void outStorageDetailLoadFail(String message);
    void outStorageScanLoadSuccess(String result);
    void outStorageScanLoadFail(String message);
    void saveSuccess(String result);
    void saveFail(String message);

    void saveOutStorageSuccess(String result);
    void saveOutStorageFail(String message);

}
