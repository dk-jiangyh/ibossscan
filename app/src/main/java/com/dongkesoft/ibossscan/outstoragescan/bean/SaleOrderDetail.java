/*******************************************************
 * Copyright(c) 2016 DongkeSoft All rights reserved. / Confidential
 * 
 * @Title : SaleOrderDetail.java
 * @Package : com.dongkesoft.ibosshj.model
 * @Description : 订单销售单明细
 * @Author : jiangyh
 * @Date : 2016年8月23日 下午12:04:12
 * @Version : 1.00
 ********************************************************/
package com.dongkesoft.ibossscan.outstoragescan.bean;

import java.io.Serializable;

/**
 * @Description : 订单销售单明细
 * @ClassName : SaleOrderDetail
 * @Author : jiangyh
 * @Date : 2016年8月23日 下午4:13:55
 */
public class SaleOrderDetail implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 单据id
	 */
	private String invoiceId;

	/**
	 * 商品金额
	 */
	private double goodsAmount;

	/**
	 * 折扣金额
	 */
	private double discountAmount;

	/**
     *  
     */
	private double markedPriceAmount;

	/**
	 * 折扣
	 */
	private double discount;

	/**
	 * 费用
	 */
	private double feeAmount;

	/**
	 * 舍零
	 */
	private double earnestAmount;

	/**
     *  
     */
	private double depositReceivedAmount;

	/**
     *  
     */
	private double receivableAmount;

	/**
	 * 总价格
	 */
	private double totalAmount;

	/**
     *  
     */
	private double accountReceivableAmount;

	/**
	 * 订单数量
	 */
	private double toSalesAmount;

	/**
	 * 未转订单数量
	 */
	private double unToSalesAmount;

	/**
     *  
     */
	private double toFactSalesAmount;

	/**
     *  
     */
	private double useEarnestAmount;

	/**
     *  
     */
	private double unUseEarnestAmount;

	/**
     *  
     */
	private double totalEarnestAmount;

	/**
     *  
     */
	private double receivableSum;

	/**
     *  
     */
	private String invoiceNo;

	/**
     *  
     */
	private String invoiceType;

	/**
     *  
     */
	private String orderSalesTypeName;

	/**
     *  
     */
	private String customerName;


	private String customerCode;

	private String contacts;

	public String getSalesNo() {
		return salesNo;
	}

	public void setSalesNo(String salesNo) {
		this.salesNo = salesNo;
	}

	private String salesNo;
	private String accountDate;


	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	private String status;


	public String getNoOutReturnQuantity() {
		return NoOutReturnQuantity;
	}

	public void setNoOutReturnQuantity(String noOutReturnQuantity) {
		NoOutReturnQuantity = noOutReturnQuantity;
	}

	/**
	 *  未出库退货数量
	 */
	private String NoOutReturnQuantity; // 王英杰 2020 11

	private String otherContacts;

	private String address;

	public String getStaffId() {
		return staffId;
	}

	public void setStaffId(String staffId) {
		this.staffId = staffId;
	}

	private String staffId;

	private String organizationId;

	private String organizationCode;

	public boolean isChecked() {
		return isChecked;
	}

	public void setChecked(boolean checked) {
		isChecked = checked;
	}

	private boolean isChecked;

	public String getOtherContacts() {
		return otherContacts;
	}

	public void setOtherContacts(String otherContacts) {
		this.otherContacts = otherContacts;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(String organizationId) {
		this.organizationId = organizationId;
	}

	public String getOrganizationCode() {
		return organizationCode;
	}

	public void setOrganizationCode(String organizationCode) {
		this.organizationCode = organizationCode;
	}

	public String getTruckNumber() {
		return truckNumber;
	}

	public void setTruckNumber(String truckNumber) {
		this.truckNumber = truckNumber;
	}

	public String getInvoiceLayoutId() {
		return invoiceLayoutId;
	}

	public void setInvoiceLayoutId(String invoiceLayoutId) {
		this.invoiceLayoutId = invoiceLayoutId;
	}

	private String truckNumber;

	private String invoiceLayoutId;

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	private String customerId;

	/**
	 *
	 */
	private String customerAddress;


	private String telephone;


	/**
	 *
	 */
	private String createDate;
	/**
	 * 部门
	 */
	private String organizationName;
	/**
	 * 业务员
	 */
	private String staffName;

	public String getWarehouseName() {
		return warehouseName;
	}

	public void setWarehouseName(String warehouseName) {
		this.warehouseName = warehouseName;
	}

	private String warehouseName;

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	private String remarks;

	public String getSubSalesMan() {
		return subSalesMan;
	}

	public void setSubSalesMan(String subSalesMan) {
		this.subSalesMan = subSalesMan;
	}

	private String subSalesMan;

	public String getOrganizationName() {
		return organizationName;
	}

	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getCustomerAddress() {
		return customerAddress;
	}

	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}

	public String getStaffName() {
		return staffName;
	}

	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}

	public String getInvoiceId() {
		return invoiceId;
	}

	public void setInvoiceId(String invoiceId) {
		this.invoiceId = invoiceId;
	}

	public String getAccountDate() {
		return accountDate;
	}

	public void setAccountDate(String accountDate) {
		this.accountDate = accountDate;
	}

	public String getInvoiceNo() {
		return invoiceNo;
	}

	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	public String getInvoiceType() {
		return invoiceType;
	}

	public void setInvoiceType(String invoiceType) {
		this.invoiceType = invoiceType;
	}

	public String getOrderSalesTypeName() {
		return orderSalesTypeName;
	}

	public void setOrderSalesTypeName(String orderSalesTypeName) {
		this.orderSalesTypeName = orderSalesTypeName;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getContacts() {
		return contacts;
	}

	public void setContacts(String contacts) {
		this.contacts = contacts;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public double getGoodsAmount() {
		return goodsAmount;
	}

	public void setGoodsAmount(double goodsAmount) {
		this.goodsAmount = goodsAmount;
	}

	public double getDiscountAmount() {
		return discountAmount;
	}

	public void setDiscountAmount(double discountAmount) {
		this.discountAmount = discountAmount;
	}

	public double getMarkedPriceAmount() {
		return markedPriceAmount;
	}

	public void setMarkedPriceAmount(double markedPriceAmount) {
		this.markedPriceAmount = markedPriceAmount;
	}

	public double getDiscount() {
		return discount;
	}

	public void setDiscount(double discount) {
		this.discount = discount;
	}

	public double getFeeAmount() {
		return feeAmount;
	}

	public void setFeeAmount(double feeAmount) {
		this.feeAmount = feeAmount;
	}

	public double getEarnestAmount() {
		return earnestAmount;
	}

	public void setEarnestAmount(double earnestAmount) {
		this.earnestAmount = earnestAmount;
	}

	public double getDepositReceivedAmount() {
		return depositReceivedAmount;
	}

	public void setDepositReceivedAmount(double depositReceivedAmount) {
		this.depositReceivedAmount = depositReceivedAmount;
	}

	public double getReceivableAmount() {
		return receivableAmount;
	}

	public void setReceivableAmount(double receivableAmount) {
		this.receivableAmount = receivableAmount;
	}

	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public double getAccountReceivableAmount() {
		return accountReceivableAmount;
	}

	public void setAccountReceivableAmount(double accountReceivableAmount) {
		this.accountReceivableAmount = accountReceivableAmount;
	}

	public double getToSalesAmount() {
		return toSalesAmount;
	}

	public void setToSalesAmount(double toSalesAmount) {
		this.toSalesAmount = toSalesAmount;
	}

	public double getUnToSalesAmount() {
		return unToSalesAmount;
	}

	public void setUnToSalesAmount(double unToSalesAmount) {
		this.unToSalesAmount = unToSalesAmount;
	}

	public double getToFactSalesAmount() {
		return toFactSalesAmount;
	}

	public void setToFactSalesAmount(double toFactSalesAmount) {
		this.toFactSalesAmount = toFactSalesAmount;
	}

	public double getUseEarnestAmount() {
		return useEarnestAmount;
	}

	public void setUseEarnestAmount(double useEarnestAmount) {
		this.useEarnestAmount = useEarnestAmount;
	}

	public double getUnUseEarnestAmount() {
		return unUseEarnestAmount;
	}

	public void setUnUseEarnestAmount(double unUseEarnestAmount) {
		this.unUseEarnestAmount = unUseEarnestAmount;
	}

	public double getTotalEarnestAmount() {
		return totalEarnestAmount;
	}

	public void setTotalEarnestAmount(double totalEarnestAmount) {
		this.totalEarnestAmount = totalEarnestAmount;
	}

	public double getReceivableSum() {
		return receivableSum;
	}

	public void setReceivableSum(double receivableSum) {
		this.receivableSum = receivableSum;
	}

}
