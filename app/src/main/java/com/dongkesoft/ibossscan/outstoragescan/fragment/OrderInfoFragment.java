package com.dongkesoft.ibossscan.outstoragescan.fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.dongkesoft.ibossscan.R;
import com.dongkesoft.ibossscan.outstoragescan.activity.SalesOutStorageScanNewActivity;
import com.dongkesoft.ibossscan.outstoragescan.adapter.AchievementAdapter;
import com.dongkesoft.ibossscan.outstoragescan.adapter.StaffsAdapter;
import com.dongkesoft.ibossscan.outstoragescan.bean.AchievementInfo;
import com.dongkesoft.ibossscan.outstoragescan.bean.ResponsiblePersonInfo;
import com.dongkesoft.ibossscan.outstoragescan.bean.SaleOrderDetail;
import com.dongkesoft.ibossscan.outstoragescan.presenter.OrderInfoPresenter;
import com.dongkesoft.ibossscan.outstoragescan.view.IOrderInfoView;
import com.dongkesoft.ibossscan.utils.Comment;
import com.dongkesoft.ibossscan.utils.ProcessDialogUtils;
import com.dongkesoft.ibossscan.utils.ToastUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import dongkesoft.com.dkmodule.mvp.presenter.BaseFragmentPresenter;
import dongkesoft.com.dkmodule.ui.fragment.BaseFragment;

/**
 *
 */
public class OrderInfoFragment extends BaseFragment <IOrderInfoView, OrderInfoPresenter>    implements IOrderInfoView  {

     @BindView(R.id.tv_CustomerCode)
    public TextView tvCustomerCode;

     @BindView(R.id.tv_CustomerName)
    public TextView tvCustomerName;

    @BindView(R.id.tv_Telephone)
    public TextView tvTelephone;

    @BindView(R.id.tv_Driver)
    public TextView tvDriver;

    @BindView(R.id.tv_Worker)
    public TextView tvWorker;

    @BindView(R.id.tv_Warehouse)
    public TextView tvWarehouse;


    public TextView tvInvoiceLayout;


    /**
     * AlertDialog
     */
    private Dialog mDialog;
    /**
     * 业绩信息列表
     */
    private ListView lvSelect;
    /**
     * 业绩信息搜索框
     */
    private EditText edtSearch;


    /**
     * 地址
     */
    @BindView(R.id.tv_Address)
    public TextView tvCustomerAddress;

    @BindView(R.id.tv_Department)
    public TextView tvDepartment;

    @BindView(R.id.tv_SalesNo)
    public TextView tvSalesNo;

    @BindView(R.id.tv_AccountDate)
    public TextView tvCreateDate;

    @BindView(R.id.edt_Remark)
    /**
     * 备注
     */
    public EditText edtRemarks;


    private List<ResponsiblePersonInfo> staffList;

    private List<ResponsiblePersonInfo> staffInfoList;

    private List<ResponsiblePersonInfo> dockInfoList;
    private String name;
    private StaffsAdapter staffAdapter;

    private StaffsAdapter dockAdapter;

    public String staffId="";

    public String dockId="";
    private List<String> departmentList;
    private List<AchievementInfo> listDataAchievementInfos;
    private List<AchievementInfo> achievementList;
    public String mDepartmentName;
    public String mDepartmentId="";

    public String mDepartmentCode="";


    /**
     * 加载司机数据
     */
    private void loadDriverData() {
        HashMap<String, String> map = new HashMap<>();
        map.put("Action", "GetStaffForDelivery");
        map.put("AccountCode", mAccountCode);
        map.put("UserCode", mUserCode);
        map.put("UserPassword", mPassword);
        map.put("SessionKey", mSessionKey);
        presenter.loadDriverData(getActivity(),map,mServerAddressIp,mServerAddressPort);

    }

    /**
     * 部门数据加载
     */
    private void departmentLoad() {
        HashMap<String, String> map = new HashMap<>();
        // 业务部门
        map.put("Action", "GetOrganizationDataSource");
        map.put("AccountCode", mAccountCode);
        map.put("UserCode", mUserCode);
        map.put("UserPassword", mPassword);
        map.put("SessionKey", mSessionKey);
        map.put("OrganizationName", "");
        map.put("OrganizationCode", "");
        map.put("IsOnlyDisplayEnd", "true");
        ProcessDialogUtils.showProcessDialog(getActivity());
          presenter.loadDepartmentData(getActivity(),map,mServerAddressIp,mServerAddressPort);

    }


    /**
     * 加载数据
     * @param searchBean
     */
    public void loadData(SaleOrderDetail searchBean) {
        tvCustomerCode.setText(searchBean == null ? "" : searchBean.getCustomerCode());
        tvCustomerName.setText(searchBean == null ? "" : searchBean.getCustomerName());
        tvTelephone.setText(searchBean == null ? "" : searchBean.getTelephone());
        tvCustomerAddress.setText(searchBean == null ? "" : searchBean.getAddress());
        edtRemarks.setText(searchBean == null ? "" : searchBean.getRemarks());
        tvWarehouse.setText(searchBean==null?"":searchBean.getWarehouseName());
        tvSalesNo.setText(searchBean==null?"":searchBean.getSalesNo());
        tvCreateDate.setText(searchBean==null?"":searchBean.getCreateDate());
        tvDepartment.setText(mDepartmentName);
    }


    /**
     * 点击事件
     */
    protected void setOnClick() {

        tvDriver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Comment.staffInfos = staffList;
                showDialog("1");
            }
        });

        tvWorker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Comment.dockInfos = staffList;
                showDialog("2");
            }
        });
        tvDepartment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               departmentLoad();
            }
        });


    }


    /**
     * 显示对话框
     * @param string
     */
    private void showDialog(String string) {
        this.name = string;
       if (mDialog == null) {
			mDialog = new AlertDialog.Builder(getActivity(),R.style.MyDialog).create();
		}

        mDialog.show();
		//mDialog.setCanceledOnTouchOutside(false);
		View view = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_choose, null);

		mDialog.setContentView(view);
		mDialog.getWindow().setGravity(Gravity.BOTTOM);
        mDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT);
        lvSelect = (ListView) view.findViewById(R.id.select_list);
        edtSearch = (EditText) view.findViewById(R.id.et_search);
      //  mDialog.getWindow().setWindowAnimations(R.style.bottom_up_animation);
       mDialog.getWindow().clearFlags(
               WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);


        if (name.equals("1")) {
            Comment.staffInfoList = Comment.staffInfos;
            staffAdapter = new StaffsAdapter(getActivity(),
                    Comment.staffInfoList);
            lvSelect.setAdapter(staffAdapter);
        }

        if (name.equals("2")) {
            Comment.dockInfoList = Comment.dockInfos;
            staffAdapter = new StaffsAdapter(getActivity(),
                    Comment.dockInfoList);
            lvSelect.setAdapter(staffAdapter);
        }
        edtSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                staffInfoList = new ArrayList<ResponsiblePersonInfo>();
                dockInfoList = new ArrayList<ResponsiblePersonInfo>();
                if (name.equals("1")) {
                    for (int i = 0; i < Comment.staffInfos.size(); i++) {
                        if ((Comment.staffInfos.get(i)
                                .getStaffName()
                                .indexOf(edtSearch.getText().toString()) >= 0)) {
                            staffInfoList.add(Comment.staffInfos.get(i));
                        }
                    }
                    Comment.staffInfoList = staffInfoList;
                    staffAdapter = new StaffsAdapter(
                            getActivity(), Comment.staffInfoList);
                    lvSelect.setAdapter(staffAdapter);
                }

                if (name.equals("2")) {
                    for (int i = 0; i < Comment.dockInfos.size(); i++) {
                        if ((Comment.dockInfos.get(i)
                                .getStaffName()
                                .indexOf(edtSearch.getText().toString()) >= 0)) {
                            dockInfoList.add(Comment.dockInfos.get(i));
                        }
                    }
                    Comment.dockInfoList = dockInfoList;
                    dockAdapter = new StaffsAdapter(
                            getActivity(), Comment.dockInfoList);
                    lvSelect.setAdapter(dockAdapter);
                }
            }

        });

        lvSelect.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {

                if (name.equals("1")) {
                    tvDriver.setText(Comment.staffInfoList.get(arg2)
                            .getStaffName());

                    staffId = String.valueOf(Comment.staffInfoList
                            .get(arg2).getStaffID());

                    ((SalesOutStorageScanNewActivity)  getActivity()).edtBarcode.setFocusable(true);
                    ((SalesOutStorageScanNewActivity)  getActivity()).edtBarcode.requestFocus();
                    mDialog.dismiss();
                    return;
                }

                if (name.equals("2")) {
                    tvWorker.setText(Comment.dockInfoList.get(arg2)
                            .getStaffName());

                    dockId = String.valueOf(Comment.dockInfoList
                            .get(arg2).getStaffID());

                    ((SalesOutStorageScanNewActivity)  getActivity()).edtBarcode.setFocusable(true);
                    ((SalesOutStorageScanNewActivity)  getActivity()).edtBarcode.requestFocus();
                    mDialog.dismiss();
                    return;
                }

            }
        });


    }

    /**
     * 加载部门对话框
     */
    private void showDepartmentDialog() {
       
        if (mDialog == null) {
            mDialog = new AlertDialog.Builder(getActivity(),R.style.MyDialog).create();
        }

        mDialog.show();
        View view = LayoutInflater.from(getActivity()).inflate(
                R.layout.dialog_choose, null);

        mDialog.setContentView(view);
        mDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,WindowManager.LayoutParams.MATCH_PARENT);
        lvSelect = (ListView) view.findViewById(R.id.select_list);
        edtSearch = (EditText) view.findViewById(R.id.et_search);
        mDialog.getWindow().setWindowAnimations(R.style.bottom_up_animation);
//        mDialog.getWindow().clearFlags(
//                WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        Comment.achievementInfoLists = Comment.achievementInfos;
     
            AchievementAdapter adapter = new AchievementAdapter(getActivity(),
                    Comment.achievementInfoLists);
            lvSelect.setAdapter(adapter);
        

        edtSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                achievementList = new ArrayList<AchievementInfo>();
              
                    for (int i = 0; i < Comment.achievementInfos.size(); i++) {
                        if (((Comment.achievementInfos.get(i)
                                .getOrganizationName().toString()
                                + Comment.achievementInfos.get(i)
                                .getOrganizationFullName().toString() + Comment.achievementInfos
                                .get(i).getOrganizationCode().toString())
                                .indexOf(edtSearch.getText().toString()) >= 0)) {
                            achievementList.add(Comment.achievementInfos.get(i));
                        }
                    }
                    Comment.achievementInfoLists = achievementList;
                    AchievementAdapter adapter = new AchievementAdapter(
                            getActivity(), Comment.achievementInfoLists);
                    lvSelect.setAdapter(adapter);
                
              
            }
        });

        lvSelect.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {


                    tvDepartment.setText(Comment.achievementInfoLists.get(arg2)
                            .getOrganizationName().toString());
                    mDepartmentName = Comment.achievementInfoLists.get(arg2)
                            .getOrganizationName().toString();
                    mDepartmentId = String.valueOf(Comment.achievementInfoLists
                            .get(arg2).getOrganizationID());
                    mDepartmentCode= Comment.achievementInfoLists.get(arg2).getOrganizationCode();
                   
                    listDataAchievementInfos.clear();
                    mDialog.dismiss();
                ((SalesOutStorageScanNewActivity)  getActivity()).edtBarcode.setFocusable(true);
                ((SalesOutStorageScanNewActivity)  getActivity()).edtBarcode.requestFocus();
                    return;


               
            }
        });
    }



    @Override
    protected OrderInfoPresenter createPresenter() {
        return new OrderInfoPresenter() ;
    }

    @Override
    protected int setMvpView() {
        return R.layout.fragment_order_info;
    }

    @Override
    protected void initView() {
        staffList = new ArrayList<ResponsiblePersonInfo>();
    }

    @Override
    protected void initData() {
        loadDriverData();
        setOnClick();
    }

    @Override
    public void refreshFragmentView() {

    }

    /**
     * 司机数据源加载成功
     * @param result
     */
    @Override
    public void driverLoadSuccess(String result) {
      ProcessDialogUtils.closeProgressDilog();
                        try {
                    JSONObject jsonObject = new JSONObject(result);
                    if (jsonObject.getInt("Status") == 0) {

                        JSONArray array = jsonObject.getJSONArray("Result");
                        if (array != null && array.length() > 0) {
                            for (int i = 0; i < array.length(); i++) {
                                JSONObject jObject = array.getJSONObject(i);
                                int staffId = jObject.optInt("StaffID");
                                String staffCode = jObject.optString("StaffCode");
                                String staffName = jObject.optString("StaffName");
                                ResponsiblePersonInfo staffModel = new ResponsiblePersonInfo();
                                staffModel.setStaffID(staffId);
                                staffModel.setStaffCode(staffCode);
                                staffModel.setStaffName(staffName);
                                staffList.add(staffModel);
                            }
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
    }

    /**
     * 司机数据源加载失败
     * @param message
     */
    @Override
    public void driverLoadFail(String message) {
        ProcessDialogUtils.closeProgressDilog();
        ToastUtil.showShortToast(getActivity(),message);

    }


    /**
     * 部门加载成功
     * @param result
     */
    @Override
    public void departmentLoadSuccess(String result) {
        ProcessDialogUtils.closeProgressDilog();

        listDataAchievementInfos = new ArrayList<AchievementInfo>();
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getInt("Status") == 0) {
                JSONArray array = new JSONArray(jsonObject
                        .getString("Result"));

                for (int i = 0; i < array.length(); i++) {
                    JSONObject jsonDatata = new JSONObject(array
                            .getString(i));

                    AchievementInfo info = new AchievementInfo();
                    info.setOrganizationID(jsonDatata
                            .getInt("OrganizationID"));
                    info.setOrganizationCode(jsonDatata
                            .getString("OrganizationCode"));
                    info.setOrganizationName(jsonDatata
                            .getString("OrganizationName"));
                    info.setOrganizationFullName(jsonDatata
                            .getString("OrganizationFullName"));

                    listDataAchievementInfos.add(info);
                }
                Comment.achievementInfos = listDataAchievementInfos;
                 showDepartmentDialog();
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }


    }

    /**
     * 部门加载失败
     * @param message
     */
    @Override
    public void departmentLoadFail(String message) {
        ProcessDialogUtils.closeProgressDilog();
        ToastUtil.showShortToast(getActivity(),message);
    }

    @Override
    public void onResume()
    {

        super.onResume();
    }
}
