package com.dongkesoft.ibossscan.common;

/**
 * Created by guanhonghou on 2018/9/26.
 */

import android.app.Activity;
import android.app.Application;
import android.os.Build;


import androidx.multidex.MultiDex;


import com.dongkesoft.ibossscan.utils.CrashHandler;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;


/*
 * Copyright(c) 2016 dongke All rights reserved. / Confidential
 * 类的信息：
 *		1.程序名称： SysApplication
 *		2.功能描述：Application
 * 编辑履历：
 *		作者				日期					版本				修改内容
 *		dongke			2016/12/07			1.00			新建
 *******************************************************************************/
public class SysApplication  extends Application {

    private static SysApplication instance;
    private List<Activity> mList = new LinkedList<Activity>();

    public SysApplication() {
        instance = this;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        CrashHandler crashHandler = CrashHandler.getInstance();
        crashHandler.init(this);
        // 只有带硬件的才能打印，其他的pda不用加载posapi
// 初始化MultiDex
        try {
            MultiDex.install(this);
        } catch (Exception e) {

        }
    }

    public synchronized static SysApplication getInstance() {
        if (null == instance) {
            instance = new SysApplication();
        }
        return instance;
    }

    // add Activity
    public void addActivity(Activity activity) {
        mList.add(activity);
    }


    public List<Activity> getAllActivity() {
        return mList;
    }

    public void exit() {
        try {
            for (Activity activity : mList) {
                if (activity != null)
                    activity.finish();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            System.exit(0);
        }
    }


    public void onLowMemory() {
        super.onLowMemory();
        System.gc();
    }
}
