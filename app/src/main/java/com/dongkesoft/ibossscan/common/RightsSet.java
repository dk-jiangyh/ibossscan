package com.dongkesoft.ibossscan.common;

public class RightsSet {

    // 缺货申请
    public static final String GROUP_OUTOFSTOCKAPPLY_ANDROID = "200113";
    // 缺货申请审核
    public static final String GROUP_OUTOFSTOCKAPPLYCHECK_ANDROID = "200114";
    // 液奶备货
    public static final String GROUP_OUTDELIVERY_ANDROID = "200115";
    // 奶粉扫码
    public static final String GROUP_DRIEDMILKOUTDELIVERY_ANDROID = "200116";
    // 生成二维码
    public static final String GROUP_SCANCODE_ANDROID = "200117";
    //出库补扫
    public static final String GROUP_OUTSTORAGESUPPLEMENTARY_ANDROID = "200118";
    //退货扫码
    public static final String GROUP_DRIEDMILKRETURNGOODS_ANDROID = "200119";
    // 退货补扫
    public static final String GROUP_DRIEDMILKRETURNGOODSSUPPLEMENTARYSCAN_ANDROID = "200120";
    // 出库扫码(单次)
    public static final String GROUP_DRIEDMILKOUTDELIVERY_ONE_ANDROID = "200121";
    // 2019-07-19
    // 仓储打码
    public static final String GROUP_STORAGE_PRINT_ANDROID = "200122";
    // 液奶其他出库
    public static final String GROUP_OTHER_OUT_ANDROID = "200123";
    // 修正二维码
    public static final String GROUP_UPDATE_QRCODE_ANDROID = "200124";

    public static final String GROUP_DELIVER_OUTOF_STOCK_ANDROID = "200125";

    public static final String GROUP_GOODS_REMAIN_ANDROID = "200126";

    public static final String GROUP_GUARANTEE_GOODS_ANDROID = "200127";
    // 二维码-删除20191211
    public static final String GROUP_DEL_SCANCODE_ANDROID = "200128";
    // 重新打印二维码20200110
    public static final String GROUP_NEW_SCANCODE_ANDROID = "200129";
    //待出库汇总表
    public static final String GROUP_TOBEOUTOFSTOCKTOTAL_ANDROID = "200147";
    //二维码替换-扫码出库完成后 替换二维码
    public static final String GROUP_OUTDELIVERY_CHANGE_QRCODE_ANDROID = "200149";
    //在其它客户端登录的时候，返回的值
    public static final int NegativeOne = 12;
    //浓度25最小，越大越费电 (第一台打印机的浓度常量)
    public static final int CONCENTRATION = 45;

}
