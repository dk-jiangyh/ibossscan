/*******************************************************************************
 * Copyright(c) 2018 dongke All rights reserved. / Confidential
 * 类的信息：
 * 1.程序名称：SetActivity
 * 2.功能描述：设置ip端口页面
 * 编辑履历：
 * 作者				日期					版本				修改内容
 * dongke			2018/6/8			1.00			新建
 *******************************************************************************/
package com.dongkesoft.ibossscan.login.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.dongkesoft.ibossscan.login.presenter.SetPresenter;
import com.dongkesoft.ibossscan.login.view.SetView;
import com.dongkesoft.ibossscan.R;

import butterknife.BindView;
import butterknife.OnClick;
import dongkesoft.com.dkmodule.common.Constants;
import dongkesoft.com.dkmodule.ui.activity.BaseActivity;

/**
 * @Description : 设置ip端口页面
 * @ClassName : SetActivity
 * @Author : zhaoyu
 * @Date :
 */
public class SetActivity extends BaseActivity<SetView, SetPresenter>
        implements SetView {
    /*
     * 保存
     * */
    @BindView(R.id.btn_save)
    Button btnSave;
    /*
     * 返回
     * */
    @BindView(R.id.btn_cancle)
    Button btnBack;
    /*
     * ip和端口的区域
     * */
    @BindView(R.id.ll_login_center)
    LinearLayout llLogin;
    /*
     * ip
     * */
    @BindView(R.id.set_ip)
    EditText edt_ip;
    /*
     * 端口
     * */
    @BindView(R.id.set_port)
    EditText edt_port;

    int  mHeight;

    /**
     * 创建一个与之关联的Presenter
     */
    @Override
    protected SetPresenter createPresenter() {
        return new SetPresenter();
    }

    /**
     * layout视图
     */
    @Override
    protected int setMvpView() {
        return R.layout.activity_set;
    }

    /**
     * 初始化view
     */
    @Override
    protected void initView() {
        dynamicState();
    }

    /**
     * 初始化数据
     */
    @Override
    protected void initData() {
        SharedPreferences sharedPreferences = getSharedPreferences(Constants.STR_SHARED_PRE_KEYS,
                Context.MODE_PRIVATE);
        edt_ip.setText(sharedPreferences.getString("ServerAddressIp", ""));
        edt_port.setText(sharedPreferences.getString("ServerAddressPort", ""));
    }

    /**
     * 单击事件
     *
     * @param view
     */
    @OnClick({R.id.btn_cancle, R.id.btn_save})
    public void onViewClicked(View view) {

        int i = view.getId();
        if (i == R.id.btn_cancle) {
            finish();
        }

        if (i == R.id.btn_save) {
            if (!edt_ip.getText().toString().equals("") && !edt_port.getText().toString().equals("")) {
                SharedPreferences sharedPreferences = getSharedPreferences(Constants.STR_SHARED_PRE_KEYS,
                        Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("ServerAddressIp", edt_ip.getText().toString().trim());
                editor.putString("ServerAddressPort", edt_port.getText().toString().trim());
                editor.commit();
                finish();
            } else if (edt_ip.getText().toString().trim().equals("")) {
                Toast.makeText(this, "请输入IP", Toast.LENGTH_SHORT).show();
                return;
            } else if (edt_port.getText().toString().trim().equals("")) {
                Toast.makeText(this, "请输入端口号", Toast.LENGTH_SHORT).show();
                return;
            }
        }
    }

    /*
     * 不同分辨率的尺寸
     * */
    private void dynamicState() {
        DisplayMetrics   metric = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metric);

        if(metric.heightPixels >=1920){
            mHeight = 490;
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) llLogin
                    .getLayoutParams();
            params.setMargins(57, mHeight,
                    57, 10);// 通过自定义坐标来放置你的控件
            llLogin.setLayoutParams(params);


            RelativeLayout.LayoutParams paramsrlLogin = (RelativeLayout.LayoutParams) btnSave
                    .getLayoutParams();
            paramsrlLogin.setMargins(160, 10, 160, 10);// 通过自定义坐标来放置你的控件
            btnSave.setLayoutParams(paramsrlLogin);

            RelativeLayout.LayoutParams paramsrlSet = (RelativeLayout.LayoutParams) btnBack
                    .getLayoutParams();
            paramsrlSet.setMargins(57 + 160, 10,
                    57 + 160, 10);// 通过自定义坐标来放置你的控件
            btnBack.setLayoutParams(paramsrlSet);
        }
        if(metric.heightPixels >= 1280 && metric.heightPixels < 1920){
            mHeight = 366;
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) llLogin
                    .getLayoutParams();
            params.setMargins(23, mHeight,
                    23, 10);// 通过自定义坐标来放置你的控件
            llLogin.setLayoutParams(params);

            RelativeLayout.LayoutParams paramsrlSet = (RelativeLayout.LayoutParams) btnBack
                    .getLayoutParams();
            paramsrlSet.setMargins(23 + 107, 10,
                    23  + 107, 10);// 通过自定义坐标来放置你的控件
            btnBack.setLayoutParams(paramsrlSet);
            RelativeLayout.LayoutParams paramsrlLogin = (RelativeLayout.LayoutParams) btnSave
                    .getLayoutParams();
            paramsrlLogin.setMargins(107, 10, 107, 10);// 通过自定义坐标来放置你的控件
            btnSave.setLayoutParams(paramsrlLogin);
        }
        if(metric.heightPixels < 1280){
            mHeight = 170;
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) llLogin
                    .getLayoutParams();
            params.setMargins(23, mHeight,
                    23, 10);// 通过自定义坐标来放置你的控件
            llLogin.setLayoutParams(params);
            RelativeLayout.LayoutParams paramsrlSet = (RelativeLayout.LayoutParams) btnBack
                    .getLayoutParams();
            paramsrlSet.setMargins(23  + 80, 10,
                    23 + 80, 10);// 通过自定义坐标来放置你的控件
            btnBack.setLayoutParams(paramsrlSet);
            RelativeLayout.LayoutParams paramsrlLogin = (RelativeLayout.LayoutParams) btnSave
                    .getLayoutParams();
            paramsrlLogin.setMargins(80, 10, 80, 10);// 通过自定义坐标来放置你的控件
            btnSave.setLayoutParams(paramsrlLogin);
        }




    }


}
