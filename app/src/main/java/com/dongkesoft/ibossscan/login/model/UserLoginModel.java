/*******************************************************************************
 * Copyright(c) 2018 dongke All rights reserved. / Confidential
 * 类的信息：
 *		1.程序名称：UserLoginModel
 *		2.功能描述：用户登录model实现
 * 编辑履历：
 *		作者				日期					版本				修改内容
 *		dongke			2018/6/8			1.00			新建
 *******************************************************************************/
package com.dongkesoft.ibossscan.login.model;

import java.util.HashMap;

import dongkesoft.com.dkmodule.common.Constants;
import dongkesoft.com.dkmodule.mvp.model.BaseModel;
import dongkesoft.com.dkmodule.responseinterface.AsyncHttpResponseHandler;
import dongkesoft.com.dkmodule.utils.ExecutorServiceUtils;
import dongkesoft.com.dkmodule.utils.OkHttpManager;
import dongkesoft.com.dkmodule.utils.exception.OkHttpException;
import dongkesoft.com.dkmodule.utils.listener.DisposeDataHandler;
import dongkesoft.com.dkmodule.utils.listener.DisposeDataListener;


/**
 * 项目名称：DKMvpDemo
 * 类描述：用户登录model实现
 * 创建人：apple
 * 创建时间：2018/6/19 下午4:16
 * 修改人：apple
 * 修改时间：2018/6/19 下午4:16
 * 修改备注：
 */
public class UserLoginModel extends BaseModel {
    private AsyncHttpResponseHandler callback;

    /**
     * 数据请求
     * @param callback
     */
    @Override
    public void execute(final AsyncHttpResponseHandler callback) {
     this.callback=callback;
        ExecutorServiceUtils.getInstance().excuteRunnable(runnable);
    }

    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            try {
                // 跳用接口
                OkHttpManager.getInstance().requestPostServerData(
                        url,
                        (HashMap<String, String>) params,
                        new DisposeDataHandler(new DisposeDataListener() {
                            @Override
                            public void onSuccess(Object responseObj) {
                                callback.onSuccess(Constants.SUCCESS, (String) responseObj);
                            }

                            @Override
                            public void onFailure(OkHttpException exception) {
                                callback.onFailure(exception.getErrorCode(), exception.getErrorMsg());
                            }
                        }));


            } catch (Exception e) {
                callback.onFailure(Constants.ACTION_INVALID_STATUS, e.getMessage());
            }
        }
    };

}
