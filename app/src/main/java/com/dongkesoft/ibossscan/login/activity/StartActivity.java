/*******************************************************************************
 * Copyright(c) 2018 dongke All rights reserved. / Confidential
 * 类的信息：
 * 1.程序名称：StartActivity
 * 2.功能描述：欢迎页面
 * 编辑履历：
 * 作者				日期					版本				修改内容
 * dongke			2018/6/8			1.00			新建
 *******************************************************************************/
package com.dongkesoft.ibossscan.login.activity;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.dongkesoft.ibossscan.R;

/**
 * @Description : 欢迎页面
 * @ClassName : StartActivity
 * @Author : zhaoyu
 * @Date :
 */
public class StartActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Intent intent = new Intent(StartActivity.this, LoginActivity.class);
        startActivity(intent);
        StartActivity.this.finish();
    }
}
