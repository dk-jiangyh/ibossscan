package com.dongkesoft.ibossscan.login.presenter;

/**
 * Created by guanhonghou on 2018/9/26.
 */
/*******************************************************************************
 * Copyright(c) 2018 dongke All rights reserved. / Confidential
 * 类的信息：
 *		1.程序名称：UserLoginPresenter
 *		2.功能描述：用户登录presenter实现
 * 编辑履历：
 *		作者				日期					版本				修改内容
 *		dongke			2018/6/8			1.00			新建
 *******************************************************************************/

import android.content.Context;
import android.content.SharedPreferences;

import com.dongkesoft.ibossscan.login.view.IUserLoginView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


import dongkesoft.com.dkmodule.bean.ReqCallBack;
import dongkesoft.com.dkmodule.common.Constants;
import dongkesoft.com.dkmodule.mvp.presenter.BasePresenter;
import dongkesoft.com.dkmodule.utils.OkAsyncHttpClient;


/**
 * 项目名称：DKMvpDemo
 * 类描述：用户登录presenter实现
 * 创建人：apple
 * 创建时间：2018/6/19 下午4:20
 * 修改人：apple
 * 修改时间：2018/6/19 下午4:20
 * 修改备注：
 */
public class UserLoginPresenter extends BasePresenter<IUserLoginView> {
    /**
     * preferences
     */
    private SharedPreferences preferences;

    /**
     * 构造函数
     */
    public UserLoginPresenter() {
        //this.userLoginView = userLoginView;//未优化前的方法
        //this.userLoginMobel = new UserLoginModel();//实例化用户登录业务层
    }

    /**
     * 登录
     */
    public void login(Context context, HashMap<String, String> map, String ip, String port) {

        OkAsyncHttpClient.getInstance(context).requestPostByAsynWithForm(String.format(Constants.LOGIN_URL,
                ip, port), map, new ReqCallBack<String>() {
            @Override
            public void onReqSuccess(final String result) {
                {
                    //需要在UI线程执行
                    mViewHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            // 逻辑判断
                            try {
                                JSONObject obj = new JSONObject(result);

                                int status = obj.optInt("Status");
                                String message = obj.optString("Message");
                                //
                                if (status == 0) {
                                    getView().toMainActivity(result);
                                } else {
                                    getView().showFailedError(message);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            }

            @Override
            public void onReqFailed(final String errorMsg) {
                {
                    mViewHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            getView().showFailedError(errorMsg);
                        }
                    });
                }
            }
        });
    }

    public void checkUp(Context context, HashMap<String, String> map, String ip, String port) {

        OkAsyncHttpClient.getInstance(context).requestPostByAsynWithForm(String.format(Constants.LOGIN_URL,
                ip, port), map, new ReqCallBack<String>() {
            @Override
            public void onReqSuccess(final String result) {
                {
                    //需要在UI线程执行
                    mViewHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            // 逻辑判断
                            try {
                                JSONObject obj = new JSONObject(result);

                                int status = obj.optInt("Status");
                                String message = obj.optString("Message");
                                if (status == 0) {
                                    getView().toVersion(result);
                                } else {
                                    getView().showVersionError(message);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            }

            @Override
            public void onReqFailed(final String errorMsg) {
                {
                    mViewHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            getView().showFailedError(errorMsg);
                        }
                    });
                }
            }
        });
    }

    /**
     * 保存登录信息
     *
     * @param params
     * @param jobj
//     */
//    private void saveLoginInfo(Map<String, String> params, JSONObject jobj) {
//        preferences = ((LoginActivity) getView()).getSharedPreferences(Constants.STR_SHARED_PRE_KEYS,
//                Context.MODE_PRIVATE);
//
//        SharedPreferences.Editor editor = preferences.edit();
//
//        editor.putString("AccountCode", params.get("AccountCode"));
//        editor.putString("UserCode", params.get("UserCode"));
//        editor.putString("UserPassword", params.get("UserPassword"));
//        editor.putString("NoUserPassword", params.get("NoUserPassword"));
//        if (jobj != null) {
//            String str = "";
//            JSONArray jsonArray = jobj.optJSONArray("Rights");
//            //
//            if (jsonArray != null && jsonArray.length() > 0) {
//                for (int i = 0; i < jsonArray.length(); i++) {
//                    JSONObject jsonObject2 = null;
//                    try {
//                        jsonObject2 = jsonArray.getJSONObject(i);
//                        String code = jsonObject2.getString("FunctionCode");
//                        str = str + "," + code;
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//
//                }
//
//
//            }
//
//            editor.putString("Rights", str);
//            try {
//                editor.putString("AccountID",
//                        jobj.getString("AccountID"));
//                editor.putString("UserID",
//                        jobj.getString("UserID"));
//                editor.putString("UserName",
//                        jobj.getString("UserName"));
//                editor.putString("SessionKey",
//                        jobj.getString("SessionKey"));
//                editor.putString("LicenseCode",
//                        jobj.optString("LicenseCode"));
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//
//
//            editor.commit();
//        }
//
//
//    }


    /**
     * 获取参数
     *
     * @return
     */
    @Override
    protected HashMap<String, String> getLoginParams(Map<String, String> params) {
        //  HashMap<String, String> paramsMap = super.getLoginParams();
        HashMap<String, String> paramsMap = new HashMap<String, String>();
        paramsMap.putAll(params);
        return paramsMap;
    }

    /**
     * 获取url--如果都是通用的获取url，则改行数可以不写
     *
     * @return
     */

    @Override
    protected String getURL() {
        //  String url = super.getURL();
        return String.format(Constants.URL, Constants.SERVER_IP, Constants.SERVER_PORT);
    }



}
