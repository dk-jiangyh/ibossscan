package com.dongkesoft.ibossscan.login.view;

/**
 * Created by guanhonghou on 2018/9/26.
 */

/*******************************************************************************
 * Copyright(c) 2018 dongke All rights reserved. / Confidential
 * 类的信息：
 *		1.程序名称：IUserLoginView
 *		2.功能描述：完整的登陆接口
 * 编辑履历：
 *		作者				日期					版本				修改内容
 *		dongke			2018/6/8			1.00			新建
 *******************************************************************************/


import dongkesoft.com.dkmodule.mvp.view.MvpView;

/**
 * 项目名称：DKMvpDemo
 * 类描述：完整的登陆接口。 View 接口
 * 创建人：apple
 * 创建时间：2018/6/19 下午4:12
 * 修改人：apple
 * 修改时间：2018/6/19 下午4:12
 * 修改备注：
 */
public interface IUserLoginView extends MvpView {

    /**
     * 获得用户信息
     * @return
     */
    String getUserName();
    /**
     * 获得用户密码
     * @return
     */
    String getPassword();



    /**
     * 登陆成功
     */
    void toMainActivity(String result);

    /**
     * 登陆失败
     * @param message
     */
    void showFailedError(String message);

    void hideLoading();

    void showLoading();

    void toVersion(String result);
    
    void showVersionError(String message);

}
