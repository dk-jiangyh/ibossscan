/*******************************************************************************
 * Copyright(c) 2016 DongkeSoft All rights reserved. / Confidential
 *
 * @Title : LoginActivity.java
 * @Package : com.dongkesoft.iboss.activity
 * @Description : 用户登录信息
 * @Author : dongke
 * @Date : 2016年8月23日 下午12:04:12
 * @Version : 1.00
 *******************************************************************************/
package com.dongkesoft.ibossscan.login.activity;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.provider.Settings;
import android.text.TextUtils;
import android.text.method.PasswordTransformationMethod;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.FileProvider;

import com.dongkesoft.ibossscan.common.IBossBasePopupWindow;
import com.dongkesoft.ibossscan.home.activity.HomeActivity;
import com.dongkesoft.ibossscan.login.presenter.UserLoginPresenter;
import com.dongkesoft.ibossscan.login.view.IUserLoginView;
import com.dongkesoft.ibossscan.R;
import com.dongkesoft.ibossscan.utils.CommonProgressDialog;
import com.dongkesoft.ibossscan.utils.ProcessDialogUtils;
import com.dongkesoft.ibossscan.utils.SoftKeyBoardListener;
import com.dongkesoft.ibossscan.utils.ToastUtil;
import com.dongkesoft.ibossscan.utils.UpgradePopUpWindow;

import butterknife.BindView;
import butterknife.OnClick;
import dongkesoft.com.dkmodule.common.Constants;
import dongkesoft.com.dkmodule.helper.PermissionHelper;
import dongkesoft.com.dkmodule.ui.activity.BaseActivity;
import dongkesoft.com.dkmodule.utils.Installation;
import dongkesoft.com.dkmodule.utils.Md5Utils;
import dongkesoft.com.dkmodule.utils.NetWorkUtils;

/**
 * @Description : 用户登录信息
 * @ClassName : LoginActivity
 * @Author : DongKe
 * @Date : 2016年8月23日 下午4:13:55
 */
@SuppressWarnings("deprecation")
@SuppressLint("HandlerLeak")
public class LoginActivity extends BaseActivity<IUserLoginView, UserLoginPresenter>
        implements IUserLoginView {

    /**
     * 账套
     */
    @BindView(R.id.accountEditText)
    EditText etAccount;

    /**
     * 用户名
     */
    @BindView(R.id.userEditText)
    EditText etUser;


    /**
     * 密码
     */
    @BindView(R.id.passwordEditText)
    EditText etPassword;
    /**
     * CheckBox
     */
    private boolean mAuto = false;

    private static CommonProgressDialog mDialog = null;
    private static String Downfile = "东科iboss" + ".apk";

    private static String version = null;
    private static String path = null;
    private static final int DOWNLOAD = 1;
    private static final int DOWNLOAD_FINISH = 2;
    private static String mSavePath;
    PermissionHelper permissionHelper;
    private int apkSize;

    private SharedPreferences preferences;

    private int length;

    /**
     * IP,端口号
     */
    private String strIP, strPort;
    /**
     * 初始Check
     */
    private boolean check = false;
    private boolean cancelUpdate = false;

//	@BindView(R.id.cb_autoPass)
//	 CheckBox cbAutoPass;

    @BindView(R.id.btn_login)
    Button btnLogin;

    @BindView(R.id.ll_login_center)
    LinearLayout llLogin;


    @BindView(R.id.btn_set)
    Button btnSet;
    @BindView(R.id.tv_version)
    TextView tvVersion;
    private int mHeight;
    private DisplayMetrics metric;
    private long lastClickTime;
    private static final int REQUEST_CODE = 0; // 请求码
    //private IBossBasePopupWindow upgradeWindow;
    //private ArrayList<HideSettingBean> hideSettingList;
    private static final int PERMISSON_REQUESTCODE = 0;

    /**
     * 判断是否需要检测，防止不停的弹框
     */
    private boolean isNeedCheck = true;

    private String appVersion;

    private String rights;

    /**
     * 需要进行检测的权限数组
     */
    protected String[] needPermissions = {Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.READ_PHONE_STATE, Manifest.permission.CAMERA,
            Manifest.permission.RECORD_AUDIO, Manifest.permission.CALL_PHONE};
    private IBossBasePopupWindow upgradeWindow;


    @Override
    protected UserLoginPresenter createPresenter() {
        return new UserLoginPresenter();
    }

    @Override
    protected int setMvpView() {
        return R.layout.activity_login;
    }

    @Override
    protected void initView() {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        etPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
        SharedPreferences preferences = getSharedPreferences(Constants.STR_SHARED_PRE_KEYS,
                Context.MODE_PRIVATE);


        etAccount.setText(preferences.getString("AccountCode", ""));
        etUser.setText(preferences.getString("UserCode", ""));
        etPassword.setText(preferences.getString("NoUserPassword", ""));

        try {
            tvVersion.setText("版本:" + getVersionName());
        } catch (Exception e) {
            e.printStackTrace();
        }
        dynamicState();
        etAccount.setSelection(etAccount.length());
        etUser.setSelection(etUser.length());
        etPassword.setSelection(etPassword.length());


    }

    @Override
    protected void initData() {
//		permissionHelper = new PermissionHelper(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
//				Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.BLUETOOTH, Manifest.permission.BLUETOOTH_ADMIN, Manifest.permission.READ_PHONE_STATE, Manifest.permission.CALL_PHONE, Manifest.permission.ACCESS_LOCATION_EXTRA_COMMANDS}, 100);
//
//		permissionHelper.request(new PermissionHelper.PermissionCallback() {
//			@Override
//			public void onPermissionGranted() {
//
//			}
//
//			@Override
//			public void onIndividualPermissionGranted(String[] grantedPermission) {
//
//			}
//
//			@Override
//			public void onPermissionDenied() {
//
//			}
//
//			@Override
//			public void onPermissionDeniedBySystem() {
//				permissionHelper.openAppDetailsActivity();
//			}
//		});
    }

    private void dynamicState() {
        metric = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metric);
        if (metric.heightPixels >= 1920) {
            mHeight = 490;
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) llLogin.getLayoutParams();
            params.setMargins(57, mHeight, 57, 10);// 通过自定义坐标来放置你的控件
            llLogin.setLayoutParams(params);

            RelativeLayout.LayoutParams paramsrlLogin = (RelativeLayout.LayoutParams) btnLogin.getLayoutParams();
            paramsrlLogin.setMargins(160, 10, 160, 10);// 通过自定义坐标来放置你的控件
            btnLogin.setLayoutParams(paramsrlLogin);

            RelativeLayout.LayoutParams paramsrlSet = (RelativeLayout.LayoutParams) btnSet.getLayoutParams();
            paramsrlSet.setMargins(57 + 160, 10, 57 + 160, 10);// 通过自定义坐标来放置你的控件
            btnSet.setLayoutParams(paramsrlSet);
        }
        if (metric.heightPixels >= 1280 && metric.heightPixels < 1920) {
            mHeight = 366;
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) llLogin.getLayoutParams();
            params.setMargins(23, mHeight, 23, 10);// 通过自定义坐标来放置你的控件
            llLogin.setLayoutParams(params);

            RelativeLayout.LayoutParams paramsrlSet = (RelativeLayout.LayoutParams) btnSet.getLayoutParams();
            paramsrlSet.setMargins(23 + 107, 10, 23 + 107, 10);// 通过自定义坐标来放置你的控件
            btnSet.setLayoutParams(paramsrlSet);
            RelativeLayout.LayoutParams paramsrlLogin = (RelativeLayout.LayoutParams) btnLogin.getLayoutParams();
            paramsrlLogin.setMargins(107, 10, 107, 10);// 通过自定义坐标来放置你的控件
            btnLogin.setLayoutParams(paramsrlLogin);
        }
        if (metric.heightPixels < 1280) {
            mHeight = 170;
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) llLogin.getLayoutParams();
            params.setMargins(23, mHeight, 23, 10);// 通过自定义坐标来放置你的控件
            llLogin.setLayoutParams(params);
            RelativeLayout.LayoutParams paramsrlSet = (RelativeLayout.LayoutParams) btnSet.getLayoutParams();
            paramsrlSet.setMargins(23 + 80, 10, 23 + 80, 10);// 通过自定义坐标来放置你的控件
            btnSet.setLayoutParams(paramsrlSet);
            RelativeLayout.LayoutParams paramsrlLogin = (RelativeLayout.LayoutParams) btnLogin.getLayoutParams();
            paramsrlLogin.setMargins(80, 10, 80, 10);// 通过自定义坐标来放置你的控件
            btnLogin.setLayoutParams(paramsrlLogin);
        }
    }

    @Override
    protected void onResume() {

        super.onResume();

        if (Build.VERSION.SDK_INT >= 23 && getApplicationInfo().targetSdkVersion >= 23) {
            if (isNeedCheck) {
                checkPermissions(needPermissions);
            }
        }

        SharedPreferences preferences = getSharedPreferences(Constants.STR_SHARED_PRE_KEYS,
                Context.MODE_PRIVATE);
        strIP = preferences.getString("ServerAddressIp", "");
        strPort = preferences.getString("ServerAddressPort", "");

        setoncick();
    }


    /**
     * @param permissions
     * @since 2.5.0 requestPermissions方法是请求某一权限，
     */
    private void checkPermissions(String... permissions) {
        try {
            if (Build.VERSION.SDK_INT >= 23 && getApplicationInfo().targetSdkVersion >= 23) {
                List<String> needRequestPermissonList = findDeniedPermissions(permissions);
                if (null != needRequestPermissonList && needRequestPermissonList.size() > 0) {
                    String[] array = needRequestPermissonList.toArray(new String[needRequestPermissonList.size()]);
                    Method method = getClass().getMethod("requestPermissions",
                            new Class[]{String[].class, int.class});

                    method.invoke(this, array, PERMISSON_REQUESTCODE);
                }
            }
        } catch (Throwable e) {
        }
    }

    /**
     * 获取权限集中需要申请权限的列表
     *
     * @param permissions
     * @return
     * @since 2.5.0 checkSelfPermission方法是在用来判断是否app已经获取到某一个权限
     * shouldShowRequestPermissionRationale方法用来判断是否
     * 显示申请权限对话框，如果同意了或者不在询问则返回false
     */
    private List<String> findDeniedPermissions(String[] permissions) {
        List<String> needRequestPermissonList = new ArrayList<String>();
        if (Build.VERSION.SDK_INT >= 23 && getApplicationInfo().targetSdkVersion >= 23) {
            try {
                for (String perm : permissions) {
                    Method checkSelfMethod = getClass().getMethod("checkSelfPermission", String.class);
                    Method shouldShowRequestPermissionRationaleMethod = getClass()
                            .getMethod("shouldShowRequestPermissionRationale", String.class);
                    if ((Integer) checkSelfMethod.invoke(this, perm) != PackageManager.PERMISSION_GRANTED
                            || (Boolean) shouldShowRequestPermissionRationaleMethod.invoke(this, perm)) {
                        needRequestPermissonList.add(perm);
                    }
                }
            } catch (Throwable e) {

            }
        }
        return needRequestPermissonList;
    }

    /**
     * 检测是否所有的权限都已经授权
     *
     * @param grantResults
     * @return
     * @since 2.5.0
     */
    private boolean verifyPermissions(int[] grantResults) {
        for (int result : grantResults) {
            if (result != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }

    /**
     * 申请权限结果的回调方法
     */
    @TargetApi(23)
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] paramArrayOfInt) {
        if (requestCode == PERMISSON_REQUESTCODE) {
            if (!verifyPermissions(paramArrayOfInt)) {
                showMissingPermissionDialog();
                isNeedCheck = false;
            }
        }
    }

    /**
     * 显示提示信息
     *
     * @since 2.5.0
     */
    private void showMissingPermissionDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("权限申请");
        builder.setMessage("缺少相关权限");

        // 拒绝, 退出应用
        builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });

        builder.setPositiveButton("设置", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                startAppSettings();
            }
        });

        builder.setCancelable(false);

        builder.show();
    }

    /**
     * 监听事件
     *
     * @author Administrator
     * @since 2016年12月7日
     */
    private void setoncick() {

        etAccount.setOnFocusChangeListener(mOnFocusChangeListener);
        etUser.setOnFocusChangeListener(mOnFocusChangeListener);
        etPassword.setOnFocusChangeListener(mOnFocusChangeListener);
//		cbAutoPass.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//			@Override
//			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//				mAuto = isChecked;
//			}
//		});

        SoftKeyBoardListener.setListener(LoginActivity.this, new SoftKeyBoardListener.OnSoftKeyBoardChangeListener() {
            @Override
            public void keyBoardShow(int height) {
                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) llLogin.getLayoutParams();
                if (metric.heightPixels >= 1920) {
                    params.setMargins(57,
                            metric.heightPixels - height - llLogin.getHeight() - 60 + getStatusBarHeight(), 57, 0);// 通过自定义坐标来放置你的控件
                } else if (metric.heightPixels >= 1280 && metric.heightPixels < 1920) {
                    params.setMargins(23,
                            metric.heightPixels - height - llLogin.getHeight() - 40 + getStatusBarHeight(), 23, 0);// 通过自定义坐标来放置你的控件
                } else if (metric.heightPixels < 1280) {
                    params = (RelativeLayout.LayoutParams) llLogin.getLayoutParams();
                    params.setMargins(23, mHeight, 23, 10);// 通过自定义坐标来放置你的控件

                }

                llLogin.setLayoutParams(params);
            }

            @Override
            public void keyBoardHide(int height) {
                if (metric.heightPixels >= 1920) {
                    RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) llLogin.getLayoutParams();
                    params.setMargins(57, mHeight, 57, 10);// 通过自定义坐标来放置你的控件
                    llLogin.setLayoutParams(params);

                    RelativeLayout.LayoutParams paramsrlLogin = (RelativeLayout.LayoutParams) btnLogin.getLayoutParams();
                    paramsrlLogin.setMargins(160, 10, 160, 10);// 通过自定义坐标来放置你的控件
                    btnLogin.setLayoutParams(paramsrlLogin);

                    RelativeLayout.LayoutParams paramsrlSet = (RelativeLayout.LayoutParams) btnSet.getLayoutParams();
                    paramsrlSet.setMargins(57 + 160, 10, 57 + 160, 10);// 通过自定义坐标来放置你的控件
                    btnSet.setLayoutParams(paramsrlSet);
                }
                if (metric.heightPixels >= 1280 && metric.heightPixels < 1920) {

                    RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) llLogin.getLayoutParams();
                    params.setMargins(23, mHeight, 23, 10);// 通过自定义坐标来放置你的控件
                    llLogin.setLayoutParams(params);

                    RelativeLayout.LayoutParams paramsrlSet = (RelativeLayout.LayoutParams) btnSet.getLayoutParams();
                    paramsrlSet.setMargins(23 + 107, 10, 23 + 107, 10);// 通过自定义坐标来放置你的控件
                    btnSet.setLayoutParams(paramsrlSet);
                    RelativeLayout.LayoutParams paramsrlLogin = (RelativeLayout.LayoutParams) btnLogin.getLayoutParams();
                    paramsrlLogin.setMargins(107, 10, 107, 10);// 通过自定义坐标来放置你的控件
                    btnLogin.setLayoutParams(paramsrlLogin);
                }
                if (metric.heightPixels < 1280) {
                    RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) llLogin.getLayoutParams();
                    params.setMargins(23, mHeight, 23, 10);// 通过自定义坐标来放置你的控件
                    llLogin.setLayoutParams(params);
                    RelativeLayout.LayoutParams paramsrlSet = (RelativeLayout.LayoutParams) btnSet.getLayoutParams();
                    paramsrlSet.setMargins(23 + 80, 10, 23 + 80, 10);// 通过自定义坐标来放置你的控件
                    btnSet.setLayoutParams(paramsrlSet);
                    RelativeLayout.LayoutParams paramsrlLogin = (RelativeLayout.LayoutParams) btnLogin.getLayoutParams();
                    paramsrlLogin.setMargins(80, 10, 80, 10);// 通过自定义坐标来放置你的控件
                    btnLogin.setLayoutParams(paramsrlLogin);
                }
            }
        });
    }


    /**
     * 单击事件
     *
     * @param view
     */
    @OnClick({R.id.btn_login, R.id.btn_set})
    public void onViewClicked(View view) {
        int i = view.getId();
        if (i == R.id.btn_set) {
            Intent intent = new Intent(LoginActivity.this, SetActivity.class);
            startActivity(intent);
        }
        if (i == R.id.btn_login) {
            if (etAccount.getText().toString().equals("")) {
                ToastUtil.showShortToast(LoginActivity.this, "请输入帐套");

            } else {
                if (etUser.getText().toString().equals("")) {
                    ToastUtil.showShortToast(LoginActivity.this, "请输入用户名");
                } else {
                    if (etPassword.getText().toString().equals("")) {
                        ToastUtil.showShortToast(LoginActivity.this, "请输入密码");
                    } else if (strIP == "") {
                        ToastUtil.showShortToast(LoginActivity.this, "请输IP");
                    } else if (strPort == "") {
                        ToastUtil.showShortToast(LoginActivity.this, "请输端口号");
                    } else {
                        login(false);


                    }
                }
            }
        }
    }


    /**
     * 登录
     *
     * @param isAuto
     * @author Administrator
     * @since 2016年12月7日
     */
    private void login(final boolean isAuto) {
        if (!NetWorkUtils.isNetworkAvailable(LoginActivity.this)) {
            ToastUtil.showShortToast(LoginActivity.this, "网络出错，请检查网络");
            return;
        }
        try {
            ProcessDialogUtils.showProcessDialog(LoginActivity.this);
            String VersionName = "";

            VersionName = getVersionName();
            String Mac = "00:00:00:00:00";
            String Ip = "127.0.0.1";

            Ip = getIpAddressString();// getIpAddress();
            Mac = getLocalMacAddressFromIp(Ip);

            // 登陆参数
            HashMap<String, String> map = new HashMap<>();

            map.put("Action", "SMDLogin");
            SharedPreferences preferences = getSharedPreferences(Constants.STR_SHARED_PRE_KEYS,
                    Context.MODE_PRIVATE);
            mServerAddressIp = preferences.getString("ServerAddressIp", "");
            mServerAddressPort = preferences.getString("ServerAddressPort", "");


//			if (isAuto) {
//				map.put("AccountCode", preferences.getString("AccountCode", ""));
//				map.put("UserCode", preferences.getString("UserCode", ""));
//				map.put("UserPassword", preferences.getString("UserPassword", ""));
//
//			} else {
            map.put("AccountCode", etAccount.getText().toString());
            map.put("UserCode", etUser.getText().toString());
            map.put("UserPassword", Md5Utils.encode(etPassword.getText().toString().trim()));
            //}

            map.put("MACAddress", Mac);
            map.put("IPAddress", Ip);
            map.put("PhoneCode", Installation.getIMEIDeviceId(LoginActivity.this));
            map.put("PhoneType", Build.MODEL);
            map.put("AppVersion", VersionName);
            map.put("SystemType", "1");
            map.put("SystemVersion", "Android " + Build.VERSION.RELEASE);

            presenter.login(this, map, mServerAddressIp, mServerAddressPort);


        } catch (Exception e) {
            e.printStackTrace();
            ProcessDialogUtils.closeProgressDilog();
            ToastUtil.showShortToast(LoginActivity.this, "登录异常" + e.getMessage());
        }
    }

    /*
     *  正常登陆
     * */
    @Override
    public void toMainActivity(String result) {
        String token = "";
        ProcessDialogUtils.closeProgressDilog();
        String userName = "";
        try {
            JSONObject jsonObject = new JSONObject(result);

            SharedPreferences preferences =
                    getSharedPreferences(Constants.STR_SHARED_PRE_KEYS,
                            Context.MODE_PRIVATE);

            Editor editor = preferences.edit();
            editor.putString("AccountCode", etAccount.getText().toString());
            editor.putString("UserCode", etUser.getText().toString());

            editor.putString("UserPassword",
                    Md5Utils.encode(etPassword.getText().toString().trim()));
            // 服务器端没有匹配，暂时注释掉
            String str = "";
            JSONArray jsonArray = jsonObject.optJSONArray("Rights");
            if (jsonArray != null && jsonArray.length() > 0) {
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject2 = jsonArray.getJSONObject(i);
                    String code = jsonObject2.getString("FunctionCode");
                    str = str + "," + code;
                }
            }
            editor.putString("Rights", str);
            editor.putString("AccountID", jsonObject.getString("AccountID"));
            editor.putString("UserID", jsonObject.getString("UserID"));
            editor.putString("OrganizationId", jsonObject.optString("OrgID"));
            editor.putString("UserName", jsonObject.getString("UserName"));
            editor.putString("SessionKey", jsonObject.getString("SessionKey"));
            editor.putString("LicenseCode", jsonObject.optString("LicenseCode"));
            editor.putString("NoUserPassword", etPassword.getText().toString().trim());
            editor.putBoolean("selfmotionlogin", mAuto);
            editor.commit();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        checkUp();

    }

    /**
     * Handler对象
     *
     * @author Administrator
     * @since 2016年12月7日
     */
    static Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                    Bundle b = msg.getData();
                    String result = b.getString("Result");
                    JSONObject jo;
                    try {
                        jo = new JSONObject(result);
                        int status = jo.optInt("Status");
                        if (status == 0) {

                            String s = jo.getString("Result");
                            String[] arr = s.split(",");
                            version = arr[0];
                            path = arr[1];
                            path = path.replace("/", "\\");
                        }

                    } catch (Exception e) {
                    }

                    break;

            }
        }
    };

    /**
     * 下载窗体
     */
    private void ShowDownloadDialog() {
        mDialog = new CommonProgressDialog(this);
        mDialog.setIndeterminate(false);
        mDialog.setCanceledOnTouchOutside(false);
        mDialog.setCancelable(false);
        mDialog.show();
        UpdateThread ut = new UpdateThread();
        ut.start();
    }

    @Override
    public String getUserName() {
        return null;
    }

    @Override
    public String getPassword() {
        return null;
    }


    @Override
    public void showFailedError(String message) {
        ProcessDialogUtils.closeProgressDilog();
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showLoading() {

    }

    @Override
    public void toVersion(String result) {
        JSONObject obj = null;
        try {
            obj = new JSONObject(result);
            int status = obj.optInt("Status");
            if (status == 0) {
                String result1 = obj.optString("Result");
                if (!TextUtils.isEmpty(result1)) {
                    String[] arry = result1.split(",");
                    version = arry[0];
                    path = arry[1];
                    String localVersionNo = getVersionName().replace(".", "");
                    String serverVersionNo = version.replace(".", "");
                    check = CompareServerVersion(localVersionNo, serverVersionNo);
                    if (check) {
                        upgradeWindow = new IBossBasePopupWindow(LoginActivity.this,
                                R.layout.popup_window_exit);
                        upgradeWindow.setPopUpWindowCallBack(new IBossBasePopupWindow.IPopUpWindowCallBack() {

                            @Override
                            public void popUpWindowCallBack(View view) {
                                TextView tvPopupWindowMessage = (TextView) view
                                        .findViewById(R.id.tv_popup_window_message);
                                TextView tvPopupWindowTitle = (TextView) view
                                        .findViewById(R.id.tv_popup_window_title);

                                tvPopupWindowTitle.setText("提示");
                                tvPopupWindowMessage.setText("发现新版本，请立刻升级!!! ");

                                // 对布局文件进行初始化
                                RelativeLayout llCancel = (RelativeLayout) view
                                        .findViewById(R.id.ll_cancel);
                                // 对布局中的控件添加事件监听
                                TextView tvCancel = (TextView) llCancel
                                        .findViewById(R.id.tv_popup_window_cancel);
                                tvCancel.setText("取消");

                                llCancel.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        if (Math.abs(System.currentTimeMillis()
                                                - lastClickTime) < Constants.MIN_CLICK_TIME) {
                                            return;
                                        }
                                        lastClickTime = System.currentTimeMillis();
                                        upgradeWindow.dismiss();
                                    }
                                });
                                RelativeLayout llOK = (RelativeLayout) view.findViewById(R.id.ll_ok);
                                // 对布局中的控件添加事件监听

                                TextView tvOk = (TextView) llOK.findViewById(R.id.tv_popup_window_ok);
                                tvOk.setText("确定");
                                llOK.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        if (Math.abs(System.currentTimeMillis()
                                                - lastClickTime) < Constants.MIN_CLICK_TIME) {
                                            return;
                                        }
                                        lastClickTime = System.currentTimeMillis();
                                        ShowDownloadDialog();
                                        upgradeWindow.dismiss();
                                    }
                                });
                            }
                        });
                        upgradeWindow.show(false, etPassword, 0, 0);
                    } else {
                        Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                        startActivity(intent);
                        finish();
                    }
                }
            }else{
                Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                startActivity(intent);
                finish();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public void showVersionError(String message) {
        Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
        startActivity(intent);
        finish();
    }

    public class UpdateThread extends Thread {
        @Override
        public void run() {
            Looper.prepare();
            try {

                if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {

                    String sdpath = Environment.getExternalStorageDirectory() + "/";
                    mSavePath = sdpath + "/ibossscan/";
                    String url1 = "http://" + strIP + ":" + strPort + "/WebService/" + path;
                    URL url = new URL(url1);

                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.connect();

                    length = conn.getContentLength();
                    mDialog.mProgress.setMax(length);
                    //
                    InputStream is = conn.getInputStream();

                    File file = new File(mSavePath);

                    if (!file.exists()) {
                        file.mkdir();
                    }
                    File apkFile = new File(mSavePath, Downfile);
                    FileOutputStream fos = new FileOutputStream(apkFile);
                    int count = 0;
                    //
                    byte buf[] = new byte[1024];
                    //
                    do {
                        int numread = is.read(buf);
                        count += numread;
                        Message m = new Message();
                        Bundle b = new Bundle();
                        b.putInt("process", count);
                        m.setData(b);
                        m.what = DOWNLOAD;
                        mHandler.sendMessage(m);
                        if (numread <= 0) {
                            mHandler.sendEmptyMessage(DOWNLOAD_FINISH);
                            mDialog.dismiss();

                            break;
                        }
                        //
                        fos.write(buf, 0, numread);
                    } while (!cancelUpdate);//
                    fos.close();
                    is.close();
                }

            } catch (MalformedURLException e) {
                mDialog.dismiss();
                ToastUtil.showShortToast(LoginActivity.this, "下载失败！！！");
                e.printStackTrace();
            } catch (IOException e) {
                ToastUtil.showShortToast(LoginActivity.this, "下载失败！！！");
                mDialog.dismiss();
                e.printStackTrace();
            }

            Looper.loop();
        }
    }

    /**
     * Handler对象
     *
     * @author Administrator
     * @since 2016年12月7日
     */
    private Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                //
                case DOWNLOAD:
                    Bundle bu = msg.getData();
                    int progress = bu.getInt("process");
                    mDialog.mProgress.setProgress(progress);
                    mDialog.setProgressNumber();
                    mDialog.setProgressPercent();

                    break;
                case DOWNLOAD_FINISH:
                    // 安装apk文件
                    installApk();
                    break;
                default:
                    break;
            }
        }

        ;
    };

    /**
     * 安装apk
     *
     * @author Administrator
     * @since 2016年12月7日
     */
    private void installApk() {
        File apkfile = new File(mSavePath, Downfile);
        if (!apkfile.exists()) {
            return;
        }
        Intent intent = new Intent(Intent.ACTION_VIEW);

        if (Build.VERSION.SDK_INT >= 24) {

            Uri contentUri = FileProvider.getUriForFile(this, "com.dongkesoft.ibossscan.fileprovider", apkfile);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            intent.setDataAndType(contentUri, "application/vnd.android.package-archive");

        } else {
            intent.setDataAndType(Uri.parse("file://" + apkfile.toString()), "application/vnd.android.package-archive");
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        }

        LoginActivity.this.startActivity(intent);
    }

    /**
     * 版本比较
     *
     * @author Administrator
     * @since 2016年12月7日
     */
    public static boolean CompareServerVersion(String localVersion, String ServerVersion) {
        boolean b = false;
        String localVersionNo = localVersion.replace(".", "");
        String serverVersionNo = ServerVersion.replace(".", "");
        if (localVersionNo.startsWith("0")) {
            localVersionNo = localVersionNo.replaceFirst("^0*", "");
        }
        if (serverVersionNo.startsWith("0")) {
            serverVersionNo = serverVersionNo.replaceFirst("^0*", "");
        }

        if (Long.parseLong(serverVersionNo) > Long.parseLong(localVersionNo)) {
            b = true;
        }
        return b;

    }

    private void checkUp() {
        HashMap<String, String> map = new HashMap<>();

        map.put("Action", "GetOutApkVersionAndApkPath");
        SharedPreferences preferences = getSharedPreferences(Constants.STR_SHARED_PRE_KEYS,
                Context.MODE_PRIVATE);
        mServerAddressIp = preferences.getString("ServerAddressIp", "");
        mServerAddressPort = preferences.getString("ServerAddressPort", "");
        String sessionKey = preferences.getString("SessionKey", "");
        map.put("AccountCode", etAccount.getText().toString());
        map.put("UserCode", etUser.getText().toString());
        map.put("UserPassword", Md5Utils.encode(etPassword.getText().toString().trim()));
        map.put("SessionKey", sessionKey);
        presenter.checkUp(this, map, mServerAddressIp, mServerAddressPort);
    }

    /**
     * 获取app版本号
     *
     * @return
     * @throws Exception
     */
    private String getVersionName() throws Exception {

        // 获取packagemanager的实例
        PackageManager packageManager = getPackageManager();
        // getPackageName()是你当前类的包名，0代表是获取版本信息
        PackageInfo packInfo = packageManager.getPackageInfo(getPackageName(), 0);
        String version = packInfo.versionName;
        return version;
    }

    // 根据busybox获取本地Mac
    public static String getLocalMacAddressFromBusybox() {
        String result = "";
        String Mac = "";
        result = callCmd("busybox ifconfig", "HWaddr");

        // 如果返回的result == null，则说明网络不可取
        if (result == null) {
            return "网络出错，请检查网络";
        }

        // 对该行数据进行解析
        // 例如：eth0 Link encap:Ethernet HWaddr 00:16:E8:3E:DF:67
        if (result.length() > 0 && result.contains("HWaddr") == true) {
            Mac = result.substring(result.indexOf("HWaddr") + 6, result.length() - 1);

            /*
             * if(Mac.length()>1){ Mac = Mac.replaceAll(" ", ""); result = ""; String[] tmp
             * = Mac.split(":"); for(int i = 0;i<tmp.length;++i){ result +=tmp[i]; } }
             */
            result = Mac;

        }
        return result;
    }

    /**
     * callCmd
     *
     * @author Administrator
     * @since 2016年12月7日
     */
    private static String callCmd(String cmd, String filter) {
        String result = "";
        String line = "";
        try {
            Process proc = Runtime.getRuntime().exec(cmd);
            InputStreamReader is = new InputStreamReader(proc.getInputStream());
            BufferedReader br = new BufferedReader(is);

            // 执行命令cmd，只取结果中含有filter的这一行
            while ((line = br.readLine()) != null && line.contains(filter) == false) {
                // result += line;

            }

            result = line;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Android 使用GPRS 获取本机IP地址方法
     */
    public static String getIpAddressString() {
        try {
            for (Enumeration<NetworkInterface> enNetI = NetworkInterface
                    .getNetworkInterfaces(); enNetI.hasMoreElements(); ) {
                NetworkInterface netI = enNetI.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = netI
                        .getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (inetAddress instanceof Inet4Address && !inetAddress.isLoopbackAddress()) {
                        return inetAddress.getHostAddress();
                    }
                }
            }
        } catch (SocketException e) {
            e.printStackTrace();
        }
        return "127.0.0.1";

    }

    /**
     * 根据IP获取本地Mac
     *
     * @param ip
     * @return
     * @author Administrator
     * @since 2016年12月7日
     */
    public static String getLocalMacAddressFromIp(String ip) {
        String tString = "00:00:00:00:00:00";
        try {
            byte[] mac;
            NetworkInterface ne = NetworkInterface.getByInetAddress(InetAddress.getByName(ip));
            mac = ne.getHardwareAddress();
            tString = byte2hex(mac);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tString;
    }

    public static String byte2hex(byte[] b) {
        StringBuffer hs = new StringBuffer(b.length);
        String tString = "";
        int length = b.length;
        for (int i = 0; i < length; i++) {
            tString = Integer.toHexString(b[i] & 0xFF);
            if (tString.length() == 1)
                hs = hs.append("0").append(tString);
            else {
                hs = hs.append(tString);
            }
        }
        return String.valueOf(hs);
    }

    private OnFocusChangeListener mOnFocusChangeListener = new OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            EditText textView = (EditText) v;
            String hint;
            if (hasFocus) {
                hint = textView.getHint().toString();
                textView.setTag(hint);
                textView.setHint("");
            } else {
                hint = textView.getTag().toString();
                textView.setHint(hint);
            }
        }
    };

    private int getStatusBarHeight() {
        Class<?> c = null;

        Object obj = null;

        Field field = null;

        int x = 0, sbar = 0;

        try {

            c = Class.forName("com.android.internal.R$dimen");

            obj = c.newInstance();

            field = c.getField("status_bar_height");

            x = Integer.parseInt(field.get(obj).toString());

            sbar = getBaseContext().getResources().getDimensionPixelSize(x);

        } catch (Exception e1) {

            e1.printStackTrace();

        }

        return sbar;
    }

//	public void GetSystemHideSettingValuesByType() {
//		RequestParams params = new RequestParams();
//		params.put("Action", "GetSystemHideSettingValuesByType");
//		params.put("AccountCode", mPreferences.getString("AccountCode", ""));
//		params.put("UserCode", mPreferences.getString("UserCode", ""));
//		params.put("UserPassword", mPreferences.getString("UserPassword", ""));
//		params.put("SessionKey", mPreferences.getString("SessionKey", ""));
//		client.post(String.format(Constants.URL, mServerAddressIp, mServerAddressPort), params,
//				new AsyncHttpResponseHandler(this) {
//					@Override
//					public void onSuccess(int statusCode, String content) {
//						super.onSuccess(statusCode, content);
//						try {
//							JSONObject jsonObject = new JSONObject(content);
//
//							if (jsonObject.getInt("Status") == 0) {
//								hideSettingList = new ArrayList<HideSettingBean>();
//								JSONArray array = jsonObject.getJSONArray("Result");
//								for (int i = 0; i < array.length(); i++) {
//									JSONObject jsonObj = new JSONObject(array.getString(i));
//									HideSettingBean bean = new HideSettingBean();
//									bean.setSettingType(jsonObj.optString("SettingType"));
//									bean.setSettingValues(jsonObj.optString("SettingValues"));
//									hideSettingList.add(bean);
//								}
//								Editor editor = preferences.edit();
//								for (int i = 0; i < hideSettingList.size(); i++) {
//									editor.putString(hideSettingList.get(i).getSettingType(),
//											hideSettingList.get(i).getSettingValues());
//								}
//								editor.commit();
//							}
//						} catch (JSONException e) {
//							e.printStackTrace();
//						}
//
//					}
//
//					@Override
//					public void onFailure(Throwable error, String content) {
//
//						super.onFailure(error, content);
//
//					}
//				});
//
//	}


    /**
     * 检测是否所有的权限都已经授权
     *
     * @param grantResults
     * @return
     * @since 2.5.0
     *
     */
//	private boolean verifyPermissions(int[] grantResults) {
//		for (int result : grantResults) {
//			if (result != PackageManager.PERMISSION_GRANTED) {
//				return false;
//			}
//		}
//		return true;
//	}


//	@Override
//	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
//										   @NonNull int[] grantResults) {
//		super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//		if (permissionHelper != null) {
//			permissionHelper.onRequestPermissionsResult(requestCode, permissions, grantResults);
//		}
//	}


    /**
     * 启动应用的设置
     *
     * @since 2.5.0
     */
    private void startAppSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        intent.setData(Uri.parse("package:" + getPackageName()));
        startActivity(intent);
    }


}
