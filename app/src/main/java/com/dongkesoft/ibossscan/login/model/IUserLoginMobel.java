/*******************************************************************************
 * Copyright(c) 2018 dongke All rights reserved. / Confidential
 * 类的信息：
 *		1.程序名称：IUserLoginMobel
 *		2.功能描述：用户登录model接口
 * 编辑履历：
 *		作者				日期					版本				修改内容
 *		dongke			2018/6/8			1.00			新建
 *******************************************************************************/
package com.dongkesoft.ibossscan.login.model;


import dongkesoft.com.dkmodule.mvp.model.IBaseMobel;
import dongkesoft.com.dkmodule.responseinterface.AsyncHttpResponseHandler;

/**
 * 项目名称：DKMvpDemo
 * 类描述：用户登录model接口
 * 创建人：apple
 * 创建时间：2018/6/19 下午4:16
 * 修改人：apple
 * 修改时间：2018/6/19 下午4:16
 * 修改备注：
 */
public interface IUserLoginMobel extends IBaseMobel {
    /**
     * 用户登录
     *
     * @param name
     * @param pwd
     * @param loginListener
     */
    void login(String name, String pwd, AsyncHttpResponseHandler loginListener);
}
