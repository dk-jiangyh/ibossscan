/*******************************************************************************
 * Copyright(c) 2018 dongke All rights reserved. / Confidential
 * 类的信息：
 * 1.程序名称：AbnormalSaveActivity
 * 2.功能描述：异常保存
 * 编辑履历：
 * 作者				日期					版本				修改内容
 * dongke			2018/6/8			1.00			新建
 *******************************************************************************/
package com.dongkesoft.ibossscan.abnormalsave.activity;

import android.app.AlertDialog;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.dongkesoft.ibossscan.home.bean.ValueFlagInfo;
import com.dongkesoft.ibossscan.R;
import com.dongkesoft.ibossscan.home.adapter.ValueFlagAdapter;
import com.dongkesoft.ibossscan.productsearch.bean.ProductSearchModel;
import com.dongkesoft.ibossscan.abnormalsave.presenter.AbnormalSavePresenter;
import com.dongkesoft.ibossscan.abnormalsave.view.AbnormalSaveView;
import com.dongkesoft.ibossscan.utils.AlertAnimateUtil;
import com.dongkesoft.ibossscan.utils.CostUtils;
import com.dongkesoft.ibossscan.utils.ProcessDialogUtils;
import com.dongkesoft.ibossscan.utils.StringUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import dongkesoft.com.dkmodule.ui.activity.BaseActivity;

public class AbnormalSaveActivity extends BaseActivity<AbnormalSaveView,
        AbnormalSavePresenter> implements AbnormalSaveView {

    /*
     * 标题
     * */
    @BindView(R.id.tv_center)
    TextView tvTitle;
    /*
     * 返回按钮
     * */
    @BindView(R.id.iv_left)
    ImageView ivLeft;

    @BindView(R.id.twoDimenEdt)
    EditText twoDimenEdt;


    /**
     * 商品编码
     */
    @BindView(R.id.tv_code)
    TextView tv_code;
    /**
     * 唯一编码
     */
    @BindView(R.id.tv_only_code)
    TextView tv_only_code;
    /**
     * 包装
     */
    @BindView(R.id.tv_package)
    TextView tv_package;
    /**
     * 规格
     */
    @BindView(R.id.tv_specification)
    TextView tv_specification;
    /**
     * 出库数量
     */
    @BindView(R.id.tv_delivery_quantity)
    TextView tv_delivery_quantity;
    /**
     * 色号
     */
    @BindView(R.id.tv_color_number)
    TextView tv_color_number;
    /**
     * 打码数量
     */
    @BindView(R.id.tv_input_quantity)
    TextView tv_input_quantity;
    /**
     * 剩余数量
     */
    @BindView(R.id.tv_RemainQuantity)
    TextView tv_RemainQuantity;
    /**
     * 打码箱数
     */
    @BindView(R.id.tv_box)
    TextView tv_box;
    /**
     * 打码片数
     */
    @BindView(R.id.tv_piece)
    TextView tv_piece;

    /**
     * 随机数
     */
    @BindView(R.id.tv_random_code)
    TextView tv_random_code;
    /**
     * 打码日期
     */
    @BindView(R.id.tv_ProductCodeDate)
    TextView tv_ProductCodeDate;
    /**
     * 破损
     */
    @BindView(R.id.tv_AllNoSalesQuantity)
    TextView tv_AllNoSalesQuantity;
    /**
     * 剩余箱数
     */
    @BindView(R.id.tv_RemainBoxQuantity)
    TextView tv_RemainBoxQuantity;
    /**
     * 剩余散数
     */
    @BindView(R.id.tv_RemainOddQuantity)
    TextView tv_RemainOddQuantity;
    /**
     * 异常类型
     */
    @BindView(R.id.tv_ModifyType)
    TextView tv_ModifyType;
    /**
     * 异常箱数
     */
    @BindView(R.id.edt_ModifyBoxQuantity)
    EditText edt_ModifyBoxQuantity;
    /**
     *库区
     */
    @BindView(R.id.tv_Warehouse)
    TextView tv_Warehouse;
    /**
     *仓位号
     */
    @BindView(R.id.tv_Position)
    TextView tv_Position;
    @BindView(R.id.btn_save)
    Button btn_save;
    /**
     * 备注
     */
    @BindView(R.id.edt_Remarks)
    EditText edt_Remarks;

    /**
     * 根据不同的key显示不同的DaiLog
     */
    private String key;
    /**
     * 对话框
     */
    private AlertDialog mDialog;
    /**
     * 选择列表
     */
    private ListView lvSelect;
    private String batchNo;
    private List<ProductSearchModel> milkStockingRemainOneList;
    private List<ValueFlagInfo> typelist;
    String flagid = "";
    String mDetailID = "";
    String mNoSalesQuantity = "";
    String mRemarks = "";

    @Override
    public void success(String result) {
        ProcessDialogUtils.closeProgressDilog();
        setTwoDimenFocus();
        DecimalFormat format = new DecimalFormat("0");
        try {
            JSONObject obj = new JSONObject(result);
            JSONObject json = obj.optJSONObject("Result");
            JSONArray array = json.optJSONArray("Table");

            milkStockingRemainOneList = new ArrayList<ProductSearchModel>();
            if (array != null && array.length() > 0) {
                for (int i = 0; i < array.length(); i++) {
                    JSONObject jsonObjectDetail = array
                            .getJSONObject(i);
                    ProductSearchModel info = new ProductSearchModel();
                    info.setDetailID(jsonObjectDetail.optString("DetailID"));
                    info.setCodeID(jsonObjectDetail.optString("CodeID"));
                    info.setCode(jsonObjectDetail.optString("Code"));
                    info.setInventoryID(jsonObjectDetail.optString("InventoryID"));
                    info.setWarehouseID(jsonObjectDetail.optString("WarehouseID"));
                    info.setWarehouseName(jsonObjectDetail.optString("WarehouseName"));
                    info.setPositionNumber(jsonObjectDetail.optString("PositionNumber"));
                    info.setOnlyCode(jsonObjectDetail.optString("OnlyCode"));
                    info.setPackage(jsonObjectDetail.optString("Package"));
                    info.setGradeID(jsonObjectDetail.optString("GradeID"));
                    info.setGradeName(jsonObjectDetail.optString("GradeName"));
                    info.setSpecification(jsonObjectDetail.optString("Specification"));
                    info.setColorNumber(jsonObjectDetail.optString("ColorNumber"));
                    info.setInputQuantity(jsonObjectDetail.optString("InputQuantity"));
                    info.setBox(jsonObjectDetail.optString("Box"));
                    info.setPiece(jsonObjectDetail.optString("Piece"));
                    info.setDeliveryQuantity(jsonObjectDetail.optString("DeliveryQuantity"));
                    info.setAllNoSalesQuantity(jsonObjectDetail.optString("AllNoSalesQuantity"));
                    info.setBatchNo(jsonObjectDetail.optString("BatchNo"));
                    info.setRandomCode(jsonObjectDetail.optString("RandomCode"));
                    info.setProductCodeDate(jsonObjectDetail.optString("ProductCodeDate"));
                    info.setAccountOrganizationID(jsonObjectDetail.optString("AccountOrganizationID"));
                    info.setAccountID(jsonObjectDetail.optString("AccountID"));

                    milkStockingRemainOneList.add(info);
                }
                mDetailID = milkStockingRemainOneList.get(0).getDetailID();
                tv_code.setText(StringUtils.isEmpty(milkStockingRemainOneList.get(0).getCode()) ? "" : milkStockingRemainOneList.get(0).getCode());
                tv_only_code.setText(StringUtils.isEmpty(milkStockingRemainOneList.get(0).getOnlyCode()) ? "" : milkStockingRemainOneList.get(0).getOnlyCode());
                tv_package.setText(StringUtils.isEmpty(milkStockingRemainOneList.get(0).getPackage()) ? "0" : milkStockingRemainOneList.get(0).getPackage());
                tv_specification.setText(StringUtils.isEmpty(milkStockingRemainOneList.get(0).getSpecification()) ? "" : milkStockingRemainOneList.get(0).getSpecification());
                tv_color_number.setText(StringUtils.isEmpty(milkStockingRemainOneList.get(0).getColorNumber()) ? "" : milkStockingRemainOneList.get(0).getColorNumber());
                tv_delivery_quantity.setText(StringUtils.isEmpty(milkStockingRemainOneList.get(0).getDeliveryQuantity()) ? "0" : format.format(Double.parseDouble(milkStockingRemainOneList.get(0).getDeliveryQuantity())));
                tv_input_quantity.setText(StringUtils.isEmpty(milkStockingRemainOneList.get(0).getInputQuantity()) ? "0" : format.format(Double.parseDouble(milkStockingRemainOneList.get(0).getInputQuantity())));
                tv_box.setText(StringUtils.isEmpty(milkStockingRemainOneList.get(0).getBox()) ? "0" : format.format(Double.parseDouble(milkStockingRemainOneList.get(0).getBox())));
                tv_piece.setText(StringUtils.isEmpty(milkStockingRemainOneList.get(0).getPiece()) ? "0" : format.format(Double.parseDouble(milkStockingRemainOneList.get(0).getPiece())));
                tv_Warehouse.setText(StringUtils.isEmpty(milkStockingRemainOneList.get(0).getWarehouseName()) ? "" : milkStockingRemainOneList.get(0).getWarehouseName());
                tv_Position.setText(StringUtils.isEmpty(milkStockingRemainOneList.get(0).getPositionNumber()) ? "" : milkStockingRemainOneList.get(0).getPositionNumber());
                tv_random_code.setText(StringUtils.isEmpty(milkStockingRemainOneList.get(0).getRandomCode()) ? "" : milkStockingRemainOneList.get(0).getRandomCode());
                try {
                    tv_ProductCodeDate.setText(StringUtils.isEmpty(milkStockingRemainOneList.get(0).getProductCodeDate()) ? "" : CostUtils.ConverToString(CostUtils.ConverToDate(milkStockingRemainOneList.get(0).getProductCodeDate())));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                tv_AllNoSalesQuantity.setText(StringUtils.isEmpty(milkStockingRemainOneList.get(0).getAllNoSalesQuantity()) ? "" : format.format(Double.parseDouble(milkStockingRemainOneList.get(0).getAllNoSalesQuantity())));
                //打码数量-出库数量-破损
                String RemainQuantity = "0";
                RemainQuantity = String.valueOf(format.format(Double.parseDouble(milkStockingRemainOneList.get(0).getInputQuantity()) - Double.parseDouble(milkStockingRemainOneList.get(0).getDeliveryQuantity()) - Double.parseDouble(milkStockingRemainOneList.get(0).getAllNoSalesQuantity())));
                //剩余数量
                tv_RemainQuantity.setText(RemainQuantity);

                String RemainBoxQuantity = "0";
                RemainBoxQuantity = format.format(Math.floor(Double.parseDouble(RemainQuantity) / Double.parseDouble(tv_package.getText().toString())));
                //剩余箱数
                tv_RemainBoxQuantity.setText(RemainBoxQuantity);
                String RemainOddQuantity = "0";
                RemainOddQuantity = format.format(Double.parseDouble(RemainQuantity) % Double.parseDouble(tv_package.getText().toString()));
                //剩余片数
                tv_RemainOddQuantity.setText(RemainOddQuantity);


            } else {
                mDetailID = "";
                tv_code.setText("");
                tv_only_code.setText("");
                tv_package.setText("");
                tv_specification.setText("");
                tv_color_number.setText("");
                tv_delivery_quantity.setText("");
                tv_input_quantity.setText("");
                tv_RemainQuantity.setText("");
                tv_box.setText("");
                tv_Position.setText("");
                tv_Warehouse.setText("");
                tv_piece.setText("");

                tv_random_code.setText("");
                tv_ProductCodeDate.setText("");
                tv_AllNoSalesQuantity.setText("");
                tv_RemainBoxQuantity.setText("");
                tv_RemainOddQuantity.setText("");
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void showFailedError(String message) {
        setTwoDimenFocus();
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
        mDetailID = "";
        tv_code.setText("");
        tv_only_code.setText("");
        tv_package.setText("");
        tv_specification.setText("");
        tv_color_number.setText("");
        tv_delivery_quantity.setText("");
        tv_input_quantity.setText("");
        tv_RemainQuantity.setText("");
        tv_box.setText("");
        tv_Position.setText("");
        tv_Warehouse.setText("");
        tv_piece.setText("");

        tv_random_code.setText("");
        tv_ProductCodeDate.setText("");
        tv_AllNoSalesQuantity.setText("");
        tv_RemainBoxQuantity.setText("");
        tv_RemainOddQuantity.setText("");
        tv_ModifyType.setText("");
        edt_ModifyBoxQuantity.setText("");
        edt_Remarks.setText("");
        ProcessDialogUtils.closeProgressDilog();
    }

    @Override
    public void getRelogin(String message) {
        ProcessDialogUtils.closeProgressDilog();
        AlertAnimateUtil.showReLoginDialog(
                AbnormalSaveActivity.this, "异常登录",
                message);
    }

    @Override
    public void saveSuccess(String result) {

        setTwoDimenFocus();
        Toast.makeText(getApplicationContext(), result, Toast.LENGTH_SHORT).show();
        mDetailID = "";
        tv_code.setText("");
        tv_only_code.setText("");
        tv_package.setText("");
        tv_specification.setText("");
        tv_color_number.setText("");
        tv_delivery_quantity.setText("");
        tv_input_quantity.setText("");
        tv_RemainQuantity.setText("");
        tv_box.setText("");
        tv_Position.setText("");
        tv_Warehouse.setText("");
        tv_piece.setText("");

        tv_random_code.setText("");
        tv_ProductCodeDate.setText("");
        tv_AllNoSalesQuantity.setText("");
        tv_RemainBoxQuantity.setText("");
        tv_RemainOddQuantity.setText("");
        tv_ModifyType.setText("");
        edt_ModifyBoxQuantity.setText("");
        edt_Remarks.setText("");
        ProcessDialogUtils.closeProgressDilog();
    }

    @Override
    public void getDictionarySuccess(String result) {
        ProcessDialogUtils.closeProgressDilog();

        try {
            JSONObject obj = new JSONObject(result);
            JSONObject json = obj.optJSONObject("Result");
            JSONArray array = json.optJSONArray("Table");

            typelist = new ArrayList<ValueFlagInfo>();
            if (array != null && array.length() > 0) {
                for (int i = 0; i < array.length(); i++) {
                    JSONObject jsonObjectDetail = array
                            .getJSONObject(i);
                    ValueFlagInfo info = new ValueFlagInfo();
                    info.setValuename(jsonObjectDetail.optString("DictionaryValue"));
                    info.setValueid(jsonObjectDetail.optInt("DictionaryID"));


                    typelist.add(info);
                }
            }
            showDialog("1");
        }catch (Exception e){

        }
    }

    @Override
    protected AbnormalSavePresenter createPresenter() {
        return new AbnormalSavePresenter();
    }

    @Override
    protected int setMvpView() {
        return R.layout.activity_two_dimeninfo_modify;
    }

    @Override
    protected void initView() {
        tvTitle.setVisibility(View.VISIBLE);
        ivLeft.setVisibility(View.VISIBLE);
        tvTitle.setText("商品破损");
    }

    @Override
    protected void initData() {
        allListeners();
    }

    /**
     * 点击事件
     *
     * @param view
     */
    @OnClick({R.id.iv_left, R.id.tv_ModifyType, R.id.btn_save})
    public void OnClick(View view) {
        int i = view.getId();
        if (i == R.id.iv_left) {
            finish();
        }
        if (i == R.id.tv_ModifyType) {


            ProcessDialogUtils.showProcessDialog(AbnormalSaveActivity.this);
            // 传递参数
            HashMap<String, String> map = new HashMap<>();
            map.put("Action", "GetDataDictionary");
            map.put("AccountCode", mAccountCode);
            map.put("UserCode", mUserCode);
            map.put("UserPassword", mPassword);
            map.put("SessionKey", mSessionKey);
            // map.put("LicenseCode", mLicenseCode);
            map.put("DictionaryType", "STO009");//
            presenter.getDictionary(AbnormalSaveActivity.this, map, mServerAddressIp, mServerAddressPort);


        }
        if (i == R.id.btn_save) {
            if (!check()) {
                return;
            }
            saveData();
        }
    }


    /*
     * 条码输入框重新获得焦点
     */
    private void setTwoDimenFocus() {
        twoDimenEdt.setText("");
        twoDimenEdt.setEnabled(true);
        twoDimenEdt.setFocusable(true);
        twoDimenEdt.setFocusableInTouchMode(true);
        twoDimenEdt.requestFocus();
        twoDimenEdt.findFocus();
        twoDimenEdt
                .setFilters(new InputFilter[]{new InputFilter() {
                    @Override
                    public CharSequence filter(
                            CharSequence source,
                            int start, int end,
                            Spanned dest, int dstart,
                            int dend) {

                        return null;
                    }
                }});
    }

    /**
     * 扫码文本事件变化监听
     */
    private void allListeners() {
        //扫描条码事件
        twoDimenEdt.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable arg0) {

                if (twoDimenEdt.getText().toString().contains("\n")) {


                    batchNo = twoDimenEdt.getText().toString()
                            .trim().replaceAll("\\n", "");
                    batchNo = twoDimenEdt.getText().toString()
                            .trim().replaceAll("\\r\\n", "");
                    //  batchNo = batchNo.substring(0, batchNo.length() - 2);


                    if (!batchNo.contains("→")) {
                        Toast.makeText(AbnormalSaveActivity.this, "二维码信息错误", Toast.LENGTH_SHORT).show();
                        twoDimenEdt.setText("");
                        twoDimenEdt.requestFocus();
                        twoDimenEdt.findFocus();
                        return;
                    }
                    twoDimenEdt.setFocusable(false);
                    twoDimenEdt.setEnabled(false);
                    twoDimenEdt.setFilters(new InputFilter[]{new InputFilter() {
                        @Override
                        public CharSequence filter(
                                CharSequence source, int start,
                                int end, Spanned dest,
                                int dstart, int dend) {
                            return source.length() < 1 ? dest
                                    .subSequence(dstart, dend)
                                    : "";
                        }
                    }});

                    ProcessDialogUtils.showProcessDialog(AbnormalSaveActivity.this);
                    // 传递参数
                    HashMap<String, String> map = new HashMap<>();
                    map.put("Action", "GetQrGoodsCodeForMobile");
                    map.put("AccountCode", mAccountCode);
                    map.put("UserCode", mUserCode);
                    map.put("UserPassword", mPassword);
                    map.put("SessionKey", mSessionKey);
                    map.put("BatchNo", batchNo);
                    presenter.getData(AbnormalSaveActivity.this, map, mServerAddressIp, mServerAddressPort);

                }

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
            }
        });
    }

    @SuppressWarnings("deprecation")
    private void showDialog(String string) {
        this.key = string;
        if (mDialog == null) {
            mDialog = new AlertDialog.Builder(AbnormalSaveActivity.this).create();
        }
        mDialog.setCanceledOnTouchOutside(false);

        View view = LayoutInflater.from(AbnormalSaveActivity.this).inflate(
                R.layout.dialog_choose, null);
        mDialog.show();
        mDialog.setContentView(view);
        mDialog.getWindow().setGravity(Gravity.CENTER);
        mDialog.getWindow().setLayout(
                getWindowManager().getDefaultDisplay().getWidth(),
                getWindowManager().getDefaultDisplay().getHeight());
        mDialog.setCanceledOnTouchOutside(true);
        mDialog.getWindow().setWindowAnimations(R.style.bottom_up_animation);
        lvSelect = (ListView) view.findViewById(R.id.select_list);
        LinearLayout  ll_bussiness = (LinearLayout) view.findViewById(R.id.ll_bussiness);
        ll_bussiness.setVisibility(View.GONE);
        mDialog.getWindow().clearFlags(
                WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);

        if (key.equals("1")) {
            ValueFlagAdapter adapter = new ValueFlagAdapter(
                    AbnormalSaveActivity.this, typelist);
            lvSelect.setAdapter(adapter);
        }

        lvSelect.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {

                if (key.equals("1")) {
                    tv_ModifyType.setText(typelist.get(arg2)
                            .getValuename().toString());
                    flagid = String.valueOf(typelist.get(arg2).getValueid());

                    mDialog.dismiss();
                    return;
                }

            }
        });
    }

    /**
     * 保存数据
     */
    private void saveData() {
        mRemarks = edt_Remarks.getText().toString();

        if ( Double.parseDouble(StringUtils.isEmpty(edt_ModifyBoxQuantity.getText().toString())?"0":edt_ModifyBoxQuantity.getText().toString())<=0) {
            Toast.makeText(this, "破损数量必须大于零", Toast.LENGTH_SHORT).show();
            return  ;
        }
        mNoSalesQuantity = edt_ModifyBoxQuantity.getText().toString();
        ProcessDialogUtils.showProcessDialog(AbnormalSaveActivity.this);
        // 传递参数
        HashMap<String, String> map = new HashMap<>();
        map.put("Action", "SaveOutNoSalesForMobile");
        map.put("AccountCode", mAccountCode);
        map.put("UserCode", mUserCode);
        map.put("UserPassword", mPassword);
        map.put("SessionKey", mSessionKey);
        // map.put("LicenseCode", mLicenseCode);
        map.put("QRTypeID", flagid);//破损类型
        map.put("DetailID", mDetailID);//Code 商品编码
        map.put("NoSalesQuantity", mNoSalesQuantity);//破损数量
        map.put("Remarks", mRemarks);// 备注
        presenter.saveData(AbnormalSaveActivity.this, map, mServerAddressIp, mServerAddressPort);
    }

    /**
     * 数据校验
     *
     * @return
     */
    private boolean check() {

        if ("".equals(tv_code.getText().toString()) || "".equals(tv_only_code.getText().toString())) {
            Toast.makeText(this, "请选择商品信息", Toast.LENGTH_SHORT).show();
            return false;
        }

        if ("".equals(tv_ModifyType.getText().toString())) {
            Toast.makeText(this, "请选择异常类型", Toast.LENGTH_SHORT).show();
            return false;
        }
        if ("".equals(edt_ModifyBoxQuantity.getText().toString())) {
            Toast.makeText(this, "请输入异常数量", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (Double.parseDouble(edt_ModifyBoxQuantity.getText().toString()) > Double.parseDouble(tv_RemainQuantity.getText().toString())) {
            Toast.makeText(this, "异常数量不能大于剩余数量", Toast.LENGTH_SHORT).show();
            return false;

        }


        return true;
    }
}
