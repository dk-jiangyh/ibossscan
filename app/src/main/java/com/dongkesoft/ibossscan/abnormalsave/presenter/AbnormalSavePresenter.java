/*******************************************************************************
 * Copyright(c) 2018 dongke All rights reserved. / Confidential
 * 类的信息：
 * 1.程序名称：AbnormalSavePresenter
 * 2.功能描述：异常保存的逻辑层
 * 编辑履历：
 * 作者				日期					版本				修改内容
 * dongke			2018/6/8			1.00			新建
 *******************************************************************************/
package com.dongkesoft.ibossscan.abnormalsave.presenter;

import android.content.Context;


import com.dongkesoft.ibossscan.common.RightsSet;
import com.dongkesoft.ibossscan.abnormalsave.view.AbnormalSaveView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import dongkesoft.com.dkmodule.bean.ReqCallBack;
import dongkesoft.com.dkmodule.common.Constants;
import dongkesoft.com.dkmodule.mvp.presenter.BasePresenter;
import dongkesoft.com.dkmodule.utils.OkAsyncHttpClient;

public class AbnormalSavePresenter extends BasePresenter<AbnormalSaveView> {

    public void getData(Context context, HashMap<String, String> map, String ip, String port){

        OkAsyncHttpClient.getInstance(context).requestPostByAsynWithForm(String.format(Constants.PRINT_QR_URL,ip,port), map, new ReqCallBack<String>() {
            @Override
            public void onReqSuccess(final String result) {
                {
                    //需要在UI线程执行
                    mViewHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            // 逻辑判断
                            try {
                                JSONObject obj = new JSONObject(result);
                                int status = obj.optInt("Status");
                                String message = obj.optString("Message");
                                if(getView()!=null) {

                                    if (status == 0) {
                                        getView().success(result);
                                    } else if (status == RightsSet.NegativeOne) {
                                        getView().getRelogin(message);
                                    } else {
                                        getView().showFailedError(message);
                                    }
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            }
            @Override
            public void onReqFailed(final String errorMsg) {
                {
                    mViewHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            if(getView()!=null) {
                                getView().showFailedError(errorMsg);
                            }
                        }
                    });
                }
            }
        });
    }


    public void saveData(Context context, HashMap<String, String> map, String ip, String port){

        OkAsyncHttpClient.getInstance(context).requestPostByAsynWithForm(String.format(Constants.PRINT_QR_URL,ip,port), map, new ReqCallBack<String>() {
            @Override
            public void onReqSuccess(final String result) {
                {
                    //需要在UI线程执行
                    mViewHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            // 逻辑判断
                            try {
                                JSONObject obj = new JSONObject(result);
                                int status = obj.optInt("Status");
                                String message = obj.optString("Message");
                                if(getView()!=null) {

                                    if (status == 0) {
                                        getView().saveSuccess(message);
                                    } else if (status == RightsSet.NegativeOne) {
                                        getView().getRelogin(message);
                                    } else {
                                        getView().showFailedError(message);
                                    }
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            }
            @Override
            public void onReqFailed(final String errorMsg) {
                {
                    mViewHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            if(getView()!=null) {
                                getView().showFailedError(errorMsg);
                            }
                        }
                    });
                }
            }
        });
    }


    public void getDictionary(Context context, HashMap<String, String> map, String ip, String port){

        OkAsyncHttpClient.getInstance(context).requestPostByAsynWithForm(String.format(Constants.PRINT_QR_URL,ip,port), map, new ReqCallBack<String>() {
            @Override
            public void onReqSuccess(final String result) {
                {
                    //需要在UI线程执行
                    mViewHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            // 逻辑判断
                            try {
                                JSONObject obj = new JSONObject(result);
                                int status = obj.optInt("Status");
                                String message = obj.optString("Message");
                                if(getView()!=null) {

                                    if (status == 0) {
                                        getView().getDictionarySuccess(result);
                                    } else if (status == RightsSet.NegativeOne) {
                                        getView().getRelogin(message);
                                    } else {
                                        getView().showFailedError(message);
                                    }
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            }
            @Override
            public void onReqFailed(final String errorMsg) {
                {
                    mViewHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            if(getView()!=null) {
                                getView().showFailedError(errorMsg);
                            }
                        }
                    });
                }
            }
        });
    }
}
