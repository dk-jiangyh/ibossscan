package com.dongkesoft.ibossscan.abnormalsave.view;

import dongkesoft.com.dkmodule.mvp.view.MvpView;

public interface AbnormalSaveView extends MvpView {
    void success(String result);
    void showFailedError(String message);
    void getRelogin(String message);
    void saveSuccess(String result);
    void getDictionarySuccess(String result);
}
