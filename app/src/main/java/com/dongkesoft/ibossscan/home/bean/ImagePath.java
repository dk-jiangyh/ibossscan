package com.dongkesoft.ibossscan.home.bean;

/*******************************************************
 * Copyright(c) 2016 DongkeSoft All rights reserved. / Confidential
 *
 * @Title : ImagePath.java
 * @Package : com.dongkesoft.iboss.model
 * @Description : 图片信息
 * @Author : jiangyh
 * @Date : 2016年8月23日 下午12:04:12
 * @Version : 1.00
 ********************************************************/


import java.io.Serializable;

/**
 * @Description : 图片信息
 * @ClassName : ImagePath
 * @Author : jiangyh
 * @Date : 2016年8月23日 下午4:13:55
 */
public class ImagePath implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * 标示
     */
    private String flag;

    /**
     * 服务器地址
     */
    private String serverPath;

    public String getServerImagePath() {
        return serverImagePath;
    }

    public void setServerImagePath(String serverImagePath) {
        this.serverImagePath = serverImagePath;
    }

    private String serverImagePath;

    /**
     * 本地地址
     */
    private String localPath;

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getServerPath() {
        return serverPath;
    }

    public void setServerPath(String serverPath) {
        this.serverPath = serverPath;
    }

    public String getLocalPath() {
        return localPath;
    }

    public void setLocalPath(String localPath) {
        this.localPath = localPath;
    }

}
