package com.dongkesoft.ibossscan.home.view;

import dongkesoft.com.dkmodule.mvp.view.MvpView;

public interface IUserView extends MvpView {

    void showFailedError(String message);
    void success(String result);
    void getRelogin(String message);
}
