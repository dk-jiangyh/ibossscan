/*******************************************************
 * Copyright(c) 2016 DongkeSoft All rights reserved. / Confidential
 * 
 * @Title : ValueFlagInfo.java
 * @Package : com.dongkesoft.ibosshj.model
 * @Description : 标示信息
 * @Author : jiangyh
 * @Date : 2016年8月23日 下午12:04:12
 * @Version : 1.00
 ********************************************************/
package com.dongkesoft.ibossscan.home.bean;

import java.io.Serializable;

/**
 * @Description : 标示信息
 * @ClassName : ValueFlagInfo
 * @Author : jiangyh
 * @Date : 2016年8月23日 下午4:13:55
 */
public class ValueFlagInfo implements Serializable {
	/**
	 * @Field serialVersionUID :
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 标示名称
	 */
	private String valuename;

	/**
	 * 标示id
	 */
	private int valueid;

	public String getValuename() {
		return valuename;
	}

	public void setValuename(String valuename) {
		this.valuename = valuename;
	}

	public int getValueid() {
		return valueid;
	}

	public void setValueid(int valueid) {
		this.valueid = valueid;
	}
}
