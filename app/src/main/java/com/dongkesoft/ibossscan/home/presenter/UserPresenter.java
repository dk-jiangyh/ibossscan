package com.dongkesoft.ibossscan.home.presenter;

import android.content.Context;

import com.dongkesoft.ibossscan.home.view.IUserView;
import com.dongkesoft.ibossscan.common.RightsSet;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

import dongkesoft.com.dkmodule.bean.ReqCallBack;
import dongkesoft.com.dkmodule.common.Constants;
import dongkesoft.com.dkmodule.mvp.presenter.BasePresenter;
import dongkesoft.com.dkmodule.utils.OkAsyncHttpClient;

public class UserPresenter extends BasePresenter<IUserView> {

    public void getUsegetUserListrList(Context context, Map<String, String> params,String ip,String port) {

        String url = String.format(Constants.URL, ip,
                port);
        OkAsyncHttpClient.getInstance(context).requestPostByAsyn(url, params, new ReqCallBack<String>() {
            @Override
            public void onReqSuccess(final String result) {
                {
                    //需要在UI线程执行
                    mViewHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            // 逻辑判断
                            try {
                                JSONObject obj = new JSONObject(result);
                                int status = obj.optInt("Status");
                                String message = obj.optString("Message");
                                String resultStr = obj.optString("Data");

                                if (status == 1) {
                                    getView().success(resultStr);

                                }
                                else if(status== RightsSet.NegativeOne)
                                {
                                    getView().getRelogin(message);
                                }

                                else {
                                    getView().showFailedError(message);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            }

            @Override
            public void onReqFailed(final String errorMsg) {
                {
                    mViewHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            getView().showFailedError(errorMsg);
                        }
                    });
                }
            }
        });
    }

}
