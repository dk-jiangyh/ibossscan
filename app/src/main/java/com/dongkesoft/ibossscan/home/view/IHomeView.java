package com.dongkesoft.ibossscan.home.view;

import dongkesoft.com.dkmodule.mvp.view.MvpView;

/**
 * Created by guanhonghou on 2018/9/26.
 */
public interface IHomeView extends MvpView {
    void showFailedError(String message);
    void success(String result);
    void guaranteeGoodsSuccess(String result);
    void guaranteeGoodsFailedError(String message);
    void showWarehouseSuccess(String result);
    void showProductSuccess(String result);
    void showReturnSuccess(String result);
    void getRelogin(String message);
}
