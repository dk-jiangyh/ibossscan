package com.dongkesoft.ibossscan.home.adapter;

/**
 * Created by guanhonghou on 2018/9/26.
 */

import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.dongkesoft.ibossscan.home.bean.FragmentGridViewModel;
import com.dongkesoft.ibossscan.R;


/**
 * @author Administrator
 * @since 2016年12月28日
 */
public class FragmentImageGridAdapter extends IBossBaseAdapter<FragmentGridViewModel> {

    public FragmentImageGridAdapter(Context context) {
        super(context);
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public int getItemLayoutId(int getItemViewType) {
        return R.layout.item_grid;
    }

    @Override
    public void handleItem(int itemViewType, int position,
                           FragmentGridViewModel item,
                           IBossBaseAdapter.ViewHolder holder,
                           boolean isRecycle) {
        ImageView im = holder.get(R.id.iv_image, ImageView.class);
        TextView tvTitle = holder.get(R.id.tv_title, TextView.class);

        FragmentGridViewModel model = (FragmentGridViewModel) item;

        tvTitle.setText(model.getImageTitle());
        im.setImageResource(model.getImageID());
    }
}

