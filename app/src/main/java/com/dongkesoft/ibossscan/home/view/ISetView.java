package com.dongkesoft.ibossscan.home.view;

import dongkesoft.com.dkmodule.mvp.view.MvpView;

/**
 * Created by guanhonghou on 2018/9/26.
 */

public interface ISetView extends MvpView {
    void showFailedError(String message);
    void success(String result);
    void getRelogin(String message);
    void changeUserSuccess(String result);
    void changeUserFailedError(String message);

}
