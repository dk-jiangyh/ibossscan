package com.dongkesoft.ibossscan.home.bean;

/**
 * Created by guanhonghou on 2018/9/26.
 */

/*******************************************************
 * Copyright(c) 2016 DongkeSoft All rights reserved. / Confidential
 *
 * @Title : TabInfo.java
 * @Package : com.dongkesoft.iboss.model
 * @Description : 主页导航信息
 * @Author : jiangyh
 * @Date : 2016年8月23日 下午12:04:12
 * @Version : 1.00
 ********************************************************/

import java.io.Serializable;

import dongkesoft.com.dkmodule.ui.fragment.BaseFragment;

/**
 * @Description :主页导航信息
 * @ClassName : TabInfo
 * @Author : jiangyh
 * @Date : 2016年8月23日 下午4:13:55
 */
public class TabInfo implements Serializable {
    /**
     * @Field serialVersionUID :
     */
    private static final long serialVersionUID = 1L;

    /**
     * 导航fragment
     */
    BaseFragment fragment;

    /**
     * 导航索引
     */
    int index;

    /**
     * 导航class
     */
    String clss;

    public BaseFragment getFragment() {
        return fragment;
    }

    public void setFragment(BaseFragment fragment) {
        this.fragment = fragment;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getClss() {
        return clss;
    }

    public void setClss(String clss) {
        this.clss = clss;
    }

}
