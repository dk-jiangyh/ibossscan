package com.dongkesoft.ibossscan.home.adapter;


import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.TextUtils;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.dongkesoft.ibossscan.home.bean.ImagePath;
import com.dongkesoft.ibossscan.R;

import dongkesoft.com.dkmodule.bean.ReqCallBack;
import dongkesoft.com.dkmodule.utils.OkAsyncHttpClient;


/*
 * Copyright(c) 2012 dongke All rights reserved. / Confidential
 * 类的信息：
 *		1.程序名称： DetailImgAdapter
 *		2.功能描述：详细列表适配
 * 编辑履历：
 *		作者				日期					版本				修改内容
 *		东科				2016/12/07			 1.00			新建
 *******************************************************************************/
public class ImageDetailAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    private List<ImagePath> mlist;
    private Context context;

    @SuppressWarnings("static-access")
    public ImageDetailAdapter(List<ImagePath> imgpathList, Context context) {
        super();
        this.mlist = imgpathList;
        this.context = context;
        inflater = inflater.from(context);
    }

    @Override
    public int getCount() {
        return mlist.size();
    }

    @Override
    public Object getItem(int position) {
        return mlist.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.gridview_item, null);
            viewHolder = new ViewHolder();
            viewHolder.image = (ImageView) convertView
                    .findViewById(R.id.detailimgview);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }


        viewHolder.image.setBackgroundResource(R.drawable.default_img);
        ImagePath path = mlist.get(position);

        if (!TextUtils.isEmpty(path.getServerPath())) {
            Map<String,String> params=new HashMap<String,String>();

            params.put("data",path.getServerImagePath());
            OkAsyncHttpClient.getInstance(context).downLoadFile(path.getServerPath(),path.getLocalPath(),params, new ReqCallBack<File>(){
                @Override
                public void onReqSuccess(File result) {
//                    Glide.with(context)
//                            .load(result)
//                            .into(viewHolder.image);
                }

                @Override
                public void onReqFailed(String errorMsg) {

                }
            });
        }

        return convertView;
    }

    public static class ViewHolder {
        public TextView title;
        public ImageView image;

    }

    public static Bitmap convertStringToIcon(String st) {
        // OutputStream out;
        Bitmap bitmap = null;
        try {
            // out = new FileOutputStream("/sdcard/aa.jpg");
            byte[] bitmapArray;
            bitmapArray = Base64.decode(st, Base64.DEFAULT);
            bitmap = BitmapFactory.decodeByteArray(bitmapArray, 0,
                    bitmapArray.length);
            // bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
            return bitmap;
        } catch (Exception e) {
            return null;
        }
    }
}
