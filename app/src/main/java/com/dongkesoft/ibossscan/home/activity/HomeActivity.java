package com.dongkesoft.ibossscan.home.activity;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.dongkesoft.ibossscan.home.adapter.FragmentImageGridAdapter;
import com.dongkesoft.ibossscan.home.bean.FragmentGridViewModel;
import com.dongkesoft.ibossscan.home.presenter.HomePresenter;
import com.dongkesoft.ibossscan.home.view.IHomeView;
import com.dongkesoft.ibossscan.outstoragescan.activity.SalesOutStorageScanNewActivity;
import com.dongkesoft.ibossscan.productCoding.activity.ScanCodeActivity;
import com.dongkesoft.ibossscan.common.SysApplication;
import com.dongkesoft.ibossscan.R;
import com.dongkesoft.ibossscan.productCoding.activity.ScanCodeNewActivity;
import com.dongkesoft.ibossscan.productsearch.activity.ProductSearchActivity;
import com.dongkesoft.ibossscan.abnormalsave.activity.AbnormalSaveActivity;
import com.dongkesoft.ibossscan.outstoragescan.activity.SalesOutStorageScanActivity;
import com.dongkesoft.ibossscan.utils.BasePopupWindow;
import com.dongkesoft.ibossscan.utils.ProcessDialogUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnItemClick;
import dongkesoft.com.dkmodule.common.Constants;
import dongkesoft.com.dkmodule.ui.activity.BaseFragmentActivity;


public class HomeActivity extends BaseFragmentActivity<IHomeView, HomePresenter>
        implements IHomeView {


    @BindView(R.id.tv_center)
    TextView tvTitle;

    @BindView(R.id.tv_right)
    TextView tvRight;

    @BindView(R.id.gv_fragement_business)
    GridView gv;

    private FragmentImageGridAdapter adapter;
    /**
     * 最后点击时间
     */
    private long lastClickTime;

    private int mNotificationNum;

    List<FragmentGridViewModel> businessLists;
    private BasePopupWindow BasePopupWindowLogout;
    Timer timer = new Timer();

    public boolean stop = false;


    @Override
    protected HomePresenter createPresenter() {
        return new HomePresenter();
    }

    @Override
    protected int setMvpView() {
        return R.layout.activity_home;
    }

    @Override
    protected void initView() {
        tvTitle.setVisibility(View.VISIBLE);
        tvTitle.setText("出库扫码");
//        tvRight.setVisibility(View.VISIBLE);
//        tvRight.setText("设置");

        ImageView iv = (ImageView) findViewById(R.id.iv_fragement_business_title);
        iv.setBackgroundResource(R.mipmap.business_title);
        // jiang add 2017-07-28 屏幕分辨率大于1920*1080时，主页图片显示不全 start
        DisplayMetrics metric = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metric);
        // 屏幕分辨率大于1920*1080时，主页图片显示不全 图片高度宽度放大
        if (metric.widthPixels > 1080) {
            LinearLayout llImage = (LinearLayout) findViewById(R.id.ll_fragement_business_title);
            llImage.setLayoutParams(new LinearLayout.LayoutParams(metric.widthPixels, metric.widthPixels * 467 / 1080));
            iv.setLayoutParams(new LinearLayout.LayoutParams(metric.widthPixels, metric.widthPixels * 467 / 1080));
        }
        //  timer.schedule(timerTask, 1000, 60000);
        SysApplication.getInstance().addActivity(this);
        businessLists = new ArrayList<FragmentGridViewModel>();

//        // 厚的pda打码设备 20200411 加入仓储打码中正常和返漏打码的权限
//        if (Build.MODEL.equals("PDA") || Build.MODEL.equals("Android Handheld Terminal")) {
//            initReturnData();
//        }

    }

    @Override
    protected void initData() {

        adapter = new FragmentImageGridAdapter(HomeActivity.this);
        businessLists = new ArrayList<FragmentGridViewModel>();
        FragmentGridViewModel model = new FragmentGridViewModel();


        model = new FragmentGridViewModel();
        model.setImageID(R.drawable.scancode);
        model.setImageTitle("出库扫码");
        model.setSize("0");
        businessLists.add(model);

//        model = new FragmentGridViewModel();
//        model.setImageID(R.drawable.coding);
//        model.setImageTitle("商品打码");
//        model.setSize("0");
//        businessLists.add(model);

//        model = new FragmentGridViewModel();
//        model.setImageID(R.drawable.query);
//        model.setImageTitle("商品剩余查询");
//        model.setSize("0");
//        businessLists.add(model);
//
//        model = new FragmentGridViewModel();
//        model.setImageID(R.mipmap.outstoragescan);
//        model.setImageTitle("商品破损");
//        model.setSize("0");
//        businessLists.add(model);


        adapter.setData(businessLists);
        gv.setAdapter(adapter);

        Intent intent = new Intent(HomeActivity.this, SalesOutStorageScanNewActivity.class);
        startActivity(intent);


    }

    @OnItemClick(R.id.gv_fragement_business)
    public void onItemClick(int position) {
        if (Math.abs(System.currentTimeMillis() - lastClickTime) < Constants.MIN_CLICK_TIME) {
            return;
        }
        lastClickTime = System.currentTimeMillis();
        String title = businessLists.get(position).getImageTitle();
        if (title.equals("商品打码")) {
            Intent intent = new Intent(HomeActivity.this, ScanCodeNewActivity.class);
            startActivity(intent);
        }

        if (title.equals("出库扫码")) {
            Intent intent = new Intent(HomeActivity.this, SalesOutStorageScanNewActivity.class);
            startActivity(intent);
        }

        if (title.equals("商品剩余查询")) {
            Intent intent = new Intent(HomeActivity.this, ProductSearchActivity.class);
            startActivity(intent);
        }
        if (title.equals("商品破损")) {
            Intent intent = new Intent(HomeActivity.this, AbnormalSaveActivity.class);
            startActivity(intent);
        }
    }

    /**
     * 返漏打码权限
     */
    private void initReturnData() {
        HashMap<String, String> map = new HashMap<>();
        map.put("token", token);
        map.put("module", "PrintQRCode");
        map.put("action", "GetReturnPurview");
        map.put("data", "");

    }


    private String getData() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("CreateTimeFr", "");
            obj.put("CreateTimeTo", "");

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return obj.toString();
    }


    @Override
    public void getRelogin(String message) {
//        ProcessDialogUtils.closeProgressDilog();
//
//        AlertAnimateUtil.showReLoginDialog(
//                HomeActivity.this, "异常登录",
//                message);
    }


    //返回键监听
    @Override
    public void onBackPressed() {

        BasePopupWindowLogout = new BasePopupWindow(
                this, R.layout.popup_window_exit);
        BasePopupWindowLogout
                .setPopUpWindowCallBack(new BasePopupWindow.IPopUpWindowCallBack() {

                    @Override
                    public void popUpWindowCallBack(View view) {
                        TextView tvPopupWindowMessage = (TextView) view
                                .findViewById(R.id.tv_popup_window_message);
                        TextView tvPopupWindowTitle = (TextView) view
                                .findViewById(R.id.tv_popup_window_title);
                        tvPopupWindowTitle.setText("退出程序");
                        try {
                            tvPopupWindowMessage.setText("是否要退出当前登录？");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        // 对布局文件进行初始化
                        RelativeLayout llCancel = (RelativeLayout) view
                                .findViewById(R.id.ll_cancel);
                        // 对布局中的控件添加事件监听
                        llCancel.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                BasePopupWindowLogout.dismiss();
                            }
                        });
                        RelativeLayout llOK = (RelativeLayout) view
                                .findViewById(R.id.ll_ok);
                        // 对布局中的控件添加事件监听
                        llOK.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                LogOut();

                            }
                        });
                    }
                });
        BasePopupWindowLogout.show(false,
                findViewById(R.id.tv_center), 0, 0);

    }

    Handler handler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == 1) {
                Bundle b = msg.getData();
                String result = b.getString("result");
                try {
                    JSONObject jobj = new JSONObject(result);
                    int status = jobj.optInt("Status");
                    if (status == 1) {
                        String resultStr = jobj.optString("Result");
                        JSONArray resultArray = new JSONArray(resultStr);
                        if (resultArray != null && resultArray.length() > 0) {
                            for (int i = 0; i < resultArray.length(); i++) {
                                final JSONObject resultObj = (JSONObject) resultArray.opt(i);
                                final String invoiceName = resultObj.optString("INVOICENAME");
                                final String pushContent = resultObj.optString("PUSHCONTENT");
                                String channelId = null;
                                NotificationManager manger = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                                    channelId = "1";
                                    NotificationChannel channel = new NotificationChannel(channelId, "Channel1", NotificationManager.IMPORTANCE_DEFAULT);
                                    channel.enableLights(true);
                                    channel.setLightColor(Color.RED);
                                    channel.setShowBadge(true);
                                    manger.createNotificationChannel(channel);

                                }
                            }
                        }

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }
    };


    //调用登出接口
    public void LogOut() {
        // ProcessDialogUtils.showProcessDialog(this);
        HashMap<String, String> map = new HashMap<>();
        ProcessDialogUtils.showProcessDialog(HomeActivity.this);
        map.put("Action", "SMDLogout");
        map.put("AccountCode", mAccountCode);
        map.put("UserCode", mUserCode);
        map.put("UserPassword", mPassword);
        map.put("SessionKey", mSessionKey);
        presenter.LogOut(HomeActivity.this, map, mServerAddressIp, mServerAddressPort);
    }

    //登出失败
    @Override
    public void showFailedError(String message) {
//        ProcessDialogUtils.closeProgressDilog();
//        if (BasePopupWindowLogout != null) {
//            BasePopupWindowLogout.dismiss();
//        }
        finish();

    }

    //登出成功
    @Override
    public void success(String result) {
//        ProcessDialogUtils.closeProgressDilog();
//        if (BasePopupWindowLogout != null) {
//            BasePopupWindowLogout.dismiss();
//        }

        // timerTask = null;
        finish();
    }

    @Override
    public void guaranteeGoodsSuccess(String result) {
        // ProcessDialogUtils.closeProgressDilog();
        try {
            JSONObject resultObj = new JSONObject(result);
            int status = resultObj.optInt("Status");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void guaranteeGoodsFailedError(String message) {

    }

    @Override
    public void showWarehouseSuccess(String result) {

    }

    @Override
    public void showProductSuccess(String result) {

    }

    @Override
    public void showReturnSuccess(String result) {

    }


    /**
     * 单击事件
     *
     * @param view
     */
    @OnClick({R.id.tv_right})
    public void onLeftViewClicked(View view) {
        int i = view.getId();
        if (i == R.id.tv_right) {
//            Intent intent = new Intent(HomeActivity.this, SettingActivity.class);
//            startActivity(intent);
        }
    }

}
