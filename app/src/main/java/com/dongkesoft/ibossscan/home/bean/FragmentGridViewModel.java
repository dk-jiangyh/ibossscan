package com.dongkesoft.ibossscan.home.bean;

/**
 * Created by guanhonghou on 2018/9/26.
 */

/*******************************************************
 * Copyright(c) 2016 DongkeSoft All rights reserved. / Confidential
 *
 * @Title : FragmentGridViewModel.java
 * @Package : com.dongkesoft.iboss.model
 * @Description : FragmentGridView对象
 * @Author : jiangyh
 * @Date : 2016年8月23日 下午12:04:12
 * @Version : 1.00
 ********************************************************/


import java.io.Serializable;

/**
 * @author Administrator
 * @since 2016年12月28日
 */
public class FragmentGridViewModel implements Serializable {
    /**
     * @Field serialVersionUID :
     */
    private static final long serialVersionUID = 1L;
    /**
     * @Field imageID :
     */
    private int imageID;

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    private String size;
    /**
     * @Field imageTitle :
     */
    private String imageTitle;

    public int getImageID() {
        return imageID;
    }

    public void setImageID(int imageID) {
        this.imageID = imageID;
    }

    public String getImageTitle() {
        return imageTitle;
    }

    public void setImageTitle(String imageTitle) {
        this.imageTitle = imageTitle;
    }

}

