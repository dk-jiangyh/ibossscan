package com.dongkesoft.ibossscan.home.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


import com.dongkesoft.ibossscan.home.bean.ValueFlagInfo;
import com.dongkesoft.ibossscan.R;

import java.util.List;

/* 
 * Copyright(c) 2012 dongke All rights reserved. / Confidential
 * 类的信息：
 *		1.程序名称： ValueFlagAdapter
 *		2.功能描述：单个值适配
 * 编辑履历：
 *		作者				日期					版本				修改内容
 *		东科				2016/12/07			 1.00			新建
 *******************************************************************************/
public class ValueFlagAdapter extends BaseAdapter {
	private List<ValueFlagInfo> mList;
	private Context mContext;

	public ValueFlagAdapter(Context pContext, List<ValueFlagInfo> pList) {
		this.mContext = pContext;
		this.mList = pList;
	}

	@Override
	public int getCount() {
		return mList.size();
	}

	@Override
	public Object getItem(int position) {
		return mList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater _LayoutInflater = LayoutInflater.from(mContext);
		convertView = _LayoutInflater.inflate(R.layout.value_flag_item, null);
		if (convertView != null) {
			TextView _TextView2 = (TextView) convertView
					.findViewById(R.id.valueflagitem);
			_TextView2.setText(mList.get(position).getValuename());
		}
		return convertView;
	}

}
