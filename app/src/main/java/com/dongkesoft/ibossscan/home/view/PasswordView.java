package com.dongkesoft.ibossscan.home.view;

import dongkesoft.com.dkmodule.mvp.view.MvpView;

/**
 * Created by Administrator on 2018/10/25.
 */

public interface PasswordView extends MvpView{
    void success(String result);
    void failed(String message);
}
