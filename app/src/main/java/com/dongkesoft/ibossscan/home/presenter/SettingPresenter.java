package com.dongkesoft.ibossscan.home.presenter;

import android.content.Context;

import com.dongkesoft.ibossscan.home.view.ISetView;
import com.dongkesoft.ibossscan.common.RightsSet;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import dongkesoft.com.dkmodule.bean.ReqCallBack;
import dongkesoft.com.dkmodule.common.Constants;
import dongkesoft.com.dkmodule.mvp.presenter.BasePresenter;
import dongkesoft.com.dkmodule.utils.OkAsyncHttpClient;

/**
 * Created by guanhonghou on 2018/9/26.
 */

public class SettingPresenter extends BasePresenter<ISetView> {
    public void LogOut(Context context, HashMap<String,String> map, String ip, String port){

        OkAsyncHttpClient.getInstance(context).requestPostByAsyn(String.format(Constants.URL,ip,port), map, new ReqCallBack<String>() {
            @Override
            public void onReqSuccess(final String result) {
                {
                    //需要在UI线程执行
                    mViewHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            // 逻辑判断
                            try {
                                JSONObject obj = new JSONObject(result);
                                int status = obj.optInt("Status");
                                String message = obj.optString("Message");
                                //
                                if (status == 1) {
                                    getView().success(result);
                                }
                                else if(status== RightsSet.NegativeOne)
                                {
                                    getView().getRelogin(message);
                                }
                                else {
                                    getView().showFailedError(message);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            }
            @Override
            public void onReqFailed(final String errorMsg) {
                {
                    mViewHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            getView().showFailedError(errorMsg);
                        }
                    });
                }
            }
        });
    }

    public void changeUser(Context context, HashMap<String,String> map, String ip, String port){

        OkAsyncHttpClient.getInstance(context).requestPostByAsyn(String.format(Constants.URL,ip,port), map, new ReqCallBack<String>() {
            @Override
            public void onReqSuccess(final String result) {
                {
                    //需要在UI线程执行
                    mViewHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            // 逻辑判断
                            try {
                                JSONObject obj = new JSONObject(result);
                                int status = obj.optInt("Status");
                                String message = obj.optString("Message");

                                if (status == 1) {
                                    getView().changeUserSuccess(result);
                                }
                                else if(status==RightsSet.NegativeOne){
                                    getView().getRelogin(message);
                                }

                                else {
                                    getView().changeUserFailedError(message);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            }
            @Override
            public void onReqFailed(final String errorMsg) {
                {
                    mViewHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            getView().changeUserFailedError(errorMsg);
                        }
                    });
                }
            }
        });
    }
}
