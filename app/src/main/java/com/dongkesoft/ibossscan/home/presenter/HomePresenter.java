package com.dongkesoft.ibossscan.home.presenter;

import android.content.Context;

import com.dongkesoft.ibossscan.home.view.IHomeView;
import com.dongkesoft.ibossscan.common.RightsSet;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import dongkesoft.com.dkmodule.bean.ReqCallBack;
import dongkesoft.com.dkmodule.common.Constants;
import dongkesoft.com.dkmodule.mvp.presenter.BasePresenter;
import dongkesoft.com.dkmodule.utils.OkAsyncHttpClient;

/**
 * Created by guanhonghou on 2018/9/26.
 */

public class HomePresenter extends BasePresenter<IHomeView> {
    /**
     *
     * @param context
     * @param map
     * @param ip
     * @param port
     */
    public void LogOut(Context context, HashMap<String, String> map, String ip, String port) {

        OkAsyncHttpClient.getInstance(context).requestPostByAsynWithForm(String.format(Constants.PRINT_QR_URL, ip,
                port), map, new ReqCallBack<String>() {
            @Override
            public void onReqSuccess(final String result) {
                {
                    //需要在UI线程执行
                    mViewHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            // 逻辑判断
                            try {
                                JSONObject obj = new JSONObject(result);
                                int status = obj.optInt("Status");
                                String message = obj.optString("Message");
                                if (status == 0) {
                                    getView().success(result);
                                }
                                else
                                {
                                    getView().showFailedError(message);
                                }



                            } catch (JSONException e) {
                                e.printStackTrace();
                                getView().showFailedError("网络异常");
                            }
                        }
                    });
                }
            }

            @Override
            public void onReqFailed(final String errorMsg) {
                {
                    mViewHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            getView().showFailedError(errorMsg);
                        }
                    });
                }
            }
        });
    }

    /**
     * 仓库权限
     * @param context
     * @param map
     * @param ip
     * @param port
     */
    public void getWarehouseList(Context context, HashMap<String, String> map, String ip, String port) {

        OkAsyncHttpClient.getInstance(context).requestPostByAsyn(String.format(Constants.URL, ip, port), map, new ReqCallBack<String>() {
            @Override
            public void onReqSuccess(final String result) {
                {
                    //需要在UI线程执行
                    mViewHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            // 逻辑判断
                            try {
                                JSONObject obj = new JSONObject(result);
                                int status = obj.optInt("Status");
                                String message = obj.optString("Message");
                                //
                                if (status == 1) {
                                    getView().showWarehouseSuccess(result);
                                }
                                else if(status== RightsSet.NegativeOne)
                                {
                                    getView().getRelogin(message);

                                }

                                else {
                                    getView().showFailedError(message);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            }

            @Override
            public void onReqFailed(final String errorMsg) {
                {
                    mViewHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            getView().showFailedError(errorMsg);
                        }
                    });
                }
            }
        });
    }

    /**
     * 保货保号
     * @param context
     * @param map
     * @param ip
     * @param port
     */
    public void getGuaranteeGoodsList(Context context, HashMap<String, String> map, String ip, String port) {

        OkAsyncHttpClient.getInstance(context).requestPostByAsyn(String.format(Constants.URL, ip, port), map, new ReqCallBack<String>() {
            @Override
            public void onReqSuccess(final String result) {
                {
                    //需要在UI线程执行
                    mViewHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            // 逻辑判断
                            try {
                                JSONObject obj = new JSONObject(result);
                                int status = obj.optInt("Status");
                                String message = obj.optString("Message");
                                //
                                if (status == 1) {
                                    getView().guaranteeGoodsSuccess(result);
                                }
                                else if(status==RightsSet.NegativeOne)
                                {
                                    getView().getRelogin(message);
                                }

                                else {
                                    getView().guaranteeGoodsFailedError(message);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            }

            @Override
            public void onReqFailed(final String errorMsg) {
                {
                    mViewHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            getView().showFailedError(errorMsg);
                        }
                    });
                }
            }
        });
    }

    /**
     * 生产产区权限
     * @param context
     * @param map
     * @param ip
     * @param port
     */
    public void getProductList(Context context, HashMap<String, String> map, String ip, String port) {

        OkAsyncHttpClient.getInstance(context).requestPostByAsyn(String.format(Constants.URL, ip, port), map, new ReqCallBack<String>() {
            @Override
            public void onReqSuccess(final String result) {
                {
                    //需要在UI线程执行
                    mViewHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            // 逻辑判断
                            try {
                                JSONObject obj = new JSONObject(result);
                                int status = obj.optInt("Status");
                                String message = obj.optString("Message");
                                //
                                if (status == 1) {
                                    getView().showProductSuccess(result);
                                }
                                else if(status==RightsSet.NegativeOne)
                                {
                                    getView().getRelogin(message);
                                }

                                else {
                                    //getView().showFailedError(message);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            }

            @Override
            public void onReqFailed(final String errorMsg) {
                {
                    mViewHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            //getView().showFailedError(errorMsg);
                        }
                    });
                }
            }
        });
    }

    /**
     * 返漏权限
     * @param context
     * @param map
     * @param ip
     * @param port
     */
    public void getReturnList(Context context, HashMap<String, String> map, String ip, String port) {

        OkAsyncHttpClient.getInstance(context).requestPostByAsyn(String.format(Constants.URL, ip, port), map, new ReqCallBack<String>() {
            @Override
            public void onReqSuccess(final String result) {
                {
                    //需要在UI线程执行
                    mViewHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            // 逻辑判断
                            try {
                                JSONObject obj = new JSONObject(result);
                                int status = obj.optInt("Status");
                                String message = obj.optString("Message");
                                //
                                if (status == 1) {
                                    getView().showReturnSuccess(result);
                                }
                                else if(status==RightsSet.NegativeOne)
                                {
                                    getView().getRelogin(message);
                                }

                                else {
                                    //getView().showFailedError(message);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            }

            @Override
            public void onReqFailed(final String errorMsg) {
                {
                    mViewHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            //getView().showFailedError(errorMsg);
                        }
                    });
                }
            }
        });
    }

}
